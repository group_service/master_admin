package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.HebelaLogger;
import com.hebela.core.bo.Admin;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Order;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.Publisher;
import com.hebela.masterdata.dao.OrderDao;
import com.hebela.masterdata.dao.PublisherDao;
import com.hebela.masterdata.dao.TrackingActionDao;
import com.hebela.masterdata.obj.PublisherReportOverview;
import com.hebela.masterdata.obj.RoleModel;
import com.hebela.masterdata.web.publisher.PublisherSearchRequest;
import com.hebela.tracking.bo.TrackingAction;
import com.hebela.web.MicroserviceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class PublisherServiceImpl extends AbstractStatusService<Publisher> implements PublisherService {
    @Autowired
    PublisherDao publisherDao;
    @Autowired
    OrderDao orderDao;
    @Autowired
    TrackingActionDao trackingActionDao;

    @Autowired
    MicroserviceUtils microserviceUtils;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Publisher get(Integer publisherId) {
        Publisher dbBo = null;
        Integer adminId = null;
        if (publisherId != null && User.getContext().getAuthorities().stream().anyMatch(authority -> authority.getRole().equals(RoleModel.ROLE_PUBLISHER_ADMIN)
                || authority.getRole().equals(RoleModel.ROLE_ADMIN)
                || authority.getRole().equals(RoleModel.ROLE_ROOT))) {
            dbBo = publisherDao.getPublisher(publisherId, adminId);
        } else if (User.getContextId() != null) {
            dbBo = publisherDao.getPublisher(publisherId, User.getContextId());
        }
        return dbBo;
    }

    @Override
    public Publisher doInsert(Publisher bo) {
        Admin admin = null;
        try {
            bo.getAdmin().setPasswordToken(passwordEncoder.encode(bo.getAdmin().getPassword()));
            admin = microserviceUtils.post(microserviceUtils.getHost(MicroserviceUtils.HELA_CORE_SERVICE) + "/api/hela-core/admin/insertPublisher", bo.getAdmin(), new ParameterizedTypeReference<Admin>() {
            });
        } catch (Exception e) {
            HebelaLogger.error(e);
            throw new HebelaException("Tạo tài khoản lỗi, vui lòng kiểm tra lại thông tin");
        }
        bo.setAdminId(admin.getId());
        publisherDao.save(bo);
        return bo;
    }

    @Override
    public Page<Publisher> findPublisher(PublisherSearchRequest model) {
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        model.setKeyword(model.getKeyword() != null ? "%" + model.getKeyword() + "%" : "%%");
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        List<Integer> statusList = model.getStatus() == null ? Arrays.asList(Publisher.STATUS_ACTIVE, Product.STATUS_INACTIVE)
                : Collections.singletonList(model.getStatus());
        Page<Publisher> publisherPage = this.publisherDao.findPublisher(
                model.getKeyword(),
                model.getType(),
                model.getIsTrash(),
                statusList,
                pageable
        );
        if (publisherPage.getContent().size() > 0) {
            for (Publisher publisher : publisherPage.getContent()) {
                PublisherSearchRequest publisherSearchRequest = new PublisherSearchRequest();
                publisherSearchRequest.setAffCode(publisher.getAffCode());
                List<Order> orderList = findOrder(publisherSearchRequest);
                publisher.setOrderCount(orderList.size());
            }
        }
        return publisherPage;
    }

    /*Report Overview*/
    @Override
    public PublisherReportOverview getReportOverview(PublisherSearchRequest model) {
        PublisherReportOverview reportOverview = new PublisherReportOverview();
        Publisher dbPublisher = this.get(model.getPublisherId());
        if (dbPublisher != null) {
            model.setAffCode(dbPublisher.getAffCode());
            List<Order> orderList = findOrder(model);
            reportOverview = calReportOverview(orderList, model);
            reportOverview.setPublisher(dbPublisher);
        }
        return reportOverview;
    }

    private PublisherReportOverview calReportOverview(List<Order> orderList, PublisherSearchRequest model) {
        PublisherReportOverview reportOverview = new PublisherReportOverview();
        reportOverview.setCountClick(countByEventAction(model, TrackingAction.ACTION_VISIT));
        reportOverview.setCountOrderTotal(orderList.size());
        reportOverview.setMoneyTotal(orderList.stream().mapToLong(Order::getTotal).sum());

        reportOverview.setCountOrderInprogress(orderList.stream().filter(item -> {
            return (item.getStatus() & Order.STATUS_COMPLETED) == 0 && (item.getStatus() & Order.STATUS_CANCEL) == 0;
        }).count());
        reportOverview.setMoneyInprogress(orderList.stream().filter(item -> {
            return (item.getStatus() & Order.STATUS_COMPLETED) == 0 && (item.getStatus() & Order.STATUS_CANCEL) == 0;
        }).mapToLong(Order::getTotal).sum());

        reportOverview.setCountOrderCompleted(orderList.stream().filter(item -> {
            return (item.getStatus() & Order.STATUS_COMPLETED) > 0;
        }).count());
        reportOverview.setMoneyCompleted(orderList.stream().filter(item -> {
            return (item.getStatus() & Order.STATUS_COMPLETED) > 0;
        }).mapToLong(Order::getTotal).sum());

        reportOverview.setCountOrderCancel(orderList.stream().filter(item -> {
            return (item.getStatus() & Order.STATUS_CANCEL) > 0;
        }).count());
        reportOverview.setMoneyCancel(orderList.stream().filter(item -> {
            return (item.getStatus() & Order.STATUS_CANCEL) > 0;
        }).mapToLong(Order::getTotal).sum());
        return reportOverview;
    }

    private List<Order> findOrder(PublisherSearchRequest model) {
        Instant fromDate = model.getFromDate();
        Instant toDate = model.getToDate();
        return orderDao.findAll((root, query, builder) -> {
            query.distinct(true);
            Root<TrackingAction> trackingActionRoot = query.from(TrackingAction.class);
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Order._orderRootId), trackingActionRoot.get(TrackingAction._orderId)));
            predicates.add(builder.equal(trackingActionRoot.get(TrackingAction._affCode), model.getAffCode()));
            if (toDate != null) {
                predicates.add(builder.lessThanOrEqualTo(root.get(Order._createdDate), toDate.plus(1, ChronoUnit.DAYS)));
            }
            if (fromDate != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Order._createdDate), fromDate));
            }
            query.orderBy(builder.desc(root.get(Order._createdDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    private long countByEventAction(PublisherSearchRequest model, String eventAction) {
        Instant fromDate = model.getFromDate();
        Instant toDate = model.getToDate();
        return trackingActionDao.count((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(TrackingAction._affCode), model.getAffCode()));
            predicates.add(builder.equal(root.get(TrackingAction._action), eventAction));
            if (toDate != null) {
                predicates.add(builder.lessThanOrEqualTo(root.get(TrackingAction._createdDate), toDate.plus(1, ChronoUnit.DAYS)));
            }
            if (fromDate != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(TrackingAction._createdDate), fromDate));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

}

