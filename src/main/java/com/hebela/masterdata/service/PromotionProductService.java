package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.PromotionProduct;
import com.hebela.masterdata.web.promotionProduct.PromotionProductRequest;

import java.util.List;

public interface PromotionProductService extends Service<PromotionProduct, Integer> {

    PromotionProduct doActivate(PromotionProduct bo);

    PromotionProduct doDeactivate(PromotionProduct bo);

    List<PromotionProduct> findPromotionProduct(Integer categoryId);

    List<PromotionProduct> findBySearch(PromotionProductRequest model);

    PromotionProduct doInsertAll(List<PromotionProduct> promotionProducts);

    PromotionProduct doUpdateAll(List<PromotionProduct> promotionProducts);
}