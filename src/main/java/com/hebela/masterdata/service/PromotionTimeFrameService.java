package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.PromotionTimeFrame;
import com.hebela.masterdata.web.promotionTimeFrame.PromotionTimeFrameRequest;
import org.springframework.data.domain.Page;

public interface PromotionTimeFrameService extends Service<PromotionTimeFrame, Integer> {

    PromotionTimeFrame doActivate(PromotionTimeFrame bo);

    PromotionTimeFrame doDeactivate(PromotionTimeFrame bo);

    Page<PromotionTimeFrame> findPromotionTimeFrame(PromotionTimeFrameRequest model);

    PromotionTimeFrame getDetail(Integer key);
}

