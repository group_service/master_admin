package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.masterdata.bo.ProductGallery;
import org.springframework.stereotype.Service;

@Service
public class ProductGalleryServiceImpl extends AbstractService<ProductGallery, Integer> implements ProductGalleryService {
}
