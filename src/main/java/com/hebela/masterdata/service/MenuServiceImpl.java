package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.masterdata.bo.Menu;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceImpl extends AbstractStatusService<Menu> implements MenuService {

    @Override
    public Menu doInsert(Menu bo) {
        validateData(bo);
        return super.doInsert(bo);
    }

    private void validateData(Menu bo) {
        if (StringUtils.isEmpty(bo.getName()) || StringUtils.isEmpty(bo.getType())) {
            throw new HebelaException("action.invalid");
        }
    }
}
