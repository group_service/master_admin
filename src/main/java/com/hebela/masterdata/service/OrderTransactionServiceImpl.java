package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.masterdata.bo.OrderTransaction;
import org.springframework.stereotype.Service;

@Service
public class OrderTransactionServiceImpl extends AbstractService<OrderTransaction, Integer> {
}
