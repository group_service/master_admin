package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.obj.RecommendationFilterRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface RecommendationService extends StatusService<Recommendation> {
    Recommendation doRemoveObject(Recommendation bo);

    Recommendation doPriorityObject(Recommendation bo);

    List<Recommendation> findWithExtends(Recommendation bo);

    Recommendation doAddObject(Recommendation bo);

    List<Recommendation> findByTerm(String term);

    List<Recommendation> findRecommendHasObjectLink();
}

