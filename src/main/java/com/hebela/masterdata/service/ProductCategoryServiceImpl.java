package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductCategoryServiceImpl extends AbstractStatusService<ProductCategory> implements ProductCategoryService {

    private final ProductCategoryDao productCategoryDao;
    private final ProductDao productDao;
    private final ProductComboDao comboDao;
    private final AttributeCategoryLinkDao attributeCategoryLinkDao;
    private final MetaConfigDao metaConfigDao;

    public ProductCategoryServiceImpl(ProductCategoryDao productCategoryDao, ProductDao productDao, ProductComboDao comboDao, AttributeCategoryLinkDao attributeCategoryLinkDao, MetaConfigDao metaConfigDao) {
        this.productCategoryDao = productCategoryDao;
        this.productDao = productDao;
        this.comboDao = comboDao;
        this.attributeCategoryLinkDao = attributeCategoryLinkDao;
        this.metaConfigDao = metaConfigDao;
    }

    /**
     * @param parent:                     Danh mục cha
     * @param productCategoryItemAllList: Tất cả danh mục
     * @param outputList:                 Danh sách trả về
     */
    private void _buildFlatChildren(ProductCategory parent, List<ProductCategory> productCategoryItemAllList, List<ProductCategory> outputList) {
        List<ProductCategory> children = productCategoryItemAllList.stream().filter(item -> item.getParentId() != null && item.getParentId().equals(parent.getId())).collect(Collectors.toList());
        if (children.size() > 0) {
            for (ProductCategory item : children) {
                outputList.add(item);
                _buildFlatChildren(item, productCategoryItemAllList, outputList);
            }
        }
    }

    private void _buildParent(ProductCategory productCategory, List<ProductCategory> productCategoryList, List<ProductCategory> outputList) {
        if (productCategory.getParentId() != null) {
            Optional<ProductCategory> optionalProductCategory = productCategoryList.stream().filter(item -> item.getId().equals(productCategory.getParentId())).findFirst();
            if (optionalProductCategory.isPresent()) {
                outputList.add(optionalProductCategory.get());
                _buildParent(optionalProductCategory.get(), productCategoryList, outputList);
            }
        }

    }

    private void _buildTreeChildren(ProductCategory parent, List<ProductCategory> productCategoryItemAllList, List<ProductCategory> outputList) {
        List<ProductCategory> children = productCategoryItemAllList.stream().filter(item -> item.getParentId() != null && item.getParentId().equals(parent.getId())).collect(Collectors.toList());
        if (children.size() > 0) {
            parent.setChildren(children);
            for (ProductCategory item : children) {
                _buildTreeChildren(item, productCategoryItemAllList, outputList);
            }
        }
    }

    private List<ProductCategory> buildFlat(List<ProductCategory> productCategoryList) {
        List<ProductCategory> outputList = new ArrayList<>();
        List<ProductCategory> rootList = productCategoryList.stream().filter(item -> item.getLevel() == 1).collect(Collectors.toList());
        for (ProductCategory productCategory : rootList) {
            outputList.add(productCategory);
            _buildFlatChildren(productCategory, productCategoryList, outputList);
        }
        return outputList;
    }

    private List<ProductCategory> buildReverse(List<ProductCategory> productCategoryList, Integer productCategoryId) {
        List<ProductCategory> outputList = new ArrayList<>();
        Optional<ProductCategory> productCategoryOptional = productCategoryList.stream().filter(item -> item.getId().equals(productCategoryId)).findFirst();
        if (productCategoryOptional.isPresent()) {
            outputList.add(productCategoryOptional.get());
            _buildParent(productCategoryOptional.get(), productCategoryList, outputList);
        }
        Collections.reverse(outputList);
        return outputList;
    }

    private List<ProductCategory> buildTree(List<ProductCategory> productCategoryList) {
        List<ProductCategory> outputList = new ArrayList<>();
        List<ProductCategory> rootList = productCategoryList.stream().filter(item -> item.getLevel() == 1).collect(Collectors.toList());
        for (ProductCategory productCategory : rootList) {
            outputList.add(productCategory);
            _buildTreeChildren(productCategory, productCategoryList, outputList);
        }
        return outputList;
    }

    @Override
    public ProductCategory doInsert(ProductCategory bo) {
        if (bo.getParentId() == null || (bo.getParentId() != null && bo.getParentId() == -1)) {
            bo.setParentId(null);
            bo.setLevel(1);
            Integer maxPriority = this.productCategoryDao.getMaxPriorityRoot();
            bo.setPriority(maxPriority == null ? 1 : maxPriority + 1);
        }
        if (bo.getParentId() != null) {
            ProductCategory dbParent = this.productCategoryDao.getOne(bo.getParentId());
            bo.setLevel(dbParent.getLevel() + 1);
            Integer maxPriority = this.productCategoryDao.getMaxPriority(bo.getParentId());
            bo.setPriority(maxPriority == null ? 1 : maxPriority + 1);
            bo.setType(dbParent.getType());
        }
        long count = this.productCategoryDao.count();
        if (count == 0) {
            bo.setIsDefault(Boolean.TRUE);
        }
        bo.setIsTrash(Boolean.FALSE);
        super.doInsert(bo);

        if (bo.getAttributeIdList() != null) {
            bo.getAttributeIdList().forEach(attributeId -> {
                AttributeCategoryLink attributeLink = new AttributeCategoryLink();
                attributeLink.setProductCategoryId(bo.getId());
                attributeLink.setAttributeId(attributeId);
                attributeLink.setCreatedBy(User.getContextId());
                attributeCategoryLinkDao.save(attributeLink);
            });
        }

        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setProductCategoryId(bo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_CATEGORY);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        return bo;
    }

    @Override
    public ProductCategory getDetail(Integer id) {
        ProductCategory dbBo = super.get(id);
        if (dbBo.getParentId() != null) {
            dbBo.setParentName(super.get(dbBo.getParentId()).getName());
        }

        AttributeCategoryLink attributeCategoryLink = new AttributeCategoryLink();
        attributeCategoryLink.setProductCategoryId(dbBo.getId());
        List<AttributeCategoryLink> dbLinkList = attributeCategoryLinkDao.findAll(Example.of(attributeCategoryLink));
        if (dbLinkList.size() > 0) {
            List<Integer> attributeIdList = dbLinkList.stream().map(AttributeCategoryLinkBase::getAttributeId).collect(Collectors.toList());
            dbBo.setAttributeIdList(attributeIdList);
        }

        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(id, MetaConfig.TYPE_CATEGORY)));
        dbMetaConfigOptional.ifPresent(dbBo::setMetaConfig);
        return dbBo;
    }

    @Override
    public ProductCategory doUpdate(ProductCategory bo) {
        ProductCategory dbBo = super.get(bo.getId());
        bo.setParentId(dbBo.getParentId());
        bo.setPriority(dbBo.getPriority());
        bo.setLevel(dbBo.getLevel());
        bo.setIsDefault(dbBo.getIsDefault());
        bo.setStatus(dbBo.getStatus());
        bo.setIsTrash(dbBo.getIsTrash());
        bo.setModifiedBy(User.getContextId());
        dbBo.copyData(bo);
        updateAttribute(dbBo, bo.getAttributeIdList());
        this.dao.save(dbBo);
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_CATEGORY, User.getContextId(), bo.getMetaConfig());
        }
        return bo;
    }

    private void updateAttribute(ProductCategory dbBo, List<Integer> attributeIdList) {
        /*Cập nhật thay đổi bảng AttributeCategoryLink*/
        AttributeCategoryLink attributeCategoryLink = new AttributeCategoryLink();
        attributeCategoryLink.setProductCategoryId(dbBo.getId());
        List<AttributeCategoryLink> dbLinkList = attributeCategoryLinkDao.findAll(Example.of(attributeCategoryLink));
        List<AttributeCategoryLink> insertLinkList = new ArrayList<>();
        if (attributeIdList != null && attributeIdList.size() > 0) {
            for (Integer attributeId : attributeIdList) {
                AttributeCategoryLink _attributeCategoryLink = new AttributeCategoryLink();
                _attributeCategoryLink.setProductCategoryId(dbBo.getId());
                _attributeCategoryLink.setAttributeId(attributeId);
                _attributeCategoryLink.setCreatedBy(User.getContextId());
                insertLinkList.add(_attributeCategoryLink);
            }
        }

        if (insertLinkList == null || insertLinkList.size() == 0) {
            this.attributeCategoryLinkDao.deleteAll(dbLinkList);
        } else {
            for (Iterator<AttributeCategoryLink> insertIterator = insertLinkList.iterator(); insertIterator.hasNext(); ) {
                AttributeCategoryLink insertItem = insertIterator.next();
                for (Iterator<AttributeCategoryLink> removeIterator = dbLinkList.iterator(); removeIterator.hasNext(); ) {
                    AttributeCategoryLink removeItem = removeIterator.next();
                    if (removeItem.getAttributeId().equals(insertItem.getAttributeId())) {
                        removeIterator.remove();
                        insertIterator.remove();
                    }
                }
            }
            if (dbLinkList.size() > 0) {
                this.attributeCategoryLinkDao.deleteAll(dbLinkList);
            }
            if (insertLinkList.size() > 0) {
                this.attributeCategoryLinkDao.saveAll(insertLinkList);
            }
        }

    }

    @Override
    public ProductCategory doTrash(ProductCategory bo) {
        ProductCategory category = super.get(bo.getId());
        if (ProductCategory.TYPE_COMBO.equals(category.getType())) {
            ProductCombo example = new ProductCombo();
            example.setProductCategoryId(category.getId());
            if (this.comboDao.count(Example.of(example)) > 0) {
                throw new HebelaException("product.category.can.not.trash");
            }
        } else {
            Product example = new Product();
            example.setProductCategoryId(category.getId());
            if (this.productDao.count(Example.of(example)) > 0) {
                throw new HebelaException("product.category.can.not.trash");
            }
        }
        setIsTrashForProductCategoryChildren(bo);
        return super.doTrash(bo);
    }

    public ProductCategory doSetDefault(ProductCategory bo) {
        ProductCategory dbBo = this.productCategoryDao.getOne(bo.getId());
        ProductCategory example = new ProductCategory();
        example.setIsDefault(true);
        Optional<ProductCategory> optionalProductCategory = this.productCategoryDao.findOne(Example.of(example));
        if (optionalProductCategory.isPresent()) {
            ProductCategory oldDefaultProductCategory = optionalProductCategory.get();
            oldDefaultProductCategory.setIsDefault(Boolean.FALSE);
            this.productCategoryDao.save(oldDefaultProductCategory);
        }
        dbBo.setModifiedDate(Instant.now());
        dbBo.setIsDefault(Boolean.TRUE);
        return this.productCategoryDao.save(dbBo);
    }

    @Override
    public List<ProductCategory> findFlat(String type) {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setType(type);
        productCategory.setIsTrash(Boolean.FALSE);
        List<ProductCategory> productCategoryList = this.productCategoryDao.findAll(Example.of(productCategory), Sort.by(Sort.Order.asc(ProductCategory._priority)));
        List<ProductCategory> productCategoryListReturn = buildFlat(productCategoryList);
        for (ProductCategory item : productCategoryListReturn) {
            if (item.getLevel() > 1) {
                for (int i = 1; i < item.getLevel(); i++) {
                    item.setName("---- " + item.getName());
                }
            }
        }
        return productCategoryListReturn;
    }

    @Override
    public List<ProductCategory> findTree(String type) {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setType(type);
        productCategory.setIsTrash(Boolean.FALSE);
        List<ProductCategory> productCategoryList = this.productCategoryDao.findAll(Example.of(productCategory), Sort.by(Sort.Order.asc(ProductCategory._priority)));
        return this.buildTree(productCategoryList);
    }

    @Override
    public List<ProductCategory> findByTerm(String term) {
        return this.productCategoryDao.findByTerm(term);
    }

    public List<ProductCategory> findByKeyword(ProductCategory bo) {
        ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        if (bo.getIsTrash()) {
            List<ProductCategory> productCategoryList = productCategoryDao.findWithExtends(bo.getStatus() != null ? bo.getStatus() : 1, bo.getIsTrash() != null ? bo.getIsTrash() : false);
//            List<ProductCategory> productCategoryList = this.productCategoryDao.findAll(Example.of(bo, matcher)).stream()
//                    .sorted(Comparator.comparing(ProductCategoryBase::getPriority)).collect(Collectors.toList());
            return productCategoryList;
        } else {
            List<ProductCategory> productCategoryList = productCategoryDao.findWithExtends(bo.getStatus() != null ? bo.getStatus() : 1, bo.getIsTrash() != null ? bo.getIsTrash() : false);
            return buildFlat(productCategoryList);
        }
    }

    @Override
    public ProductCategory doRestore(ProductCategory bo) {
        checkProductCategoryParentIsTrash(bo);
        return super.doRestore(bo);
    }

    /*
           đưa HelpCategory cha vào thùng rác => đưa toàn bộ HelpCategory con vào thùng rác
         */
    private void setIsTrashForProductCategoryChildren(ProductCategory categoryParent) {
        List<ProductCategory> categoryChildrenList = getProductCategoryChildren(categoryParent);
        if (categoryChildrenList.size() > 0) {
            for (ProductCategory children : categoryChildrenList) {
                children.setIsTrash(Boolean.TRUE);
                this.dao.save(children);
                setIsTrashForProductCategoryChildren(children);
            }
        }
    }

    private List<ProductCategory> getProductCategoryChildren(ProductCategory categoryParent) {
        ProductCategory example = new ProductCategory();
        example.setParentId(categoryParent.getId());
        return productCategoryDao.findAll(Example.of(example));
    }

    /*
      kiểm tra HelpCategory cha có ở trong thùng rác không
    */
    private void checkProductCategoryParentIsTrash(ProductCategory productCategory) {
        Integer parentId = productCategory.getParentId();
        if (parentId != null) {
            ProductCategory categoryParent = this.dao.getOne(parentId);
            if (Boolean.TRUE.equals(categoryParent.getIsTrash())) {
                String[] error = new String[1];
                error[0] = categoryParent.getName();
                throw new HebelaException("category.parent.missing", error);
            }
            if (categoryParent.getParentId() != null) {
                checkProductCategoryParentIsTrash(categoryParent);
            }
        }
    }
}
