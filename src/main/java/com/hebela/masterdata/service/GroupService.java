package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Group;
public interface GroupService extends StatusService<Group> {
    Group getDetail(Integer id);
}

