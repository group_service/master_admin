package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.PromotionCategory;

public interface PromotionCategoryService extends Service<PromotionCategory, Integer> {

    PromotionCategory doActivate(PromotionCategory bo);

    PromotionCategory doDeactivate(PromotionCategory bo);
}

