package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Landing;
import com.hebela.masterdata.bo.MetaConfig;
import com.hebela.masterdata.dao.MetaConfigDao;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LandingServiceImpl extends AbstractStatusService<Landing> implements LandingService {
    private final MetaConfigDao metaConfigDao;

    public LandingServiceImpl(MetaConfigDao metaConfigDao) {
        this.metaConfigDao = metaConfigDao;
    }

    @Override
    public Landing get(Integer key) {
        Landing dbBo = super.get(key);
        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(dbBo.getId(), MetaConfig.TYPE_LANDING_PAGE)));
        dbMetaConfigOptional.ifPresent(dbBo::setMetaConfig);
        return dbBo;
    }

    @Override
    public Landing doInsert(Landing bo) {
        bo.setStatus(Landing.STATUS_INACTIVE);
        Landing dbBo = super.doInsert(bo);
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setLandingId(dbBo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_LANDING_PAGE);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        return dbBo;
    }

    @Override
    public Landing doUpdate(Landing bo) {
        Landing dbBo = super.get(bo.getId());
        dbBo.setName(bo.getName());
        dbBo.setSlug(bo.getSlug());
        dbBo.setType(bo.getType());
        dbBo.setContent(bo.getContent());
        dbBo.setContentWeb(bo.getContentWeb());
        dbBo.setContentMobile(bo.getContentMobile());
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_LANDING_PAGE, User.getContextId(), bo.getMetaConfig());
        }
        return super.doUpdate(bo);
    }

    @Override
    public Landing doActivate(Landing bo) {
        Landing dbBo = super.get(bo.getId());
        if (Landing.STATUS_ACTIVE.equals(dbBo.getStatus()) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Landing.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public Landing doDeactivate(Landing bo) {
        Landing dbBo = super.get(bo.getId());
        if (!Landing.STATUS_ACTIVE.equals(dbBo.getStatus()) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Landing.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }
}

