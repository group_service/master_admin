package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.MetaConfig;
import com.hebela.masterdata.bo.PageConfig;

import java.util.List;
import java.util.Map;

public interface PageConfigService extends StatusService<PageConfig> {
}

