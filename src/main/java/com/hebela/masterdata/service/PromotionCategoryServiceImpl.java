package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.bo.extend.PlatformExtend;
import com.hebela.masterdata.dao.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@Service
public class PromotionCategoryServiceImpl extends AbstractService<PromotionCategory, Integer> implements PromotionCategoryService {
    @Autowired
    PromotionCategoryDao promotionCategoryDao;
    @Autowired
    PromotionConfigDao promotionConfigDao;
    @Autowired
    ObjectImageDao objectImageDao;
    @Autowired
    PromotionTimeFrameDao promotionTimeFrameDao;
    @Autowired
    PromotionProductDao promotionProductDao;

    @Override
    public PromotionCategory get(Integer key) {
        PromotionCategory dbBo = super.get(key);
        dbBo.setPromotionConfig(this.promotionConfigDao.findOne((root, query, builder) ->
                builder.equal(root.get(PromotionConfig._categoryId), dbBo.getId())).orElse(null));
        List<ObjectImage> objectImages = objectImageDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.and(
                    builder.equal(root.get(ObjectImage._objectClass), ObjectImage.OBJ_CLASS_PROMOTION_CATEGORY),
                    builder.equal(root.get(ObjectImage._objectClassId), dbBo.getId())
            ));
            query.orderBy(
                    builder.asc(root.get(ObjectImage._priority)),
                    builder.asc(root.get(ObjectImage._createdDate))
            );
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        dbBo.setObjectImageList(objectImages);
        return dbBo;
    }

    @Override
    public PromotionCategory doInsert(PromotionCategory bo) {
        this.validatePlatform(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        if (bo.getStatus() == null) {
            bo.setStatus(Coupon.STATUS_INACTIVE);
        }
        bo.setPriority(1);
        PromotionCategory dbBo = super.doInsert(bo);
        bo.setId(dbBo.getId());
        this.setPromotionConfig(bo);
        List<ObjectImage> objectImageList = bo.getObjectImageList();
        if (objectImageList != null && objectImageList.size() > 0) {
            for (ObjectImage objectImageClient : objectImageList) {
                objectImageClient.setObjectClass(ObjectImage.OBJ_CLASS_PROMOTION_CATEGORY);
                objectImageClient.setObjectClassId(dbBo.getId());
                objectImageDao.customUpdate(null, User.getContextId(), objectImageClient);
            }
        }
        return dbBo;
    }

    @Override
    public PromotionCategory doUpdate(PromotionCategory bo) {
        this.validateUpdatePromotionCategory(bo);
        this.validatePlatform(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        PromotionCategory dbBo = super.get(bo.getId());

        // update lại nền tảng nếu category giảm bớt nền tảng áp dụng
        if (bo.getPlatform() < dbBo.getPlatform()) {
            this.updatePlatformPromotionProduct(dbBo.getId(), bo.getPlatform());
        }

        dbBo.setPromotionId(bo.getPromotionId());
        dbBo.setName(bo.getName());
        dbBo.setDescription(bo.getDescription());
        dbBo.setDescriptionCondition(bo.getDescriptionCondition());
        dbBo.setImagePromotion(bo.getImagePromotion());
        dbBo.setPlatform(bo.getPlatform());
        promotionCategoryDao.save(dbBo);
        this.setPromotionConfig(bo);
        return dbBo;
    }

    public void updatePlatformPromotionProduct(Integer categoryId, Integer platformRemove) {
        List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionProduct._categoryId), categoryId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        // tìm nền tảng nào bị loại
        for (PromotionProduct dbBo : promotionProducts) {
            for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                if ((platformRemove & platform) <= 0) {
                    if ((dbBo.getPlatform() & platform) > 0) {
                        dbBo.setPlatform(dbBo.getPlatform() - platform);
                    }
                }
            }
            dbBo.setModifiedBy(User.getContextId());
            this.promotionProductDao.save(dbBo);
        }
    }

    private void validatePlatform(PromotionCategory bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        if (bo.getPromotionId() == null || bo.getTimeFrameId() == null || bo.getName() == null || StringUtils.isEmpty(bo.getDescription())
                || StringUtils.isEmpty(bo.getDescriptionCondition())) {
            bo.getErrorList().add(new HebelaError("Thông tin danh mục không hợp lệ."));
            return;
        }
        Integer platformCategory = bo.getPlatform();
        if (platformCategory == null || platformCategory == 0) {
            bo.getErrorList().add(new HebelaError("Thiếu thông tin nền tảng hiển thị."));
            return;
        }
        PromotionTimeFrame timeFrame = this.promotionTimeFrameDao.findById(bo.getTimeFrameId()).orElse(null);
        if (timeFrame == null) {
            bo.getErrorList().add(new HebelaError("Khung giờ không tồn tại hoặc đã bị xoá"));
            return;
        }
        if (Instant.now().isAfter(timeFrame.getEndTime())) {
            bo.getErrorList().add(new HebelaError("Khung giờ đã kết thúc, không thể cập nhật"));
            return;
        }

        PromotionConfig config = bo.getPromotionConfig();
        if (config == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thông tin cấu hình danh mục"));
            return;
        } else {
            if (PromotionConfig.TYPE_DISCOUNT_MONEY.equals(config.getType())) {
                if (config.getDiscountMin() > config.getDiscountMax()) {
                    bo.getErrorList().add(new HebelaError("Giảm tiền tối thiểu phải bé hơn hoặc bằng giảm tiền tối đa"));
                    return;
                }
            }
        }

        Integer platformTimeFrame = timeFrame.getPlatform();
        for (Integer platform : PlatformExtend.ALL_PLATFORM) {
            if (platformTimeFrame != null && (platformTimeFrame & platform) <= 0 && (platformCategory & platform) > 0) {
                bo.getErrorList().add(new HebelaError("Khung giờ không áp dụng cho nền tảng này. Vui lòng xem lại thông tin khung giờ"));
                return;
            }
        }
    }

    private void validateUpdatePromotionCategory(PromotionCategory bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        long countProduct = promotionProductDao.count((root, query, builder) -> builder.and(
                builder.equal(root.get(PromotionProduct._categoryId), bo.getId())
        ));
        if (countProduct > 0) {
            bo.getErrorList().add(new HebelaError("Danh mục này đã có sản phẩm đăng kí, không thể cập nhật hoặc xoá"));
        }
    }

    @Override
    public PromotionCategory doDelete(PromotionCategory bo) {
        this.validateUpdatePromotionCategory(bo);
        if (bo.getErrorList().size() > 0) {
            throw new HebelaException(bo.getErrorList().get(0).getMessage());
        }
        return super.doDelete(bo);
    }

    @Override
    public PromotionCategory doActivate(PromotionCategory bo) {
        PromotionCategory dbBo = super.get(bo.getId());
        if (dbBo.getStatus().equals(PromotionCategory.STATUS_ACTIVE)) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(PromotionCategory.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionCategoryDao.save(dbBo);
        return dbBo;
    }

    @Override
    public PromotionCategory doDeactivate(PromotionCategory bo) {
        PromotionCategory dbBo = super.get(bo.getId());
        if (!dbBo.getStatus().equals(PromotionCategory.STATUS_ACTIVE)) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(PromotionCategory.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionCategoryDao.save(dbBo);
        this.deactivatePromotionProduct(dbBo.getId());
        return dbBo;
    }

    public void deactivatePromotionProduct(Integer categoryId) {
        List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionProduct._categoryId), categoryId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        for (PromotionProduct dbBo : promotionProducts) {
            dbBo.setStatus(PromotionProduct.STATUS_INACTIVE);
            dbBo.setModifiedBy(User.getContextId());
            this.promotionProductDao.save(dbBo);
        }
    }

    void setPromotionConfig(PromotionCategory bo) {
        PromotionConfig config = bo.getPromotionConfig();
        if (config != null) {
            PromotionConfig configDb = this.promotionConfigDao.findOne((root, query, builder) ->
                    builder.equal(root.get(PromotionConfig._categoryId), bo.getId())).orElse(null);
            if (configDb == null) {
                // insert PromotionConfig
                configDb = new PromotionConfig();
                configDb.setPromotionId(bo.getPromotionId());
                configDb.setTimeFrameId(bo.getTimeFrameId());
                configDb.setCategoryId(bo.getId());
                configDb.setType(config.getType());
                if (PromotionConfig.TYPE_PRICE_SAME.equals(configDb.getType())) {
                    configDb.setPriceSame(config.getPriceSame());
                } else if (PromotionConfig.TYPE_DISCOUNT_MONEY.equals(configDb.getType())) {
                    configDb.setDiscountMin(config.getDiscountMin());
                    configDb.setDiscountMax(config.getDiscountMax());
                }
                config.setType(config.getType());
                config.setCreatedBy(User.getContextId());
                promotionConfigDao.save(configDb);
            } else {
                // update PromotionConfig
                if (PromotionConfig.TYPE_PRICE_SAME.equals(configDb.getType())) {
                    configDb.setPriceSame(config.getPriceSame());
                } else if (PromotionConfig.TYPE_DISCOUNT_MONEY.equals(configDb.getType())) {
                    configDb.setDiscountMin(config.getDiscountMin());
                    configDb.setDiscountMax(config.getDiscountMax());
                }
            }
        }
    }
}

