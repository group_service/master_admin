package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.Coupon;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.web.coupon.CouponSearchRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CouponService extends Service<Coupon, Integer> {
    Coupon doActivate(Coupon bo);

    Coupon doDeactivate(Coupon bo);

    Coupon doAddProduct(Coupon bo);

    Coupon doRemoveProductGift(Coupon bo);

    Coupon doAddProductGift(Coupon bo);

    Coupon doRemoveProduct(Coupon bo);

    Page<Coupon> findBySearch(CouponSearchRequest bo);

    Page<Coupon> findBySearchCouponActive(CouponSearchRequest bo);

    List<Product> findGift(Integer couponId);
}

