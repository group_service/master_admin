package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.ShippingFee;

public interface ShippingFeeService extends Service<ShippingFee, Integer> {
    ShippingFee doAddProvince(ShippingFee bo);

    ShippingFee doRemoveProvince(ShippingFee bo);
}

