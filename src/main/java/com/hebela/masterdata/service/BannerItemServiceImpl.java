package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Banner;
import com.hebela.masterdata.bo.BannerItem;
import com.hebela.masterdata.bo.ObjectLink;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.bo.extend.PlatformExtend;
import com.hebela.masterdata.dao.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
public class BannerItemServiceImpl extends AbstractStatusService<BannerItem> implements BannerItemService {
    private final BannerItemDao bannerItemDao;
    private final BannerDao bannerDao;
    private final ProductDao productDao;
    private final ProductComboDao comboDao;
    private final ObjectLinkDao objectLinkDao;
    private final CouponDao couponDao;

    public BannerItemServiceImpl(BannerItemDao bannerItemDao, BannerDao bannerDao, ProductDao productDao, ProductComboDao comboDao, ObjectLinkDao objectLinkDao, CouponDao couponDao) {
        this.bannerItemDao = bannerItemDao;
        this.bannerDao = bannerDao;
        this.productDao = productDao;
        this.comboDao = comboDao;
        this.objectLinkDao = objectLinkDao;
        this.couponDao = couponDao;
    }

    @Override
    public BannerItem get(Integer key) {
        BannerItem dbBo = super.get(key);
        dbBo.setObjectLink(objectLinkDao.getObjectLink(ObjectLink.OBJ_CLASS_BANNER_ITEM, dbBo.getId()).orElse(null));
        return dbBo;
    }

    @Override
    public BannerItem doInsert(BannerItem bo) {
        validateData(bo);
        bo.setPriority(1);
        BannerItem dbBo = super.doInsert(bo);
        objectLinkDao.customUpdate(ObjectLink.OBJ_CLASS_BANNER_ITEM, dbBo.getId(), User.getContextId(), bo.getObjectLink());
        return dbBo;
    }

    @Override
    public BannerItem doUpdate(BannerItem bo) {
        validateData(bo);
        BannerItem dbBo = super.doUpdate(bo);
        objectLinkDao.customUpdate(ObjectLink.OBJ_CLASS_BANNER_ITEM, dbBo.getId(), User.getContextId(), bo.getObjectLink());
        return dbBo;
    }

    @Override
    public BannerItem doRestore(BannerItem bo) {
        validateData(bo);
        return super.doRestore(bo);
    }

    private void validateData(BannerItem bo) {
        if (StringUtils.isEmpty(bo.getName()) || bo.getPlatform() == null || bo.getObjectLink() == null) {
            throw new HebelaException("action.invalid");
        }
        String objectType = bo.getObjectLink().getObjectType();
        if (StringUtils.isNotEmpty(objectType)) {
            if (!ObjectLink.TYPE_CUSTOM_LINK.equals(objectType) && !ObjectLink.TYPE_ALL_BRAND.equals(objectType)
                    && !ObjectLink.TYPE_ALL_COUPON.equals(objectType) && !ObjectLink.TYPE_CART.equals(objectType)) {
                if (bo.getObjectLink().getObjectId() == null) {
                    throw new HebelaException("objectLink.objectId.is.null");
                }
            }
        }

        Banner banner = bannerDao.findById(bo.getBannerId()).orElse(null);
        if (banner == null) {
            throw new HebelaException("banner.obj.missing");
        }
        if (Boolean.TRUE.equals(banner.getIsTrash())) {
            throw new HebelaException("banner.is.trash");
        }
        if (Banner.BANNER_POPUP.equals(objectType) || Banner.BANNER_TOP.equals(objectType)) {
            validateBanner(bo.getBannerId(), bo.getId(), bo.getPlatform());
        }
    }

    private void validateBanner(Integer bannerId, Integer bannerItemId, Integer platformClient) {
        List<BannerItem> bannerItems = bannerItemDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(BannerItem._bannerId), bannerId));
            predicates.add(builder.equal(root.get(BannerItem._isTrash), Boolean.FALSE));
            if (bannerItemId != null) {
                predicates.add(builder.notEqual(root.get(BannerItem._id), bannerItemId));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        if (bannerItems.size() > 0) {
            for (BannerItem item : bannerItems) {
                Integer platformDb = item.getPlatform();
                for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                    if (platformDb != null && (platformDb & platform) > 0 && (platformClient & platform) > 0) {
                        String[] error = new String[1];
                        error[0] = item.getName();
                        throw new HebelaException("banner.obj.duplicate.platform", error);
                    }
                }
            }
        }
    }

    @Override
    public List<BannerItem> find(BannerItem bannerItem) {
        return bannerItemDao.findWithBannerId(bannerItem.getBannerId() != null ? bannerItem.getBannerId() : 0,
                bannerItem.getIsTrash() != null ? bannerItem.getIsTrash() : false);
    }
}
