package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.bo.extend.PlatformExtend;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.web.promotionProduct.PromotionProductRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class PromotionProductServiceImpl extends AbstractService<PromotionProduct, Integer> implements PromotionProductService {
    @Autowired
    PromotionProductDao promotionProductDao;
    @Autowired
    ProductDao productDao;
    @Autowired
    CouponDao couponDao;
    @Autowired
    PromotionDao promotionDao;
    @Autowired
    PromotionTimeFrameDao promotionTimeFrameDao;
    @Autowired
    PromotionCategoryDao promotionCategoryDao;
    @Autowired
    CouponProductLinkDao couponProductLinkDao;
    @Autowired
    SuperVoucherDao superVoucherDao;
    @Autowired
    SuperVoucherProductLinkDao superVoucherProductLinkDao;

    @Override
    public PromotionProduct get(Integer key) {
        return super.get(key);
    }

    @Override
    public PromotionProduct doInsert(PromotionProduct bo) {
        this.validateInput(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        bo.setPriority(this.promotionProductDao.getPriorityMaxByCategoryId(bo.getCategoryId()) + 1);
        bo.setSaleCount(0);
        bo.setStatus(bo.getStatus() != null ? bo.getStatus() : PromotionProduct.STATUS_INACTIVE);
        return super.doInsert(bo);
    }

    @Override
    public PromotionProduct doUpdate(PromotionProduct bo) {
        this.validateInput(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        PromotionProduct dbBo = super.get(bo.getId());
        dbBo.setPricePromotion(bo.getPricePromotion());
        dbBo.setMaxPurchaseQuantity(bo.getMaxPurchaseQuantity());
        dbBo.setPromotionQuantity(bo.getPromotionQuantity());
        dbBo.setPlatform(bo.getPlatform());
        return super.doUpdate(dbBo);
    }

    // đăng ký 1 list sản phẩm vào chương trình
    @Override
    public PromotionProduct doInsertAll(List<PromotionProduct> promotionProducts) {
        if (promotionProducts == null || promotionProducts.size() == 0) {
            throw new HebelaException("Tất cả sản phẩm đã được lưu vào chương trình");
        }
        long countId = promotionProducts.stream().filter(promotionProduct -> promotionProduct.getId() != null).count();
        if (countId > 0) {
            throw new HebelaException("action.invalid");
        }
        PromotionProduct bo = this.validateUpdateAll(promotionProducts);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        int priority = this.promotionProductDao.getPriorityMaxByCategoryId(promotionProducts.get(0).getCategoryId());
        for (PromotionProduct promotionProduct : promotionProducts) {
            priority++;
            promotionProduct.setPriority(priority);
            promotionProduct.setSaleCount(0);
            promotionProduct.setStatus(promotionProduct.getStatus() != null ? promotionProduct.getStatus() : PromotionProduct.STATUS_INACTIVE);
            promotionProduct.setCreatedBy(User.getContextId());
            this.promotionProductDao.save(promotionProduct);
        }
        return bo;
    }

    // cập nhật 1 list sản phẩm trong 1 chương trình
    @Override
    public PromotionProduct doUpdateAll(List<PromotionProduct> promotionProducts) {
        long countId = promotionProducts.stream().filter(promotionProduct -> promotionProduct.getId() == null).count();
        if (countId > 0) {
            throw new HebelaException("action.invalid");
        }
        PromotionProduct bo = this.validateUpdateAll(promotionProducts);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        List<Integer> promotionProductIds = promotionProducts.stream().map(PromotionProduct::getId).collect(Collectors.toList());
        Map<Integer, PromotionProduct> promotionProductMap = this.promotionProductDao.findAllById(promotionProductIds).stream()
                .collect(Collectors.toMap(PromotionProduct::getId, product -> product));
        PromotionProduct dbBo;
        for (PromotionProduct promotionProduct : promotionProducts) {
            dbBo = promotionProductMap.get(promotionProduct.getId());
            if (dbBo != null) {
                dbBo.setPricePromotion(promotionProduct.getPricePromotion());
                dbBo.setMaxPurchaseQuantity(promotionProduct.getMaxPurchaseQuantity());
                dbBo.setPromotionQuantity(promotionProduct.getPromotionQuantity());
                dbBo.setPlatform(promotionProduct.getPlatform());
                dbBo.setModifiedBy(User.getContextId());
                this.promotionProductDao.save(dbBo);
            }
        }
        return bo;
    }

    @Override
    public PromotionProduct doDelete(PromotionProduct bo) {
        return super.doDelete(bo);
    }

    @Override
    public PromotionProduct doActivate(PromotionProduct bo) {
        PromotionProduct dbBo = super.get(bo.getId());
        if (dbBo.getStatus().equals(PromotionProduct.STATUS_ACTIVE)) {
            throw new HebelaException("action.invalid");
        }
        this.validateInput(dbBo);
        if (dbBo.getErrorList().size() > 0) {
            return dbBo;
        }
        dbBo.setStatus(PromotionProduct.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionProductDao.save(dbBo);
        return dbBo;
    }

    @Override
    public PromotionProduct doDeactivate(PromotionProduct bo) {
        PromotionProduct dbBo = super.get(bo.getId());
        if (!dbBo.getStatus().equals(Promotion.STATUS_ACTIVE)) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(PromotionProduct.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionProductDao.save(dbBo);
        return dbBo;
    }

    @Override
    public List<PromotionProduct> findPromotionProduct(Integer categoryId) {
        Integer orgId = getOrgId();
        return this.promotionProductDao.findPromotionProduct(categoryId, orgId);
    }

    @Override
    public List<PromotionProduct> findBySearch(PromotionProductRequest model) {
        String keyword = StringUtils.isEmpty(model.getKeyword()) ? null : "%" + model.getKeyword() + "%";
        List<Integer> status = model.getStatus() == null ? Arrays.asList(PromotionProduct.STATUS_ACTIVE, PromotionProduct.STATUS_INACTIVE)
                : Collections.singletonList(model.getStatus());
        return this.promotionProductDao.findBySearch(model.getTimeFrameId(), model.getCategoryId(), status, model.getType(), keyword);
    }

    private List<Coupon> checkIsProductInActiveCoupon(List<Integer> productIds, Promotion promotion) {
        return this.couponDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Subquery<Long> subqueryProduct = query.subquery(Long.class);
            Root<CouponProductLink> couponProductLinkSubqueryRoot = subqueryProduct.from(CouponProductLink.class);
            subqueryProduct
                    .select(couponProductLinkSubqueryRoot.get(CouponProductLink._id))
                    .where(builder.and(
                            builder.equal(couponProductLinkSubqueryRoot.get(CouponProductLink._couponId), root.get(Coupon._id)),
                            (couponProductLinkSubqueryRoot.get(CouponProductLink._productId).in(productIds))
                    ));
            predicates.add(builder.exists(subqueryProduct));
            predicates.add(builder.equal(root.get(Coupon._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Coupon._status), Coupon.STATUS_ACTIVE));
            predicates.add(builder.lessThan(root.get(Coupon._couponUsed), root.get(Coupon._couponNumber)));
            predicates.add(
                    builder.or(
                            builder.between(root.get(Coupon._startTime),
                                    promotion.getStartTime(),
                                    promotion.getEndTime()
                            ),
                            builder.between(root.get(Coupon._endTime),
                                    promotion.getStartTime(),
                                    promotion.getEndTime()
                            ),
                            builder.and(
                                    builder.lessThanOrEqualTo(root.get(Coupon._startTime), promotion.getStartTime()),
                                    builder.greaterThanOrEqualTo(root.get(Coupon._endTime), promotion.getEndTime())
                            )
                    )
            );
            query.distinct(true);
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    public List<CouponProductLink> getProductInActiveCoupon(List<Integer> productIds, Promotion promotion) {
        return this.couponProductLinkDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            final List<Predicate> predicateCoupons = new ArrayList<>();
            Subquery<List> subqueryCoupon = query.subquery(List.class);
            Root<Coupon> couponRoot = subqueryCoupon.from(Coupon.class);
            predicateCoupons.add(builder.equal(couponRoot.get(Coupon._isTrash), Boolean.FALSE));
            predicateCoupons.add(builder.equal(couponRoot.get(Coupon._status), Coupon.STATUS_ACTIVE));
            predicateCoupons.add(builder.lessThan(couponRoot.get(Coupon._couponUsed), couponRoot.get(Coupon._couponNumber)));
            predicateCoupons.add(
                    builder.or(
                            builder.between(couponRoot.get(Coupon._startTime),
                                    promotion.getStartTime(),
                                    promotion.getEndTime()
                            ),
                            builder.between(couponRoot.get(Coupon._endTime),
                                    promotion.getStartTime(),
                                    promotion.getEndTime()
                            ),
                            builder.and(
                                    builder.lessThanOrEqualTo(couponRoot.get(Coupon._startTime), promotion.getStartTime()),
                                    builder.greaterThanOrEqualTo(couponRoot.get(Coupon._endTime), promotion.getEndTime())
                            )
                    )
            );
            subqueryCoupon
                    .select(couponRoot.get(Coupon._id))
                    .where(predicateCoupons.toArray(new Predicate[0]));

            predicates.add(root.get(CouponProductLink._couponId).in(subqueryCoupon));
            predicates.add(root.get(CouponProductLink._productId).in(productIds));
            query.distinct(true);
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    private PromotionProduct validateUpdateAll(List<PromotionProduct> promotionProducts) {
        PromotionProduct bo = new PromotionProduct();
        bo.setErrorList(new ArrayList<>());
        if (promotionProducts.size() == 0) {
            bo.getErrorList().add(new HebelaError("Không có thông tin sản phẩm"));
            return bo;
        }
        List<Integer> productIds = promotionProducts.stream().map(PromotionProduct::getProductId).collect(Collectors.toList());
        List<Integer> promotionProductIds = promotionProducts.stream().map(PromotionProduct::getId).filter(Objects::nonNull).collect(Collectors.toList());
        List<Product> productList = this.productDao.findAllById(productIds);
        Map<Integer, Product> productMap = productList.stream().collect(Collectors.toMap(Product::getId, product -> product));

        Integer promotionId = promotionProducts.get(0).getPromotionId();
        Integer timeFrameId = promotionProducts.get(0).getTimeFrameId();
        Integer categoryId = promotionProducts.get(0).getCategoryId();

        PromotionCategory category = this.promotionCategoryDao.findById(categoryId).orElse(null);
        if (category == null) {
            bo.getErrorList().add(new HebelaError("Danh mục chương trình không tồn tại hoặc đã bị xoá"));
            return bo;
        }
        Integer platformCategory = category.getPlatform();

        // check điều kiện promotionProduct
        Product product;
        for (PromotionProduct promotionProduct : promotionProducts) {
            product = productMap.get(promotionProduct.getProductId());
            if (promotionProduct.getPromotionQuantity() == null || promotionProduct.getPromotionQuantity() == 0
                    || promotionProduct.getMaxPurchaseQuantity() == null || promotionProduct.getMaxPurchaseQuantity() == 0) {
                bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + product.getSku() + " - Tên sản phẩm: " + product.getName()
                        + "]. Số lượng tham gia chương trình phải lớn hơn 0 hoặc Số lượng áp dụng FS phải lớn hơn 0"));
            }
            if (promotionProduct.getPromotionId() == null || promotionProduct.getTimeFrameId() == null || promotionProduct.getCategoryId() == null
                    || promotionProduct.getProductId() == null || promotionProduct.getPricePromotion() == null) {
                bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + product.getSku() + " - Tên sản phẩm: " + product.getName()
                        + "], thiếu thông tin đăng kí chương trình"));
            }

            if (!promotionId.equals(promotionProduct.getPromotionId()) && !timeFrameId.equals(promotionProduct.getTimeFrameId())) {
                bo.getErrorList().add(new HebelaError("Thông tin chương trình hoặc khung giờ đăng ký không hợp lệ."));
            }

            Integer platformProduct = promotionProduct.getPlatform();
            if (platformProduct == null || platformProduct == 0) {
                bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + product.getSku() + " - Tên sản phẩm: " + product.getName()
                        + "], thiếu thông tin nền tảng áp dụng"));
            }

            for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                if (platformProduct != null && platformProduct > 0) {
                    if (platformCategory != null && (platformCategory & platform) <= 0 && (platformProduct & platform) > 0) {
                        bo.getErrorList().add(new HebelaError("Danh mục chương trình không áp dụng cho nền tảng này. Vui lòng kiểm tra lại thông tin "
                                + "sản phẩm [SKU: " + product.getSku() + " - Tên sản phẩm: " + product.getName() + "]"));
                    }
                }
            }
        }
        if (bo.getErrorList().size() > 0) {
            return bo;
        }

        // check chương trình đăng ký sản phẩm
        Promotion promotionDb = this.promotionDao.findById(promotionId).orElse(null);
        if (promotionDb == null) {
            bo.getErrorList().add(new HebelaError("Thông tin chương trình không hợp lệ."));
            return bo;
        }

        if (Instant.now().isAfter(promotionDb.getPrepareEndTime())) {
            bo.getErrorList().add(new HebelaError("Thời gian chuẩn bị của chương trình đã kết thúc. Không thể đăng ký hoặc cập nhập sản phẩm"));
            return bo;
        }

        // Sản phẩm không được tham gia 2 danh mục cùng ACTIVE trong cùng 1 khung giờ của cùng 1 promotion
        this.checkProductInPromotionTimeFrame(promotionProductIds, productIds, promotionId, timeFrameId, bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }

        // check sản phẩm thuộc super voucher có thời gian diễn ra cùng với flash sale
        PromotionTimeFrame timeFrameDb = this.promotionTimeFrameDao.findById(timeFrameId).orElse(null);
        if (timeFrameDb == null) {
            bo.getErrorList().add(new HebelaError("Thông tin khung giờ không hợp lệ."));
            return bo;
        }
        Map<Integer, List<SuperVoucher>> promotionMap = findSuperVoucherActive(timeFrameDb, productIds);
        for (Integer productId : promotionMap.keySet()) {
            Product productSuperVoucher = productMap.get(productId);
            List<SuperVoucher> superVouchers = promotionMap.get(productSuperVoucher.getId());
            if (superVouchers != null && superVouchers.size() > 0) {
                for (SuperVoucher superVoucher : superVouchers) {
                    bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + productSuperVoucher.getSku() + " - Tên sản phẩm: " + productSuperVoucher.getName()
                            + "], đang tham gia super voucher" + superVoucher.getName() + " Mã = " + superVoucher.getCode()
                            + " nên không thể tham gia chương trình"));
                }
            }
        }
        if (bo.getErrorList().size() > 0) {
            return bo;
        }

        // check sản phẩm thuộc coupon active có thời gian diễn ra cùng với chương trình
        List<CouponProductLink> productInCoupon = this.getProductInActiveCoupon(productIds, promotionDb);
        if (productInCoupon.size() > 0) {
            List<Integer> productIdsInCoupon = productInCoupon.stream().map(CouponProductLink::getProductId).distinct().collect(Collectors.toList());
            List<Integer> couponId = productInCoupon.stream().map(CouponProductLink::getCouponId).distinct().collect(Collectors.toList());
            List<Coupon> activeCouponList = this.couponDao.findAll((root, query, builder) ->
                    builder.and(root.get(Coupon._id).in(couponId))
            );
            Product productCoupon;
            for (Integer productId : productIdsInCoupon) {
                productCoupon = productMap.get(productId);
                if (productCoupon != null) {
                    bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + productCoupon.getSku() + " - Tên sản phẩm: " + productCoupon.getName()
                            + "]. Đang tham gia coupon " + activeCouponList.get(0).getName() + " Mã = " + activeCouponList.get(0).getCode()
                            + " nên không thể tham gia chương trình"));
                } else {
                    bo.getErrorList().add(new HebelaError("Có sản phẩm đang tham gia coupon " + activeCouponList.get(0).getName()
                            + " Mã = " + activeCouponList.get(0).getCode() + " nên không thể tham gia chương trình"));
                }
            }
            return bo;
        }

        for (PromotionProduct promotionProduct : promotionProducts) {
            product = productMap.get(promotionProduct.getProductId());
            if (product == null) {
                bo.getErrorList().add(new HebelaError("Không tìm được thông tin sản phẩm đăng ký chương trình."));
                return bo;
            }
            if (Boolean.TRUE.equals(product.getIsTrash()) || !Product.TYPE_SELL.equals(product.getType()) || !Product.STATUS_ACTIVE.equals(product.getStatus())) {
                bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + product.getSku() + " - Tên sản phẩm: " + product.getName() + "] đăng ký chương trình không hợp lệ."));
            }
//            if (!product.getOrgId().equals(User.getContextId())) {
//                bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + product.getSku() + " - Tên sản phẩm: " + product.getName() + "] đăng ký promotion không hợp lệ."));
//                return;
//            }
        }

        // mỗi sản phẩm chỉ tham gia 1 promotion và thời gian diễn ra không giao nhau
        List<PromotionProduct> productsWithPromotions;
        if (promotionProductIds.size() > 0) {
            productsWithPromotions = this.promotionProductDao.findFlashSaleByProductId(promotionId, productIds, promotionProductIds, promotionDb.getStartTime(), promotionDb.getEndTime());
        } else {
            productsWithPromotions = this.promotionProductDao.findFlashSaleByProductId(promotionId, productIds, promotionDb.getStartTime(), promotionDb.getEndTime());
        }
        this.checkProductInPromotion(productsWithPromotions, promotionDb, bo);
        return bo;
    }

    public Map<Integer, List<SuperVoucher>> findSuperVoucherActive(PromotionTimeFrame promotionTimeFrame, List<Integer> productId) {
        if (productId != null && productId.size() > 0) {
            List<SuperVoucherProductLink> voucherProductLinks = this.superVoucherProductLinkDao.findAll((root, query, builder) ->
                    root.get(SuperVoucherProductLink._productId).in(productId));
            if (voucherProductLinks.size() > 0) {
                List<Integer> superVoucherId = voucherProductLinks.stream().map(SuperVoucherProductLink::getSuperVoucherId).distinct().collect(Collectors.toList());
                List<SuperVoucher> superVoucherList = this.superVoucherDao.findAll((root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    predicates.add(root.get(SuperVoucher._id).in(superVoucherId));
                    predicates.add(builder.equal(root.get(SuperVoucher._isTrash), Boolean.FALSE));
                    predicates.add(builder.equal(root.get(SuperVoucher._status), SuperVoucher.STATUS_ACTIVE));
                    predicates.add(
                            builder.or(builder.between(root.get(SuperVoucher._startDate),
                                    promotionTimeFrame.getStartTime(),
                                    promotionTimeFrame.getEndTime()
                                    ),
                                    builder.between(root.get(SuperVoucher._endDate),
                                            promotionTimeFrame.getStartTime(),
                                            promotionTimeFrame.getEndTime()
                                    ),
                                    builder.and(
                                            builder.lessThanOrEqualTo(root.get(SuperVoucher._startDate), promotionTimeFrame.getStartTime()),
                                            builder.greaterThanOrEqualTo(root.get(SuperVoucher._endDate), promotionTimeFrame.getEndTime())
                                    )
                            )
                    );
                    return builder.and(predicates.toArray(new Predicate[0]));
                });
                Map<Integer, SuperVoucher> superVoucherMap = superVoucherList.stream().collect(Collectors.toMap(SuperVoucher::getId, superVoucher -> superVoucher));
                voucherProductLinks = voucherProductLinks.stream()
                        .filter(productLink -> superVoucherMap.get(productLink.getSuperVoucherId()) != null)
                        .collect(Collectors.toList());
                Map<Integer, List<SuperVoucher>> result = new HashMap<>();

                for (SuperVoucherProductLink productLink : voucherProductLinks) {
                    result.computeIfAbsent(productLink.getProductId(), k -> new ArrayList<>());
                    result.get(productLink.getProductId()).add(superVoucherMap.get(productLink.getSuperVoucherId()));
                }
                return result;
            }
        }
        return new HashMap<>();
    }

    private void validateInput(PromotionProduct bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        if (bo.getPromotionQuantity() == null || bo.getPromotionQuantity() == 0
                || bo.getMaxPurchaseQuantity() == null || bo.getMaxPurchaseQuantity() == 0) {
            bo.getErrorList().add(new HebelaError("Số lượng tham gia FS phải lớn hơn 0 hoặc Số lượng áp dụng FS phải lớn hơn 0."));
        }

        if (bo.getPromotionId() == null || bo.getTimeFrameId() == null || bo.getCategoryId() == null
                || bo.getProductId() == null || bo.getPricePromotion() == null) {
            bo.getErrorList().add(new HebelaError("Thông tin sản phẩm đăng ký promotion bị thiếu."));
        }
        Integer platformProduct = bo.getPlatform();
        if (platformProduct == null || platformProduct == 0) {
            bo.getErrorList().add(new HebelaError("Thiếu thông tin nền tảng hiển thị."));
            return;
        }
        Product product = this.productDao.findById(bo.getProductId()).orElse(null);
        if (product == null || Boolean.TRUE.equals(product.getIsTrash())
                || !Product.TYPE_SELL.equals(product.getType()) || !Product.STATUS_ACTIVE.equals(product.getStatus())) {
            bo.getErrorList().add(new HebelaError("Thông tin sản phẩm đăng ký promotion không hợp lệ."));
        }

        PromotionCategory category = this.promotionCategoryDao.findById(bo.getCategoryId()).orElse(null);
        if (category == null) {
            bo.getErrorList().add(new HebelaError("Danh mục chương trình không tồn tại hoặc đã bị xoá"));
            return;
        }
        if (!PromotionCategory.STATUS_ACTIVE.equals(category.getStatus())) {
            bo.getErrorList().add(new HebelaError("Danh mục chương trình chưa kích hoạt"));
            return;
        }
        Integer platformCategory = category.getPlatform();
        for (Integer platform : PlatformExtend.ALL_PLATFORM) {
            if (platformCategory != null && (platformCategory & platform) <= 0 && (platformProduct & platform) > 0) {
                bo.getErrorList().add(new HebelaError("Danh mục chương trình không áp dụng cho nền tảng này. Vui lòng kiểm tra lại thông tin danh mục"));
                return;
            }
        }

        Promotion promotionDb = this.promotionDao.findById(bo.getPromotionId()).orElse(null);
        if (promotionDb == null) {
            bo.getErrorList().add(new HebelaError("Thông tin promotion không hợp lệ."));
            return;
        }
        if (Instant.now().isAfter(promotionDb.getPrepareEndTime())) {
            bo.getErrorList().add(new HebelaError("Thời gian chuẩn bị của chương trình đã kết thúc. Không thể thêm mới hay cập nhập sản phẩm"));
            return;
        }

        if (product != null) {
            // check sản phẩm thuộc coupon active có thời gian diễn ra cùng với flash sale
            List<Coupon> activeCouponList = checkIsProductInActiveCoupon(Collections.singletonList(product.getId()), promotionDb);
            if (activeCouponList.size() > 0) {
                bo.getErrorList().add(new HebelaError("Sản phẩm này đang tham gia coupon " + activeCouponList.get(0).getName()
                        + " Mã = " + activeCouponList.get(0).getCode() + " nên không thể tham gia chương trình"));
            }

            // check sản phẩm thuộc super voucher có thời gian diễn ra cùng với flash sale
            PromotionTimeFrame timeFrameDb = this.promotionTimeFrameDao.findById(bo.getTimeFrameId()).orElse(null);
            if (timeFrameDb == null) {
                bo.getErrorList().add(new HebelaError("Thông tin khung giờ không hợp lệ."));
                return;
            }

            Map<Integer, List<SuperVoucher>> promotionMap = findSuperVoucherActive(timeFrameDb, Collections.singletonList(bo.getProductId()));
            List<SuperVoucher> superVouchers = promotionMap.get(bo.getProductId());
            if (superVouchers != null && superVouchers.size() > 0) {
                for (SuperVoucher superVoucher : superVouchers) {
                    bo.getErrorList().add(new HebelaError("Sản phẩm này đang tham gia Super voucher: " + superVoucher.getName()
                            + " - Mã = " + superVoucher.getCode() + " nên không thể tham gia chương trình"));
                }
                return;
            }
        }

        // Sản phẩm không được tham gia 2 danh mục cùng ACTIVE trong cùng 1 khung giờ của cùng 1 promotion
        this.checkProductInPromotionTimeFrame(Collections.singletonList(bo.getId()), Collections.singletonList(bo.getProductId()), promotionDb.getId(), bo.getTimeFrameId(), bo);
        if (bo.getErrorList().size() > 0) {
            return;
        }

        // mỗi sản phẩm chỉ tham gia 1 promotion và thời gian diễn ra không giao nhau
        List<PromotionProduct> productsWithPromotions;
        if (bo.getId() != null) {
            productsWithPromotions = this.promotionProductDao.findFlashSaleByProductId(promotionDb.getId(), Collections.singletonList(bo.getProductId()), Collections.singletonList(bo.getId()), promotionDb.getStartTime(), promotionDb.getEndTime());
        } else {
            productsWithPromotions = this.promotionProductDao.findFlashSaleByProductId(promotionDb.getId(), Collections.singletonList(bo.getProductId()), promotionDb.getStartTime(), promotionDb.getEndTime());
        }
        this.checkProductInPromotion(productsWithPromotions, promotionDb, bo);
    }

    public void checkProductInPromotion(List<PromotionProduct> productsWithPromotions, Promotion promotionDb, PromotionProduct bo) {
        Promotion promotion;
        List<Integer> productIdAlreadyExist = new ArrayList<>();
        for (PromotionProduct promotionProduct : productsWithPromotions) {
            if (!productIdAlreadyExist.contains(promotionProduct.getProductId())) {
                promotion = promotionProduct.getPromotion();
                if (promotionDb.getStartTime().getEpochSecond() == promotion.getStartTime().getEpochSecond()
                        || promotionDb.getEndTime().getEpochSecond() == promotion.getEndTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + promotionProduct.getSku() + " - Tên sản phẩm: " + promotionProduct.getName() + "] đã tham gia chương trình " + promotion.getName() + " có cùng thời gian diễn ra"));
                    productIdAlreadyExist.add(promotionProduct.getProductId());
                    continue;
                }
                if (promotionDb.getStartTime().getEpochSecond() > promotion.getStartTime().getEpochSecond()
                        && promotionDb.getStartTime().getEpochSecond() < promotion.getEndTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + promotionProduct.getSku() + " - Tên sản phẩm: " + promotionProduct.getName() + "] đã tham gia chương trình " + promotion.getName() + " có cùng thời gian diễn ra"));
                    productIdAlreadyExist.add(promotionProduct.getProductId());
                    continue;
                }
                if (promotionDb.getEndTime().getEpochSecond() < promotion.getEndTime().getEpochSecond()
                        && promotionDb.getEndTime().getEpochSecond() > promotion.getStartTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + promotionProduct.getSku() + " - Tên sản phẩm: " + promotionProduct.getName() + "] đã tham gia chương trình " + promotion.getName() + " có cùng thời gian diễn ra"));
                    continue;
                }
                if (promotionDb.getStartTime().getEpochSecond() < promotion.getStartTime().getEpochSecond()
                        && promotionDb.getEndTime().getEpochSecond() > promotion.getEndTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + promotionProduct.getSku() + " - Tên sản phẩm: " + promotionProduct.getName() + "] đã tham gia chương trình " + promotion.getName() + " có cùng thời gian diễn ra"));
                    productIdAlreadyExist.add(promotionProduct.getProductId());
                }
            }
        }
    }

    public void checkProductInPromotionTimeFrame(List<Integer> promotionProductIds, List<Integer> productIds, Integer promotionId, Integer timeFrameId, PromotionProduct bo) {
        promotionProductIds = promotionProductIds.stream().filter(Objects::nonNull).collect(Collectors.toList());
        List<PromotionCategory> promotionCategories = this.promotionCategoryDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionCategory._promotionId), promotionId));
            predicates.add(builder.equal(root.get(PromotionCategory._timeFrameId), timeFrameId));
            predicates.add(builder.equal(root.get(PromotionCategory._status), PromotionCategory.STATUS_ACTIVE));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        if (promotionCategories.size() > 0) {
            List<Integer> promotionCategoriesId = promotionCategories.stream().map(PromotionCategory::getId).collect(Collectors.toList());
            List<PromotionProduct> promotionProductsDB;
            if (promotionProductIds.size() > 0) {
                promotionProductsDB = this.promotionProductDao.findByPromotionIdAndTimeFrameId(promotionId, timeFrameId, productIds, promotionProductIds, promotionCategoriesId);
            } else {
                promotionProductsDB = this.promotionProductDao.findByPromotionIdAndTimeFrameId(promotionId, timeFrameId, productIds, promotionCategoriesId);
            }
            for (PromotionProduct promotionProduct : promotionProductsDB) {
                bo.getErrorList().add(new HebelaError("Sản phẩm [SKU: " + promotionProduct.getSku() + " - Tên sản phẩm: " + promotionProduct.getName()
                        + "], đã được đăng ký trong khung giờ này. Vui lòng kiểm tra lại"));
            }
        }
    }
}

