package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.web.gift.ProductGiftResponse;
import lombok.val;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.hebela.masterdata.util.ListProcess.distinctByKey;


@Service
public class ProductGiftServiceImpl extends AbstractService<ProductGift, Integer> implements ProductGiftService {

    private final ProductDao productDao;
    private final ProductComboDao productComboDao;
    private final ProductGiftDao productGiftDao;
    private final CouponProductLinkDao couponProductLinkDao;
    private final CouponDao couponDao;

    public ProductGiftServiceImpl(ProductDao productDao, ProductComboDao productComboDao, ProductGiftDao productGiftDao, CouponProductLinkDao couponProductLinkDao, CouponDao couponDao) {
        this.productDao = productDao;
        this.productComboDao = productComboDao;
        this.productGiftDao = productGiftDao;
        this.couponProductLinkDao = couponProductLinkDao;
        this.couponDao = couponDao;
    }


    @Override
    public ProductGift doInsert(ProductGift bo) {
        if ((bo.getProduct() == null && bo.getProductCombo() == null) || (bo.getProduct() != null && bo.getProductCombo() != null)) {
            throw new HebelaException("gift.info.invalid");
        }
        List<ProductGift> productGiftList = bo.getProductGiftList();
        if (productGiftList == null || productGiftList.size() == 0) {
            throw new HebelaException("gift.list.not.selected");
        }
        checkProductGiftExists(bo);
        validateProductGift(bo);
        for (ProductGift productGift : productGiftList) {
            productGift.setCreatedBy(User.getContextId());
            if (bo.getProduct() != null) {
                productGift.setProductId(bo.getProduct().getId());
            } else {
                productGift.setProductComboId(bo.getProductCombo().getId());
            }
        }
        this.productGiftDao.saveAll(productGiftList);
        return null;
    }

    private List<CouponProductLink> getProductsInCouponActive(Integer productId) {
        return this.couponProductLinkDao.findAll((root, query, builder) -> {
            Root<Coupon> couponRoot = query.from(Coupon.class);
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(CouponProductLink._couponId), couponRoot.get(Coupon._id)));
            if (productId != null) {
                predicates.add(builder.equal(root.get(CouponProductLink._productId), productId));
            }
            predicates.add(builder.equal(couponRoot.get(Coupon._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(couponRoot.get(Coupon._status), Coupon.STATUS_ACTIVE));
            predicates.add(builder.greaterThan(couponRoot.get(Coupon._endTime), Instant.now()));
            query.orderBy(builder.asc(root.get(CouponProductLink._productId)));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    private List<Coupon> couponIncludeProduct(Integer productId) {
        List<CouponProductLink> couponProductLinkList = getProductsInCouponActive(productId);
        if (couponProductLinkList.size() > 0) {
            List<Integer> couponIdList = couponProductLinkList.stream().map(CouponProductLink::getCouponId).distinct().collect(Collectors.toList());
            return this.couponDao.findAll((root, query, builder) -> builder.and(root.get(Coupon._id).in(couponIdList)));
        }
        return null;
    }

    @Override
    public ProductGift doUpdate(ProductGift bo) {
        ProductGift productGift = new ProductGift();
        productGift.setOrgId(this.getOrgId());
        if (bo.getProduct() != null) {
            productGift.setProductId(bo.getProduct().getId());
        } else {
            productGift.setProductComboId(bo.getProductCombo().getId());
        }
        validateProductGift(bo);
        List<ProductGift> giftByProduct = this.productGiftDao.findAll(Example.of(productGift));
        List<ProductGift> productGiftList = bo.getProductGiftList().stream()
                .filter(distinctByKey(ProductGift::getProductGiftId)).collect(Collectors.toList());
        productGiftList.forEach(item -> item.setCreatedBy(User.getContextId()));
        for (Iterator<ProductGift> insertIterator = productGiftList.iterator(); insertIterator.hasNext(); ) {
            ProductGift insertItem = insertIterator.next();
            for (Iterator<ProductGift> removeIterator = giftByProduct.iterator(); removeIterator.hasNext(); ) {
                ProductGift removeItem = removeIterator.next();
                removeItem.setModifiedBy(User.getContextId());
                if (removeItem.getProductGiftId().equals(insertItem.getProductGiftId())
                        && removeItem.getNumberOfGift().equals(insertItem.getNumberOfGift())) {
                    insertIterator.remove();
                    removeIterator.remove();
                }
            }
        }
        this.productGiftDao.deleteAll(giftByProduct);
        this.productGiftDao.saveAll(productGiftList);
        return null;
    }

    public List<ProductGift> getDetailByProduct(Integer productId) {
        List<ProductGift> giftList = this.productGiftDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(ProductGift._productId), productId)
        ));
        this.addGiftList(giftList);
        return giftList;
    }

    public List<ProductGift> getDetailByCombo(Integer comboId) {
        List<ProductGift> giftList = this.productGiftDao.findAll((root, query, builder) -> builder.and(builder.equal(root.get(ProductGift._productComboId), comboId)));
        this.addGiftList(giftList);
        return giftList;
    }

    public List<ProductGift> getDetailByProductGift(Integer productGiftId) {
        return this.productGiftDao.findAllByProductGiftId(productGiftId);
    }

    public ProductGiftResponse getDetail(Integer productGiftId) {
        val result = new ProductGiftResponse();
        Optional<ProductGift> gift = this.productGiftDao.findById(productGiftId);
        if (gift.isPresent()) {
            ProductGift productGift = gift.get();
            if (productGift.getProductId() != null) {
                Product product = this.productDao.findById(productGift.getProductId()).orElse(null);
                if (product == null) {
                    throw new HebelaException("product.not.exists");
                }
                List<ProductGift> giftList = this.productGiftDao.findAll((root, query, builder) -> {
                    query.orderBy(builder.asc(root.get(ProductGift._priority)), builder.asc(root.get(ProductGift._createdDate)));
                    return builder.and(builder.equal(root.get(ProductGift._productId), productGift.getProductId()));
                });
                this.addGiftsInComBo(giftList);
                result.setProductGiftList(giftList);
                result.setProductId(product.getId());
                result.setProduct(product);
            } else {
                ProductCombo combo = this.productComboDao.findById(productGift.getProductComboId()).orElse(null);
                if (combo == null) {
                    throw new HebelaException("combo.obj.missing");
                }
                result.setProductComboId(combo.getId());
                result.setProductCombo(combo);
                ProductGift example = new ProductGift();
                example.setOrgId(this.getOrgId());
                example.setProductComboId(combo.getId());
                List<ProductGift> giftList = this.productGiftDao.findAll(Example.of(example));
                this.addGiftsInComBo(giftList);
                result.setProductGiftList(giftList);
            }
            return result;
        } else {
            throw new HebelaException("gift.obj.missing");
        }
    }

    private void addGiftsInComBo(List<ProductGift> giftList) {
        if (giftList.size() == 0) {
            return;
        }
        List<Integer> productGiftIdList = giftList.stream().map(ProductGift::getProductGiftId).collect(Collectors.toList());
        List<Product> productGiftList = this.productDao.getGiftByIds(productGiftIdList);
        Map<Integer, Product> productMap = productGiftList.stream().collect(Collectors.toMap(Product::getId, p -> p));
        for (Iterator<ProductGift> iterator = giftList.iterator(); iterator.hasNext(); ) {
            ProductGift productGift = iterator.next();
            val product = productMap.get(productGift.getProductGiftId());
            if (product != null && productGift.getNumberOfGift() != null) {
                productGift.setAmount(Integer.min(product.getAmount(), productGift.getNumberOfGift()));
                productGift.setProductGiftId(product.getId());
                productGift.setName(product.getName());
                productGift.setImageThumbnail(product.getImageThumbnail());
                productGift.setSku(product.getSku());
                productGift.setSlug(product.getSlug());
            } else {
                iterator.remove();
            }
        }
    }

    private void addGiftList(List<ProductGift> giftList) {
        if (giftList.size() == 0) {
            return;
        }
        List<Integer> productGiftIdList = giftList.stream().map(ProductGift::getProductGiftId).collect(Collectors.toList());
        List<Product> productGiftList = this.productDao.getGiftByIds(productGiftIdList);
        Map<Integer, Product> productMap = productGiftList.stream().collect(Collectors.toMap(Product::getId, p -> p));
        for (Iterator<ProductGift> iterator = giftList.iterator(); iterator.hasNext(); ) {
            ProductGift productGift = iterator.next();
            val product = productMap.get(productGift.getProductGiftId());
            if (product != null && productGift.getNumberOfGift() != null && product.getAmount() > 0) {
                productGift.setAmount(Integer.min(product.getAmount(), productGift.getNumberOfGift()));
                productGift.setProductGiftId(product.getId());
                productGift.setName(product.getName());
                productGift.setImageThumbnail(product.getImageThumbnail());
                productGift.setSku(product.getSku());
                productGift.setSlug(product.getSlug());
            } else {
                iterator.remove();
            }
        }
    }

    @Override
    public List<ProductGift> find(ProductGift bo) {
        List<ProductGift> result = new ArrayList<>();
        int orgId = getOrgId();
        List<ProductGift> dbBoList = this.productGiftDao.findGift(orgId);
        if (dbBoList.size() > 0) {
            List<Integer> comboIdList = dbBoList.stream().map(ProductGiftBase::getProductComboId).collect(Collectors.toList());
            List<Integer> giftProductIdList = dbBoList.stream().map(ProductGiftBase::getProductId).collect(Collectors.toList());
            List<Integer> giftIdList = dbBoList.stream().map(ProductGiftBase::getProductGiftId).collect(Collectors.toList());
            List<Integer> productIdList = new ArrayList<>();
            productIdList.addAll(giftProductIdList);
            productIdList.addAll(giftIdList);
            List<Product> productList = this.productDao.getGiftByIds(productIdList);
            List<ProductCombo> comboList = this.productComboDao.getByIds(comboIdList);
            Map<Integer, ProductCombo> comboMap = comboList.stream().collect(Collectors.toMap(ProductCombo::getId, combo -> combo));
            Map<Integer, Product> productMap = productList.stream().collect(Collectors.toMap(Product::getId, product -> product));
            MultiValuedMap<Integer, ProductGift> productGiftMap = new ArrayListValuedHashMap<>();
            MultiValuedMap<Integer, ProductGift> comboGiftMap = new ArrayListValuedHashMap<>();
            dbBoList.forEach(item -> {
                item.setLevel(2);
                if (item.getProductId() != null) {
                    productGiftMap.put(item.getProductId(), item);
                } else {
                    comboGiftMap.put(item.getProductComboId(), item);
                }
                item.setName(productMap.get(item.getProductGiftId()).getName());
            });
            if (productList.size() > 0) {
                Set<Integer> productKeySet = productGiftMap.keySet();
                for (Integer productId : productKeySet) {
                    ProductGift productGift = new ProductGift();
                    productGift.setProductId(productId);
                    productGift.setName(this.productDao.getOne(productId).getName());
                    productGift.setLevel(1);
                    result.add(productGift);
                    result.addAll(productGiftMap.get(productId));
                }
            }
            if (comboList.size() > 0) {
                Set<Integer> comboKeySet = comboGiftMap.keySet();
                for (Integer comboId : comboKeySet) {
                    if (comboMap.get(comboId) != null) {
                        ProductGift productGift = new ProductGift();
                        productGift.setProductComboId(comboId);
                        productGift.setName(comboMap.get(comboId).getName());
                        productGift.setLevel(1);
                        result.add(productGift);
                        result.addAll(comboGiftMap.get(comboId));
                    }
                }
            }
        }
        return result;
    }

    /**
     * kết quả phương thức phụ thuộc orgId
     */
    @Override
    public List<Product> findProductForGift(String term, Integer productId, Integer typeProduct) {
        List<Integer> removeProductIds = new ArrayList<>();
        List<Integer> typeProducts = new ArrayList<>();
        typeProducts.add(Product.TYPE_SELL);
        if (productId != null) {
            removeProductIds.add(productId);
        }
        if (Product.TYPE_SELL.equals(typeProduct)) {
            removeProductIds = productGiftDao.findProductIdInProductGift();
        } else {
            typeProducts.add(Product.TYPE_GIFT);
        }
        List<Integer> productIdInCouponActive = getProductsInCouponActive(null).stream()
                .map(CouponProductLink::getProductId).distinct().collect(Collectors.toList());
        removeProductIds.addAll(productIdInCouponActive);
        List<Integer> finalRemoveProductIds = removeProductIds;
        return this.productDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Product._orgId), getOrgId()));
            predicates.add(builder.equal(root.get(Product._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Product._status), Product.STATUS_ACTIVE));
            predicates.add(root.get(Product._type).in(typeProducts));
            predicates.add(builder.like(root.get(Product._name), "%" + term + "%"));
            predicates.add(builder.greaterThan(root.get(Product._amount), 0));
            if (finalRemoveProductIds.size() > 0) {
                predicates.add(root.get(Product._id).in(finalRemoveProductIds).not());
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    public void validateProductGift(ProductGift bo) {
        int orgId = getOrgId();
        if (bo.getProduct() != null) {
            Product dbProduct = productDao.findById(bo.getProduct().getId()).orElse(null);
            if (dbProduct == null || Boolean.TRUE.equals(dbProduct.getIsTrash()) || !Product.TYPE_SELL.equals(dbProduct.getType())
                    || !dbProduct.getOrgId().equals(orgId) || !Product.STATUS_ACTIVE.equals(dbProduct.getStatus())) {
                throw new HebelaException("gift.product.invalid");
            }
            /**
             * check sản phẩm chương trình khuyến mãi đang active thì không được thêm quà tặng
             */
            List<Coupon> couponList = couponIncludeProduct(dbProduct.getId());
            if (couponList != null && couponList.size() > 0) {
                List<String> couponCodeList = couponList.stream().map(Coupon::getCode).collect(Collectors.toList());
                throw new HebelaException("gift.product.belong.to.coupon", couponCodeList.toArray());
            }
        } else {
            ProductCombo dbCombo = productComboDao.findById(bo.getProductCombo().getId()).orElse(null);
            if (dbCombo == null || Boolean.TRUE.equals(dbCombo.getIsTrash()) || !dbCombo.getOrgId().equals(orgId)
                    || !ProductCombo.STATUS_ACTIVE.equals(dbCombo.getStatus())) {
                throw new HebelaException("gift.combo.invalid");
            }
        }

        // quà tặng không được thuộc sản phẩm trong combo
        List<Integer> productGiftIdList = bo.getProductGiftList().stream().map(ProductGift::getProductGiftId)
                .collect(Collectors.toList());
        List<Product> giftList = productDao.findAllById(productGiftIdList);
        for (Product gift : giftList) {
            if (Boolean.TRUE.equals(gift.getIsTrash()) || !gift.getOrgId().equals(orgId)
                    || Product.TYPE_PRODUCT_IN_COMBO.equals(gift.getType())) {
                throw new HebelaException("gift.product.invalid");
            }
        }
//        List<Integer> productGiftIdList = bo.getProductGiftList().stream().map(ProductGiftBase::getProductGiftId).collect(Collectors.toList());
//        List<CouponGiftLink> couponGiftLinkList = getGiftInCouponActive(null);
//        List<Integer> productIdInCouponActive = couponGiftLinkList.stream()
//                .map(CouponGiftLink::getProductId).distinct().collect(Collectors.toList());
//        for (Integer productGiftId : productGiftIdList) {
//            if (productIdInCouponActive.contains(productGiftId)) {
//                List<Integer> couponIdList = couponGiftLinkList.stream()
//                        .filter(couponGiftLink -> couponGiftLink.getProductId().equals(productGiftId))
//                        .map(CouponGiftLink::getCouponId)
//                        .distinct().collect(Collectors.toList());
//                List<Coupon> couponList = couponDao.findAll((root, query, builder) -> builder.and(root.get(Coupon._id).in(couponIdList)));
//                if (couponList.size() > 0) {
//                    List<String> couponCodeList = couponList.stream().map(Coupon::getCode).collect(Collectors.toList());
//                    throw new HebelaException("gift.product.belong.to.coupon", couponCodeList.toArray());
//                }
//            }
//        }
    }

    public void checkProductGiftExists(ProductGift bo) {
        if (bo.getProduct() == null && bo.getProductCombo() == null) {
            throw new HebelaException("gift.info.invalid");
        }
        if (bo.getProduct() != null) {
            List<ProductGift> gifts = productGiftDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(ProductGift._productId), bo.getProduct().getId())));
            if (gifts.size() > 0) {
                throw new HebelaException("gift.product.exists");
            }
        } else {
            List<ProductGift> gifts = productGiftDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(ProductGift._productComboId), bo.getProductCombo().getId())));
            if (gifts.size() > 0) {
                throw new HebelaException("gift.combo.exists");
            }
        }
    }

    @Override
    public List<ProductCombo> findComboForGift() {
        List<Integer> removeProductComboIds = this.productGiftDao.findProductComboIdInProductGift();
        return this.productComboDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(ProductCombo._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(ProductCombo._orgId), getOrgId()));
            if (removeProductComboIds != null && removeProductComboIds.size() > 0) {
                predicates.add(root.get(ProductCombo._id).in(removeProductComboIds).not());
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }
}

