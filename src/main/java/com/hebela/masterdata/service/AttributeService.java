package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Attribute;
import com.hebela.masterdata.bo.AttributeData;
import com.hebela.masterdata.bo.Unit;
import com.hebela.masterdata.web.attribute.AttributeRequest;

import java.util.List;

public interface AttributeService extends StatusService<Attribute> {
    List<Attribute> findBySearch(AttributeRequest attributeRequest);

    List<Attribute> findByCategory(Integer categoryId);

    List<Attribute> findByProduct(Integer productId);

    AttributeData doInsertAttributeData(AttributeData bo);

    List<Unit> findUnit();

    AttributeData doUpdateAttributeData(AttributeData bo);

    AttributeData doDeleteAttributeData(AttributeData bo);
}

