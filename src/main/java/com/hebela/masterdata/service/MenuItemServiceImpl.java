package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.MenuItem;
import com.hebela.masterdata.bo.ObjectLink;
import com.hebela.masterdata.dao.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuItemServiceImpl extends AbstractStatusService<MenuItem> implements MenuItemService {
    @Autowired
    private MenuItemDao menuItemDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private ProductComboDao comboDao;
    @Autowired
    private ObjectLinkDao objectLinkDao;
    @Autowired
    private CouponDao couponDao;

    /*Store Front*/

    /*Admin UI*/
    @Override
    public List<MenuItem> findTreeFlat(MenuItem menuItem) {
        List<MenuItem> outputList = null;
        List<MenuItem> menuItemList = menuItemDao.findWithExtends(menuItem.getMenuId() != null ? menuItem.getMenuId() : 0, menuItem.getStatus() != null ? menuItem.getStatus() : 1, menuItem.getIsTrash() != null ? menuItem.getIsTrash() : false);
        if (menuItemList.size() > 0) {
            outputList = buildTreeFlat(menuItemList);
        }
        return outputList;
    }

    private List<MenuItem> buildTreeFlat(List<MenuItem> menuItemList) {
        List<MenuItem> outputList = new ArrayList<>();
        List<MenuItem> rootList = menuItemList.stream().filter(item -> item.getLevel() == 1).collect(Collectors.toList());
        for (MenuItem menuItem : rootList) {
            outputList.add(menuItem);
            _buildTreeChildren(menuItem, menuItemList, outputList);
        }
        /**
         * lấy các menuItem con bị hủy mà menuItem cha đã được khôi phục
         */
        for (MenuItem item : menuItemList) {
            if (!outputList.contains(item)) {
                outputList.add(0, item);
            }
        }
        return outputList;
    }

    private void _buildTreeChildren(MenuItem parent, List<MenuItem> menuItemAllList, List<MenuItem> outputList) {
        List<MenuItem> children = menuItemAllList.stream().filter(item -> item.getParentId() != null && item.getParentId().equals(parent.getId())).collect(Collectors.toList());
        if (children.size() > 0) {
            parent.setChildren(children);
            for (MenuItem item : parent.getChildren()) {
                outputList.add(item);
                _buildTreeChildren(item, menuItemAllList, outputList);
            }
        }
    }

    @Override
    public MenuItem doInsert(MenuItem bo) {
        validateData(bo);
        if (bo.getParentId() != null && bo.getParentId() == -1) {
            bo.setParentId(null);
            bo.setLevel(1);
        }
        if (bo.getParentId() != null) {
            MenuItem dbParent = menuItemDao.getOne(bo.getParentId());
            bo.setMenuId(dbParent.getMenuId());
            bo.setLevel(dbParent.getLevel() + 1);
        }
        bo.setPriority(1);
        MenuItem dbBo = super.doInsert(bo);
        objectLinkDao.customUpdate(ObjectLink.OBJ_CLASS_MENUITEM, dbBo.getId(), User.getContextId(), bo.getObjectLink());
        return bo;
    }

    private void validateData(MenuItem bo) {
        if (StringUtils.isEmpty(bo.getName()) || bo.getObjectLink() == null) {
            throw new HebelaException("action.invalid");
        }
        String objectType = bo.getObjectLink().getObjectType();
        if (StringUtils.isEmpty(objectType)) {
            throw new HebelaException("objectLink.objectId.is.null");
        } else {
            if (!ObjectLink.TYPE_CUSTOM_LINK.equals(objectType) && !ObjectLink.TYPE_ALL_BRAND.equals(objectType)
                    && !ObjectLink.TYPE_ALL_COUPON.equals(objectType) && !ObjectLink.TYPE_CART.equals(objectType)) {
                if (bo.getObjectLink().getObjectId() == null) {
                    throw new HebelaException("objectLink.objectId.is.null");
                }
            }
        }
    }

    @Override
    public MenuItem doUpdate(MenuItem bo) {
        validateData(bo);
        MenuItem dbBo = menuItemDao.getOne(bo.getId());
        bo.setMenuId(dbBo.getMenuId());
        bo.setParentId(dbBo.getParentId());
        bo.setPriority(dbBo.getPriority());
        bo.setLevel(dbBo.getLevel());
        bo.setStatus(dbBo.getStatus());
        bo.setIsTrash(dbBo.getIsTrash());
        bo.setModifiedBy(User.getContextId());

        dbBo.copyData(bo);
        this.dao.save(dbBo);
        objectLinkDao.customUpdate(ObjectLink.OBJ_CLASS_MENUITEM, dbBo.getId(), User.getContextId(), bo.getObjectLink());
        return bo;
    }

    @Override
    public MenuItem get(Integer key) {
        MenuItem dbBo = super.get(key);
        if (dbBo.getParentId() != null) {
            dbBo.setParentName(super.get(dbBo.getParentId()).getName());
        }
        dbBo.setObjectLink(objectLinkDao.getObjectLink(ObjectLink.OBJ_CLASS_MENUITEM, dbBo.getId()).orElse(null));
        return dbBo;
    }

    @Override
    public MenuItem doTrash(MenuItem bo) {
        setIsTrashForMenuChildren(bo);
        return super.doTrash(bo);
    }

    @Override
    public MenuItem doRestore(MenuItem bo) {
        checkMenuParentIsTrash(bo);
        return super.doRestore(bo);
    }

    /*
    đưa menu cha vào thùng rác => đưa toàn bộ menu con vào thùng rác
     */
    private void setIsTrashForMenuChildren(MenuItem menuParent) {
        List<MenuItem> menuChildrenList = getMenuItemChildren(menuParent);
        if (menuChildrenList.size() > 0) {
            for (MenuItem children : menuChildrenList) {
                children.setIsTrash(Boolean.TRUE);
                this.dao.save(children);
                setIsTrashForMenuChildren(children);
            }
        }
    }

    private List<MenuItem> getMenuItemChildren(MenuItem menuParent) {
        MenuItem menuExample = new MenuItem();
        menuExample.setParentId(menuParent.getId());
        return menuItemDao.findAll(Example.of(menuExample));
    }

    /*
    kiểm tra menu cha có ở trong thùng rác không
     */
    private void checkMenuParentIsTrash(MenuItem menuItem) {
        if (menuItem.getParentId() != null) {
            MenuItem menuParent = this.dao.getOne(menuItem.getParentId());
            if (Boolean.TRUE.equals(menuParent.getIsTrash())) {
                String[] error = new String[1];
                error[0] = menuParent.getName();
                throw new HebelaException("menu.parent.missing", error);
            }
            if (menuParent.getParentId() != null) {
                checkMenuParentIsTrash(menuParent);
            }
        }
    }
}
