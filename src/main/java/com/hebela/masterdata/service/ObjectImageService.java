package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.ObjectImage;

import java.util.List;

public interface ObjectImageService extends Service<ObjectImage, Integer> {

    List<ObjectImage> findObjectImage(List<Integer> objectImageList, String objectClass, Integer objectClassId);

    List<ObjectImage> doAddObjectImage(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId);

    List<ObjectImage> doUpdateObjectImage(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId);

    List<ObjectImage> doUpdateObjectImagePriority(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId);

    List<ObjectImage> doDeleteObjectImage(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId);
}

