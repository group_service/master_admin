package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.EmailConfig;
import com.hebela.masterdata.web.emailTemplateConfig.EmailConfigRequest;
import org.springframework.data.domain.Page;

public interface EmailConfigService extends Service<EmailConfig, Integer> {

    Page<EmailConfig> findBySearch(EmailConfigRequest model);

    EmailConfig doActive(EmailConfig emailTemplateConfig);

    EmailConfig doDeActive(EmailConfig emailTemplateConfig);
}

