package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.web.product.ProductSearchRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService extends StatusService<Product> {

    Product doActivate(Product product);

    Product doDeactivate(Product product);

    ProductPrice doUpdatePrice(ProductPrice productPrice);

    ProductPrice doDeletePrice(ProductPrice productPrice);

    Page<Product> findBySearch(ProductSearchRequest model);

    List<Product> findByTerm(String term);

    Product getDetail(Integer id);

    List<ProductGallery> findGallery(Integer productId);

    List<ProductAttributeLink> findAttribute(Integer id);

    List<Product> findProductForOrder(String term);

    void doSyncAmount(Product product);
}

