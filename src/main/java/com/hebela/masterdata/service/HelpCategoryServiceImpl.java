package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.HelpCategory;
import com.hebela.masterdata.bo.HelpItem;
import com.hebela.masterdata.bo.MetaConfig;
import com.hebela.masterdata.dao.HelpCategoryDao;
import com.hebela.masterdata.dao.HelpItemDao;
import com.hebela.masterdata.dao.MetaConfigDao;
import com.hebela.masterdata.util.ListProcess;
import lombok.val;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class HelpCategoryServiceImpl extends AbstractStatusService<HelpCategory> implements HelpCategoryService {

    private final HelpItemDao helpItemDao;
    private final HelpCategoryDao helpCategoryDao;
    private final MetaConfigDao metaConfigDao;

    public HelpCategoryServiceImpl(HelpItemDao helpItemDao, HelpCategoryDao helpCategoryDao, MetaConfigDao metaConfigDao) {
        this.helpItemDao = helpItemDao;
        this.helpCategoryDao = helpCategoryDao;
        this.metaConfigDao = metaConfigDao;
    }

    @Override
    public HelpCategory get(Integer key) {
        HelpCategory dbBo = super.get(key);
        if (dbBo.getParentId() != null) {
            val parent = super.get(dbBo.getParentId());
            dbBo.setParentName(parent.getName());
        }
        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(key, MetaConfig.TYPE_HELP_CATEGORY)));
        dbMetaConfigOptional.ifPresent(dbBo::setMetaConfig);
        return dbBo;
    }

    @Override
    public HelpCategory doInsert(HelpCategory bo) {
        HelpCategory dbBo = super.doInsert(bo);
        bo.setId(dbBo.getId());
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setHelpCategoryId(bo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_HELP_CATEGORY);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }

        return dbBo;
    }

    @Override
    public HelpCategory doUpdate(HelpCategory bo) {
        HelpCategory dbBo = super.doUpdate(bo);
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_HELP_CATEGORY, User.getContextId(), bo.getMetaConfig());
        }
        return dbBo;
    }

    @Override
    public List<HelpCategory> find(HelpCategory bo) {
        List<HelpCategory> helpCategories = super.find(bo).stream().sorted(Comparator.comparing(HelpCategory::getPriority)).collect(Collectors.toList());
        if (bo.getIsTrash()) {
            return helpCategories;
        }

        List<HelpCategory> helpCategoriesList = ListProcess.buildFlat(helpCategories);
        for (HelpCategory item : helpCategoriesList) {
            if (item.getLevel() > 1) {
                for (int i = 1; i < item.getLevel(); i++) {
                    item.setName("---- " + item.getName());
                }
            }
        }
        return helpCategoriesList;
    }

    @Override
    public HelpCategory doTrash(HelpCategory bo) {
        List<HelpCategory> allHelpCategories = helpCategoryDao.findAll();
        List<Integer> idHelpCategories = new ArrayList<>();
        /* huỷ các danh mục con */
        setIsTrashForHelpCategoryChildren(allHelpCategories, bo, idHelpCategories);
        /* huỷ các bài viết có cùng danh mục */
        idHelpCategories.add(bo.getId());
        setIsTrashForHelpItem(idHelpCategories);
        return super.doTrash(bo);
    }

    @Override
    public HelpCategory doRestore(HelpCategory bo) {
        checkHelpCategoryParentIsTrash(bo);
        return super.doRestore(bo);
    }

    /*
    đưa danh mục bài viết vào thùng rác => đưa toàn bộ bài viết gắn với danh mục đó vào thùng rác
    */
    private void setIsTrashForHelpItem(List<Integer> allIdHelpCategories) {
        if (allIdHelpCategories.size() > 0) {
            List<HelpItem> allHelpItems = helpItemDao.findHelpItemByHelpCategoryIdIn(allIdHelpCategories);
            for (HelpItem helpItem : allHelpItems) {
                helpItem.setIsTrash(Boolean.TRUE);
                helpItem.setModifiedBy(User.getContextId());
                helpItem.setModifiedDate(Instant.now());
                helpItemDao.save(helpItem);
            }
        }
    }

    /*
      đưa HelpCategory cha vào thùng rác => đưa toàn bộ HelpCategory con vào thùng rác
    */
    private void setIsTrashForHelpCategoryChildren(List<HelpCategory> allHelpCategories, HelpCategory helpCategory,
                                                   List<Integer> output) {
        List<HelpCategory> helpCategories = getHelpCategoryChildren(allHelpCategories, helpCategory);
        if (helpCategories.size() > 0) {
            for (HelpCategory children : helpCategories) {
                children.setIsTrash(Boolean.TRUE);
                children.setModifiedBy(User.getContextId());
                children.setModifiedDate(Instant.now());
                this.dao.save(children);
                output.add(children.getId());
                setIsTrashForHelpCategoryChildren(allHelpCategories, children, output);
            }
        }
    }

    private List<HelpCategory> getHelpCategoryChildren(List<HelpCategory> allHelpCategories, HelpCategory parent) {
        return allHelpCategories.stream()
                .filter(item -> (item.getParentId() != null && item.getParentId().equals(parent.getId())))
                .collect(Collectors.toList());
    }

    /*
      kiểm tra HelpCategory cha có ở trong thùng rác không
    */
    private void checkHelpCategoryParentIsTrash(HelpCategory helpCategory) {
        Integer parentId = helpCategory.getParentId();
        if (parentId != null) {
            HelpCategory helpCategoryParent = this.dao.getOne(parentId);
            if (Boolean.TRUE.equals(helpCategoryParent.getIsTrash())) {
                String[] error = new String[1];
                error[0] = helpCategoryParent.getName();
                throw new HebelaException("category.parent.missing", error);
            }
            if (helpCategoryParent.getParentId() != null) {
                checkHelpCategoryParentIsTrash(helpCategoryParent);
            }
        }
    }
}

