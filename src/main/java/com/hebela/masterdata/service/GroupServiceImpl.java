package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl extends AbstractStatusService<Group> implements GroupService {
    @Autowired
    private GroupProductLinkDao groupProductLinkDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private AttributeDao attributeDao;
    @Autowired
    private ProductAttributeLinkDao productAttributeLinkDao;
    @Autowired
    private GroupDao groupDao;

    @Override
    public List<Group> find(Group bo) {
        return groupDao.findAllGroups(bo.getIsTrash(), getOrgId());
    }


    /*
     * Get Detail để cập nhật group
     * Lấy danh sách thuộc tính chung
     * Lấy danh sách product
     * */
    @Override
    public Group getDetail(Integer id) {
        Group dbBo = super.get(id);
        dbBo.setAttributeIdList(groupProductLinkDao.findAttributeIdByGroup(dbBo.getId()));
        dbBo.setAttributeList(attributeDao.findByGroup(dbBo.getId()));
        List<Product> productList = groupProductLinkDao.findProductByGroup(dbBo.getId());
        if (productList.size() > 0) {
            productList.forEach(product -> {
                List<Attribute> attributeList = attributeDao.findByProduct(product.getId());
                product.setAttributeList(attributeList);
            });
            dbBo.setProductList(productList);
        }
        return dbBo;
    }

    /*
     * Cập nhập nhóm
     * Validate & Action:
     *   - Nhóm phải không bị hủy
     *   - Thực hiện xóa nếu đã có sản phẩm gắn với nhóm:
     *       + Upate trường groupId tại bảng TBL_PRODUCT về null với các sản phẩm đã gắn với nhóm trước đó
     *       + Xóa trong bảng TBL_GROUP_PRODUCT_LINK
     *   - Nếu danh sách sản phẩm > 0:
     *      + Validate: Danh sách thuộc tính chung phải > 0
     *      + Tất cả các sản phẩm đưa vào phải chứa tất cả các thuộc tính chung đưa lên
     *      + Thêm dữ liệu mới
     *           . Bảng TBL_PRODUCT: update groupId
     *           . Lấy product_attribute_link_id từ bảng TBL_PRODCUT_ATTRIBUTE_LINK (productId,attributeCommonId)
     *           . Thêm vào bảng TBL_GROUP_PRODUCT_LINK (groupId, productId, attributeId, product_attribute_link_id)
     * */
    @Override
    public Group doUpdate(Group bo) {
        Group dbBo = super.get(bo);
        if (dbBo.getIsTrash().equals(Boolean.TRUE)) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setName(bo.getName());
        dbBo.setSlug(bo.getSlug());
        dao.save(dbBo);
        /*Xóa dữ liệu cũ*/
        GroupProductLink groupProductLink = new GroupProductLink();
        groupProductLink.setGroupId(dbBo.getId());
        List<GroupProductLink> groupProductLinkList = groupProductLinkDao.findAll(Example.of(groupProductLink));
        if (groupProductLinkList.size() > 0) {
            List<Integer> productIdList = groupProductLinkList.stream().map(GroupProductLink::getProductId).collect(Collectors.toList());
            updateProductGroupId(productIdList, null);
            groupProductLinkDao.deleteAll(groupProductLinkList);
        }

        if (bo.getProductList() == null || bo.getProductList().size() == 0) {
            return dbBo;
        }

        List<Integer> productIdList = bo.getProductList().stream().map(Product::getId).collect(Collectors.toList());
        List<GroupProductLink> groupProductLinkList1 = groupProductLinkDao.findByProductList(dbBo.getId(),productIdList);
        groupProductLinkDao.deleteAll(groupProductLinkList1);

        /*Validate tính hợp lệ*/
        if (bo.getAttributeIdList() == null || bo.getAttributeIdList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        bo.getProductList().forEach(item1 -> {
            bo.getAttributeIdList().forEach(attributeId -> {
                ProductAttributeLink productAttributeLink = new ProductAttributeLink();
                productAttributeLink.setProductId(item1.getId());
                productAttributeLink.setAttributeId(attributeId);
                if (productAttributeLinkDao.count(Example.of(productAttributeLink)) == 0) {
                    throw new HebelaException("action.invalid");
                }
            });
        });

        /*Thêm dữ liệu mới*/
        bo.getProductList().forEach(item1 -> {
            updateProductGroupId(productIdList, dbBo.getId());
            bo.getAttributeIdList().forEach(attributeId -> {
                ProductAttributeLink productAttributeLink = new ProductAttributeLink();
                productAttributeLink.setProductId(item1.getId());
                productAttributeLink.setAttributeId(attributeId);
                productAttributeLink = productAttributeLinkDao.findOne(Example.of(productAttributeLink)).orElse(null);
                if (productAttributeLink == null) {
                    throw new HebelaException("action.invalid");
                }
                GroupProductLink groupProductLinkInsert = new GroupProductLink();
                groupProductLinkInsert.setGroupId(dbBo.getId());
                groupProductLinkInsert.setProductId(item1.getId());
                groupProductLinkInsert.setAttributeId(attributeId);
                groupProductLinkInsert.setProductAttributeLinkId(productAttributeLink.getId());
                groupProductLinkInsert.setCreatedBy(User.getContextId());
                groupProductLinkDao.save(groupProductLinkInsert);
            });
        });
        return dbBo;
    }

    private void updateProductGroupId(List<Integer> productIdList, Integer newGroupId) {
        if (productIdList.size() > 0) {
            productIdList.forEach(id -> {
                Product dbProduct = productDao.getOne(id);
                dbProduct.setGroupId(newGroupId);
                productDao.save(dbProduct);
            });
        }
    }
}

