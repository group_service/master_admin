package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.GiftAccountLink;

public interface GiftAccountLinkService extends Service<GiftAccountLink, Integer> {
}

