package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.masterdata.bo.MetaConfig;
import com.hebela.masterdata.bo.PageConfig;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.bo.extend.PlatformExtend;
import com.hebela.masterdata.dao.PageConfigDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
public class PageConfigServiceImpl extends AbstractStatusService<PageConfig> implements PageConfigService {
    private final PageConfigDao pageConfigDao;

    public PageConfigServiceImpl(PageConfigDao pageConfigDao) {
        this.pageConfigDao = pageConfigDao;
    }

    @Override
    public List<PageConfig> find(PageConfig bo) {
        return pageConfigDao.findAll(Example.of(bo), Sort.by(Sort.Order.asc(PageConfig._code)));
    }

    @Override
    public PageConfig doInsert(PageConfig bo) {
        validateData(bo);
        return super.doInsert(bo);
    }

    @Override
    public PageConfig doUpdate(PageConfig bo) {
        validateData(bo);
        return super.doUpdate(bo);
    }

    @Override
    public PageConfig doRestore(PageConfig bo) {
        validateData(bo);
        return super.doRestore(bo);
    }

    private void validateData(PageConfig bo) {
        if (StringUtils.isEmpty(bo.getName()) || StringUtils.isEmpty(bo.getCode())) {
            throw new HebelaException("action.invalid");
        }
        validatePageConfig(bo.getCode(), bo.getId(), bo.getPlatform());
    }

    private void validatePageConfig(String code, Integer pageConfigId, Integer platformClient) {
        List<PageConfig> pageConfigList = pageConfigDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PageConfig._code), code));
            predicates.add(builder.equal(root.get(PageConfig._isTrash), Boolean.FALSE));
            if (pageConfigId != null) {
                predicates.add(builder.notEqual(root.get(PageConfig._id), pageConfigId));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        if (pageConfigList.size() > 0) {
            for (PageConfig item : pageConfigList) {
                Integer platformDb = item.getPlatform();
                for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                    if (platformDb != null && (platformDb & platform) > 0 && (platformClient & platform) > 0) {
                        String[] error = new String[1];
                        error[0] = item.getName();
                        throw new HebelaException("page.config.obj.duplicate.platform", error);
                    }
                }
            }
        }
    }
}

