package com.hebela.masterdata.service;
import com.hebela.core.AbstractService;
import com.hebela.masterdata.bo.GiftAccountLink;
import org.springframework.stereotype.Service;


@Service
public class GiftAccountLinkServiceImpl extends AbstractService<GiftAccountLink, Integer> implements GiftAccountLinkService {
}

