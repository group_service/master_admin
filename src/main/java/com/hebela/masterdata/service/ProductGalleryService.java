package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.ProductGallery;

public interface ProductGalleryService extends Service<ProductGallery, Integer> {
}
