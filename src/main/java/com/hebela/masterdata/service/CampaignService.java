package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.Campaign;

import java.util.List;

public interface CampaignService extends Service<Campaign, Integer> {

    List<Campaign> findAll(Campaign campaign);

    List<Campaign> findAllShop(Campaign campaign);
}

