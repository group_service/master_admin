package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.Cart;
import com.hebela.masterdata.bo.extend.CartAccount;
import com.hebela.masterdata.bo.extend.CartView;
import com.hebela.masterdata.bo.extend.Checkout;
import com.hebela.masterdata.web.cart.CartSearchRequest;
import com.hebela.masterdata.web.cart.cartview.CartViewRequest;
import com.hebela.masterdata.web.cart.checkout.CheckoutRequest;
import org.springframework.data.domain.Page;

public interface CartService extends Service<Cart, Integer> {
    CartView view(CartViewRequest model);

    CartView doAdd(CartViewRequest model);

    Cart doRemove(Cart cart, Integer accountId);

    CartView doUpdate(CartViewRequest model);

    Checkout checkout(CheckoutRequest checkoutRequest);

    Page<CartAccount> findBySearch(CartSearchRequest cartSearchRequest);
}

