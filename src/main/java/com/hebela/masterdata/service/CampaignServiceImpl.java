package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Campaign;
import com.hebela.masterdata.bo.Coupon;
import com.hebela.masterdata.bo.Voucher;
import com.hebela.masterdata.dao.CampaignDao;
import com.hebela.masterdata.dao.CouponDao;
import com.hebela.masterdata.dao.VoucherDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CampaignServiceImpl extends AbstractStatusService<Campaign> implements CampaignService {

    @Autowired
    protected CouponDao couponDao;
    @Autowired
    protected VoucherDao voucherDao;
    @Autowired
    private CampaignDao campaignDao;

    @Override
    public Campaign doInsert(Campaign bo) {
        if (StringUtils.isEmpty(bo.getName())) {
            throw new HebelaException("campaign.name.invalid");
        }
        return super.doInsert(bo);
    }

    @Override
    public List<Campaign> find(Campaign bo) {
        Integer orgId = null;
        if (Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
            orgId = bo.getOrgId();
        } else {
            orgId = User.getContext().getOrgId();
        }
        bo.setOrgId(orgId);
//        Coupon coupon = new Coupon();
//        coupon.setIsTrash(Boolean.FALSE);
//        coupon.setStatus(Coupon.STATUS_ACTIVE);
//        List<Coupon> couponList = couponDao.findAll(Example.of(coupon));
        List<Campaign> campaignList = campaignDao.findAll(Example.of(bo), Sort.by(Sort.Order.desc(Campaign._createdDate)));
//        for (Campaign campaign : campaignList) {
//            campaign.setCouponCount(couponList.stream().filter(item -> item.getCampaignId().equals(campaign.getId())).count());
//        }
        return campaignList;
    }

    @Override
    public Campaign doDelete(Campaign bo) {
        Coupon coupon = new Coupon();
        coupon.setCampaignId(bo.getId());
        if (couponDao.count(Example.of(coupon)) > 0) {
            throw new HebelaException("campaign.belong.to.coupon");
        }
        Voucher voucher = new Voucher();
        voucher.setCampaignId(bo.getId());
        if (voucherDao.count(Example.of(voucher)) > 0) {
            throw new HebelaException("campaign.belong.to.voucher");
        }
        return super.doDelete(bo);
    }

    @Override
    public Campaign doTrash(Campaign bo) {
        /** huỷ coupon */
        Coupon coupon = new Coupon();
        coupon.setCampaignId(bo.getId());
        List<Coupon> couponListByCampaign = couponDao.findAll(Example.of(coupon));
        for (Coupon dbCoupon : couponListByCampaign) {
            dbCoupon.setIsTrash(Boolean.TRUE);
        }
        couponDao.saveAll(couponListByCampaign);
        /** huỷ voucher */
        Voucher voucher = new Voucher();
        voucher.setCampaignId(bo.getId());
        List<Voucher> voucherListByCampaign = voucherDao.findAll(Example.of(voucher));
        for (Voucher dbVoucher : voucherListByCampaign) {
            dbVoucher.setIsTrash(Boolean.TRUE);
        }
        voucherDao.saveAll(voucherListByCampaign);
        return super.doTrash(bo);
    }

    // lấy tất cả chiến dịch của các shop và sàn
    @Override
    public List<Campaign> findAll(Campaign campaign) {
        if (Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
            campaign.setOrgId(Organization.ROOT_ACCOUNT.getId());
        } else {
            return null;
        }
        return this.campaignDao.findAll(campaign.getIsTrash(), campaign.getStatus(), campaign.getOrgId());
    }

    // lấy tất cả chiến dịch của các shop không lấy chiến dịch của sàn
    @Override
    public List<Campaign> findAllShop(Campaign campaign) {
        if (Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
            campaign.setOrgId(Organization.ROOT_ACCOUNT.getId());
        } else {
            return null;
        }
        return this.campaignDao.findAllShop(campaign.getIsTrash(), campaign.getStatus());
    }
}

