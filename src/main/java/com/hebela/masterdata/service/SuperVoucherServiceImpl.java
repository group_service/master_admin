package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Account;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.CartView;
import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.bo.extend.PlatformExtend;
import com.hebela.masterdata.bo.extend.VoucherAccountExtend;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.enumeration.VoucherErrorEnum;
import com.hebela.masterdata.web.voucher.VoucherSearchRequest;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SuperVoucherServiceImpl extends AbstractStatusService<SuperVoucher> implements SuperVoucherService {
    @Autowired
    private SuperVoucherAccountLinkDao superVoucherAccountLinkDao;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private SuperVoucherDao superVoucherDao;
    @Autowired
    private CampaignDao campaignDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private SuperVoucherProductLinkDao superVoucherProductLinkDao;
    @Autowired
    private PromotionDao promotionDao;
    @Autowired
    private PromotionTimeFrameDao promotionTimeFrameDao;
    @Autowired
    private PromotionProductDao promotionProductDao;


    @Override
    public SuperVoucher get(Integer key) {
        SuperVoucher superVoucher = super.get(key);
        List<VoucherAccountExtend> accountList = this.accountDao.findAll((root, query, builder) -> {
            Root<SuperVoucherAccountLink> linkRoot = query.from(SuperVoucherAccountLink.class);
            return builder.and(
                    builder.equal(root.get(Account._id), linkRoot.get(SuperVoucherAccountLink._accountId)),
                    builder.equal(linkRoot.get(SuperVoucherAccountLink._superVoucherId), superVoucher.getId())
            );
        }).stream().map(VoucherAccountExtend::toExtend).collect(Collectors.toList());
        superVoucher.setAccountList(accountList);
        superVoucher.setProductList(
                this.productDao.findAll((root, query, builder) -> {
                    Root<ProductPrice> priceRoot = query.from(ProductPrice.class);
                    Root<SuperVoucherProductLink> superVoucherProductLinkRoot = query.from(SuperVoucherProductLink.class);
                    query.multiselect(root, priceRoot, superVoucherProductLinkRoot);
                    query.orderBy(builder.asc(superVoucherProductLinkRoot.get(CouponProductLink._createdDate)));
                    return builder.and(
                            builder.equal(root.get(Product._id), superVoucherProductLinkRoot.get(SuperVoucherProductLink._productId)),
                            builder.equal(superVoucherProductLinkRoot.get(SuperVoucherProductLink._superVoucherId), key),
                            builder.equal(root.get(Product._id), priceRoot.get(ProductPrice._productId)),
                            builder.lessThanOrEqualTo(priceRoot.get(ProductPrice._startDate), Instant.now()),
                            builder.greaterThan(priceRoot.get(ProductPrice._endDate), Instant.now())
                    );
                }));
        return superVoucher;
    }

    @Override
    public SuperVoucher doInsert(SuperVoucher bo) {
        if (bo.getProductList() == null || bo.getProductList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        if (bo.getVoucherNumber() == null) {
            bo.setVoucherNumber(0);
        }
        if (bo.getStatus() == null) {
            bo.setStatus(SuperVoucher.STATUS_INACTIVE);
        }
        if (!User.getContext().getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
            bo.setOrgId(User.getContext().getOrgId());
        }
        bo.setVoucherUsed(0);
        if (SuperVoucher.STATUS_ACTIVE.equals(bo.getStatus())) {
            this.validProductForSuperVoucher(bo, bo.getProductList());
            if (bo.getErrorList() != null && bo.getErrorList().size() > 0) {
                return bo;
            }
        }
        SuperVoucher superVoucher = super.doInsert(bo);
        this.updateProductSuperVoucher(superVoucher, bo.getProductList());
        if (SuperVoucher.USER_TYPE_SPECIFIED.equals(superVoucher.getUserType())) {
            if (bo.getAccountList() != null && bo.getAccountList().size() > 0) {
                this.updateAccountOfVoucher(superVoucher.getId(), bo.getAccountList());
            } else {
                throw new HebelaException("voucher.account.list.missing");
            }
        }
        return superVoucher;
    }

    @Override
    public SuperVoucher doRestore(SuperVoucher bo) {
        SuperVoucher superVoucher = super.get(bo.getId());
        Campaign campaign = campaignDao.findById(superVoucher.getCampaignId()).orElse(null);
        if (campaign == null || Boolean.TRUE.equals(campaign.getIsTrash())) {
            throw new HebelaException("campaign.not.exists");
        }
        return super.doRestore(bo);
    }

    @Override
    public SuperVoucher doUpdate(SuperVoucher bo) {
        // check sản phẩm có đang tham gia FlashSale có thời gian diễn ra giao nhau
        if (SuperVoucher.STATUS_ACTIVE.equals(bo.getStatus())) {
            this.validProductInSuperVoucher(bo);
            if (bo.getErrorList() != null && bo.getErrorList().size() > 0) {
                return bo;
            }
        }

        SuperVoucher dbBo = super.get(bo);
        bo.setVoucherUsed(dbBo.getVoucherUsed());
        bo.setOrgId(dbBo.getOrgId());
        bo.setDiscountType(dbBo.getDiscountType());
        bo.setUserType(dbBo.getUserType());
        SuperVoucher superVoucher = super.doUpdate(bo);
        if (SuperVoucher.USER_TYPE_SPECIFIED.equals(superVoucher.getUserType())) {
            if (bo.getAccountList() != null && bo.getAccountList().size() > 0) {
                SuperVoucherAccountLink example = new SuperVoucherAccountLink();
                example.setSuperVoucherId(superVoucher.getId());
                List<SuperVoucherAccountLink> dbLinkList = this.superVoucherAccountLinkDao.findAll(Example.of(example));
                List<SuperVoucherAccountLink> clientLinkList = new ArrayList<>();
                for (VoucherAccountExtend account : bo.getAccountList()) {
                    SuperVoucherAccountLink link = new SuperVoucherAccountLink();
                    link.setSuperVoucherId(superVoucher.getId());
                    link.setAccountId(account.getId());
                    link.setCreatedBy(User.getContextId());
                    clientLinkList.add(link);
                }
                this.superVoucherAccountLinkDao.saveAll(ListUtils.removeAll(clientLinkList, dbLinkList));
                this.superVoucherAccountLinkDao.deleteAll(ListUtils.removeAll(dbLinkList, clientLinkList));
            } else {
                throw new HebelaException("voucher.account.list.missing");
            }
        }
        return superVoucher;
    }

    @Override
    public SuperVoucher doTrash(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo.getId());
        dbBo.setStatus(SuperVoucher.STATUS_INACTIVE);
        dbBo.setIsTrash(Boolean.TRUE);
        dbBo.setModifiedBy(User.getContextId());
        this.superVoucherDao.save(dbBo);
        return dbBo;
    }

    @Override
    public SuperVoucher doDelete(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo);
        if (dbBo == null || dbBo.getVoucherUsed() != 0) {
            throw new HebelaException("voucher.can.not.detele");
        }
        return super.doDelete(bo);
    }

    /*
     * Kích hoạt voucher
     * voucher phải không hủy, ở trạng chái chưa kích hoạt
     * */
    @Override
    public SuperVoucher doActivate(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo.getId());
        if (dbBo.getStatus().equals(SuperVoucher.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        this.validProductInSuperVoucher(bo);
        if (bo.getErrorList() != null && bo.getErrorList().size() > 0) {
            return bo;
        }

        dbBo.setStatus(Coupon.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.superVoucherDao.save(dbBo);
        return dbBo;
    }

    /*
     * Hủy kích hoạt voucher
     * Pre: voucher phải không hủy, ở trạng thái kích hoạt
     * */
    @Override
    public SuperVoucher doDeactivate(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo.getId());
        if (!dbBo.getStatus().equals(SuperVoucher.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(SuperVoucher.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.superVoucherDao.save(dbBo);
        return dbBo;
    }

    private void updateProductSuperVoucher(SuperVoucher superVoucher, List<Product> clientProductList) {
        SuperVoucherProductLink example = new SuperVoucherProductLink();
        example.setSuperVoucherId(superVoucher.getId());
        List<SuperVoucherProductLink> dbList = this.superVoucherProductLinkDao.findAll(Example.of(example));
        List<Integer> dbProductIdList = dbList.stream().map(SuperVoucherProductLink::getProductId).collect(Collectors.toList());
        for (Product product : clientProductList) {
            if (!dbProductIdList.contains(product.getId())) {
                SuperVoucherProductLink productLink = new SuperVoucherProductLink();
                productLink.setSuperVoucherId(superVoucher.getId());
                productLink.setProductId(product.getId());
                if (SuperVoucher.FIXED_MONEY.equals(superVoucher.getDiscountType())) {
                    productLink.setFixedPrice(product.getFixedPrice());
                }
                this.superVoucherProductLinkDao.save(productLink);
            }
        }
    }

    @Override
    public SuperVoucher doUpdateFixedPrice(SuperVoucher bo) {
        Product product = bo.getProduct();
        if (product.getFixedPrice() == null || product.getFixedPrice() < 0) {
            throw new HebelaException("super.voucher.fixed.price.invalid");
        }
        SuperVoucherProductLink example = new SuperVoucherProductLink();
        example.setSuperVoucherId(bo.getId());
        example.setProductId(bo.getProduct().getId());
        SuperVoucherProductLink productLinkDB = this.superVoucherProductLinkDao.findOne(Example.of(example)).orElse(null);
        if (productLinkDB == null) {
            throw new HebelaException("data.input.invalid");
        }
        productLinkDB.setFixedPrice(product.getFixedPrice());
        this.superVoucherProductLinkDao.save(productLinkDB);
        return bo;
    }

    /*
     * Thêm sản phẩm áp dụng vào coupon
     * Pre:
     *      - Coupon không được hủy
     *      - Số sản phẩm thêm phải > 0 và hợp lệ
     * */
    @Override
    public SuperVoucher doAddProduct(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getProductList() == null || bo.getProductList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        SuperVoucher superVoucher = validProductForSuperVoucher(dbBo, bo.getProductList());
        if (superVoucher.getErrorList() != null && superVoucher.getErrorList().size() > 0) {
            return superVoucher;
        }
        this.updateProductSuperVoucher(dbBo, bo.getProductList());
        return superVoucher;
    }

    private SuperVoucher validProductForSuperVoucher(SuperVoucher dbSuperVoucher, List<Product> clientProductList) {
        List<HebelaError> hebelaErrorList = new ArrayList<>();
        List<Integer> productIdList = clientProductList.stream().map(Product::getId).collect(Collectors.toList());
        Map<Integer, Product> productMap = this.productDao.findAllById(productIdList).stream().collect(Collectors.toMap(Product::getId, product -> product));
        Product dbProduct;
        for (Product product : clientProductList) {
            if (SuperVoucher.FIXED_MONEY.equals(dbSuperVoucher.getDiscountType())) {
                if (product.getFixedPrice() == null || product.getFixedPrice() < 0) {
                    hebelaErrorList.add(new HebelaError("Giá chỉ định không hợp lệ. Sản phẩm " + product.getName()
                            + " [SKU: " + product.getSku() + "]"));
                    continue;
                }
            }
            dbProduct = productMap.get(product.getId());
            if (!dbProduct.getStatus().equals(Product.STATUS_ACTIVE)
                    || dbProduct.getIsTrash().equals(Boolean.TRUE)
                    || !dbProduct.getType().equals(Product.TYPE_SELL)) {
                hebelaErrorList.add(new HebelaError("Sản phẩm" + dbProduct.getName() + " [ SKU - " + dbProduct.getSku()
                        + " ] không thuộc loại kinh doanh và đang đăng bán"));
            }
        }
        this.checkProductInFlashSale(dbSuperVoucher, productIdList, productMap);
        if (hebelaErrorList.size() > 0) {
            dbSuperVoucher.setErrorList(hebelaErrorList);
        }
        return dbSuperVoucher;
    }

    private void validProductInSuperVoucher(SuperVoucher bo) {
        // check sản phẩm có đang tham gia FlashSale có thời gian diễn ra giao nhau
        SuperVoucherProductLink productLink = new SuperVoucherProductLink();
        productLink.setSuperVoucherId(bo.getId());
        List<Integer> productLinkIds = this.superVoucherProductLinkDao.findAll(Example.of(productLink))
                .stream().map(SuperVoucherProductLink::getProductId).collect(Collectors.toList());
        Map<Integer, Product> productMap = this.productDao.findAllById(productLinkIds).stream().collect(Collectors.toMap(Product::getId, product -> product));
        this.checkProductInFlashSale(bo, productLinkIds, productMap);
    }

    private void checkProductInFlashSale(SuperVoucher dbSuperVoucher, List<Integer> productIds, Map<Integer, Product> productMap) {
        if (dbSuperVoucher.getErrorList() == null) {
            dbSuperVoucher.setErrorList(new ArrayList<>());
        }
        Map<Integer, Promotion> promotionMap = findProductInFlashSale(dbSuperVoucher, productIds);
        if (promotionMap.keySet().size() > 0) {
            dbSuperVoucher.getErrorList().add(new HebelaError("Thời gian diễn ra có sản phẩm thuộc FlashSale."));
        }
        Promotion promotion;
        Product dbProduct;
        for (Integer productId : productMap.keySet()) {
            promotion = promotionMap.get(productId);
            if (promotion != null) {
                dbProduct = productMap.get(productId);
                dbSuperVoucher.getErrorList().add(new HebelaError("Sản phẩm " + dbProduct.getName() + " [ SKU - " + dbProduct.getSku()
                        + " ] đang tham gia Flash sale: [" + promotion.getName() + "] nên không áp dụng Super Voucher"));
            }
        }
    }

    private Map<Integer, Promotion> findProductInFlashSale(SuperVoucher superVoucher, List<Integer> productIds) {
        Map<Integer, Promotion> result = new HashMap<>();
        if (productIds.size() > 0) {
            List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                predicates.add((root.get(PromotionProduct._productId).in(productIds)));
                predicates.add(builder.equal(root.get(PromotionProduct._status), Promotion.STATUS_ACTIVE));
                return builder.and(predicates.toArray(new Predicate[0]));
            });
            if (promotionProducts.size() > 0) {
                List<Integer> promotionTimeFrameIds = promotionProducts.stream().map(PromotionProduct::getTimeFrameId).distinct().collect(Collectors.toList());
                List<PromotionTimeFrame> promotionTimeFrames = promotionTimeFrameDao.findAll((root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    predicates.add(root.get(PromotionTimeFrame._id).in(promotionTimeFrameIds));
                    predicates.add(builder.equal(root.get(PromotionTimeFrame._status), Promotion.STATUS_ACTIVE));
                    predicates.add(
                            builder.or(builder.between(root.get(PromotionTimeFrame._startTime),
                                    superVoucher.getStartDate(),
                                    superVoucher.getEndDate()
                                    ),
                                    builder.between(root.get(PromotionTimeFrame._endTime),
                                            superVoucher.getStartDate(),
                                            superVoucher.getEndDate()
                                    ),
                                    builder.and(
                                            builder.lessThanOrEqualTo(root.get(PromotionTimeFrame._startTime), superVoucher.getStartDate()),
                                            builder.greaterThanOrEqualTo(root.get(PromotionTimeFrame._endTime), superVoucher.getEndDate())
                                    )
                            )
                    );
                    return builder.and(predicates.toArray(new Predicate[0]));
                });
                if (promotionTimeFrames.size() > 0) {
                    List<Integer> promotionIds = promotionTimeFrames.stream().map(PromotionTimeFrame::getPromotionId).distinct().collect(Collectors.toList());
                    List<Promotion> promotionList = this.promotionDao.findAllById(promotionIds);

                    List<PromotionTimeFrame> timeFrameList;
                    for (Promotion promotionDb : promotionList) {
                        timeFrameList = promotionTimeFrames.stream()
                                .filter(timeFrame -> timeFrame.getPromotionId().equals(promotionDb.getId()))
                                .collect(Collectors.toList());
                        promotionDb.setPromotionTimeFrameList(timeFrameList);
                    }
                    Map<Integer, Promotion> promotionMap = promotionList.stream().collect(Collectors.toMap(Promotion::getId, promotion -> promotion));
                    for (PromotionProduct promotionProduct : promotionProducts) {
                        if (promotionMap.get(promotionProduct.getPromotionId()) != null) {
                            result.put(promotionProduct.getProductId(), promotionMap.get(promotionProduct.getPromotionId()));
                        }
                    }
                }
            }
        }
        return result;
    }

    /*
     * Remove sản phẩm khỏi SuperVoucher
     * Pre: SuperVoucher không được hủy
     * */
    @Override
    public SuperVoucher doRemoveProduct(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getProduct() == null || bo.getProduct().getId() == null) {
            throw new HebelaException("action.invalid");
        }
        SuperVoucherProductLink superVoucherProductLink = new SuperVoucherProductLink();
        superVoucherProductLink.setSuperVoucherId(bo.getId());
        superVoucherProductLink.setProductId(bo.getProduct().getId());
        List<SuperVoucherProductLink> superVoucherProductLinks = this.superVoucherProductLinkDao.findAll(Example.of(superVoucherProductLink));
        if (superVoucherProductLinks.size() > 0) {
            this.superVoucherProductLinkDao.deleteAll(superVoucherProductLinks);
        }
        return bo;
    }

    @Override
    public SuperVoucher doAddAccountOfVoucher(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getAccountList() == null || bo.getAccountList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        this.updateAccountOfVoucher(bo.getId(), bo.getAccountList());
        return dbBo;
    }

    private void updateAccountOfVoucher(Integer voucherId, List<VoucherAccountExtend> clientAccountList) {
        SuperVoucherAccountLink example = new SuperVoucherAccountLink();
        example.setSuperVoucherId(voucherId);
        List<SuperVoucherAccountLink> dbList = this.superVoucherAccountLinkDao.findAll(Example.of(example));
        List<Integer> dbAccountIdList = dbList.stream().map(SuperVoucherAccountLink::getAccountId).collect(Collectors.toList());
        List<SuperVoucherAccountLink> clientList = new ArrayList<>();
        for (VoucherAccountExtend account : clientAccountList) {
            if (!dbAccountIdList.contains(account.getId())) {
                SuperVoucherAccountLink voucherAccountLink = new SuperVoucherAccountLink();
                voucherAccountLink.setSuperVoucherId(voucherId);
                voucherAccountLink.setAccountId(account.getId());
                clientList.add(voucherAccountLink);
            }
        }
        this.superVoucherAccountLinkDao.saveAll(clientList);
    }

    /*
     * Remove account khỏi voucher
     * Pre:
     *      - voucher không được hủy và phải có thông tin account
     * */
    @Override
    public SuperVoucher doRemoveAccountOfVoucher(SuperVoucher bo) {
        SuperVoucher dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getVoucherAccountExtend() == null) {
            throw new HebelaException("action.invalid");
        }
        SuperVoucherAccountLink voucherAccountLink = new SuperVoucherAccountLink();
        voucherAccountLink.setSuperVoucherId(bo.getId());
        voucherAccountLink.setAccountId(bo.getVoucherAccountExtend().getId());
        List<SuperVoucherAccountLink> voucherAccountLinkList = superVoucherAccountLinkDao.findAll(Example.of(voucherAccountLink));
        if (voucherAccountLinkList.size() > 0) {
            superVoucherAccountLinkDao.deleteAll(voucherAccountLinkList);
        }
        return bo;
    }

    public List<Account> findAccount(String keyword) {
        return this.accountDao.findAll((root, query, builder) -> builder.or(
                builder.like(root.get(Account._mobile), "%" + keyword + "%"),
                builder.like(root.get(Account._fullname), "%" + keyword + "%")
        ));
    }

    @Override
    public Page<SuperVoucher> findBySearch(VoucherSearchRequest model) {
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        Instant endDate = model.getEndDate();

        return superVoucherDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<Campaign> campaignRoot = query.from(Campaign.class);
            predicates.add(builder.equal(root.get(SuperVoucher._campaignId), campaignRoot.get(Campaign._id)));
            query.distinct(true);
            query.multiselect(root, campaignRoot.get(Campaign._name));
            predicates.add(builder.equal(root.get(SuperVoucher._isTrash), model.getIsTrash()));
            if (model.getStatus() != null) {
                predicates.add(builder.equal(root.get(SuperVoucher._status), model.getStatus()));
            }
            if (model.getStatusTime() != null) {
                predicates.add(builder.equal(root.get(SuperVoucher._status), SuperVoucher.STATUS_ACTIVE));
                //Voucher sắp diễn ra
                if (SuperVoucher.STATUS_TIME_UPCOMING.equals(model.getStatusTime())) {
                    predicates.add(builder.greaterThan(root.get(SuperVoucher._startDate), Instant.now()));
                    //Voucher đang diễn ra
                } else if (SuperVoucher.STATUS_TIME_ONGOING.equals(model.getStatusTime())) {
                    predicates.add(builder.lessThanOrEqualTo(root.get(SuperVoucher._startDate), Instant.now()));
                    predicates.add(builder.greaterThanOrEqualTo(root.get(SuperVoucher._endDate), Instant.now()));
                } else {
                    //Voucher đã kết thúc
                    predicates.add(builder.lessThan(root.get(SuperVoucher._endDate), Instant.now()));
                }
            }
            if (model.getStartDate() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(SuperVoucher._startDate), model.getStartDate()));
            }
            if (endDate != null) {
                predicates.add(builder.lessThan(root.get(SuperVoucher._endDate), endDate.plus(1, ChronoUnit.DAYS)));
            }
            if (StringUtils.isNotEmpty(model.getName())) {
                predicates.add(builder.like(root.get(SuperVoucher._name), "%" + model.getName() + "%"));
            }
            if (StringUtils.isNotEmpty(model.getCode())) {
                predicates.add(builder.like(root.get(SuperVoucher._code), "%" + model.getCode() + "%"));
            }
            if (!User.getContext().getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
                predicates.add(builder.equal(root.get(SuperVoucher._orgId), User.getContext().getOrgId()));
            } else if (model.getOrganizationId() != null) {
                predicates.add(builder.equal(root.get(SuperVoucher._orgId), model.getOrganizationId()));
            }
            if (model.getCampaignId() != null) {
                predicates.add(builder.equal(root.get(SuperVoucher._campaignId), model.getCampaignId()));
            }
            if (model.getVoucherStatusUseTypeList().size() == 1) {
                // Lượng voucher đã dùng hết
                if (SuperVoucher.STATUS_USED.equals(model.getVoucherStatusUseTypeList().get(0))) {
                    predicates.add(builder.equal(root.get(SuperVoucher._voucherUsed), root.get(SuperVoucher._voucherNumber)));
                } else {
                    // Lượng voucher chưa dùng hết
                    predicates.add(builder.lessThan(root.get(SuperVoucher._voucherUsed), root.get(SuperVoucher._voucherNumber)));
                }
            }
            if (StringUtils.isNotEmpty(model.getMobile()) || StringUtils.isNotEmpty(model.getFullname())) {
                Root<Account> accountRoot = query.from(Account.class);
                Root<SuperVoucherAccountLink> voucherAccountLinkRoot = query.from(SuperVoucherAccountLink.class);
                predicates.add(builder.equal(root.get(SuperVoucher._id), voucherAccountLinkRoot.get(SuperVoucherAccountLink._superVoucherId)));
                predicates.add(builder.equal(accountRoot.get(Account._id), voucherAccountLinkRoot.get(SuperVoucherAccountLink._accountId)));
                if (StringUtils.isNotEmpty(model.getMobile())) {
                    predicates.add(builder.like(accountRoot.get(Account._mobile), "%" + model.getMobile() + "%"));
                }
                if (StringUtils.isNotEmpty(model.getFullname())) {
                    predicates.add(builder.like(accountRoot.get(Account._fullname), "%" + model.getFullname() + "%"));
                }
            }
            query.orderBy(
                    builder.desc(root.get(SuperVoucher._createdDate)),
                    builder.desc(root.get(SuperVoucher._startDate)),
                    builder.desc(root.get(SuperVoucher._endDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    /*
     * Tìm kiếm danh sách voucher theo account đang đăng nhập
     */
    @Override
    public List<SuperVoucher> findByAccount(Integer platform, CartView cartView) {
        return findVoucherActive(platform, cartView);
    }

    public List<SuperVoucher> findVoucherActive(Integer platform, CartView view) {
        List<SuperVoucher> superVoucherList = this.superVoucherDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Subquery<SuperVoucherAccountLink> subQuery = query.subquery(SuperVoucherAccountLink.class);
            Root<SuperVoucherAccountLink> accountLinkRoot = subQuery.from(SuperVoucherAccountLink.class);
            subQuery.select(accountLinkRoot.get(SuperVoucherAccountLink._superVoucherId))
                    .where(builder.and(
                            builder.equal(root.get(SuperVoucher._id), accountLinkRoot.get(SuperVoucherAccountLink._superVoucherId)),
                            builder.equal(accountLinkRoot.get(SuperVoucherAccountLink._accountId), User.getContextId())
                    ));
            predicates.add(builder.equal(root.get(SuperVoucher._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(SuperVoucher._status), SuperVoucher.STATUS_ACTIVE));
            predicates.add(builder.isNotNull(root.get(SuperVoucher._platform)));
            predicates.add(builder.or(
                    builder.equal(root.get(SuperVoucher._userType), SuperVoucher.USER_TYPE_ALL),
                    builder.exists(subQuery)
            ));
            predicates.add(
                    builder.and(
                            builder.lessThan(root.get(SuperVoucher._startDate), Instant.now()),
                            builder.greaterThan(root.get(SuperVoucher._endDate), Instant.now())
                    )
            );
            query.orderBy(builder.asc(root.get(SuperVoucher._name)));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        return checkPlatformVoucher(platform, superVoucherList, view);
    }

    public List<SuperVoucher> checkPlatformVoucher(Integer platform, List<SuperVoucher> superVoucherList, CartView cartView) {
        List<SuperVoucher> superVouchers = new ArrayList<>();
        if (superVoucherList.size() > 0) {
            for (SuperVoucher superVoucher : superVoucherList) {
                if ((superVoucher.getPlatform() & platform) > 0) {
                    superVoucher.setIsEligible(true);
                    this.checkVoucher(superVoucher, cartView);
                    superVouchers.add(superVoucher);
                } else {
                    superVoucher.setIsEligible(false);
                    this.setCartErrorList(superVoucher, platform);
                }
            }
        }
        return superVouchers;
    }

    private void setCartErrorList(SuperVoucher superVoucher, Integer platform) {
        if ((superVoucher.getPlatform() & platform) == 0) {
            if ((superVoucher.getPlatform() & PlatformExtend.APP) > 0) {
                /*voucher chỉ áp dụng cho phiên bản app*/
                superVoucher.getCartErrorList().add(VoucherErrorEnum.platform_app.getValue());
                return;
            }
            if ((superVoucher.getPlatform() & PlatformExtend.WEB) > 0) {
                /*voucher chỉ áp dụng cho phiên bản website*/
                superVoucher.getCartErrorList().add(VoucherErrorEnum.platform_web.getValue());
                return;
            }
            if ((superVoucher.getPlatform() & PlatformExtend.WEB_MOBILE) > 0) {
                /*voucher chỉ áp dụng cho phiên bản website mobile*/
                superVoucher.getCartErrorList().add(VoucherErrorEnum.platform_web_mobile.getValue());
            }
        }
    }

    public void checkVoucher(SuperVoucher superVoucher, CartView cartView) {
        if (Instant.now().isBefore(superVoucher.getStartDate())) {
            /* chưa đến thời gian kích hoạt*/
            superVoucher.getCartErrorList().add(VoucherErrorEnum.start_date_invalid.getValue());
            superVoucher.setIsEligible(false);
            return;
        }
        if (Instant.now().isAfter(superVoucher.getEndDate())) {
            /* chưa đến thời gian kích hoạt*/
            superVoucher.getCartErrorList().add(VoucherErrorEnum.end_date_invalid.getValue());
            superVoucher.setIsEligible(false);
            return;
        }
        if (superVoucher.getVoucherUsed() >= superVoucher.getVoucherNumber()) {
            /* voucher đã sử dụng hết lượt*/
            superVoucher.getCartErrorList().add(VoucherErrorEnum.order_remaining_invalid.getValue());
            superVoucher.setIsEligible(false);
        }
    }
}
