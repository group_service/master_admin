package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.Feedback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface FeedbackService extends Service<Feedback, Integer> {

    Page<Feedback> findFeedBack(Feedback obj, Pageable pageable);
}

