package com.hebela.masterdata.service;
import com.hebela.core.AbstractService;
import com.hebela.masterdata.bo.PromotionConfig;
import org.springframework.stereotype.Service;


@Service
public class PromotionConfigServiceImpl extends AbstractService<PromotionConfig, Integer> implements PromotionConfigService {
}

