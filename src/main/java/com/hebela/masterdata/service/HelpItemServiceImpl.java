package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.HelpCategory;
import com.hebela.masterdata.bo.HelpItem;
import com.hebela.masterdata.bo.MetaConfig;
import com.hebela.masterdata.dao.HelpCategoryDao;
import com.hebela.masterdata.dao.HelpItemDao;
import com.hebela.masterdata.dao.MetaConfigDao;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class HelpItemServiceImpl extends AbstractStatusService<HelpItem> implements HelpItemService {

    private final HelpItemDao helpItemDao;
    private final HelpCategoryDao helpCategoryDao;
    private final MetaConfigDao metaConfigDao;

    public HelpItemServiceImpl(HelpItemDao helpItemDao, HelpCategoryDao helpCategoryDao, MetaConfigDao metaConfigDao) {
        this.helpItemDao = helpItemDao;
        this.helpCategoryDao = helpCategoryDao;
        this.metaConfigDao = metaConfigDao;
    }

    @Override
    public HelpItem get(Integer key) {
        HelpItem dbBo = super.get(key);
        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(key, MetaConfig.TYPE_HELP_ITEM)));
        dbMetaConfigOptional.ifPresent(dbBo::setMetaConfig);
        return dbBo;
    }

    @Override
    public HelpItem doInsert(HelpItem bo) {
        HelpItem dbBo = super.doInsert(bo);
        bo.setId(dbBo.getId());
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setHelpItemId(bo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_HELP_ITEM);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        return dbBo;
    }

    @Override
    public HelpItem doUpdate(HelpItem bo) {
        HelpItem dbBo = super.doUpdate(bo);
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_HELP_ITEM, User.getContextId(), bo.getMetaConfig());
        }
        return dbBo;
    }

    @Override
    public List<HelpItem> find(HelpItem bo) {
        return helpItemDao.findByPriority(bo.getIsTrash());
    }

    @Override
    public HelpItem doRestore(HelpItem bo) {
        HelpCategory helpCategory = helpCategoryDao.findById(bo.getHelpCategoryId()).orElse(null);
        if (helpCategory == null) {
            throw new HebelaException("help.category.obj.missing");
        }
        if (Boolean.TRUE.equals(helpCategory.getIsTrash())) {
            throw new HebelaException("help.category.is.trash");
        }
        return super.doRestore(bo);
    }
}

