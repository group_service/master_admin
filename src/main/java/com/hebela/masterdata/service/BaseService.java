package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.Bo;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.CouponDao;
import com.hebela.masterdata.dao.ProductPriceDao;
import com.hebela.masterdata.dao.ShippingFeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class BaseService<T extends Bo<K>, K extends Serializable> extends AbstractService<T, K> {
    @Autowired
    protected ProductPriceDao productPriceDao;
    @Autowired
    protected CouponDao couponDao;
    @Autowired
    protected ShippingFeeDao shippingFeeDao;

    protected void applyPrice(Product product) {
        Assert.notNull(product, "Sản phẩm không thể null");
        Optional<ProductPrice> productPriceOptional = this.productPriceDao.findOne((root, query, builder) ->
                builder.and(
                        builder.lessThanOrEqualTo(root.get(ProductPrice._startDate), Instant.now()),
                        builder.greaterThan(root.get(ProductPrice._endDate), Instant.now()),
                        builder.equal(root.get(ProductPrice._productId), product.getId())
                )
        );
        productPriceOptional.ifPresent(product::setProductPrice);
    }

    /**
     * Tìm active coupon đang diễn ra dựa vào product Id
     * Coupon chỉ bao gồm các thông tin trên bảng coupon
     * Trả về new Coupon() nếu không tìm thấy coupon active
     *
     * @param productId: id của sản phẩm bất kỳ thuộc coupon
     */
    protected Coupon getCouponActiveByProductId(Integer productId, Integer orgId, Integer platform) {
        Coupon coupon = new Coupon();
        coupon.setType(Coupon.TYPE_NONE);
        coupon.setId(0);
        Coupon activatedCoupon = couponDao.findOne((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<CouponProductLink> productLinkRoot = query.from(CouponProductLink.class);
            predicates.add(builder.equal(root.get(Coupon._id), productLinkRoot.get(CouponProductLink._couponId)));
            if (orgId != null) {
                predicates.add(builder.equal(root.get(Coupon._orgId), orgId));
            }
            predicates.add(builder.greaterThan(root.get(Coupon._couponNumber), 0));
            predicates.add(builder.greaterThan(root.get(Coupon._couponNumber), root.get(Coupon._couponUsed)));
            predicates.add(builder.equal(root.get(Coupon._status), Coupon.STATUS_ACTIVE));
            predicates.add(builder.equal(root.get(Coupon._isTrash), Boolean.FALSE));
            predicates.add(builder.lessThan(root.get(Coupon._startTime), Instant.now()));
            predicates.add(builder.greaterThan(root.get(Coupon._endTime), Instant.now()));
            predicates.add(builder.equal(productLinkRoot.get(CouponProductLink._productId), productId));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        }).orElse(coupon);
        if (activatedCoupon.getPlatform() != null && ((activatedCoupon.getPlatform() & platform) > 0)) {
            return activatedCoupon;
        }
        return coupon;
    }

    /**
     * Tìm coupon đang diễn ra dựa vào coupon Id
     * Coupon chỉ bao gồm các thông tin trên bảng coupon
     * Trả về new Coupon() nếu không tìm thấy coupon active
     *
     * @param bo: coupon
     */
    protected Coupon getCouponActiveByCouponId(Coupon bo, Integer platform) {
        Coupon coupon = new Coupon();
        coupon.setType(Coupon.TYPE_NONE);
        coupon.setId(0);
        Coupon activatedCoupon = couponDao.findOne((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Coupon._id), bo.getId()));
            predicates.add(builder.greaterThan(root.get(Coupon._couponNumber), 0));
            predicates.add(builder.greaterThan(root.get(Coupon._couponNumber), root.get(Coupon._couponUsed)));
            predicates.add(builder.equal(root.get(Coupon._status), Coupon.STATUS_ACTIVE));
            predicates.add(builder.lessThan(root.get(Coupon._startTime), Instant.now()));
            predicates.add(builder.greaterThan(root.get(Coupon._endTime), Instant.now()));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        }).orElse(coupon);
        if (activatedCoupon.getPlatform() != null && ((activatedCoupon.getPlatform() & platform) > 0)) {
            return activatedCoupon;
        }
        return coupon;
    }

    protected List<ShippingFee> findShippingFree(Integer provinceId, Integer moneyMin, Integer moneyMax, Integer shippingFeeIdDb) {
        List<ShippingFee> shippingFeeList = this.shippingFeeDao.findShippingFeeByInclude(provinceId, moneyMin, moneyMax, shippingFeeIdDb);
        if (shippingFeeList != null && shippingFeeList.size() > 0) {
            return shippingFeeList;
        }
        shippingFeeList = this.shippingFeeDao.findShippingFeeByExcludeAll(moneyMin, moneyMax, shippingFeeIdDb);
        if (shippingFeeList != null && shippingFeeList.size() > 0) {
            return shippingFeeList;
        }
        List<Integer> shippingFeeIdList = shippingFeeDao.findShippingFeeIdExclude(provinceId, moneyMin, moneyMax, shippingFeeIdDb);
        if (shippingFeeIdList != null && shippingFeeIdList.size() > 0) {
            shippingFeeList = this.shippingFeeDao.findShippingFeeByNotInExclude(shippingFeeIdList, moneyMin, moneyMax, shippingFeeIdDb);
        } else {
            shippingFeeList = this.shippingFeeDao.findShippingFeeByExclude(moneyMin, moneyMax, shippingFeeIdDb);
        }
        if (shippingFeeList != null && shippingFeeList.size() > 0) {
            return shippingFeeList;
        }
        return null;
    }
}
