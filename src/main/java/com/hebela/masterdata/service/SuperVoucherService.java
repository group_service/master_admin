package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.core.bo.Account;
import com.hebela.masterdata.bo.SuperVoucher;
import com.hebela.masterdata.bo.extend.CartView;
import com.hebela.masterdata.web.voucher.VoucherSearchRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SuperVoucherService extends StatusService<SuperVoucher> {
    List<Account> findAccount(String keyword);

    SuperVoucher doActivate(SuperVoucher bo);

    SuperVoucher doDeactivate(SuperVoucher bo);

    Page<SuperVoucher> findBySearch(VoucherSearchRequest bo);

    SuperVoucher doAddAccountOfVoucher(SuperVoucher superVoucher);

    SuperVoucher doRemoveAccountOfVoucher(SuperVoucher superVoucher);

    List<SuperVoucher> findByAccount(Integer platform, CartView view);

    SuperVoucher doAddProduct(SuperVoucher bo);

    SuperVoucher doRemoveProduct(SuperVoucher bo);

    SuperVoucher doUpdateFixedPrice(SuperVoucher bo);
}

