package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.HebelaLogger;
import com.hebela.core.bo.Account;
import com.hebela.core.bo.Admin;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.core.obj.StatusInfo;
import com.hebela.kafka.ExternalPublisher;
import com.hebela.kafka.InternalPublisher;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.Checkout;
import com.hebela.masterdata.bo.extend.LoyaltyCoin;
import com.hebela.masterdata.bo.extend.OrderExtend;
import com.hebela.masterdata.bo.extend.Seller;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.util.OrderUtil;
import com.hebela.masterdata.web.order.AdminOrderFilterRequest;
import com.hebela.masterdata.web.order.CancelOrderRequest;
import com.hebela.masterdata.web.order.OrderAccountResponse;
import com.hebela.masterdata.web.order.ShopViewOrderResponse;
import com.hebela.masterdata.web.order.insert.OrderRootResponse;
import com.hebela.masterdata.web.order.merchant.MerchantOrderUpdateRequest;
import com.hebela.util.BeanUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl extends AbstractStatusService<Order> implements OrderService {
    @PersistenceContext
    protected EntityManager entityManager;
    @Value("${order.payment.deadline}")
    private int orderPaymentDeadline;
    @Value("${order.payment.max}")
    private int maxOnlinePaymentOrder;
    private final AccountAddressDao accountAddressDao;
    private final ProductDao productDao;
    private final OrderDao orderDao;
    private final OrderRootDao orderRootDao;
    private final OrderItemDao orderItemDao;
    private final InternalPublisher internalPublisher;
    private final CouponDao couponDao;
    private final PaymentDao paymentDao;
    private final AccountDao accountDao;
    private final VoucherAccountLinkDao voucherAccountLinkDao;
    private final VoucherDao voucherDao;
    private final ExternalPublisher externalPublisher;
    private final ProductComboDao productComboDao;
    private final ProductCategoryDao productCategoryDao;
    private final OrderTransactionDao orderTransactionDao;
    private final OrderVoucherDao orderVoucherDao;

    public OrderServiceImpl(OrderRootDao orderRootDao, AccountAddressDao accountAddressDao, ProductDao productDao,
                            AccountDao accountDao, PaymentDao paymentDao, OrderDao orderDao, OrderItemDao orderItemDao,
                            InternalPublisher internalPublisher, VoucherAccountLinkDao voucherAccountLinkDao,
                            CouponDao couponDao, VoucherDao voucherDao, ExternalPublisher externalPublisher, ProductComboDao productComboDao,
                            ProductCategoryDao productCategoryDao, OrderTransactionDao orderTransactionDao, OrderVoucherDao orderVoucherDao) {
        this.orderRootDao = orderRootDao;
        this.accountAddressDao = accountAddressDao;
        this.productDao = productDao;
        this.accountDao = accountDao;
        this.paymentDao = paymentDao;
        this.orderDao = orderDao;
        this.orderItemDao = orderItemDao;
        this.internalPublisher = internalPublisher;
        this.voucherAccountLinkDao = voucherAccountLinkDao;
        this.couponDao = couponDao;
        this.voucherDao = voucherDao;
        this.externalPublisher = externalPublisher;
        this.productComboDao = productComboDao;
        this.productCategoryDao = productCategoryDao;
        this.orderTransactionDao = orderTransactionDao;
        this.orderVoucherDao = orderVoucherDao;
    }

    /*Store Front*/
    @Override
    public OrderRoot doInsert(List<Order> orderList, Checkout checkout) {
        Map<Integer, Order> orderMap = orderList.stream().collect(Collectors.toMap(Order::getSellerId, order -> order));
        AccountAddress accountAddress = accountAddressDao.findById(orderList.get(0).getAccountAddressId()).get();
        if (orderList.get(0).getPaymentMethod() == null) {
            throw new HebelaException("order.payment.method.missing");
        }
        Payment payment = this.paymentDao.findFirstByMethod(orderList.get(0).getPaymentMethod());
        accountAddress.setFullAddress(orderList.get(0).getAccountAddress().getFullAddress());
        if (!orderList.get(0).getAccountId().equals(accountAddress.getAccountId())) {
            throw new HebelaException("order.list.invalid");
        }
        Integer accountId = orderList.get(0).getAccountId();
        Account account = accountDao.findById(accountId).get();
        List<Order> newOrderList = new ArrayList<>();
        Map<Integer, Integer> mapReserveAmount = new HashMap<>();
        Map<Integer, Integer> mapReserveComboAmount = new HashMap<>();
        OrderRoot orderRoot = new OrderRoot();
        orderRoot.setPaymentStatus(Payment.PAYMENT_STATUS_UNPAID);
        orderRoot.setAccountId(accountId);
        orderRoot.setOrderRootCode(OrderUtil.getRandomCode());
        if (!Payment.TYPE_COD.equals(payment.getMethod())) {
            // Admin khong the tao don hang ONLINE
            List<Integer> paymentTypeOnlineList = new ArrayList<>();
            paymentTypeOnlineList.add(Payment.TYPE_ONLINE_BAOKIM);
            paymentTypeOnlineList.add(Payment.TYPE_ONLINE_VNPAY);
            paymentTypeOnlineList.add(Payment.TYPE_ONLINE_MOMO);
            paymentTypeOnlineList.add(Payment.TYPE_ONLINE_ZALOPAY);
            long count = this.orderDao.count((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                query.distinct(true);
                predicates.add(builder.equal(root.get(Order._accountId), User.getContextId()));
                predicates.add(builder.equal(root.get(Order._status), Order.STATUS_WAIT_FOR_PAY));
                predicates.add(builder.equal(root.get(Order._paymentStatus), Payment.PAYMENT_STATUS_UNPAID));
                predicates.add(root.get(Order._paymentMethod).in(paymentTypeOnlineList));
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            });
            if (count > maxOnlinePaymentOrder) {
                throw new HebelaException("order.payment.max.invalid");
            }
            orderRoot.setMrcOrderId(orderRoot.getOrderRootCode() + "1");
        }
        orderRoot.setTotal(checkout.getTotal());
        orderRoot.setSubTotal(checkout.getSubTotal());
        orderRoot.setVoucherDiscount(checkout.getVoucherDiscount());
        orderRoot.setCouponDiscount(checkout.getCouponDiscount());
        orderRoot.setShippingFee(checkout.getShippingFee());
        orderRoot.setPaymentMethod(payment.getMethod());
        OrderRoot dbOrderRoot = this.orderRootDao.save(orderRoot);
        /* Update voucher used */
        Voucher voucherRoot = null;
        for (Voucher voucherParam : checkout.getVoucherSelectedList()) {
            voucherRoot = this.voucherDao.lockByCode(voucherParam.getCode());
            if (voucherRoot != null) {
                if (voucherRoot.getVoucherNumber() > voucherRoot.getVoucherUsed()) {
                    voucherRoot.setVoucherUsed(voucherRoot.getVoucherUsed() + 1);
                    this.voucherDao.save(voucherRoot);
                } else {
                    throw new HebelaException("voucher.out.of.time");
                }
                orderRoot.setVoucherCode(voucherRoot.getCode());
            } else {
                throw new HebelaException("voucher.missing");
            }
//            VoucherAccountLink voucherAccountLink = new VoucherAccountLink();
//            voucherAccountLink.setVoucherId(checkout.getVoucher().getId());
//            voucherAccountLink.setAccountId(accountId);
//            List<VoucherAccountLink> voucherAccountLinkList = this.voucherAccountLinkDao.findAll(Example.of(voucherAccountLink));
//            this.voucherAccountLinkDao.deleteAll(voucherAccountLinkList);
        }
        List<OrderTransaction> orderTransactionList = new ArrayList<>();
        for (Seller seller : checkout.getSellers()) {
            Order order = orderMap.get(seller.getId());
            orderRoot.getOrderList().add(order);
            order.setCode(OrderUtil.getRandomCode());
            order.setPaymentMethod(orderRoot.getPaymentMethod());
            order.setAccountId(account.getId());
            order.setAccount(account);
            order.setAccountAddress(accountAddress);
            order.setPromotionInfo(BeanUtils.clone(checkout.getCouponInfo(seller)));
            order.setMerchantId(seller.getId());
            order.setSellerInfo(checkout.buildSellerInfo(seller));
            order.setPaymentInfo(payment);
            order.setIsSyncErp(Boolean.FALSE);
            if (Payment.TYPE_ONLINE_BAOKIM.equals(payment.getMethod())) {
                order.setStatus(Order.STATUS_WAIT_FOR_PAY);
                order.setPaymentDeadline(Instant.now().plusSeconds(orderPaymentDeadline));
            } else {
                order.setStatus(Order.STATUS_PENDING);
            }
            order.setShippingFee(seller.getShippingFee());
            order.setPaymentStatus(checkout.getPaymentStatus());
            Long totalSeller = seller.getSubTotal() + seller.getShippingFee() - seller.getCouponDiscount() - seller.getVoucherDiscount();
            if (checkout.getPaymentStatus().equals(Payment.PAYMENT_STATUS_PAID)) {
                order.setAmountPrepay(totalSeller);
            }
            order.setTotal(totalSeller);
            order.setVoucherDiscount(seller.getVoucherDiscount());
            order.setSubTotal(seller.getSubTotal());
            order.setCouponDiscount(seller.getCouponDiscount());
            order.setOrderRootId(dbOrderRoot.getId());
            Order dbOrder = super.doInsert(order);
            order.setSeller(seller);
            /*thao tác trên item**/
            List<OrderItem> orderItems = checkout.getOrderItems(seller);
            if (orderItems.size() == 0) {
                throw new HebelaException("order.product.missing");
            }
            newOrderList.add(dbOrder);
            orderItems.forEach(orderItem -> {
                orderItem.setOrderId(dbOrder.getId());
                orderItem.setAccountId(order.getAccountId());
                if (orderItem.getProduct() != null) {
                    if (!OrderItem.TYPE_PRODUCT_BY_COMBO_PACKAGE.equals(orderItem.getType())) {
                        mapReserveAmount.compute(orderItem.getProductId(), (k, v) -> v == null ? orderItem.getCount() : v + orderItem.getCount());
                    }
                    orderItem.getProduct().setCategory(productCategoryDao.findById(orderItem.getProduct().getProductCategoryId()).orElseGet(null));
                } else {
                    mapReserveComboAmount.compute(orderItem.getComboId(), (k, v) -> v == null ? orderItem.getCount() : v + orderItem.getCount());
                    orderItem.getCombo().setCategory(productCategoryDao.findById(orderItem.getCombo().getProductCategoryId()).orElseGet(null));
                }
            });
            orderTransactionList.add(this.toModel(order, orderRoot, null, Admin.getContextId(), null, OrderTransaction.ORDER_TRANSACTION_TYPE_CREATE, Payment.TYPE_COD));
            dbOrder.setOrderItemList(orderItems);
            OrderItem firstItem = orderItems.get(0);
            String orderImage = firstItem.getProduct() != null ? firstItem.getProduct().getImageThumbnail() : firstItem.getCombo().getImageThumbnail();
            dbOrder.setImageThumbnail(orderImage);
            this.orderItemDao.saveAll(orderItems);
            /*Lưu thông tin sử dụng voucher sàn*/
            if (voucherRoot != null) {
                saveOrderVoucher(voucherRoot, dbOrderRoot, dbOrder);
            }
            /*Lưu thông tin sử dụng voucher shop*/
            if (seller.getVoucher() != null) {
                saveOrderVoucher(seller.getVoucher(), dbOrderRoot, dbOrder);
            }
        }
        List<Integer> couponIdList = checkout.getSellers().stream().flatMap(seller -> seller.getGroup().stream())
                .map(Coupon::getId)
                .filter(id -> !id.equals(0)).collect(Collectors.toList());
        if (couponIdList.size() > 0) {
            List<Coupon> couponList = this.couponDao.lockByIds(couponIdList);
            for (Coupon coupon : couponList) {
                for (Cart cart : coupon.getCartList()) {
                    if (cart.getCount() == 0) {
                        throw new HebelaException("order.item.amount.invalid");
                    }
                }
                if (!Coupon.TYPE_NONE.equals(coupon.getType())) {
                    if (coupon.getCouponNumber() > coupon.getCouponUsed()) {
                        coupon.setCouponUsed(coupon.getCouponUsed() + 1);
                        this.couponDao.save(coupon);
                    } else {
                        throw new HebelaException("coupon.invalid");
                    }
                }
            }
        }
        if (mapReserveAmount.keySet().size() > 0) {
            List<Product> productList = productDao.lockByIds(mapReserveAmount.keySet());
            for (Product product : productList) {
                Integer amount = mapReserveAmount.get(product.getId());
                if (product.getAmount() < amount) {
                    throw new HebelaException("order.error.out.stock");
                }
                product.setAmount(product.getAmount() - amount);
                productDao.save(product);
            }
        }
        if (mapReserveComboAmount.keySet().size() > 0) {
            List<ProductCombo> comboList = productComboDao.lockByIds(mapReserveComboAmount.keySet());
            for (ProductCombo combo : comboList) {
                Integer amount = mapReserveComboAmount.get(combo.getId());
                if (combo.getAmount() < amount) {
                    throw new HebelaException("order.error.combo.out.stock");
                }
                combo.setAmount(combo.getAmount() - amount);
                productComboDao.save(combo);
            }
        }
        this.orderTransactionDao.saveAll(orderTransactionList);
        // publish
        for (Order dbOrder : newOrderList) {
            internalPublisher.send(InternalPublisher.ACTION_CREATE, dbOrder);
        }
        return orderRoot;
    }

    private void saveOrderVoucher(Voucher voucher, OrderRoot orderRoot, Order order) {
        OrderVoucher orderVoucher = new OrderVoucher();
        orderVoucher.setOrderRootId(orderRoot.getId());
        orderVoucher.setOrderId(order.getId());
        orderVoucher.setAccountId(order.getAccountId());
        orderVoucher.setVoucherId(voucher.getId());
        orderVoucher.setVoucherCode(voucher.getCode());
        orderVoucher.setVoucherType(voucher.getVoucherType());
        orderVoucherDao.save(orderVoucher);
    }

    @Override
    public Order doSyncStatus(Order bo) {
        Integer statusDB;
        Order dbBo;
        if (bo.getId() == null) {
            if (bo.getCode() == null) {
                dbBo = orderDao.findDistinctByShippingCodeAndOdooCode(bo.getShippingCode(), bo.getOdooCode());
            } else {
                dbBo = orderDao.findDistinctByCode(bo.getCode());
            }
        } else {
            dbBo = get(bo.getId());
        }
        statusDB = dbBo.getStatus();
        if (bo.getStatus() != null) {
            dbBo.setStatus(bo.getStatus());
        }
        if (bo.getOdooId() != null) {
            dbBo.setOdooId(bo.getOdooId());
            dbBo.setIsSyncErp(bo.getIsSyncErp());
        }
        if (bo.getOdooCode() != null) {
            dbBo.setOdooCode(bo.getOdooCode());
        }
        if (bo.getShippingCode() != null) {
            dbBo.setShippingCode(bo.getShippingCode());
        }
        if (bo.getPaymentStatus() != null) {
            dbBo.setPaymentStatus(bo.getPaymentStatus());
        }
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        // đơn hàng ở trạng thái đã giao tính hoa hồng coin cho đơn hàng
        LoyaltyCoin loyaltyCoin = new LoyaltyCoin();
        if (Order.STATUS_CANCEL.equals(bo.getStatus()) || Order.STATUS_COMPLETED.equals(bo.getStatus())) {
            OrderRoot orderRoot = this.orderRootDao.findById(dbBo.getOrderRootId()).orElse(null);
            List<Order> orderList = this.orderDao.findOrderByOrderRootId(dbBo.getOrderRootId());

            //hoàn thành đơn đã giao
            if (Order.STATUS_COMPLETED.equals(bo.getStatus()) && Order.STATUS_SHIPPING.equals(statusDB)) {
                loyaltyCoin.setOrderStatus(Order.STATUS_COMPLETED);
                loyaltyCoin.setOrderRootId(dbBo.getOrderRootId());
                // trả hàng
            } else if (Order.STATUS_CANCEL.equals(bo.getStatus()) && Order.STATUS_COMPLETED.equals(statusDB)) {
                loyaltyCoin.setOrderStatus(Order.STATUS_ORDER_REFUND);
                //hủy đơn hàng khi chưa giao
            } else if (Order.STATUS_CANCEL.equals(bo.getStatus()) && !Order.STATUS_COMPLETED.equals(statusDB)) {
                loyaltyCoin.setOrderStatus(Order.STATUS_CANCEL);
            }
            loyaltyCoin.setAccountId(dbBo.getAccountId());
            loyaltyCoin.setOrderId(dbBo.getId());
            loyaltyCoin.setOrderCode(dbBo.getCode());
            loyaltyCoin.setOrderValueTotal(orderRoot != null ? orderRoot.getTotal() : null);
            List<OrderExtend> orderExtendList = new ArrayList<>();
            loyaltyCoin.setOrderExtendList(orderExtendList);
            for (Order order : orderList) {
                OrderExtend orderExtend = new OrderExtend();
                orderExtend.setOrderId(order.getId());
                orderExtend.setOrderCode(order.getCode());
                orderExtend.setOrderValue(order.getTotal());
                if (order.getId().equals(dbBo.getId())) {
                    orderExtend.setIsGiveCoinCommission(Boolean.TRUE);
                }
                orderExtendList.add(orderExtend);
            }
            internalPublisher.send(InternalPublisher.ACTION_CREATE, loyaltyCoin);
        }
        return dbBo;
    }

    @Override
    public void doUpdatePayment(OrderRoot bo) {
        HebelaLogger.info("webhook update payment status for MrcOrderId: " + bo.getMrcOrderId());
        if (Payment.PAYMENT_STATUS_PAID.equals(bo.getPaymentStatus())) {
            String orderRootCode = bo.getMrcOrderId().substring(0, 12);
            OrderRoot dbBo = this.orderRootDao.findDistinctByOrderRootCode(orderRootCode);
            if (Payment.PAYMENT_STATUS_UNPAID.equals(dbBo.getPaymentStatus())) {
                List<OrderTransaction> orderTransactionList = new ArrayList<>();
                dbBo.setPaymentStatus(bo.getPaymentStatus());
                List<Order> orderList = this.orderDao.findAll((root, query, builder) -> builder.and(builder.equal(root.get(Order._orderRootId), dbBo.getId())));
                for (Order order : orderList) {
                    order.setPaymentStatus(bo.getPaymentStatus());
                    order.setStatus(Order.STATUS_PENDING);
                    order.setPaymentDeadline(null);
                }
                this.orderDao.saveAll(orderList);
                // publish
                Account account = accountDao.findById(dbBo.getAccountId()).get();
                for (Order order : orderList) {
                    order.setAccount(account);
                    order.setOrderItemList(orderItemDao.findByOrderIdIn(Collections.singletonList(order.getId())));
                    order.getOrderItemList().forEach(orderItem -> {
                        if (orderItem.getProductId() != null) {
                            orderItem.getProduct().setCategory(productCategoryDao.findById(orderItem.getProduct().getProductCategoryId()).orElseGet(null));
                        } else {
                            orderItem.getCombo().setCategory(productCategoryDao.findById(orderItem.getCombo().getProductCategoryId()).orElseGet(null));
                        }
                    });
                    orderTransactionList.add(this.toModel(order, dbBo, bo.getTxnId(), Admin.getContextId(), null, OrderTransaction.ORDER_TRANSACTION_TYPE_PAYMENT, Payment.TYPE_ONLINE_VNPAY));
                    OrderItem firstItem = order.getOrderItemList().get(0);
                    String orderImage = firstItem.getProduct() != null ? firstItem.getProduct().getImageThumbnail() : firstItem.getCombo().getImageThumbnail();
                    order.setImageThumbnail(orderImage);
                    order.setPaymentRef(bo.getMrcOrderId());
                    externalPublisher.send("online-payment-success", order);
                }
                this.orderTransactionDao.saveAll(orderTransactionList);
                dbBo.setOrderIdRef(bo.getOrderIdRef());
                dbBo.setTxnId(bo.getTxnId());
                dbBo.setIsLocked(Boolean.FALSE);
                this.orderRootDao.save(dbBo);
            }
        }
    }

    @Override
    public OrderRoot getOrderToRefund(String orderRootCode, Integer accountId) {
        Optional<OrderRoot> orderRootOptional = orderRootDao.findOne(
                (root, query, builder) ->
                        builder.and(
                                builder.equal(root.get(OrderRoot._accountId), accountId),
                                builder.equal(root.get(OrderRoot._orderRootCode), orderRootCode),
                                builder.equal(root.get(OrderRoot._paymentStatus), Payment.PAYMENT_STATUS_PAID)
                        )
        );
        if (!orderRootOptional.isPresent()) {
            throw new HebelaException("order.code.missing");
        }
        OrderRoot orderRoot = orderRootOptional.get();
        orderRoot.setOrderList(orderDao.findAll((root, query, builder) -> builder.and(builder.equal(root.get(Order._orderRootId), orderRoot.getId()))));
        return orderRoot;
    }

    @Override
    public void doRefund(OrderRoot orderRoot) {
        orderRoot.setPaymentStatus(Payment.PAYMENT_STATUS_REFUND);
        orderRoot.getOrderList().forEach(order -> {
            order.setPaymentStatus(Payment.PAYMENT_STATUS_REFUND);
            order.setStatus(Order.STATUS_CANCEL);
            orderDao.save(order);
        });
        orderRootDao.save(orderRoot);
    }

    @Override
    public StatusInfo statusInfo(Integer accountId) {
        SessionFactory sessionFactory = ((Session) entityManager.getDelegate()).getSessionFactory();
        ClassMetadata classMetadata = sessionFactory.getClassMetadata(persistentClass);
        SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) sessionFactory;
        AbstractEntityPersister entityPersister = (AbstractEntityPersister) sessionFactoryImpl.getEntityPersister(classMetadata.getEntityName());
        StatusInfo statusInfo = new StatusInfo();
        Query query = entityManager.createNativeQuery(
                "SELECT x.type, x.label, x.description, x.value, IFNULL(y.count, 0) AS count\n" +
                        "FROM TBL_OBJECT_CATEGORY x\n" +
                        "         LEFT JOIN (SELECT a.status, COUNT(*) AS count\n" +
                        "               FROM " + entityPersister.getTableName() + " a\n" +
                        "               WHERE a.is_trash = 0 AND a.ACCOUNT_ID = ?1 \n" +
                        "               GROUP BY a.status) y ON x.value = y.status\n" +
                        "WHERE x.type = ?2"
        );
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        query.setParameter(1, accountId);
        query.setParameter(2, persistentClass.getSimpleName());
        statusInfo.setStatusCount(query.getResultList());
        return statusInfo;
    }

    @Override
    public StatusInfo merchantStatusInfo() {
        StatusInfo statusInfo = new StatusInfo();
        String trashString = "SELECT x.type, x.label, x.description, x.value, IFNULL(y.count, 0) AS count\n" +
                "FROM TBL_OBJECT_CATEGORY x LEFT JOIN (SELECT a.is_trash, a.merchant_id, COUNT(*) AS count FROM TBL_ORDER a WHERE a.merchant_id = " + getOrgId() + " GROUP BY a.is_trash ) y ON x.value = y.is_trash\n" +
                "WHERE x.type = 'Trash'  AND (y.merchant_id = " + getOrgId() + " OR y.MERCHANT_ID is null)  and x.property = 'isTrash'";
        Query trashQuery = entityManager.createNativeQuery(trashString);
        trashQuery.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        statusInfo.setTrashCount(trashQuery.getResultList());
        String statusString = "SELECT x.type, x.label, x.description, x.value, IFNULL(y.count, 0) AS count\n" +
                "FROM TBL_OBJECT_CATEGORY x\n" +
                "         LEFT JOIN (SELECT a.status, a.merchant_id, COUNT(*) AS count FROM TBL_ORDER a WHERE a.is_trash = 0 GROUP BY a.merchant_id, a.status) y\n" +
                "                   ON x.value = y.status\n" +
                "WHERE x.type = 'Order'\n" +
                "  AND (y.merchant_id = " + getOrgId() + " OR y.MERCHANT_ID IS NULL)\n" +
                "  AND x.property = 'status'";
        Query statusQuery = entityManager.createNativeQuery(statusString);
        statusQuery.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        statusInfo.setStatusCount(statusQuery.getResultList());
        return statusInfo;
    }

    @Override
    public OrderRoot doMerchantUpdateStatus(MerchantOrderUpdateRequest model) {
        if (model.getCode() == null) {
            throw new HebelaException("order.code.missing");
        }
        Order order = new Order();
        order.setIsTrash(false);
        order.setCode(model.getCode());
        Optional<Order> optional = orderDao.findOne(Example.of(order));
        if (!optional.isPresent()) {
            throw new HebelaException("order.code.missing");
        }
        Order dbOrder = optional.get();
        if (Order.STATUS_WAIT_FOR_PAY.equals(dbOrder.getStatus())) {
            if (Order.STATUS_CANCEL.equals(model.getStatus())) {
                dbOrder.setStatus(model.getStatus());
                orderDao.save(dbOrder);
                internalPublisher.send(InternalPublisher.ACTION_CANCEL, dbOrder);
            }
        }
        if (Order.STATUS_PENDING.equals(dbOrder.getStatus())) {
            if (Order.STATUS_PACKAGED.equals(model.getStatus())
                    || (Order.STATUS_CANCEL.equals(model.getStatus()) && Payment.TYPE_COD.equals(dbOrder.getPaymentMethod()))) {
                dbOrder.setStatus(model.getStatus());
                orderDao.save(dbOrder);
            }
            if (Order.STATUS_CANCEL.equals(model.getStatus()) && Payment.TYPE_ONLINE_BAOKIM.equals(dbOrder.getPaymentMethod())) {
                Optional<OrderRoot> orderRootOptional = orderRootDao.findById(dbOrder.getOrderRootId());
                if (!orderRootOptional.isPresent()) {
                    throw new HebelaException("order.code.missing");
                }
                OrderRoot orderRoot = orderRootOptional.get();
                if (orderRoot.getIsLocked()) {
                    throw new HebelaException("order.locked");
                }
                if (Payment.PAYMENT_STATUS_PAID.equals(dbOrder.getPaymentStatus())) {
                    return orderRoot;
                } else {
                    dbOrder.setStatus(model.getStatus());
                    orderDao.save(dbOrder);
                }
            }
        }
        if (Order.STATUS_PACKAGED.equals(dbOrder.getStatus())) {
            if (Order.STATUS_SHIPPING.equals(model.getStatus())) {
                dbOrder.setStatus(model.getStatus());
                orderDao.save(dbOrder);
            }
        }
        if (Order.STATUS_SHIPPING.equals(dbOrder.getStatus())) {
            if (Order.STATUS_COMPLETED.equals(model.getStatus())) {
                dbOrder.setStatus(model.getStatus());
                orderDao.save(dbOrder);
            }
        }
        return null;
    }

    // Tìm tất cả order của các shop trong hệ thống
    @Override
    public Page<Order> findMerchantOrder(AdminOrderFilterRequest model) {
        Instant fromDate = model.getFromDate();
        Instant toDate = model.getToDate();
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize(), Sort.Direction.DESC, Order._createdDate);
        return orderDao.findAll((root, query, builder) -> {
            query.distinct(true);
            Root<AccountAddress> accountAddressRoot = query.from(AccountAddress.class);
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Order._accountAddressId), accountAddressRoot.get(AccountAddress._id)));
            predicates.add(builder.equal(root.get(Order._merchantId), model.getMerchantId()));
            if (model.getStatus() != null) {
                predicates.add(builder.equal(root.get(Order._status), model.getStatus()));
            } else {
                // Hebela Trading MerchantId = 1
                if (model.getMerchantId() != 1) {
                    predicates.add(builder.notEqual(root.get(Order._status), Order.STATUS_WAIT_FOR_PAY));
                }
            }
            if (toDate != null) {
                predicates.add(builder.lessThanOrEqualTo(root.get(Order._createdDate), toDate.plus(1, ChronoUnit.DAYS)));
            }
            if (fromDate != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Order._createdDate), fromDate));
            }
            if (model.getPaymentStatus() != null) {
                predicates.add(builder.equal(root.get(Order._paymentStatus), model.getPaymentStatus()));
            }
            if (model.getKeyWord() != null) {
                StringBuilder term = new StringBuilder();
                term.append("%").append(model.getKeyWord()).append("%");
                predicates.add(builder.or(
                        builder.like(root.get(Order._code), term.toString()),
                        builder.like(accountAddressRoot.get(AccountAddress._mobile), term.toString()),
                        builder.like(accountAddressRoot.get(AccountAddress._fullname), term.toString())
                ));
            }
            query.orderBy(builder.desc(root.get(Order._createdDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public Order getByCode(String code) {
        return orderDao.getOrderByCode(code);
    }

    @Override
    public Order doUpdateRefundSuccess(Order order, OrderRoot orderRoot) {
        this.orderDao.save(order);
        this.orderRootDao.save(orderRoot);
        externalPublisher.send("update", order);
        return order;
    }

    @Override
    public Long countOrderByOrderRoot(Integer orderRootId) {
        Order order = new Order();
        order.setOrderRootId(orderRootId);
        return this.orderDao.count(Example.of(order));
    }

    // Tìm tất cả order tạo bởi tài khoản sàn Hebela
    @Override
    public Page<Order> findHebelaOrder(AdminOrderFilterRequest model) {
        Instant fromDate = model.getFromDate();
        Instant toDate = model.getToDate();
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize(), Sort.Direction.DESC, Order._createdDate);
        return orderDao.findAll((root, query, builder) -> {
            query.distinct(true);
            Root<AccountAddress> accountAddressRoot = query.from(AccountAddress.class);
            Root<OrderRoot> orderRoot = query.from(OrderRoot.class);
            query.multiselect(root, orderRoot);
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Order._accountAddressId), accountAddressRoot.get(AccountAddress._id)));
            predicates.add(builder.equal(root.get(Order._orderRootId), orderRoot.get(OrderRoot._id)));
            if (model.getSaleChannel() != null) {
                predicates.add(builder.equal(root.get(Order._saleChannel), model.getSaleChannel()));
            }
            if (model.getMerchantId() != null) {
                predicates.add(builder.equal(root.get(Order._merchantId), model.getMerchantId()));
            }
            if (model.getPaymentStatus() != null) {
                predicates.add(builder.equal(root.get(Order._paymentStatus), model.getPaymentStatus()));
            }
            if (model.getStatusList() != null && model.getStatusList().size() > 0) {
                predicates.add(root.get(Order._status).in(model.getStatusList()));
            }
            if (toDate != null) {
                predicates.add(builder.lessThanOrEqualTo(root.get(Order._createdDate), toDate.plus(1, ChronoUnit.DAYS)));
            }
            if (fromDate != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Order._createdDate), fromDate));
            }
            if (model.getKeyWord() != null) {
                StringBuilder term = new StringBuilder();
                term.append("%").append(model.getKeyWord()).append("%");
                predicates.add(builder.or(
                        builder.like(root.get(Order._code), term.toString()),
                        builder.like(accountAddressRoot.get(AccountAddress._mobile), term.toString()),
                        builder.like(accountAddressRoot.get(AccountAddress._fullname), term.toString())
                ));
            }
            query.orderBy(builder.desc(root.get(Order._createdDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public Order getByCode(String code, Integer accountId) {
        if (code == null) {
            throw new HebelaException("order.code.missing");
        }
        Order order = new Order();
        order.setAccountId(accountId);
        order.setIsTrash(false);
        order.setCode(code);
        Optional<Order> optional = orderDao.findOne(Example.of(order));
        if (!optional.isPresent()) {
            throw new HebelaException("order.code.missing");
        }
        order = optional.get();
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderId(order.getId());
        order.buildSeller(orderItemDao.findAll(Example.of(orderItem)));
        return order;
    }

    @Override
    public Order doCancel(CancelOrderRequest model, Integer accountId) {
        if (model.getCode() == null) {
            throw new HebelaException("order.code.missing");
        }
        if (accountId == null) {
            throw new HebelaException("order.user.missing");
        }
        Order dbOrder = new Order();
        dbOrder.setAccountId(accountId);
        dbOrder.setIsTrash(false);
        dbOrder.setCode(model.getCode());
        Optional<Order> optional = orderDao.findOne(Example.of(dbOrder));
        if (!optional.isPresent()) {
            throw new HebelaException("order.obj.missing");
        }
        dbOrder = optional.get();
        if (Payment.TYPE_ONLINE_BAOKIM.equals(dbOrder.getPaymentMethod())
                && (Order.STATUS_WAIT_FOR_PAY & dbOrder.getStatus()) == 0) {
            throw new HebelaException("order.obj.not.cancel");
        } else if (Payment.TYPE_COD.equals(dbOrder.getPaymentMethod()) &&
                (Order.STATUS_PENDING & dbOrder.getStatus()) == 0) {
            throw new HebelaException("order.obj.not.cancel");
        }
        Boolean pushToOdoo = Boolean.TRUE;
        if (Order.STATUS_WAIT_FOR_PAY.equals(dbOrder.getStatus())) {
            pushToOdoo = Boolean.FALSE;
        }
        dbOrder.setStatus(Order.STATUS_CANCEL);
        dbOrder.setCancelReason(model.getCancelReason());
        dbOrder.setModifiedBy(accountId);
        orderDao.save(dbOrder);
        if (pushToOdoo) {
            externalPublisher.send("cancel", dbOrder);
        }
        return dbOrder;
    }

    @Override
    public ShopViewOrderResponse detail(String code) {
        if (code == null) {
            throw new HebelaException("order.code.missing");
        }
        Order order = new Order();
        order.setIsTrash(false);
        order.setCode(code);
        Optional<Order> optional = orderDao.findOne(Example.of(order));
        if (!optional.isPresent()) {
            throw new HebelaException("order.code.missing");
        }
        order = optional.get();
        final Integer orderRootId = order.getOrderRootId();
        Long countOrder = this.orderDao.count(((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Order._orderRootId), orderRootId));
            return builder.and(predicates.toArray(new Predicate[0]));
        }));

        if (countOrder > 1) {
            order.setIsOrderUnion(Boolean.TRUE);
        }
        OrderItem orderItemExamp = new OrderItem();
        orderItemExamp.setOrderId(order.getId());
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderId(order.getId());
        if (order.getOrderRootId() != null) {
            OrderRoot orderRoot = orderRootDao.findById(order.getOrderRootId()).orElse(null);
            if (orderRoot != null) {
                order.setVoucherCode(orderRoot.getVoucherCode());
            }
        }
        Account account = this.accountDao.getAccountById(order.getAccountId());
        if (account != null) {
            order.setAccountResponse(OrderAccountResponse.toResponse(account));
        }
        List<OrderItem> orderItemList = orderItemDao.findAll(Example.of(orderItem));
//        order.getSeller().setGroup(BeanUtils.clone(order.getPromotionInfo()));

//        for (OrderItem orderItem : orderItemList) {
//            if (OrderItem.TYPE_PRODUCT.equals(orderItem.getType()) || OrderItem.TYPE_COMBO.equals(orderItem.getType())) {
//                order.getSeller().getGroup().get(orderItem.getCouponIndex()).getCartList().add(orderItem.toCart());
//            }
//            if (OrderItem.TYPE_PRODUCT_BY_COMBO.equals(orderItem.getType())) {
//                order.getSeller().getGroup().get(orderItem.getCouponIndex()).getCartList()
//                        .get(orderItem.getCartIndex()).getProductCombo().getProductList().add(orderItem.toProduct());
//            }
//            if (OrderItem.TYPE_COUPON_GIFT.equals(orderItem.getType())) {
//                order.getSeller().getGroup().get(orderItem.getCouponIndex()).getGiftList().add(orderItem.getProduct());
//            }
//            if (OrderItem.TYPE_GIFT.equals(orderItem.getType())) {
//                Product product = order.getSeller().getGroup().get(orderItem.getCouponIndex()).getCartList()
//                        .get(orderItem.getCartIndex()).getProduct();
//                if (product != null) {
//                    product.getGiftList().add(orderItem.toProductGift());
//                } else {
//                    order.getSeller().getGroup().get(orderItem.getCouponIndex()).getCartList()
//                            .get(orderItem.getCartIndex()).getProductCombo().getGiftList().add(orderItem.toProductGift());
//                }
//            }
//        }
        if (order.getOrderOldId() != null) {
            Order orderOld = orderDao.findById(order.getOrderOldId()).orElse(null);
            if (orderOld == null) {
                throw new HebelaException("Không tìm thấy thông tin đơn hàng gốc");
            }
            order.setCodeOrderOld(orderOld.getCode());
        }
        return ShopViewOrderResponse.toResponse(order, orderItemList);
    }

    private void validateCloneOrder(Order orderOld, Long amountPrepay) {
        if (orderOld.getOrderOldId() != null) {
            throw new HebelaException("order.clone.invalid");
        }
        if (!Order.STATUS_CANCEL.equals(orderOld.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        if (orderOld.getPaymentMethod() == null) {
            throw new HebelaException("order.payment.method.missing");
        }
        if (amountPrepay != null && amountPrepay > orderOld.getTotal()) {
            throw new HebelaException("order.amountPrepay.invalid");
        }
        List<Order> orderList = orderDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(Order._orderOldId), orderOld.getId()),
                builder.notEqual(root.get(Order._status), Order.STATUS_CANCEL)
        ));
        if (orderList.size() > 0) {
            String[] error = new String[1];
            error[0] = orderList.get(0).getCode();
            throw new HebelaException("order.clone.exist", error);
        }
    }

    @Override
    public OrderRootResponse doCloneOrder(Order bo) {
        if (bo.getCode() == null) {
            throw new HebelaException("order.code.missing");
        }
        if (!User.getContext().getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
            throw new HebelaException("action.invalid");
        }
        Order orderOld = new Order();
        orderOld.setIsTrash(false);
        orderOld.setCode(bo.getCode());
        Optional<Order> optional = orderDao.findOne(Example.of(orderOld));
        if (!optional.isPresent()) {
            throw new HebelaException("order.code.missing");
        }
        orderOld = optional.get();
        this.validateCloneOrder(orderOld, bo.getAmountPrepay());
        OrderItem orderItemExample = new OrderItem();
        orderItemExample.setOrderId(orderOld.getId());
        List<OrderItem> orderItemOldList = orderItemDao.findAll(Example.of(orderItemExample));
        OrderRoot orderRootOld = orderRootDao.findById(orderOld.getOrderRootId()).orElse(null);
        if (orderRootOld == null) {
            throw new HebelaException("order.root.missing");
        }
        AccountAddress accountAddress = orderOld.getAccountAddress();
        Payment payment = this.paymentDao.findFirstByMethod(orderOld.getPaymentMethod());
        Integer accountId = orderOld.getAccountId();
        Map<Integer, Integer> mapReserveAmount = new HashMap<>();
        OrderRoot orderRoot = new OrderRoot();
        orderRoot.setAccountId(accountId);
        orderRoot.setOrderRootCode(OrderUtil.getRandomCode());
        orderRoot.setSubTotal(orderOld.getSubTotal());
        orderRoot.setCouponDiscount(orderOld.getCouponDiscount());
        orderRoot.setVoucherDiscount(orderOld.getVoucherDiscount());
        orderRoot.setTotal(orderOld.getTotal());
        orderRoot.setShippingFee(orderOld.getShippingFee());
        //check lại
        orderRoot.setVoucherCode(orderRootOld.getVoucherCode());
        orderRoot.setPaymentStatus(orderOld.getPaymentStatus());
        orderRoot.setPaymentMethod(payment.getMethod());
        if (Payment.TYPE_ONLINE_BAOKIM.equals(payment.getMethod())) {
            Order orderCount = new Order();
            orderCount.setAccountId(accountId);
            orderCount.setPaymentMethod(Payment.TYPE_ONLINE_BAOKIM);
            orderCount.setStatus(Order.STATUS_WAIT_FOR_PAY);
            orderCount.setPaymentStatus(Payment.PAYMENT_STATUS_UNPAID);
            long count = this.orderDao.count(Example.of(orderCount));
            if (count > maxOnlinePaymentOrder) {
                throw new HebelaException("order.payment.max.invalid");
            }
            // check lại
            orderRoot.setMrcOrderId(orderRoot.getOrderRootCode() + "1");
        }
        OrderRoot orderRootDb = this.orderRootDao.save(orderRoot);
        // Tạo order mới
        Order orderNew = new Order();
        orderRoot.getOrderList().add(orderNew);
        orderNew.setCode(OrderUtil.getRandomCode());
        orderNew.setAccountId(accountId);
        orderNew.setOrderRootId(orderRootDb.getId());
        orderNew.setOrderOldId(orderOld.getId());
        orderNew.setSaleChannel(Order.SALE_CHANNEL_ADMIN);
        orderNew.setPaymentInfo(payment);
        orderNew.setPromotionInfo(orderOld.getPromotionInfo());
        orderNew.setMerchantId(orderOld.getMerchantId());
        orderNew.setSellerInfo(orderOld.getSellerInfo());
        orderNew.setSubTotal(orderOld.getSubTotal());
        orderNew.setCouponDiscount(orderOld.getCouponDiscount());
        orderNew.setVoucherDiscount(orderOld.getVoucherDiscount());
        orderNew.setShippingFee(orderOld.getShippingFee());
        orderNew.setShippingCode(orderOld.getShippingCode());
        orderNew.setTotal(orderOld.getTotal());
        orderNew.setAccountAddressId(orderOld.getAccountAddressId());
        orderNew.setAccountAddress(accountAddress);
        orderNew.setNote(orderOld.getNote());
        orderNew.setDescription(bo.getDescription());
        orderNew.setPaymentMethod(orderRoot.getPaymentMethod());
        orderNew.setPaymentStatus(orderOld.getPaymentStatus());
        orderNew.setIsSyncErp(Boolean.FALSE);
        if (bo.getAmountPrepay() != null) {
            orderNew.setAmountPrepay(bo.getAmountPrepay());
            if (orderNew.getTotal().equals(orderNew.getAmountPrepay())) {
                orderNew.setPaymentStatus(Payment.PAYMENT_STATUS_PAID);
            }
        }
//        orderNew.setAccount(account);
        if (Payment.TYPE_ONLINE_BAOKIM.equals(payment.getMethod())) {
            orderNew.setStatus(Order.STATUS_WAIT_FOR_PAY);
            orderNew.setPaymentDeadline(Instant.now().plusSeconds(orderPaymentDeadline));
        } else {
            orderNew.setStatus(Order.STATUS_PENDING);
        }
        Order dbOrder = super.doInsert(orderNew);
        // check lại
//        orderNew.setSeller(orderOld.getSeller());

        // Tạo orderItem mới
        List<OrderItem> orderItemNews = new ArrayList<>();
        if (orderItemOldList.size() == 0) {
            throw new HebelaException("order.product.missing");
        }
        for (OrderItem itemOld : orderItemOldList) {
            OrderItem itemNew = new OrderItem();
            itemNew.setOrderId(dbOrder.getId());
            itemNew.setAccountId(itemOld.getAccountId());
            itemNew.setCartId(itemOld.getCartId());
            itemNew.setType(itemOld.getType());
            itemNew.setCouponIndex(itemOld.getCouponIndex());
            itemNew.setCartIndex(itemOld.getCartIndex());
            itemNew.setProductId(itemOld.getProductId());
            itemNew.setCouponId(itemOld.getCouponId());
            itemNew.setComboId(itemOld.getComboId());
            itemNew.setPrice(itemOld.getPrice());
            itemNew.setCount(itemOld.getCount());
            itemNew.setCreatedBy(User.getContextId());
            if (itemOld.getProduct() != null) {
                itemNew.setProduct(itemOld.getProduct());
                mapReserveAmount.compute(itemNew.getProductId(), (k, v) -> v == null ? itemNew.getCount() : v + itemNew.getCount());
            } else {
                itemNew.setCombo(itemOld.getCombo());
                for (Product product : itemNew.getCombo().getProductList()) {
                    mapReserveAmount.compute(product.getId(), (k, v) -> v == null ? itemNew.getCount() : v + itemNew.getCount());
                }
            }
            orderItemNews.add(itemNew);
        }
        dbOrder.setOrderItemList(orderItemNews);
        OrderItem firstItem = orderItemNews.get(0);
        String orderImage = firstItem.getProduct() != null ? firstItem.getProduct().getImageThumbnail() : firstItem.getCombo().getImageThumbnail();
        dbOrder.setImageThumbnail(orderImage);
        this.orderItemDao.saveAll(orderItemNews);

//        List<Integer> couponIdList = checkout.getSellers().stream().flatMap(seller -> seller.getGroup().stream())
//                .map(Coupon::getId)
//                .filter(id -> !id.equals(0)).collect(Collectors.toList());
//        if (couponIdList.size() > 0) {
//            List<Coupon> couponList = this.couponDao.lockByIds(couponIdList);
//            for (Coupon coupon : couponList) {
//                for (Cart cart : coupon.getCartList()) {
//                    if (cart.getCount() == 0) {
//                        throw new HebelaException("order.item.amount.invalid");
//                    }
//                }
////                if (!Coupon.TYPE_NONE.equals(coupon.getType())) {
////                    if (coupon.getCouponNumber() > coupon.getCouponUsed()) {
////                        coupon.setCouponUsed(coupon.getCouponUsed() + 1);
////                        this.couponDao.save(coupon);
////                    } else {
////                        throw new HebelaException("coupon.invalid");
////                    }
////                }
//            }
//        }

        OrderTransaction orderTransaction = this.toModel(dbOrder, orderRoot, null, Admin.getContextId(), Admin.getContextId(), OrderTransaction.ORDER_TRANSACTION_TYPE_CREATE, Payment.TYPE_COD);
        List<Product> productList = productDao.lockByIds(mapReserveAmount.keySet());
        for (Product product : productList) {
            Integer amount = mapReserveAmount.get(product.getId());
            if (product.getAmount() < amount) {
                throw new HebelaException("order.error.out.stock");
            }
            product.setAmount(product.getAmount() - amount);
            productDao.save(product);
        }
        this.orderTransactionDao.save(orderTransaction);
        // publish
        dbOrder.setAccount(accountDao.findById(accountId).get());
        internalPublisher.send(InternalPublisher.ACTION_CREATE, dbOrder);
        return OrderRootResponse.toResponse(orderRoot, null);
    }

    @Override
    public Order doRefundDone(String orderCode) {
        if (orderCode == null) {
            throw new HebelaException("action.invalid");
        }
        Order orderDB = this.orderDao.getOrderByCode(orderCode);
        if (orderDB == null) {
            throw new HebelaException("action.invalid");
        }
        orderDB.setPaymentStatus(Payment.PAYMENT_STATUS_REFUND);
        return this.orderDao.save(orderDB);
    }

    @Override
    public List<Account> findAccount(String keyword) {
        return this.accountDao.findAll((root, query, builder) -> builder.or(
                builder.like(root.get(Account._mobile), "%" + keyword + "%"),
                builder.like(root.get(Account._fullname), "%" + keyword + "%"),
                builder.like(root.get(Account._email), "%" + keyword + "%")
        ));
    }

    private OrderTransaction toModel(Order order, OrderRoot orderRoot, Long txnId, Integer userIdCreate, Integer userIdExecution, Integer type, Integer paymentMethod) {
        OrderTransaction orderTransaction = new OrderTransaction();
        orderTransaction.setOrderCode(order.getCode());
        orderTransaction.setOrderRootCode(orderRoot.getOrderRootCode());
        orderTransaction.setAmount(orderRoot.getTotal());
        orderTransaction.setRefCode(paymentMethod.equals(Payment.TYPE_COD) ? orderRoot.getOrderRootCode() : String.valueOf(txnId));
        orderTransaction.setType(type);
        orderTransaction.setCreatedBy(userIdCreate);
        orderTransaction.setExecutionTime(Instant.now());
        orderTransaction.setExecutorId(userIdExecution);
        orderTransaction.setCreatedDate(Instant.now());
        orderTransaction.setPaymentMethod(paymentMethod);
        return orderTransaction;
    }

    @Override
    public List<Order> publish(List<String> orderCodeList) {
        if (orderCodeList == null || orderCodeList.size() == 0) {
            throw new HebelaException("order.code.missing");
        }
        List<Order> orderList = new ArrayList<>();
        for (String orderCode : orderCodeList) {
            orderList.add(publish(orderCode));
        }
        return orderList;
    }

    @Override
    public Order publish(String orderCode) {
        Order orderOld = new Order();
        orderOld.setIsTrash(false);
        orderOld.setCode(orderCode);
        orderOld = orderDao.findOne(Example.of(orderOld)).orElse(null);
        if (orderOld == null) {
            throw new HebelaException("order.code.missing");
        }
        OrderItem orderItemExample = new OrderItem();
        orderItemExample.setOrderId(orderOld.getId());
        List<OrderItem> orderItemOldList = orderItemDao.findAll(Example.of(orderItemExample));
        orderOld.setOrderItemList(orderItemOldList);
        OrderItem firstItem = orderItemOldList.get(0);
        String orderImage = firstItem.getProduct() != null ? firstItem.getProduct().getImageThumbnail() : firstItem.getCombo().getImageThumbnail();
        orderOld.setImageThumbnail(orderImage);
        // publish
        orderOld.setAccount(accountDao.findById(orderOld.getAccountId()).get());
        orderOld.setAccountAddress(accountAddressDao.findById(orderOld.getAccountAddressId()).get());
        externalPublisher.send(ExternalPublisher.ACTION_CREATE, orderOld);
        return orderOld;
    }
}

