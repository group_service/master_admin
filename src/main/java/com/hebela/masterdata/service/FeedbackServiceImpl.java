package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Feedback;
import com.hebela.masterdata.bo.FeedbackGallery;
import com.hebela.masterdata.dao.FeedbackDao;
import com.hebela.masterdata.dao.FeedbackGalleryDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackServiceImpl extends AbstractService<Feedback, Integer> implements FeedbackService {

    private final FeedbackGalleryDao feedbackGalleryDao;
    private final FeedbackDao feedbackDao;

    public FeedbackServiceImpl(FeedbackGalleryDao feedbackGalleryDao, FeedbackDao feedbackDao) {
        this.feedbackGalleryDao = feedbackGalleryDao;
        this.feedbackDao = feedbackDao;
    }


    @Override
    public Feedback doInsert(Feedback bo) {
        bo.setAccountId(User.getContextId());
        super.doInsert(bo);
        saveFeedbackGallery(bo);
        return bo;
    }

    private void saveFeedbackGallery(Feedback bo) {
        List<String> imageUrlList = bo.getImageUrlList();
        if (imageUrlList != null) {
            for (String image : imageUrlList) {
                FeedbackGallery feedbackGallery = new FeedbackGallery();
                feedbackGallery.setFeedbackId(bo.getId());
                feedbackGallery.setImage(image);
                feedbackGallery.setCreatedBy(User.getContextId());
                feedbackGalleryDao.save(feedbackGallery);
            }
        }
    }

    @Override
    public Page<Feedback> findFeedBack(Feedback obj, Pageable pageable) {
        return feedbackDao.findPage(pageable);
    }
}

