package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Publisher;
import com.hebela.masterdata.obj.PublisherReportOverview;
import com.hebela.masterdata.web.publisher.PublisherSearchRequest;
import org.springframework.data.domain.Page;

public interface PublisherService extends StatusService<Publisher> {
    Page<Publisher> findPublisher(PublisherSearchRequest model);

    PublisherReportOverview getReportOverview(PublisherSearchRequest model);
}

