package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.ProductFaq;

import java.util.List;

public interface ProductFaqService extends StatusService<ProductFaq> {
    ProductFaq doAddProduct(ProductFaq bo);

    ProductFaq doRemoveProduct(ProductFaq bo);

    ProductFaq doAddCombo(ProductFaq bo);

    ProductFaq doRemoveCombo(ProductFaq bo);
}

