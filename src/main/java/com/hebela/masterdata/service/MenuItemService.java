package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.MenuItem;
import com.hebela.util.HierarchyTree;

import java.util.List;

public interface MenuItemService extends StatusService<MenuItem> {
    List<MenuItem> findTreeFlat(MenuItem bo);
}
