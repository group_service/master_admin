package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Banner;
import com.hebela.masterdata.bo.BannerItem;
import com.hebela.masterdata.dao.BannerItemDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BannerServiceImpl extends AbstractStatusService<Banner> implements BannerService {
    @Autowired
    BannerItemDao bannerItemDao;

    @Override
    public Banner doInsert(Banner bo) {
        validateData(bo);
        return super.doInsert(bo);
    }

    @Override
    public Banner doUpdateStatus(Banner bo) {
        validateData(bo);
        return super.doUpdateStatus(bo);
    }

    @Override
    public Banner doTrash(Banner bo) {
        super.doTrash(bo);
        setIsTrashForBannerItem(bo.getId());
        return bo;
    }

    /*
    huỷ banner => huỷ toàn bộ banner item gắn với banner đó
    */
    private void setIsTrashForBannerItem(Integer bannerId) {
        List<BannerItem> bannerItemList = bannerItemDao.findAll().stream()
                .filter(bannerItem -> bannerId.equals(bannerItem.getBannerId())).collect(Collectors.toList());
        for (BannerItem bannerItem : bannerItemList) {
            bannerItem.setIsTrash(Boolean.TRUE);
            bannerItem.setModifiedBy(User.getContextId());
            bannerItem.setModifiedDate(Instant.now());
            bannerItemDao.save(bannerItem);
        }
    }

    private void validateData(Banner bo) {
        if (StringUtils.isEmpty(bo.getName()) || StringUtils.isEmpty(bo.getType())) {
            throw new HebelaException("action.invalid");
        }
    }
}
