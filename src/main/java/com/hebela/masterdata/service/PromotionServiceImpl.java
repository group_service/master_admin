package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.bo.extend.PlatformExtend;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.web.promotion.PromotionSearchRequest;
import com.hebela.masterdata.web.promotionTimeFrame.PromotionTimeFrameRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class PromotionServiceImpl extends AbstractStatusService<Promotion> implements PromotionService {
    @Autowired
    PromotionDao promotionDao;
    @Autowired
    PromotionTimeFrameDao promotionTimeFrameDao;
    @Autowired
    PromotionCategoryDao promotionCategoryDao;
    @Autowired
    PromotionConfigDao promotionConfigDao;
    @Autowired
    PromotionProductDao promotionProductDao;

    @Override
    public Promotion get(Integer key) {
        return super.get(key);
    }

    @Override
    public Promotion doInsert(Promotion bo) {
        this.validateInput(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        bo.setStatus(bo.getStatus() == null ? Promotion.STATUS_INACTIVE : bo.getStatus());
        return super.doInsert(bo);
    }

    @Override
    public Promotion doUpdate(Promotion bo) {
        this.validateInput(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        Promotion dbBo = super.get(bo.getId());
        this.validatePromotion(dbBo);
        if (dbBo.getErrorList().size() > 0) {
            return dbBo;
        }
        bo.setStatus(dbBo.getStatus());
        bo.setIsTrash(dbBo.getIsTrash());

        // update lại nền tảng nếu promotion giảm bớt nền tảng áp dụng
        if (bo.getPlatform() < dbBo.getPlatform()) {
            this.updatePlatformTimeFrame(dbBo.getId(), bo.getPlatform());
            this.updatePlatformCategory(dbBo.getId(), bo.getPlatform());
            this.updatePlatformPromotionProduct(dbBo.getId(), bo.getPlatform());
        }

        return super.doUpdate(bo);
    }

    public void updatePlatformTimeFrame(Integer promotionId, Integer platformRemove) {
        List<PromotionTimeFrame> timeFrames = this.promotionTimeFrameDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionTimeFrame._promotionId), promotionId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        // tìm nền tảng nào bị loại
        for (PromotionTimeFrame dbBo : timeFrames) {
            for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                if ((platformRemove & platform) <= 0) {
                    if ((dbBo.getPlatform() & platform) > 0) {
                        dbBo.setPlatform(dbBo.getPlatform() - platform);
                    }
                }
            }
            dbBo.setModifiedBy(User.getContextId());
            this.promotionTimeFrameDao.save(dbBo);
        }
    }

    public void updatePlatformCategory(Integer promotionId, Integer platformRemove) {
        List<PromotionCategory> categories = this.promotionCategoryDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionCategory._promotionId), promotionId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        // tìm nền tảng nào bị loại
        for (PromotionCategory dbBo : categories) {
            for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                if ((platformRemove & platform) <= 0) {
                    if ((dbBo.getPlatform() & platform) > 0) {
                        dbBo.setPlatform(dbBo.getPlatform() - platform);
                    }
                }
            }
            dbBo.setModifiedBy(User.getContextId());
            this.promotionCategoryDao.save(dbBo);
        }
    }


    public void updatePlatformPromotionProduct(Integer promotionId, Integer platformRemove) {
        List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionProduct._promotionId), promotionId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        // tìm nền tảng nào bị loại
        for (PromotionProduct dbBo : promotionProducts) {
            for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                if ((platformRemove & platform) <= 0) {
                    if ((dbBo.getPlatform() & platform) > 0) {
                        dbBo.setPlatform(dbBo.getPlatform() - platform);
                    }
                }
            }
            dbBo.setModifiedBy(User.getContextId());
            this.promotionProductDao.save(dbBo);
        }
    }

    @Override
    public Promotion doTrash(Promotion bo) {
        this.validatePromotionProduct(bo);
        if (bo.getErrorList().size() > 0) {
            throw new HebelaException(bo.getErrorList().get(0).getMessage());
        }
        this.deactivateTimeFrames(bo.getId());
        this.deactivateCategories(bo.getId());
        return super.doTrash(bo);
    }

    @Override
    public Promotion doActivate(Promotion bo) {
        Promotion dbBo = super.get(bo.getId());
        if (dbBo.getStatus().equals(Promotion.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Promotion.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionDao.save(dbBo);
        return dbBo;
    }

    @Override
    public Promotion doDeactivate(Promotion bo) {
        Promotion dbBo = super.get(bo.getId());
        if (!dbBo.getStatus().equals(Promotion.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Promotion.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionDao.save(dbBo);
        this.deactivateTimeFrames(dbBo.getId());
        this.deactivateCategories(dbBo.getId());
        this.deactivatePromotionProduct(dbBo.getId());
        return dbBo;
    }

    public void deactivateTimeFrames(Integer promotionId) {
        List<PromotionTimeFrame> timeFrames = this.promotionTimeFrameDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionTimeFrame._promotionId), promotionId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        for (PromotionTimeFrame dbBo : timeFrames) {
            dbBo.setStatus(PromotionTimeFrame.STATUS_INACTIVE);
            dbBo.setModifiedBy(User.getContextId());
            this.promotionTimeFrameDao.save(dbBo);
        }
    }

    public void deactivateCategories(Integer promotionId) {
        List<PromotionCategory> categories = this.promotionCategoryDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionCategory._promotionId), promotionId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        for (PromotionCategory dbBo : categories) {
            dbBo.setStatus(PromotionCategory.STATUS_INACTIVE);
            dbBo.setModifiedBy(User.getContextId());
            this.promotionCategoryDao.save(dbBo);
        }
    }

    public void deactivatePromotionProduct(Integer promotionId) {
        List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionProduct._promotionId), promotionId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        for (PromotionProduct dbBo : promotionProducts) {
            dbBo.setStatus(PromotionProduct.STATUS_INACTIVE);
            dbBo.setModifiedBy(User.getContextId());
            this.promotionProductDao.save(dbBo);
        }
    }

    @Override
    public List<Promotion> find(Promotion bo) {
        return promotionDao.findAll(Example.of(bo), Sort.by(Sort.Order.desc(Promotion._id)));
    }

    @Override
    public List<Promotion> findPromotionActive() {
        return this.promotionDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Promotion._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Promotion._status), Promotion.STATUS_ACTIVE));
            int typeTime = Promotion.FILTER_PREPARE_TIME + Promotion.FILTER_START_TIME;
            this.setTypeTimePromotion(predicates, root, builder, typeTime);
            query.orderBy(builder.desc(root.get(Promotion._id)));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    @Override
    public Page<Promotion> findBySearch(PromotionSearchRequest model) {
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        Instant endDate = model.getEndTime();
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());

        List<Integer> promotionIds = null;
        if (StringUtils.isNotEmpty(model.getKeyword())) {
            List<PromotionProduct> productList = this.promotionProductDao.findAll((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                Subquery<List> productSubquery = query.subquery(List.class);
                Root<Product> productSubqueryRoot = productSubquery.from(Product.class);
                productSubquery.select(productSubqueryRoot.get(Product._id)).where(
                        builder.or(
                                builder.like(productSubqueryRoot.get(Product._name), "%" + model.getKeyword() + "%"),
                                builder.like(productSubqueryRoot.get(Product._sku), "%" + model.getKeyword() + "%")
                        ));
                predicates.add(root.get(PromotionProduct._productId).in(productSubquery));
                return builder.and(predicates.toArray(new Predicate[0]));
            });
            promotionIds = productList.stream().map(PromotionProduct::getPromotionId).distinct().collect(Collectors.toList());
        }

        List<Integer> finalPromotionIds = promotionIds;
        return this.promotionDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Promotion._isTrash), model.getIsTrash()));
            if (model.getStatus() != null) {
                predicates.add(builder.equal(root.get(Promotion._status), model.getStatus()));
            }
            if (model.getStartTime() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Promotion._startTime), model.getStartTime()));
            }
            if (endDate != null) {
                predicates.add(builder.lessThanOrEqualTo(root.get(Promotion._endTime), endDate));
            }
            if (StringUtils.isNotEmpty(model.getPromotionName())) {
                predicates.add(builder.like(root.get(Promotion._name), "%" + model.getPromotionName() + "%"));
            }
            if (model.getType() != null) {
                predicates.add(builder.equal(root.get(Promotion._type), model.getType()));
            }
            if (StringUtils.isNotEmpty(model.getKeyword())) {
                if (finalPromotionIds.size() == 0) {
                    // không tìm được sản phẩm
                    predicates.add(root.get(Promotion._id).isNull());
                } else {
                    predicates.add(root.get(Promotion._id).in(finalPromotionIds));
                }
            }
            int typeTime = model.getTypeTime();
            this.setTypeTimePromotion(predicates, root, builder, typeTime);
            query.orderBy(builder.desc(root.get(Promotion._id)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public Page<Promotion> findPromotionForShop(PromotionSearchRequest model) {
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        return this.promotionDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Promotion._isTrash), false));
            if (StringUtils.isNotEmpty(model.getPromotionName())) {
                predicates.add(builder.like(root.get(Promotion._name), "%" + model.getPromotionName() + "%"));
            }
            predicates.add(builder.equal(root.get(Promotion._status), Promotion.STATUS_ACTIVE));
            int typeTime = model.getTypeTime();
            this.setTypeTimePromotion(predicates, root, builder, typeTime);
            Subquery<Long> subqueryCategory = query.subquery(Long.class);
            Root<PromotionCategory> promotionCategoryRoot = subqueryCategory.from(PromotionCategory.class);
            subqueryCategory
                    .select(builder.count(promotionCategoryRoot.get(PromotionCategory._id)))
                    .where(builder.and(
                            builder.equal(promotionCategoryRoot.get(PromotionCategory._promotionId), root.get(Promotion._id)),
                            builder.equal(promotionCategoryRoot.get(PromotionCategory._status), Promotion.STATUS_ACTIVE)
                            )
                    );
            predicates.add(builder.and(builder.greaterThan(subqueryCategory.getSelection(), 0L)));
            query.orderBy(builder.desc(root.get(Promotion._id)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public Promotion findPromotion(PromotionTimeFrameRequest model) {
        Promotion dbBo = super.get(model.getPromotionId());
        Instant endDate = model.getEndTime();
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        Page<PromotionTimeFrame> promotionTimeFramePage = promotionTimeFrameDao.findAll((root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    predicates.add(builder.equal(root.get(PromotionTimeFrame._promotionId), dbBo.getId()));
                    if (model.getStatus() != null) {
                        predicates.add(builder.equal(root.get(PromotionTimeFrame._status), model.getStatus()));
                    }
                    if (model.getStartTime() != null) {
                        predicates.add(builder.greaterThanOrEqualTo(root.get(Promotion._startTime), model.getStartTime()));
                    }
                    if (endDate != null) {
                        predicates.add(builder.lessThanOrEqualTo(root.get(Promotion._endTime), endDate));
                    }
                    int typeTime = model.getTypeTime();
                    this.setTypeTimePromotionTimeFrame(predicates, root, builder, typeTime);
                    query.orderBy(builder.asc(root.get(PromotionTimeFrame._day)), builder.asc(root.get(PromotionTimeFrame._startTime)));
                    return builder.and(predicates.toArray(new Predicate[0]));
                }
                , pageable);
        dbBo.setPromotionTimeFramePage(promotionTimeFramePage);
        return dbBo;
    }

    @Override
    public Promotion findPromotionWithCategory(PromotionTimeFrameRequest model) {
        Promotion dbBo = super.get(model.getPromotionId());
        Instant endDate = model.getEndTime();
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        Page<PromotionTimeFrame> timeFramePage = promotionTimeFrameDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionTimeFrame._promotionId), dbBo.getId()));
            predicates.add(builder.equal(root.get(PromotionTimeFrame._status), PromotionTimeFrame.STATUS_ACTIVE));
            if (model.getStartTime() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Promotion._startTime), model.getStartTime()));
            }
            if (endDate != null) {
                predicates.add(builder.lessThanOrEqualTo(root.get(Promotion._endTime), endDate));
            }
            int typeTime = model.getTypeTime();
            this.setTypeTimePromotionTimeFrame(predicates, root, builder, typeTime);
            query.orderBy(builder.asc(root.get(PromotionTimeFrame._day)), builder.asc(root.get(PromotionTimeFrame._startTime)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
        dbBo.setPromotionTimeFramePage(timeFramePage);

        List<Integer> promotionTimeFrameIds = timeFramePage.getContent().stream().map(PromotionTimeFrame::getId).collect(Collectors.toList());
        if (promotionTimeFrameIds.size() > 0) {
            List<PromotionCategory> promotionCategories = promotionCategoryDao.findAll((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.equal(root.get(PromotionCategory._promotionId), dbBo.getId()));
                predicates.add((root.get(PromotionCategory._timeFrameId).in(promotionTimeFrameIds)));
                if (model.getStatus() != null) {
                    predicates.add(builder.equal(root.get(PromotionCategory._status), model.getStatus()));
                }
                if (model.getType() != null) {
                    Subquery<PromotionConfig> configSubquery = query.subquery(PromotionConfig.class);
                    Root<PromotionConfig> configRoot = configSubquery.from(PromotionConfig.class);
                    configSubquery
                            .select(configRoot.get(PromotionConfig._id))
                            .where(builder.and(
                                    builder.equal(root.get(PromotionCategory._id), configRoot.get(PromotionConfig._categoryId)),
                                    builder.equal(configRoot.get(PromotionConfig._type), model.getType())
                            ));
                    predicates.add(builder.exists(configSubquery));
                }

                query.orderBy(builder.asc(root.get(PromotionCategory._priority)), builder.desc(root.get(PromotionCategory._createdDate)));
                return builder.and(predicates.toArray(new Predicate[0]));
            });

            List<PromotionCategory> categorieList;
            for (PromotionTimeFrame timeFrame : dbBo.getPromotionTimeFramePage().getContent()) {
                categorieList = promotionCategories.stream()
                        .filter(categorie -> categorie.getTimeFrameId() != null && categorie.getTimeFrameId().equals(timeFrame.getId()))
                        .collect(Collectors.toList());
                timeFrame.setPromotionCategoryList(categorieList);
            }

            List<Integer> promotionCategoryIds = promotionCategories.stream().map(PromotionCategory::getId).collect(Collectors.toList());
            if (promotionCategoryIds.size() > 0) {
                List<PromotionConfig> configList = promotionConfigDao.findAll((root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    predicates.add(builder.equal(root.get(PromotionConfig._promotionId), dbBo.getId()));
                    predicates.add((root.get(PromotionConfig._timeFrameId).in(promotionTimeFrameIds)));
                    predicates.add((root.get(PromotionConfig._categoryId).in(promotionCategoryIds)));
                    return builder.and(predicates.toArray(new Predicate[0]));
                });
                Map<Integer, PromotionConfig> configMap = configList.stream().collect(Collectors.toMap(PromotionConfig::getCategoryId, config -> config));
                for (PromotionCategory category : promotionCategories) {
                    category.setPromotionConfig(configMap.get(category.getId()));
                }
            }
        }
        return dbBo;
    }

    private void setTypeTimePromotion(List<Predicate> predicates, Root<Promotion> root, CriteriaBuilder builder, int typeTime) {
        if (typeTime != 0) {
            predicates.add(builder.equal(root.get(Promotion._status), Promotion.STATUS_ACTIVE));
            switch (typeTime) {
                case Promotion.FILTER_PREPARE_TIME:
                    predicates.add(builder.or(
                            builder.greaterThan(root.get(Promotion._startTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_PREPARE_TIME + Promotion.FILTER_START_TIME:
                    predicates.add(builder.or(
                            builder.greaterThan(root.get(Promotion._startTime), Instant.now()),
                            builder.and(
                                    builder.lessThan(root.get(Promotion._startTime), Instant.now()),
                                    builder.greaterThan(root.get(Promotion._endTime), Instant.now())
                            ))
                    );
                    break;
                case Promotion.FILTER_PREPARE_TIME + Promotion.FILTER_END_TIME:
                    predicates.add(builder.or(
                            builder.greaterThan(root.get(Promotion._startTime), Instant.now()),
                            builder.lessThan(root.get(Promotion._endTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_START_TIME:
                    predicates.add(builder.and(
                            builder.lessThan(root.get(Promotion._startTime), Instant.now()),
                            builder.greaterThan(root.get(Promotion._endTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_START_TIME + Promotion.FILTER_END_TIME:
                    predicates.add(builder.or(
                            builder.and(
                                    builder.lessThan(root.get(Promotion._startTime), Instant.now()),
                                    builder.greaterThan(root.get(Promotion._endTime), Instant.now())),
                            builder.lessThan(root.get(Promotion._endTime), Instant.now())
                            )

                    );
                    break;
                case Promotion.FILTER_END_TIME:
                    predicates.add(builder.and(
                            builder.lessThan(root.get(Promotion._endTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_END_TIME + Promotion.FILTER_START_TIME + Promotion.FILTER_END_TIME:
                default:
                    break;
            }
        }
    }

    private void setTypeTimePromotionTimeFrame(List<Predicate> predicates, Root<PromotionTimeFrame> root, CriteriaBuilder builder, int typeTime) {
        if (typeTime != 0) {
            switch (typeTime) {
                case Promotion.FILTER_PREPARE_TIME:
                    predicates.add(builder.or(
                            builder.greaterThan(root.get(PromotionTimeFrame._startTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_PREPARE_TIME + Promotion.FILTER_START_TIME:
                    predicates.add(builder.or(
                            builder.greaterThan(root.get(PromotionTimeFrame._startTime), Instant.now()),
                            builder.and(
                                    builder.lessThan(root.get(PromotionTimeFrame._startTime), Instant.now()),
                                    builder.greaterThan(root.get(PromotionTimeFrame._endTime), Instant.now())
                            ))
                    );
                    break;
                case Promotion.FILTER_PREPARE_TIME + Promotion.FILTER_END_TIME:
                    predicates.add(builder.or(
                            builder.greaterThan(root.get(PromotionTimeFrame._startTime), Instant.now()),
                            builder.lessThan(root.get(PromotionTimeFrame._endTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_START_TIME:
                    predicates.add(builder.and(
                            builder.lessThan(root.get(PromotionTimeFrame._startTime), Instant.now()),
                            builder.greaterThan(root.get(PromotionTimeFrame._endTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_START_TIME + Promotion.FILTER_END_TIME:
                    predicates.add(builder.or(
                            builder.and(
                                    builder.lessThan(root.get(PromotionTimeFrame._startTime), Instant.now()),
                                    builder.greaterThan(root.get(PromotionTimeFrame._endTime), Instant.now())),
                            builder.lessThan(root.get(PromotionTimeFrame._endTime), Instant.now())
                            )

                    );
                    break;
                case Promotion.FILTER_END_TIME:
                    predicates.add(builder.and(
                            builder.lessThan(root.get(PromotionTimeFrame._endTime), Instant.now()))
                    );
                    break;
                case Promotion.FILTER_END_TIME + Promotion.FILTER_START_TIME + Promotion.FILTER_END_TIME:
                default:
                    break;
            }
        }
    }


    private void validatePromotion(Promotion bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        if (Promotion.STATUS_ACTIVE.equals(bo.getStatus())) {
            bo.getErrorList().add(new HebelaError("Chương trình đã kích hoạt không thể cập nhật."));
            return;
        }
        if (Boolean.TRUE.equals(bo.getIsTrash())) {
            bo.getErrorList().add(new HebelaError("Chương trình đã bị huỷ không thể cập nhật."));
            return;
        }
        this.validatePromotionProduct(bo);
    }

    private void validatePromotionProduct(Promotion bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        long countProduct = promotionProductDao.count((root, query, builder) -> builder.and(
                builder.equal(root.get(PromotionProduct._promotionId), bo.getId())
        ));
        if (countProduct > 0) {
            bo.getErrorList().add(new HebelaError("Chương trình đã có sản phẩm đăng kí không thể cập nhật hoặc huỷ."));
        }
    }

    private void validateInput(Promotion bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        if (bo.getName() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thông tin chương trình"));
        }
        if (bo.getDescription() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu điều kiện tham gia chương trình"));
        }
        if (bo.getPlatform() == null || bo.getPlatform() == 0) {
            bo.getErrorList().add(new HebelaError("Thiếu nền tảng áp dụng chương trình"));
        }
        if (bo.getPrepareStartTime() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thời gian bắt đầu chuẩn bị chương trình"));
        }
        if (bo.getPrepareEndTime() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thời gian kết thúc chuẩn bị chương trình"));
        }
        if (bo.getStartTime() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thời gian bắt đầu chương trình"));
        }
        if (bo.getEndTime() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thời gian kết thúc chương trình"));
        }
        if (bo.getDisplayTime() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thời gian hiển thị chương trình"));
        }
        if (!bo.getEndTime().isAfter(bo.getStartTime())) {
            bo.getErrorList().add(new HebelaError("Thời gian kết thúc phải sau thời gian bắt đầu"));
        }
        if (!bo.getPrepareEndTime().isAfter(bo.getPrepareStartTime())) {
            bo.getErrorList().add(new HebelaError("Thời gian kết thúc chuẩn bị phải sau thời gian bắt đầu chuẩn bị"));
        }
        if (bo.getPrepareStartTime().isAfter(bo.getDisplayTime())) {
            bo.getErrorList().add(new HebelaError("Thời gian bắt đầu chuẩn bị phải trước thời gian hiển thị"));
        }
        if (!bo.getDisplayTime().isBefore(bo.getStartTime())) {
            bo.getErrorList().add(new HebelaError("Thời gian hiển thị phải trước thời gian bắt đầu chương trình"));
        }
        if (!bo.getDisplayTime().isBefore(bo.getEndTime())) {
            bo.getErrorList().add(new HebelaError("Thời gian hiển thị phải trước thời gian kết thúc chương trình"));
        }
    }
}

