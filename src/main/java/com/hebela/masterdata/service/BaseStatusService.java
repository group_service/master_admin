package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusBo;
import com.hebela.core.StatusBo;
import com.hebela.core.StatusService;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.core.obj.StatusInfo;

public abstract class BaseStatusService<T extends StatusBo> extends BaseService<T, Integer> implements StatusService<T> {
    @Override
    public T doTrash(T bo) {
        T dbBo = get(bo.getId());
        ((AbstractStatusBo) dbBo).setIsTrash(true);
        ((AbstractStatusBo) dbBo).setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public T doRestore(T bo) {
        T dbBo = get(bo.getId());
        ((AbstractStatusBo) dbBo).setIsTrash(false);
        ((AbstractStatusBo) dbBo).setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public T doUpdateStatus(T bo) {
        T dbBo = get(bo.getId());
        ((AbstractStatusBo) dbBo).setStatus(bo.getStatus());
        ((AbstractStatusBo) dbBo).setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    @Override
    public StatusInfo statusInfo() {
        return super.statusInfo();
    }

    @Override
    public StatusInfo statusInfo(Integer orgId) {
        Integer _orgId = null;
        if (Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
            _orgId = orgId;
        }
        if (_orgId == null) {
            _orgId = getOrgId();
        }
        return super.statusInfo(_orgId);
    }
}
