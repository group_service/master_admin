package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.ProductTags;

import java.util.List;

public interface ProductTagsService extends StatusService<ProductTags> {
    List<ProductTags> findWithExtends(ProductTags bo);
}

