package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.bo.ProductGift;
import com.hebela.masterdata.web.gift.ProductGiftResponse;

import java.util.List;

public interface ProductGiftService extends Service<ProductGift, Integer> {
    List<Product> findProductForGift(String term, Integer productId, Integer typeProduct);

    List<ProductCombo> findComboForGift();

    ProductGiftResponse getDetail(Integer productGiftId);

    List<ProductGift> getDetailByProduct(Integer id);
}

