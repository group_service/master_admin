package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductPolicyServiceImpl extends AbstractStatusService<ProductPolicy> implements ProductPolicyService {
    private final ProductPolicyDao productPolicyDao;
    private final ProductPolicyLinkDao productPolicyLinkDao;
    private final ProductDao productDao;
    private final ProductCategoryDao productCategoryDao;
    private final ProductComboDao productComboDao;

    public ProductPolicyServiceImpl(ProductPolicyDao productPolicyDao, ProductPolicyLinkDao productPolicyLinkDao, ProductDao productDao, ProductCategoryDao productCategoryDao, ProductComboDao productComboDao) {
        this.productPolicyDao = productPolicyDao;
        this.productPolicyLinkDao = productPolicyLinkDao;
        this.productDao = productDao;
        this.productCategoryDao = productCategoryDao;
        this.productComboDao = productComboDao;
    }

    @Override
    public ProductPolicy doInsert(ProductPolicy bo) {
        bo.setPriority(1);
        return super.doInsert(bo);
    }

    @Override
    public List<ProductPolicy> find(ProductPolicy bo) {
        return productPolicyDao.findAll(Example.of(bo), Sort.by(Sort.Order.asc(ProductPolicy._priority),
                Sort.Order.desc(ProductPolicy._id)));
    }

    @Override
    public ProductPolicy get(Integer productPolicyId) {
        ProductPolicy dbBo = super.get(productPolicyId);
        if (dbBo != null) {
            ProductPolicyLink productPolicyLink = new ProductPolicyLink();
            productPolicyLink.setProductPolicyId(dbBo.getId());
            List<ProductPolicyLink> productPolicyLinkList = productPolicyLinkDao.findAll(Example.of(productPolicyLink));
            if (productPolicyLinkList.size() > 0) {
                List<Integer> productIdList = productPolicyLinkList.stream().map(ProductPolicyLink::getProductId).collect(Collectors.toList());
                List<Integer> productCategoryIdList = productPolicyLinkList.stream().map(ProductPolicyLink::getProductCategoryId).collect(Collectors.toList());
                List<Integer> productComboIdList = productPolicyLinkList.stream().map(ProductPolicyLink::getProductComboId).collect(Collectors.toList());
                if (productIdList.size() > 0) {
                    dbBo.setProductList(productDao.findAllById(productIdList));
                }
                if (productCategoryIdList.size() > 0) {
                    dbBo.setProductCategoryList(productCategoryDao.findAllById(productCategoryIdList));
                }
                if (productComboIdList.size() > 0) {
                    dbBo.setProductComboList(productComboDao.findAllById(productComboIdList));
                }
            }
        }
        return dbBo;
    }

    @Override
    public Product doLinkProduct(Integer productPolicyId, Product bo) {
        ProductPolicyLink productPolicyLink = new ProductPolicyLink();
        productPolicyLink.setProductPolicyId(productPolicyId);
        productPolicyLink.setProductId(bo.getId());
        this.productPolicyLinkDao.save(productPolicyLink);
        bo.setName(productDao.getOne(bo.getId()).getName());
        return bo;
    }

    @Override
    public void doUnlinkProduct(Integer productPolicyId, Product bo) {
        ProductPolicyLink productPolicyLink = new ProductPolicyLink();
        productPolicyLink.setProductPolicyId(productPolicyId);
        productPolicyLink.setProductId(bo.getId());
        List<ProductPolicyLink> productPolicyLinkList = productPolicyLinkDao.findAll(Example.of(productPolicyLink));
        this.productPolicyLinkDao.deleteAll(productPolicyLinkList);
    }

    @Override
    public ProductCategory doLinkProductCategory(Integer productPolicyId, ProductCategory bo) {
        ProductPolicyLink productPolicyLink = new ProductPolicyLink();
        productPolicyLink.setProductPolicyId(productPolicyId);
        productPolicyLink.setProductCategoryId(bo.getId());
        this.productPolicyLinkDao.save(productPolicyLink);
        bo.setName(productCategoryDao.getOne(bo.getId()).getName());
        return bo;
    }

    @Override
    public void doUnlinkProductCategory(Integer productPolicyId, ProductCategory bo) {
        ProductPolicyLink productPolicyLink = new ProductPolicyLink();
        productPolicyLink.setProductPolicyId(productPolicyId);
        productPolicyLink.setProductCategoryId(bo.getId());
        List<ProductPolicyLink> productPolicyLinkList = productPolicyLinkDao.findAll(Example.of(productPolicyLink));
        this.productPolicyLinkDao.deleteAll(productPolicyLinkList);
    }

    @Override
    public ProductCombo doLinkProductCombo(Integer productPolicyId, ProductCombo bo) {
        ProductPolicyLink productPolicyLink = new ProductPolicyLink();
        productPolicyLink.setProductPolicyId(productPolicyId);
        productPolicyLink.setProductComboId(bo.getId());
        this.productPolicyLinkDao.save(productPolicyLink);
        bo.setName(productComboDao.getOne(bo.getId()).getName());
        return bo;
    }

    @Override
    public void doUnlinkProductCombo(Integer productPolicyId, ProductCombo bo) {
        ProductPolicyLink productPolicyLink = new ProductPolicyLink();
        productPolicyLink.setProductPolicyId(productPolicyId);
        productPolicyLink.setProductComboId(bo.getId());
        List<ProductPolicyLink> productPolicyLinkList = productPolicyLinkDao.findAll(Example.of(productPolicyLink));
        this.productPolicyLinkDao.deleteAll(productPolicyLinkList);
    }
}

