package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Brand;

import java.util.List;
import java.util.Map;

public interface BrandService extends StatusService<Brand> {
    List<Brand> findWithExtends(Brand bo);
}

