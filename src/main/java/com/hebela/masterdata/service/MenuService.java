package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Menu;

public interface MenuService extends StatusService<Menu> {
}
