package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Account;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Favorite;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.dao.FavoriteDao;
import com.hebela.masterdata.dao.ProductComboDao;
import com.hebela.masterdata.dao.ProductDao;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class FavoriteServiceImpl extends AbstractService<Favorite, Integer> implements FavoriteService {

    private final ProductDao productDao;
    private final ProductComboDao productComboDao;
    private final FavoriteDao favoriteDao;

    public FavoriteServiceImpl(ProductDao productDao, ProductComboDao productComboDao, FavoriteDao favoriteDao) {
        this.productDao = productDao;
        this.productComboDao = productComboDao;
        this.favoriteDao = favoriteDao;
    }

    @Override
    public Favorite doInsert(Favorite bo) {
        Favorite result;
        Account account = (Account) Account.getContext();
        bo.setAccountId(account.getId());
        Optional<Favorite> dbBoOptional = this.dao.findOne(Example.of(bo));
        if (dbBoOptional.isPresent()) {
            result = dbBoOptional.get();
        } else {
            if (bo.getProductId() != null || bo.getComboId() != null) {
                result = super.doInsert(bo);
            } else {
                throw new HebelaException("favorite.product.or.combo.missing");
            }
        }
        this.addDetail(result);
        return result;
    }

    private void addDetail(Favorite bo) {
        if (bo.getProductId() != null) {
            bo.setProduct(this.productDao.getDetail(bo.getProductId()));
        } else if (bo.getComboId() != null) {
            ProductCombo combo = this.productComboDao.findById(bo.getComboId()).orElse(null);
            if (combo == null) {
                throw new HebelaException("combo.obj.missing");
            }
            List<Product> productList = this.productComboDao.findProduct(combo.getId());
            combo.setProductList(productList);
            bo.setProductCombo(combo);
        }
    }

    @Override
    public Favorite doDelete(Favorite bo) {
        bo.setAccountId(User.getContextId());
        Optional<Favorite> favoriteOptional = this.favoriteDao.findAll(Example.of(bo)).stream().findFirst();
        if (favoriteOptional.isPresent()) {
            this.favoriteDao.delete(favoriteOptional.get());
            return favoriteOptional.get();
        } else {
            throw new HebelaException("favorite.missing");
        }
    }
}

