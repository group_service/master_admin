package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Payment;

import java.util.List;

public interface PaymentService extends StatusService<Payment> {
}

