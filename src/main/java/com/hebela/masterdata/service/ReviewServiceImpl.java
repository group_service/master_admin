package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Account;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.web.review.ReviewSearchRequest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Service
public class ReviewServiceImpl extends AbstractStatusService<Review> implements ReviewService {
    private final ReviewDao reviewDao;
    private final ReviewSummaryDao reviewSummaryDao;
    private final ReviewGalleryDao reviewGalleryDao;
    private final OrderItemDao orderItemDao;
    private final ProductDao productDao;

    public ReviewServiceImpl(ReviewDao reviewDao, ReviewSummaryDao reviewSummaryDao, ReviewGalleryDao reviewGalleryDao, OrderItemDao orderItemDao, AccountDao accountDao, ProductDao productDao, OrderDao orderDao) {
        this.reviewDao = reviewDao;
        this.reviewSummaryDao = reviewSummaryDao;
        this.reviewGalleryDao = reviewGalleryDao;
        this.orderItemDao = orderItemDao;
        this.productDao = productDao;
    }

    @Override
    public Review doInsert(Review bo) {
        if (User.getContextId() == null) {
            throw new HebelaException("review.account.invalid");
        }
        checkDescriptionReview(bo.getDescription());
        checkRating(bo.getRating());
        checkProductEnableReview(bo);
        bo.setStatus(Review.STATUS_INACTIVE);
        bo.setHasOrder(isHasOrder(bo));
        bo.setAccountId(User.getContextId());
        super.doInsert(bo);
        saveReviewGallery(bo);
        return bo;
    }

    /*
     * Duyệt review
     * Chuyển trạng thái từ "Chưa duyệt" sang "Duyệt"
     * Review phải chưa chưa duyệt và không bị hủy
     */
    @Override
    public Review doApprove(Review bo) {
        Review review = super.get(bo.getId());
        if (review.getStatus().equals(ReviewBase.STATUS_ACTIVE) || review.getIsTrash().equals(Boolean.TRUE)) {
            throw new HebelaException("action.invalid");
        }
        review.setStatus(ReviewBase.STATUS_ACTIVE);
        review.setModifiedBy(User.getContextId());
        reviewDao.save(review);
        saveReviewSummary(bo);
        return bo;
    }

    @Override
    public Review doTrash(Review bo) {
        if (bo.getReason() == null) {
            throw new HebelaException("review.reason.missing");
        }
        Review review = super.get(bo.getId());
        if (review == null) {
            throw new HebelaException("review.missing");
        }
        super.doTrash(review);
        review.setReason(bo.getReason());
        reviewDao.save(review);
        saveReviewSummary(review);
        return bo;
    }

    @Override
    public Page<Review> findBySearch(ReviewSearchRequest model, Pageable page) {
        Integer orgId = null;
        if (Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
            orgId = model.getOrgId();
        }
        if (orgId == null) {
            orgId = getOrgId();
        }
        Instant fromDate = model.getFromDate();
        Instant toDate = model.getToDate();
        List<Integer> statusList = model.getStatus() == null ? Arrays.asList(Review.ALL_STATUS)
                : Collections.singletonList(model.getStatus());
        Integer finalOrgId = orgId;
        return reviewDao.findAll((root, query, builder) -> {
            Root<Product> productRoot = query.from(Product.class);
            Root<Account> accountRoot = query.from(Account.class);
            query.distinct(true);
            query.multiselect(root, productRoot.get(Product._name), accountRoot.get(Account._fullname), accountRoot.get(Account._avatarUrl));
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(Review._productId), productRoot.get(Product._id)));
            predicates.add(builder.equal(root.get(Review._accountId), accountRoot.get(Account._id)));
            predicates.add(builder.equal(root.get(Review._orgId), finalOrgId));
            predicates.add(root.get(Review._status).in(statusList));
            if (model.getIsTrash() != null) {
                predicates.add(builder.equal(root.get(Review._isTrash), model.getIsTrash()));
            } else {
                predicates.add(builder.equal(root.get(Review._isTrash), Boolean.FALSE));
            }
            if (fromDate != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Review._createdDate), fromDate));
            }
            if (toDate != null) {
                predicates.add(builder.lessThanOrEqualTo(root.get(Review._createdDate), toDate.plus(1, ChronoUnit.DAYS)));
            }
            if (model.getRating() != null) {
                predicates.add(builder.equal(root.get(Review._rating), model.getRating()));
            }
            if (model.getKeyword() != null) {
                predicates.add(builder.or(
                        builder.like(productRoot.get(Product._name), "%" + model.getKeyword() + "%"),
                        builder.like(productRoot.get(Product._sku), "%" + model.getKeyword() + "%")
                ));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        }, page);
    }

    @Override
    public Review getReviewDetail(Integer idReview) {
        Review review = reviewDao.getReviewById(idReview);
        ReviewGallery example = new ReviewGallery();
        example.setReviewId(review.getId());
        List<ReviewGallery> reviewGalleryList = reviewGalleryDao.findAll(Example.of(example));
        reviewGalleryList.forEach(reviewGallery -> {
            if (review.getReviewGallery() == null) {
                review.setReviewGallery(new ArrayList<>());
            }
            review.getReviewGallery().add(reviewGallery.getImage());
        });
        return review;
    }

    private boolean isHasOrder(Review bo) {
        OrderItem example = new OrderItem();
        example.setAccountId(User.getContextId());
        example.setProductId(bo.getProductId());
        long countOrderItem = orderItemDao.count(Example.of(example));
        if (countOrderItem > 0) {
            return true;
        } else {
            List<OrderItem> orderItemList = orderItemDao.findByAccountId(User.getContextId());
            if (orderItemList.size() > 0) {
                for (OrderItem orderItem : orderItemList) {
                    ProductCombo combo = orderItem.getCombo();
                    if (combo == null) {
                        break;
                    }
                    List<Product> productList = combo.getProductList();
                    if (productList != null) {
                        for (Product product : productList) {
                            if (product.getId().equals(bo.getProductId())) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /*
     * Lưu thông tin review summary
     * Review ở trạng thái là active
     * Nếu review không bị huỷ thì cộng thêm
     * Nếu review bị huỷ thì trừ đi
     */
    private void saveReviewSummary(Review bo) {
        if (Review.STATUS_ACTIVE.equals(bo.getStatus())) {
            if (Boolean.FALSE.equals(bo.getIsTrash())) {
                ReviewSummary example = new ReviewSummary();
                example.setProductId(bo.getProductId());
                ReviewSummary reviewSummary = reviewSummaryDao.findOne(Example.of(example)).orElse(null);
                if (reviewSummary != null) {
                    reviewSummary.setModifiedBy(User.getContextId());
                    ReviewSummary.plusRatingForSummary(reviewSummary, bo.getRating());
                    reviewSummaryDao.save(reviewSummary);
                } else {
                    ReviewSummary.plusRatingForSummary(example, bo.getRating());
                    example.setCreatedBy(User.getContextId());
                    reviewSummaryDao.save(example);
                }
            }
            if (Boolean.TRUE.equals(bo.getIsTrash())) {
                ReviewSummary example = new ReviewSummary();
                example.setProductId(bo.getProductId());
                ReviewSummary reviewSummary = reviewSummaryDao.findOne(Example.of(example)).orElse(null);
                if (reviewSummary != null) {
                    reviewSummary.setModifiedBy(User.getContextId());
                    ReviewSummary.subRatingForSummary(reviewSummary, bo.getRating());
                    reviewSummaryDao.save(reviewSummary);
                }
            }
        }
    }

    private void saveReviewGallery(Review bo) {
        List<String> reviewGalleryList = bo.getReviewGallery();
        if (reviewGalleryList != null) {
            for (String image : reviewGalleryList) {
                ReviewGallery reviewGallery = new ReviewGallery();
                reviewGallery.setReviewId(bo.getId());
                reviewGallery.setImage(image);
                reviewGallery.setCreatedBy(User.getContextId());
                reviewGalleryDao.save(reviewGallery);
            }
        }
    }

    public void checkProductEnableReview(Review bo) {
        if (bo.getProductId() == null) {
            throw new HebelaException("review.product.invalid");
        }
        Product product = productDao.findById(bo.getProductId()).orElse(null);
        if (product == null || Boolean.TRUE.equals(product.getIsTrash()) || !Product.STATUS_ACTIVE.equals(product.getStatus())
                || Boolean.FALSE.equals(product.getEnableReviews())) {
            throw new HebelaException("review.product.invalid");
        }
        bo.setOrgId(product.getOrgId());
    }

    public void checkRating(Integer rating) {
        if (rating == null || rating < 1 || rating > 5) {
            throw new HebelaException("review.rating.invalid");
        }
    }

    public void checkDescriptionReview(String description) {
        if (description == null) {
            throw new HebelaException("review.description.missing");
        }
        if (description.length() < Review.MIN_LENGTH_DESCRIPTION) {
            throw new HebelaException("review.description.min.invalid");
        }
        if (description.length() > Review.MAX_LENGTH_DESCRIPTION) {
            throw new HebelaException("review.description.max.invalid");
        }
    }
}
