package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.bo.extend.PlatformExtend;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.web.promotionTimeFrame.PromotionTimeFrameRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class PromotionTimeFrameServiceImpl extends AbstractService<PromotionTimeFrame, Integer> implements PromotionTimeFrameService {
    @Autowired
    PromotionTimeFrameDao promotionTimeFrameDao;
    @Autowired
    PromotionDao promotionDao;
    @Autowired
    PromotionCategoryDao promotionCategoryDao;
    @Autowired
    PromotionConfigDao promotionConfigDao;
    @Autowired
    PromotionProductDao promotionProductDao;
    @Autowired
    ObjectImageDao objectImageDao;

    @Override
    public PromotionTimeFrame get(Integer key) {
        PromotionTimeFrame dbBo = super.get(key);
        List<ObjectImage> objectImages = objectImageDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.and(
                    builder.equal(root.get(ObjectImage._objectClass), ObjectImage.OBJ_CLASS_PROMOTION_TIME_FRAME),
                    builder.equal(root.get(ObjectImage._objectClassId), dbBo.getId())
            ));
            query.orderBy(
                    builder.asc(root.get(ObjectImage._priority)),
                    builder.asc(root.get(ObjectImage._createdDate))
            );
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        dbBo.setObjectImageList(objectImages);
        return dbBo;
    }

    @Override
    public PromotionTimeFrame getDetail(Integer key) {
        PromotionTimeFrame dbBo = super.get(key);
        List<PromotionCategory> promotionCategories = promotionCategoryDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionCategory._promotionId), dbBo.getPromotionId()));
            predicates.add(builder.equal(root.get(PromotionCategory._timeFrameId), dbBo.getId()));
            query.orderBy(builder.asc(root.get(PromotionCategory._priority)), builder.desc(root.get(PromotionCategory._createdDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        dbBo.setPromotionCategoryList(promotionCategories);

        List<Integer> promotionCategoryIds = promotionCategories.stream().map(PromotionCategory::getId).collect(Collectors.toList());
        if (promotionCategoryIds.size() > 0) {
            List<PromotionConfig> configList = promotionConfigDao.findAll((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.equal(root.get(PromotionConfig._promotionId), dbBo.getPromotionId()));
                predicates.add(builder.equal(root.get(PromotionConfig._timeFrameId), dbBo.getId()));
                predicates.add((root.get(PromotionConfig._categoryId).in(promotionCategoryIds)));
                return builder.and(predicates.toArray(new Predicate[0]));
            });
            Map<Integer, PromotionConfig> configMap = configList.stream().collect(Collectors.toMap(PromotionConfig::getCategoryId, config -> config));
            for (PromotionCategory category : promotionCategories) {
                category.setPromotionConfig(configMap.get(category.getId()));
            }
        }
        return dbBo;
    }

    // tìm thông tin timeFrame cho shop
    @Override
    public Page<PromotionTimeFrame> findPromotionTimeFrame(PromotionTimeFrameRequest model) {
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        Page<PromotionTimeFrame> timeFramePage = promotionTimeFrameDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Subquery<Long> subqueryCategory = query.subquery(Long.class);
            Root<PromotionCategory> categoryRoot = subqueryCategory.from(PromotionCategory.class);
            subqueryCategory.select(builder.count(categoryRoot.get(PromotionCategory._id)))
                    .where(
                            builder.equal(categoryRoot.get(PromotionCategory._timeFrameId), root.get(PromotionTimeFrame._id)),
                            builder.equal(categoryRoot.get(PromotionCategory._status), PromotionCategory.STATUS_ACTIVE)
                    );
            predicates.add(builder.equal(root.get(PromotionTimeFrame._promotionId), model.getPromotionId()));
            predicates.add(builder.equal(root.get(PromotionTimeFrame._status), PromotionTimeFrame.STATUS_ACTIVE));
            predicates.add(builder.greaterThan(subqueryCategory.getSelection(), 0L));
            query.orderBy(builder.asc(root.get(PromotionTimeFrame._day)), builder.asc(root.get(PromotionTimeFrame._startTime)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);

        if (timeFramePage.getContent().size() > 0) {
            List<Integer> timeFrameIds = timeFramePage.getContent().stream().map(PromotionTimeFrame::getId).collect(Collectors.toList());
            List<PromotionCategory> promotionCategories = promotionCategoryDao.findAll((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.equal(root.get(PromotionCategory._promotionId), model.getPromotionId()));
                predicates.add((root.get(PromotionCategory._timeFrameId).in(timeFrameIds)));
                predicates.add(builder.equal(root.get(PromotionCategory._status), PromotionTimeFrame.STATUS_ACTIVE));
                query.orderBy(builder.asc(root.get(PromotionCategory._priority)), builder.desc(root.get(PromotionCategory._createdDate)));
                return builder.and(predicates.toArray(new Predicate[0]));
            });

            if (promotionCategories.size() > 0) {
                List<Integer> promotionCategoryIds = promotionCategories.stream().map(PromotionCategory::getId).collect(Collectors.toList());
                List<PromotionConfig> configList = promotionConfigDao.findAll((root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    predicates.add(builder.equal(root.get(PromotionConfig._promotionId), model.getPromotionId()));
                    predicates.add((root.get(PromotionConfig._timeFrameId).in(timeFrameIds)));
                    predicates.add((root.get(PromotionConfig._categoryId).in(promotionCategoryIds)));
                    return builder.and(predicates.toArray(new Predicate[0]));
                });
                Map<Integer, PromotionConfig> configMap = configList.stream().collect(Collectors.toMap(PromotionConfig::getCategoryId, config -> config));
                //set config vào category
                for (PromotionCategory category : promotionCategories) {
                    category.setPromotionConfig(configMap.get(category.getId()));
                }

                List<PromotionCategory> categorieList;
                //set category vào khung giờ
                for (PromotionTimeFrame timeFrame : timeFramePage.getContent()) {
                    categorieList = promotionCategories.stream()
                            .filter(categorie -> categorie.getTimeFrameId() != null && categorie.getTimeFrameId().equals(timeFrame.getId()))
                            .collect(Collectors.toList());
                    timeFrame.setPromotionCategoryList(categorieList);
                }
            }
        }
        return timeFramePage;
    }

    @Override
    public PromotionTimeFrame doInsert(PromotionTimeFrame bo) {
        bo.setStatus(bo.getStatus() == null ? PromotionTimeFrame.STATUS_INACTIVE : bo.getStatus());
        bo.setDay(bo.getDay().plusDays(PromotionTimeFrame.ONE_DAY));
        this.validateTimeFrame(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        PromotionTimeFrame dbBo = super.doInsert(bo);
        List<ObjectImage> objectImageList = bo.getObjectImageList();
        if (objectImageList != null && objectImageList.size() > 0) {
            for (ObjectImage objectImageClient : objectImageList) {
                objectImageClient.setObjectClass(ObjectImage.OBJ_CLASS_PROMOTION_TIME_FRAME);
                objectImageClient.setObjectClassId(dbBo.getId());
                objectImageDao.customUpdate(null, User.getContextId(), objectImageClient);
            }
        }
        return dbBo;
    }

    @Override
    public PromotionTimeFrame doUpdate(PromotionTimeFrame bo) {
        this.validateUpdateTimeFrame(bo);
        this.validatePromotionProduct(bo);
        this.validateTimeFrame(bo);
        if (bo.getErrorList().size() > 0) {
            return bo;
        }
        PromotionTimeFrame dbBo = super.get(bo.getId());
        bo.setDay(dbBo.getDay());
        // update lại nền tảng nếu timeFrame giảm bớt nền tảng áp dụng
        if (bo.getPlatform() < dbBo.getPlatform()) {
            this.updatePlatformCategory(dbBo.getId(), bo.getPlatform());
            this.updatePlatformPromotionProduct(dbBo.getId(), bo.getPlatform());
        }
        return super.doUpdate(bo);
    }

    public void updatePlatformCategory(Integer timeFrameId, Integer platformRemove) {
        List<PromotionCategory> categories = this.promotionCategoryDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionCategory._timeFrameId), timeFrameId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        // tìm nền tảng nào bị loại
        for (PromotionCategory dbBo : categories) {
            for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                if ((platformRemove & platform) <= 0) {
                    if ((dbBo.getPlatform() & platform) > 0) {
                        dbBo.setPlatform(dbBo.getPlatform() - platform);
                    }
                }
            }
            dbBo.setModifiedBy(User.getContextId());
            this.promotionCategoryDao.save(dbBo);
        }
    }


    public void updatePlatformPromotionProduct(Integer timeFrameId, Integer platformRemove) {
        List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionProduct._timeFrameId), timeFrameId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        // tìm nền tảng nào bị loại
        for (PromotionProduct dbBo : promotionProducts) {
            for (Integer platform : PlatformExtend.ALL_PLATFORM) {
                if ((platformRemove & platform) <= 0) {
                    if ((dbBo.getPlatform() & platform) > 0) {
                        dbBo.setPlatform(dbBo.getPlatform() - platform);
                    }
                }
            }
            dbBo.setModifiedBy(User.getContextId());
            this.promotionProductDao.save(dbBo);
        }
    }

    @Override
    public PromotionTimeFrame doDelete(PromotionTimeFrame bo) {
        this.validatePromotionProduct(bo);
        if (bo.getErrorList().size() > 0) {
            throw new HebelaException(bo.getErrorList().get(0).getMessage());
        }
        return super.doDelete(bo);
    }

    @Override
    public PromotionTimeFrame doActivate(PromotionTimeFrame bo) {
        PromotionTimeFrame dbBo = super.get(bo.getId());
        if (dbBo.getStatus().equals(PromotionTimeFrame.STATUS_ACTIVE)) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(PromotionTimeFrame.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionTimeFrameDao.save(dbBo);
        return dbBo;
    }

    @Override
    public PromotionTimeFrame doDeactivate(PromotionTimeFrame bo) {
        PromotionTimeFrame dbBo = super.get(bo.getId());
        if (!dbBo.getStatus().equals(Promotion.STATUS_ACTIVE)) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(PromotionTimeFrame.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.promotionTimeFrameDao.save(dbBo);
        this.deactivateCategories(dbBo.getId());
        this.deactivatePromotionProduct(dbBo.getId());
        return dbBo;
    }

    public void deactivatePromotionProduct(Integer timeFrameId) {
        List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionProduct._timeFrameId), timeFrameId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        for (PromotionProduct dbBo : promotionProducts) {
            dbBo.setStatus(PromotionProduct.STATUS_INACTIVE);
            dbBo.setModifiedBy(User.getContextId());
            this.promotionProductDao.save(dbBo);
        }
    }

    public void deactivateCategories(Integer timeFrameId) {
        List<PromotionCategory> categories = this.promotionCategoryDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionCategory._timeFrameId), timeFrameId));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        for (PromotionCategory dbBo : categories) {
            dbBo.setStatus(PromotionCategory.STATUS_INACTIVE);
            dbBo.setModifiedBy(User.getContextId());
            this.promotionCategoryDao.save(dbBo);
        }
    }

    private void validatePromotionProduct(PromotionTimeFrame bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        long countProduct = promotionProductDao.count((root, query, builder) -> builder.and(
                builder.equal(root.get(PromotionProduct._timeFrameId), bo.getId())
        ));
        if (countProduct > 0) {
            bo.getErrorList().add(new HebelaError("Khung giờ này đã có sản phẩm đăng kí, không thể cập nhật hoặc xoá"));
        }
    }


    private void validateUpdateTimeFrame(PromotionTimeFrame bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        PromotionTimeFrame dbBo = super.get(bo.getId());
        if (PromotionTimeFrame.STATUS_ACTIVE.equals(dbBo.getStatus())) {
            bo.getErrorList().add(new HebelaError("Khung giờ đang kích hoạt, không thể cập nhật"));
            return;
        }
        if (Instant.now().isAfter(dbBo.getEndTime())) {
            bo.getErrorList().add(new HebelaError("Khung giờ đã kết thúc, không thể cập nhật"));
        }
    }

    private void validateTimeFrame(PromotionTimeFrame bo) {
        if (bo.getErrorList() == null) {
            bo.setErrorList(new ArrayList<>());
        }
        if (bo.getPromotionId() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thông tin chương trình"));
        }
        if (bo.getDay() == null || bo.getStartTime() == null || bo.getEndTime() == null) {
            bo.getErrorList().add(new HebelaError("Thiếu thông tin khung giờ"));
        }

        Integer platformTimeFrame = bo.getPlatform();
        if (platformTimeFrame == null || platformTimeFrame == 0) {
            bo.getErrorList().add(new HebelaError("Thiếu thông tin nền tảng hiển thị."));
            return;
        }

        Promotion promotion = this.promotionDao.findById(bo.getPromotionId()).orElse(null);
        if (promotion == null) {
            bo.getErrorList().add(new HebelaError("Không tìm thấy chương trình hoặc chương trình đã bị xoá."));
            return;
        }

        Integer platformPromotion = promotion.getPlatform();
        for (Integer platform : PlatformExtend.ALL_PLATFORM) {
            if (platformPromotion != null && (platformPromotion & platform) <= 0 && (platformTimeFrame & platform) > 0) {
                bo.getErrorList().add(new HebelaError("Chương trình không áp dụng cho nền tảng này. Vui lòng xem lại thông tin chương trình"));
                return;
            }
        }

        if (bo.getStartTime().getEpochSecond() < promotion.getStartTime().getEpochSecond()
                || bo.getStartTime().getEpochSecond() > promotion.getEndTime().getEpochSecond()
                || bo.getEndTime().getEpochSecond() < promotion.getStartTime().getEpochSecond()
                || bo.getEndTime().getEpochSecond() > promotion.getEndTime().getEpochSecond()) {
            bo.getErrorList().add(new HebelaError("Khung giờ nằm ngoài thời gian diễn ra chương trình."));
            return;
        }

        Instant now = Instant.now();
        if (bo.getStartTime().isBefore(now) || bo.getEndTime().isBefore(now)) {
            bo.getErrorList().add(new HebelaError("Khung giờ phải lớn hơn thời gian hiện tại."));
            return;
        }

        List<PromotionTimeFrame> promotionTimeFrames = this.promotionTimeFrameDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(PromotionTimeFrame._promotionId), bo.getPromotionId()));
            if (bo.getId() != null) {
                predicates.add(builder.notEqual(root.get(PromotionTimeFrame._id), bo.getId()));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        });

        for (PromotionTimeFrame timeFrameDb : promotionTimeFrames) {
            if (timeFrameDb.getDay().equals(bo.getDay())) {
                if (bo.getStartTime().getEpochSecond() == timeFrameDb.getStartTime().getEpochSecond()
                        || bo.getEndTime().getEpochSecond() == timeFrameDb.getEndTime().getEpochSecond()
                        || bo.getStartTime().getEpochSecond() == timeFrameDb.getEndTime().getEpochSecond()
                        || bo.getEndTime().getEpochSecond() == timeFrameDb.getStartTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Khung giờ đã bị trùng vào khung giờ trước đó."));
                    return;
                }
                if (bo.getStartTime().getEpochSecond() > timeFrameDb.getStartTime().getEpochSecond()
                        && bo.getStartTime().getEpochSecond() < timeFrameDb.getEndTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Khung giờ đã bị trùng vào khung giờ trước đó."));
                    return;
                }
                if (bo.getEndTime().getEpochSecond() < timeFrameDb.getEndTime().getEpochSecond()
                        && bo.getEndTime().getEpochSecond() > timeFrameDb.getStartTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Khung giờ đã bị trùng vào khung giờ trước đó."));
                    return;
                }
                if (bo.getStartTime().getEpochSecond() < timeFrameDb.getStartTime().getEpochSecond()
                        && bo.getEndTime().getEpochSecond() > timeFrameDb.getEndTime().getEpochSecond()) {
                    bo.getErrorList().add(new HebelaError("Khung giờ đã bị trùng vào khung giờ trước đó."));
                    return;
                }
            }
        }
    }
}
