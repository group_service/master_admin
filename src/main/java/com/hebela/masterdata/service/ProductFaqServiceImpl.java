package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.masterdata.bo.ProductFaq;
import com.hebela.masterdata.bo.ProductFaqLink;
import com.hebela.masterdata.dao.ProductComboDao;
import com.hebela.masterdata.dao.ProductDao;
import com.hebela.masterdata.dao.ProductFaqDao;
import com.hebela.masterdata.dao.ProductFaqLinkDao;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductFaqServiceImpl extends AbstractStatusService<ProductFaq> implements ProductFaqService {
    private final ProductFaqDao productFaqDao;
    private final ProductFaqLinkDao productFaqLinkDao;
    private final ProductDao productDao;
    private final ProductComboDao productComboDao;

    public static final int PRIORITY_BEGIN = 0;

    public ProductFaqServiceImpl(ProductFaqDao productFaqDao, ProductFaqLinkDao productFaqLinkDao, ProductDao productDao, ProductComboDao productComboDao) {
        this.productFaqDao = productFaqDao;
        this.productFaqLinkDao = productFaqLinkDao;
        this.productDao = productDao;
        this.productComboDao = productComboDao;
    }

    @Override
    public ProductFaq doInsert(ProductFaq bo) {
        bo.setPriority(PRIORITY_BEGIN);
        return super.doInsert(bo);
    }

    @Override
    public ProductFaq get(Integer productFaqId) {
        ProductFaq dbBo = super.get(productFaqId);
        if (dbBo != null) {
            ProductFaqLink productFaqLink = new ProductFaqLink();
            productFaqLink.setProductFaqId(dbBo.getId());
            List<ProductFaqLink> productFaqLinkList = productFaqLinkDao.findAll(Example.of(productFaqLink));
            if (productFaqLinkList.size() > 0) {
                List<Integer> productIdList = productFaqLinkList.stream().filter(_productFaqLink -> _productFaqLink.getProductId() != null).map(ProductFaqLink::getProductId).collect(Collectors.toList());
                List<Integer> productComboIdList = productFaqLinkList.stream().filter(_productFaqLink -> _productFaqLink.getProductComboId() != null).map(ProductFaqLink::getProductComboId).collect(Collectors.toList());
                if (productIdList.size() > 0) {
                    dbBo.setProductList(productDao.findAllById(productIdList));
                }
                if (productComboIdList.size() > 0) {
                    dbBo.setProductComboList(productComboDao.findAllById(productComboIdList));
                }
            }
        }
        return dbBo;
    }

    @Override
    public List<ProductFaq> find(ProductFaq bo) {
        return productFaqDao.findAll(Example.of(bo), Sort.by(Sort.Order.asc(ProductFaq._priority),
                Sort.Order.desc(ProductFaq._createdDate)));
    }

    @Override
    public ProductFaq doAddProduct(ProductFaq bo) {
        ProductFaq dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getProductList() == null || bo.getProductList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        List<ProductFaqLink> insertList = new ArrayList<>();
        bo.getProductList().forEach(item -> {
            ProductFaqLink productFaqLink = new ProductFaqLink();
            productFaqLink.setProductFaqId(bo.getId());
            productFaqLink.setProductId(item.getId());
            insertList.add(productFaqLink);
        });
        updateProductOfProductFaq(dbBo.getId(), insertList);

        return dbBo;
    }

    private void updateProductOfProductFaq(Integer productFaqId, List<ProductFaqLink> clientProductList) {
        List<ProductFaqLink> dbRecommendationLinks = getProductFaqLinkList(productFaqId);
        List<Integer> dbProductIdList = dbRecommendationLinks.stream().map(ProductFaqLink::getProductId).collect(Collectors.toList());
        for (ProductFaqLink item : clientProductList) {
            if (!dbProductIdList.contains(item.getProductId())) {
                this.productFaqLinkDao.save(item);
            }
        }
    }

    @Override
    public ProductFaq doRemoveProduct(ProductFaq bo) {
        ProductFaq dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        ProductFaqLink productFaqLink = new ProductFaqLink();
        productFaqLink.setProductFaqId(bo.getId());
        productFaqLink.setProductId(bo.getProduct().getId());
        List<ProductFaqLink> productFaqLinkList = productFaqLinkDao.findAll(Example.of(productFaqLink));
        if (productFaqLinkList.size() > 0) {
            productFaqLinkDao.deleteAll(productFaqLinkList);
        }
        return dbBo;
    }

    @Override
    public ProductFaq doAddCombo(ProductFaq bo) {
        ProductFaq dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getProductComboList() == null || bo.getProductComboList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        List<ProductFaqLink> insertList = new ArrayList<>();
        bo.getProductComboList().forEach(item -> {
            ProductFaqLink productFaqLink = new ProductFaqLink();
            productFaqLink.setProductFaqId(bo.getId());
            productFaqLink.setProductComboId(item.getId());
            insertList.add(productFaqLink);
        });
        updateComboOfProductFaq(dbBo.getId(), insertList);
        return dbBo;
    }

    private void updateComboOfProductFaq(Integer recommendationId, List<ProductFaqLink> clientComboList) {
        List<ProductFaqLink> dbRecommendationLinks = getProductFaqLinkList(recommendationId);
        List<Integer> dbComboIdList = dbRecommendationLinks.stream().map(ProductFaqLink::getProductComboId).collect(Collectors.toList());
        for (ProductFaqLink item : clientComboList) {
            if (!dbComboIdList.contains(item.getProductComboId())) {
                this.productFaqLinkDao.save(item);
            }
        }
    }

    @Override
    public ProductFaq doRemoveCombo(ProductFaq bo) {
        ProductFaq dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        ProductFaqLink productFaqLink = new ProductFaqLink();
        productFaqLink.setProductFaqId(bo.getId());
        productFaqLink.setProductComboId(bo.getProductCombo().getId());
        List<ProductFaqLink> productFaqLinkList = productFaqLinkDao.findAll(Example.of(productFaqLink));
        if (productFaqLinkList.size() > 0) {
            productFaqLinkDao.deleteAll(productFaqLinkList);
        }
        return dbBo;
    }

    private List<ProductFaqLink> getProductFaqLinkList(Integer productFaqId) {
        ProductFaqLink example = new ProductFaqLink();
        example.setProductFaqId(productFaqId);
        return this.productFaqLinkDao.findAll(Example.of(example));
    }


}
