package com.hebela.masterdata.service;

import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.web.coupon.CouponSearchRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CouponServiceImpl extends BaseStatusService<Coupon> implements CouponService {
    private final CouponProductLinkDao couponProductLinkDao;
    private final CouponGiftLinkDao couponGiftLinkDao;
    private final GiftAccountLinkDao giftAccountLinkDao;
    private final ProductDao productDao;
    private final MetaConfigDao metaConfigDao;
    private final ProductGiftDao productGiftDao;
    private final CampaignDao campaignDao;
    private final PromotionDao promotionDao;
    private final PromotionProductDao promotionProductDao;

    public CouponServiceImpl(CouponProductLinkDao couponProductLinkDao, CouponGiftLinkDao couponGiftLinkDao,
                             GiftAccountLinkDao giftAccountLinkDao, ProductDao productDao, MetaConfigDao metaConfigDao,
                             ProductGiftDao productGiftDao, CampaignDao campaignDao, PromotionDao promotionDao, PromotionProductDao promotionProductDao) {
        this.couponProductLinkDao = couponProductLinkDao;
        this.couponGiftLinkDao = couponGiftLinkDao;
        this.giftAccountLinkDao = giftAccountLinkDao;
        this.productDao = productDao;
        this.metaConfigDao = metaConfigDao;
        this.productGiftDao = productGiftDao;
        this.campaignDao = campaignDao;
        this.promotionDao = promotionDao;
        this.promotionProductDao = promotionProductDao;
    }

    @Override
    public Coupon get(Integer key) {
        Coupon coupon = super.get(key);
        coupon.setProductList(
                this.productDao.findAll((root, query, builder) -> {
                    Root<ProductPrice> priceRoot = query.from(ProductPrice.class);
                    Root<CouponProductLink> couponProductLinkRoot = query.from(CouponProductLink.class);
                    query.multiselect(root, priceRoot);
                    query.orderBy(builder.asc(couponProductLinkRoot.get(CouponProductLink._createdDate)));
                    return builder.and(
                            builder.equal(root.get(Product._id), couponProductLinkRoot.get(CouponProductLink._productId)),
                            builder.equal(couponProductLinkRoot.get(CouponProductLink._couponId), key),
                            builder.equal(root.get(Product._id), priceRoot.get(ProductPrice._productId)),
                            builder.lessThanOrEqualTo(priceRoot.get(ProductPrice._startDate), Instant.now()),
                            builder.greaterThan(priceRoot.get(ProductPrice._endDate), Instant.now())
                    );
                }));
        coupon.setGiftList(
                this.productDao.findAll((root, query, builder) -> {
                    Root<ProductPrice> priceRoot = query.from(ProductPrice.class);
                    Root<CouponGiftLink> couponGiftLinkRoot = query.from(CouponGiftLink.class);
                    query.multiselect(root, priceRoot);
                    query.orderBy(builder.asc(couponGiftLinkRoot.get(CouponGiftLink._createdDate)));
                    return builder.and(
                            builder.equal(root.get(Product._id), couponGiftLinkRoot.get(CouponGiftLink._productId)),
                            builder.equal(couponGiftLinkRoot.get(CouponGiftLink._couponId), key),
                            builder.equal(root.get(Product._id), priceRoot.get(ProductPrice._productId)),
                            builder.lessThanOrEqualTo(priceRoot.get(ProductPrice._startDate), Instant.now()),
                            builder.greaterThan(priceRoot.get(ProductPrice._endDate), Instant.now())
                    );
                })
        );
        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(key, MetaConfig.TYPE_COUPON)));
        dbMetaConfigOptional.ifPresent(coupon::setMetaConfig);
        return coupon;
    }

    /*
     * Tạo mới coupon
     * Nếu Lưu nháp thì không cần check các sản phẩm và quà tặng hợp lệ
     * Nếu Lưu và Kích hoạt luôn thì cần check sản phẩm và quà tặng hợp lệ    *
     * */
    @Override
    public Coupon doInsert(Coupon bo) {
        if (bo.getStatus() == null) {
            bo.setStatus(Coupon.STATUS_INACTIVE);
        }
        bo.setIsTrash(Boolean.FALSE);
        if (bo.getCouponNumber() == null) {
            bo.setCouponNumber(0);
        }
        bo.setCouponUsed(0);
        Coupon dbBo = super.doInsert(bo);
        validateCouponInput(dbBo);
        if (Coupon.STATUS_ACTIVE.equals(bo.getStatus())) {
            List<Integer> productIdList = null;
            List<Integer> productGiftIdList = null;
            if (bo.getProductList() != null) {
                productIdList = bo.getProductList().stream().map(Product::getId).collect(Collectors.toList());
            }
            if (bo.getGiftList() != null) {
                productGiftIdList = bo.getGiftList().stream().map(Product::getId).collect(Collectors.toList());
            }
            Coupon couponResponse = validProductForCoupon(dbBo, productIdList, productGiftIdList);
            if (couponResponse.getErrorList() != null) {
                throw new HebelaException(couponResponse.getErrorList().get(0).getMessage());
            }
        }
        updateProductOfCoupon(dbBo.getId(), bo.getProductList());
        if (Coupon.DISCOUNT_TYPE_GIFT.equals(bo.getDiscountType())) {
            updateGiftOfCoupon(dbBo.getId(), bo.getGiftList());
        }
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setCouponId(dbBo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_COUPON);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        return new Coupon();
    }

    /*
     * Cập nhật coupon
     * Coupon không được hủy
     * Cho phép cập nhật các thông tin cơ bản, mô tả, điều kiện áp dụng, metaconfig
     * Không cập nhật type và discountType, chỉ cập nhật các giá trị tương ứng với mỗi type
     * Nếu coupon đang active cần kiểm tra số quà tặng không được lớn hơn danh sách quà tặng đang có
     * Không cập nhật liên kết tới sản phẩm và quà tặng
     * */
    @Override
    public Coupon doUpdate(Coupon bo) {
        Coupon dbBo = get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        if (Coupon.STATUS_ACTIVE.equals(dbBo.getStatus())) {
            CouponProductLink productLink = new CouponProductLink();
            productLink.setCouponId(dbBo.getId());
            List<Integer> productIdList = this.couponProductLinkDao.findAll(Example.of(productLink)).stream().map(CouponProductLink::getProductId).collect(Collectors.toList());
            this.checkProductInFlashSale(dbBo, productIdList);
            if (dbBo.getErrorList() != null && dbBo.getErrorList().size() > 0) {
                return dbBo;
            }
        }
        dbBo.setName(bo.getName());
        dbBo.setCode(bo.getCode());
        dbBo.setSlug(bo.getSlug());
        dbBo.setCouponLabel(bo.getCouponLabel());
        dbBo.setCouponLabelValue(bo.getCouponLabelValue());
        dbBo.setDescription(bo.getDescription());
        dbBo.setDetail(bo.getDetail());
        dbBo.setImageThumbnail(bo.getImageThumbnail());
        dbBo.setImageFeature(bo.getImageFeature());
        dbBo.setImageBanner(bo.getImageBanner());
        dbBo.setImageBannerMobile(bo.getImageBannerMobile());
        dbBo.setImagePromotion(bo.getImagePromotion());
        dbBo.setPlatform(bo.getPlatform());
        dbBo.setPlatformDisplay(bo.getPlatformDisplay());
        dbBo.setCouponHint(bo.getCouponHint());
        if (!dbBo.getStatus().equals(Coupon.STATUS_ACTIVE)) {
            dbBo.setStartTime(bo.getStartTime());
            dbBo.setEndTime(bo.getEndTime());
        }
        if (bo.getCouponNumber() == null) {
            bo.setCouponNumber(0);
        }
        if (bo.getCouponNumber() < dbBo.getCouponUsed()) {
            dbBo.setCouponNumber(dbBo.getCouponUsed());
        } else {
            dbBo.setCouponNumber(bo.getCouponNumber());
        }
        if (dbBo.getType().equals(Coupon.TYPE_BIG_VALUE)) {
            dbBo.setBuyMoneyMin(bo.getBuyMoneyMin());
        } else {
            dbBo.setBuyQuantityMin(bo.getBuyQuantityMin());
        }
        switch (dbBo.getDiscountType()) {
            case Coupon.DISCOUNT_TYPE_PERCENT:
                dbBo.setDiscountPercent(bo.getDiscountPercent());
                dbBo.setDiscountMoneyMax(bo.getDiscountMoneyMax());
                break;
            case Coupon.DISCOUNT_TYPE_MONEY:
                dbBo.setDiscountMoney(bo.getDiscountMoney());
                break;
            case Coupon.DISCOUNT_TYPE_GIFT:
            case Coupon.DISCOUNT_TYPE_GIFT_SAME:
                dbBo.setNumberOfGift(bo.getNumberOfGift());
                break;
            default:
                break;
        }
        validateCouponInput(dbBo);
        dbBo.setModifiedBy(User.getContextId());
        couponDao.save(dbBo);
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_COUPON, User.getContextId(), bo.getMetaConfig());
        }
        return dbBo;
    }

    /*
     * Kích hoạt coupon
     * Coupon phải không hủy, ở trạng chái chưa kích hoạt và các sản phẩm hợp lệ
     * */
    @Override
    public Coupon doActivate(Coupon bo) {
        Coupon dbBo = get(bo.getId());
        if (dbBo.getStatus().equals(Coupon.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        CouponProductLink productLink = new CouponProductLink();
        productLink.setCouponId(dbBo.getId());
        List<Integer> productIdList = this.couponProductLinkDao.findAll(Example.of(productLink)).stream().map(CouponProductLink::getProductId).collect(Collectors.toList());
        CouponGiftLink giftLink = new CouponGiftLink();
        giftLink.setCouponId(dbBo.getId());
        List<Integer> productGiftIdList = this.couponGiftLinkDao.findAll(Example.of(giftLink)).stream().map(CouponGiftLink::getProductId).collect(Collectors.toList());
        Coupon coupon = validProductForCoupon(dbBo, productIdList, productGiftIdList);
        if (coupon.getErrorList() == null) {
            if (Coupon.DISCOUNT_TYPE_GIFT.equals(dbBo.getDiscountType())) {
                if (dbBo.getGiftList() == null || dbBo.getGiftList().size() < dbBo.getNumberOfGift()) {
                    throw new HebelaException("coupon.gift.less.size");
                }
            }
            dbBo.setStatus(Coupon.STATUS_ACTIVE);
            dbBo.setModifiedBy(User.getContextId());
            this.couponDao.save(dbBo);
        }
        return coupon;
    }

    /*
     * Hủy kích hoạt coupon
     * Pre: Coupon phải không hủy, ở trạng chái kích hoạt
     * */
    @Override
    public Coupon doDeactivate(Coupon bo) {
        Coupon dbBo = super.get(bo.getId());
        if (!dbBo.getStatus().equals(Coupon.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Coupon.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.couponDao.save(dbBo);
        return bo;
    }

    @Override
    public Coupon doTrash(Coupon bo) {
        Coupon dbBo = get(bo.getId());
        dbBo.setIsTrash(true);
        dbBo.setStatus(Coupon.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    /*
     * Thêm sản phẩm áp dụng vào coupon
     * Pre:
     *      - Coupon không được hủy
     *      - Số sản phẩm thêm phải > 0 và hợp lệ
     * */
    @Override
    public Coupon doAddProduct(Coupon bo) {
        Coupon dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getProductList() == null || bo.getProductList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        Coupon coupon = validProductForCoupon(dbBo, bo.getProductList().stream().map(Product::getId).collect(Collectors.toList()), null);
        if (coupon.getErrorList() == null) {
            updateProductOfCoupon(dbBo.getId(), bo.getProductList());
        }
        return coupon;
    }

    /*
     * Remove sản phẩm khỏi coupon
     * Pre: Coupon không được hủy
     * */
    @Override
    public Coupon doRemoveProduct(Coupon bo) {
        Coupon dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getProduct() == null || bo.getProduct().getId() == null) {
            throw new HebelaException("action.invalid");
        }
        CouponProductLink couponProductLink = new CouponProductLink();
        couponProductLink.setCouponId(bo.getId());
        couponProductLink.setProductId(bo.getProduct().getId());
        List<CouponProductLink> couponProductLinkList = couponProductLinkDao.findAll(Example.of(couponProductLink));
        if (couponProductLinkList.size() > 0) {
            couponProductLinkDao.deleteAll(couponProductLinkList);
        }
        return bo;
    }

    /*
     * Thêm sản phẩm áp dụng vào coupon
     * Pre:
     *      - Coupon không được hủy
     *      - Số sản phẩm quà tặng thêm phải >0 và hợp lệ
     * */
    @Override
    public Coupon doAddProductGift(Coupon bo) {
        Coupon dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getGiftList() == null || bo.getGiftList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        Coupon coupon = validProductForCoupon(dbBo, new ArrayList<>(), bo.getGiftList().stream().map(Product::getId).collect(Collectors.toList()));
        if (coupon.getErrorList() == null) {
            updateGiftOfCoupon(dbBo.getId(), bo.getGiftList());
        }
        return coupon;
    }

    /*
     * Remove quà tặng khỏi coupon
     * Pre:
     *      - Coupon không được ở trạng thái hủy và có discountType = gift
     *      - Nếu coupon đang được kích hoạt và có discountType = gift thì kiểm tra số quà tặng phải lớn hơn danh sách quà tặng
     * */
    @Override
    public Coupon doRemoveProductGift(Coupon bo) {
        Coupon dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getProduct() == null || bo.getProduct().getId() == null) {
            throw new HebelaException("action.invalid");
        }
        CouponGiftLink couponGiftLink = new CouponGiftLink();
        couponGiftLink.setCouponId(bo.getId());
        List<CouponGiftLink> couponGiftLinkList = couponGiftLinkDao.findAll(Example.of(couponGiftLink));
        if (Coupon.DISCOUNT_TYPE_GIFT.equals(dbBo.getDiscountType())) {
            if (couponGiftLinkList.size() <= dbBo.getNumberOfGift()) {
                throw new HebelaException("coupon.gift.less.size");
            }
        }
        CouponGiftLink dbCouponGiftLink = couponGiftLinkList.stream().filter(item -> item.getProductId().equals(bo.getProduct().getId())).findFirst().orElse(null);
        if (dbCouponGiftLink != null) {
            this.couponGiftLinkDao.delete(dbCouponGiftLink);
            this.giftAccountLinkDao.deleteAllByCouponIdAndProductId(bo.getId(), bo.getProduct().getId());
        }
        return bo;
    }

    /*
     * Khôi phục coupon
     * Pre: Coupon phải ở trạng thái hủy
     * Post: Coupon mới khôi phục sẽ ở trạng thái chưa kích hoạt
     * */
    @Override
    public Coupon doRestore(Coupon bo) {
        Coupon dbBo = super.get(bo.getId());
        if (!Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        Campaign campaign = campaignDao.findById(dbBo.getCampaignId()).orElse(null);
        if (campaign == null || Boolean.TRUE.equals(campaign.getIsTrash())) {
            throw new HebelaException("campaign.not.exists");
        }
        dbBo.setStatus(Coupon.STATUS_INACTIVE);
        dbBo.setIsTrash(Boolean.FALSE);
        dbBo.setModifiedBy(User.getContextId());
        return couponDao.save(dbBo);
    }

    /*
     * Xóa vĩnh viễn coupon
     * Pre: Coupon phải ở trạng thái hủy và chưa được sử dụng: couponUsed = 0
     * */
    @Override
    public Coupon doDelete(Coupon bo) {
        Coupon dbBo = super.get(bo.getId());
        if (!Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        if (dbBo.getCouponUsed() > 0) {
            throw new HebelaException("Coupon đã được sử dụng không được xóa");
        }
        return super.doDelete(dbBo);
    }

    /*
     * Validate các trường không auto trong db
     * */
    private void validateCouponInput(Coupon bo) {
        if (Coupon.TYPE_BIG_VALUE.equals(bo.getType())) {
            if (bo.getBuyMoneyMin() == null || bo.getBuyMoneyMin() <= 0) {
                throw new HebelaException("Số tiền mua tối thiểu không hợp lệ");
            }
        } else {
            if (bo.getBuyQuantityMin() == null || bo.getBuyQuantityMin() <= 0) {
                throw new HebelaException("Số lượng mua tối thiểu không hợp lệ");
            }
        }
        if (Coupon.DISCOUNT_TYPE_PERCENT.equals(bo.getDiscountType())) {
            if (bo.getDiscountPercent() == null || bo.getDiscountPercent() < 1 || bo.getDiscountPercent() > 99) {
                throw new HebelaException("% giảm không hợp lệ");
            }
            if (bo.getDiscountMoneyMax() != null && bo.getDiscountMoneyMax() <= 0) {
                throw new HebelaException("Tiền giảm tối đa không hợp lệ");
            }
        } else if (Coupon.DISCOUNT_TYPE_MONEY.equals(bo.getDiscountType())) {
            if (bo.getDiscountMoney() == null || bo.getDiscountMoney() <= 0) {
                throw new HebelaException("Số tiền giảm không hợp lệ");
            }
        } else if (Coupon.DISCOUNT_TYPE_GIFT.equals(bo.getDiscountType())) {
            if (bo.getNumberOfGift() == null || bo.getNumberOfGift() <= 0) {
                throw new HebelaException("Số quà tặng không hợp lệ");
            }
        } else if (Coupon.DISCOUNT_TYPE_GIFT_SAME.equals(bo.getDiscountType())) {
            if (bo.getNumberOfGift() == null || bo.getNumberOfGift() <= 0) {
                throw new HebelaException("Số sản phẩm tặng thêm không hợp lệ");
            }
        }
        if (!bo.getEndTime().isAfter(bo.getStartTime())) {
            throw new HebelaException("Ngày kết thúc phải lớn hơn ngày bắt đầu");
        }
        if (Coupon.STATUS_ACTIVE.equals(bo.getStatus())) {
            if (Coupon.DISCOUNT_TYPE_GIFT.equals(bo.getDiscountType())) {
                if (bo.getGiftList() == null || bo.getGiftList().size() < bo.getNumberOfGift()) {
                    throw new HebelaException("coupon.gift.less.size");
                }
            }
        }
    }

    private Map<Integer, Promotion> findProductInFlashSale(Coupon coupon, List<Integer> productIds) {
        Map<Integer, Promotion> result = new HashMap<>();
        if (productIds.size() > 0) {
            List<PromotionProduct> promotionProducts = this.promotionProductDao.findAll((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                predicates.add((root.get(PromotionProduct._productId).in(productIds)));
                predicates.add(builder.equal(root.get(PromotionProduct._status), Promotion.STATUS_ACTIVE));
                return builder.and(predicates.toArray(new Predicate[0]));
            });
            if (promotionProducts.size() > 0) {
                List<Integer> promotionIds = promotionProducts.stream().map(PromotionProduct::getPromotionId).distinct().collect(Collectors.toList());
                List<Promotion> promotionList = promotionDao.findAll((root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    predicates.add(root.get(Promotion._id).in(promotionIds));
                    predicates.add(builder.equal(root.get(Promotion._isTrash), Boolean.FALSE));
                    predicates.add(builder.equal(root.get(Promotion._status), Promotion.STATUS_ACTIVE));
                    predicates.add(
                            builder.or(builder.between(root.get(Promotion._startTime),
                                    coupon.getStartTime(),
                                    coupon.getEndTime()
                                    ),
                                    builder.between(root.get(Promotion._endTime),
                                            coupon.getStartTime(),
                                            coupon.getEndTime()
                                    ),
                                    builder.and(
                                            builder.lessThanOrEqualTo(root.get(Promotion._startTime), coupon.getStartTime()),
                                            builder.greaterThanOrEqualTo(root.get(Promotion._endTime), coupon.getEndTime())
                                    )
                            )
                    );
                    query.orderBy(builder.asc(root.get(Promotion._id)));
                    return builder.and(predicates.toArray(new Predicate[0]));
                });

                Map<Integer, Promotion> promotionMap = promotionList.stream().collect(Collectors.toMap(Promotion::getId, promotion -> promotion));
                for (PromotionProduct promotionProduct : promotionProducts) {
                    if (promotionMap.get(promotionProduct.getPromotionId()) != null) {
                        result.put(promotionProduct.getProductId(), promotionMap.get(promotionProduct.getPromotionId()));
                    }
                }
            }
        }
        return result;
    }

    private void checkProductInFlashSale(Coupon dbCoupon, List<Integer> productIdList) {
        if (productIdList.size() > 0) {
            List<HebelaError> hebelaErrorList = new ArrayList<>();
            Map<Integer, Promotion> promotionMap = findProductInFlashSale(dbCoupon, productIdList);
            Map<Integer, Product> productMap = this.productDao.findAllById(productIdList).stream().collect(Collectors.toMap(Product::getId, product -> product));
            for (Integer productId : productIdList) {
                Product dbProduct = productMap.get(productId);
                Promotion promotion = promotionMap.get(productId);
                if (promotion != null) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] đang tham gia Flash sale [" + promotion.getName() + "] nên không áp dụng coupon"));
                }
            }
            if (hebelaErrorList.size() > 0) {
                dbCoupon.setErrorList(hebelaErrorList);
            }
        }
    }

    private Coupon validProductForCoupon(Coupon dbCoupon, List<Integer> productIdList, List<Integer> productGiftIdList) {
        Coupon coupon = new Coupon();
        List<HebelaError> hebelaErrorList = new ArrayList<>();
        Map<Integer, Promotion> promotionMap = findProductInFlashSale(dbCoupon, productIdList);
        Map<Integer, Product> productMap = this.productDao.findAllById(productIdList).stream().collect(Collectors.toMap(Product::getId, product -> product));
        if (productIdList.size() > 0) {
            for (Integer productId : productIdList) {
                Product dbProduct = productMap.get(productId);
                /*Kiểm tra sản phẩm có thuộc flash sale hay không*/
                Promotion promotion = promotionMap.get(productId);
                if (promotion != null) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] đang tham gia Flash sale [" + promotion.getName() + "] nên không áp dụng coupon"));
                    continue;
                }
                /*Kiểm tra sản phẩm có đang đăng bán không*/
                if (dbProduct.getType().equals(Product.TYPE_PRODUCT_IN_COMBO)) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] là sản phẩm trong combo nên không áp dụng coupon"));
                    continue;
                }
                if (!dbProduct.getStatus().equals(Product.STATUS_ACTIVE)
                        || dbProduct.getIsTrash().equals(Boolean.TRUE)
                        || !dbProduct.getType().equals(Product.TYPE_SELL)) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] không đăng bán"));
                    continue;
                }
                /**
                 * kiểm tra tất cả các sản phẩm 'của coupon này' có đang có quà tặng hay không
                 * */
                if (checkProductHasGift(productId)) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + "] đang có quà tặng"));
                    continue;
                }
                /**
                 * kiểm tra tất cả các sản phẩm 'của coupon này' có đang là sản phẩm ở các coupon đã active khác không
                 * */
                List<Coupon> activeCouponList = checkIsProductInActiveCoupon(dbCoupon, productId);
                if (activeCouponList.size() > 0) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] đang nằm trong coupon ["
                            + activeCouponList.get(0).getName() + "] đang hoạt động"));
                    continue;
                }
                /**
                 * kiểm tra tất cả các sản phẩm của coupon này có đang là quà ở các coupon đã active khác không?
                 * */
                activeCouponList = checkIsGiftInActiveCoupon(dbCoupon, productId);
                if (activeCouponList.size() > 0) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] đang là quà tặng của coupon ["
                            + activeCouponList.get(0).getName() + "] đang hoạt động"));
                    continue;
                }

                /*Kiểm tra sản phẩm và quà tặng có cùng nằm trong 1 coupon hay không*/
                if (checkProductIsGiftInCoupon(dbCoupon.getId(), productId)) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] không được vừa là sản phẩm áp dụng, vừa là quà tặng"));
                }
            }
        }
        if (productGiftIdList != null && productGiftIdList.size() > 0) {
            for (Integer productId : productGiftIdList) {
                /*Kiểm tra sản phẩm có đang đăng bán không*/
                Product dbProduct = productDao.findById(productId).get();
                if (dbProduct.getType().equals(Product.TYPE_PRODUCT_IN_COMBO)) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] là sản phẩm trong combo nên không là quà tặng coupon"));
                    continue;
                }
                if (!dbProduct.getStatus().equals(Product.STATUS_ACTIVE)
                        || dbProduct.getIsTrash().equals(Boolean.TRUE)) {
                    hebelaErrorList.add(new HebelaError("Quà tặng [ SKU - " + dbProduct.getSku() + " ] không hoạt động"));
                    continue;
                }
                /**
                 * kiểm tra tất cả các quà tặng của coupon này có đang là sản phẩm ở các coupon đã active khác không
                 * */
                List<Coupon> activeCouponList = checkIsProductInActiveCoupon(dbCoupon, productId);
                if (activeCouponList.size() > 0) {
                    hebelaErrorList.add(new HebelaError("Quà tặng [ SKU - " + dbProduct.getSku() + " ] đang là sản phẩm của coupon ["
                            + activeCouponList.get(0).getName() + "] đang hoạt động"));
                    continue;
                }
                /*Kiểm tra sản phẩm và quà tặng có cùng nằm trong 1 coupon hay không*/
                if (checkGiftIsProductInCoupon(dbCoupon.getId(), productId)) {
                    hebelaErrorList.add(new HebelaError("Sản phẩm [ SKU - " + dbProduct.getSku() + " ] không được vừa là sản phẩm áp dụng, vừa là quà tặng"));
                }
            }
        }
        if (hebelaErrorList.size() > 0) {
            coupon.setErrorList(hebelaErrorList);
        }
        return coupon;
    }

    private boolean checkProductHasGift(Integer productId) {
        long count = this.productGiftDao.count((root, query, builder) -> builder.and(builder.equal(root.get(ProductGift._productId), productId)));
        return count > 0;
    }

    /**
     * Kiểm tra sản phẩm có là quà tặng trong 1 coupon hay không
     */
    private boolean checkProductIsGiftInCoupon(Integer couponId, Integer productId) {
        long count = this.couponGiftLinkDao.count((root, query, builder) ->
                builder.and(
                        builder.equal(root.get(CouponGiftLink._couponId), couponId),
                        builder.equal(root.get(CouponGiftLink._productId), productId)
                )
        );
        return count > 0;
    }

    /**
     * Kiểm tra quà tặng có là sản phẩm trong 1 coupon hay không
     */
    private boolean checkGiftIsProductInCoupon(Integer couponId, Integer productId) {
        long count = this.couponProductLinkDao.count((root, query, builder) ->
                builder.and(
                        builder.equal(root.get(CouponProductLink._couponId), couponId),
                        builder.equal(root.get(CouponProductLink._productId), productId)
                )
        );
        return count > 0;
    }

    /**
     * kiểm tra sản phẩm 'của coupon' có đang là 'sản phẩm' ở các coupon đã active khác không
     * - Coupon đã active
     * - Kiểm tra theo khoảng thời gian
     * - Coupon không nằm trong Trash
     */
    private List<Coupon> checkIsProductInActiveCoupon(Coupon coupon, Integer productId) {
        return this.couponDao.findAll(
                (root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    Root<CouponProductLink> couponProductLinkRoot = query.from(CouponProductLink.class);
                    query.distinct(true);
                    predicates.add(builder.equal(root.get(Coupon._status), Coupon.STATUS_ACTIVE));
                    predicates.add(builder.equal(root.get(Coupon._id), couponProductLinkRoot.get(CouponProductLink._couponId)));
                    /** chỉ tìm trong coupon khác nên sử dụng notEqual*/
                    predicates.add(builder.notEqual(root.get(Coupon._id), coupon.getId()));
                    predicates.add(builder.equal(couponProductLinkRoot.get(CouponProductLink._productId), productId));
                    predicates.add(builder.greaterThan(root.get(Coupon._endTime), Instant.now()));
                    predicates.add(builder.notEqual(root.get(Coupon._isTrash), Boolean.TRUE));
                    predicates.add(
                            builder.or(builder.between(root.get(Coupon._startTime),
                                    coupon.getStartTime(),
                                    coupon.getEndTime()
                                    ),
                                    builder.between(root.get(Coupon._endTime),
                                            coupon.getStartTime(),
                                            coupon.getEndTime()
                                    ),
                                    builder.and(
                                            builder.lessThanOrEqualTo(root.get(Coupon._startTime), coupon.getStartTime()),
                                            builder.greaterThanOrEqualTo(root.get(Coupon._endTime), coupon.getStartTime())
                                    )
                            )
                    );
                    return builder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
        );
    }

    /**
     * Kiểm tra sản phẩm 'của coupon' có đang là 'quà' của coupon active không\
     * - Coupon đã active
     * - Kiểm tra theo khoảng thời gian
     * - Coupon không nằm trong Trash
     */
    private List<Coupon> checkIsGiftInActiveCoupon(Coupon coupon, Integer productId) {
        return this.couponDao.findAll(
                (root, query, builder) -> {
                    final List<Predicate> predicates = new ArrayList<>();
                    Root<CouponGiftLink> couponGiftLinkRoot = query.from(CouponGiftLink.class);
                    query.distinct(true);
                    predicates.add(builder.equal(root.get(Coupon._status), Coupon.STATUS_ACTIVE));
                    predicates.add(builder.equal(root.get(Coupon._id), couponGiftLinkRoot.get(CouponGiftLink._couponId)));
                    predicates.add(builder.notEqual(root.get(Coupon._id), coupon.getId()));
                    predicates.add(builder.equal(couponGiftLinkRoot.get(CouponGiftLink._productId), productId));
                    predicates.add(builder.notEqual(root.get(Coupon._isTrash), Boolean.TRUE));
                    predicates.add(builder.greaterThan(root.get(Coupon._endTime), Instant.now()));
                    predicates.add(
                            builder.or(builder.between(root.get(Coupon._startTime),
                                    coupon.getStartTime(),
                                    coupon.getEndTime()
                                    ),
                                    builder.between(root.get(Coupon._endTime),
                                            coupon.getStartTime(),
                                            coupon.getEndTime()
                                    ),
                                    builder.and(
                                            builder.lessThanOrEqualTo(root.get(Coupon._startTime), coupon.getStartTime()),
                                            builder.greaterThanOrEqualTo(root.get(Coupon._endTime), coupon.getStartTime())
                                    )
                            )
                    );
                    return builder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
        );
    }

    public Coupon getCouponDetail(Integer platform, Object object, Pageable pageRequest) {
        return this.getCouponDetail(platform, object, null, null, null, null, null, pageRequest);
    }

    public Coupon getCouponDetail(Integer platform, Object object, Integer categoryId, List<Integer> brandIdList,
                                  String sortDirection, String sortBy, Integer rating, Pageable pageRequest) {
        Coupon dbBo = null;
        if (object instanceof Coupon) {
            dbBo = getCouponActiveByCouponId((Coupon) object, platform);
        } else if (object instanceof Product) {
            dbBo = getCouponActiveByProductId(((Product) object).getId(), null, platform);
        }
        if (dbBo == null) {
            throw new HebelaException("object.missing");
        }
        if (!Coupon.TYPE_NONE.equals(dbBo.getType())) {
            Page<Product> productPage = findProductOfCoupon(dbBo.getId(), categoryId, brandIdList, sortDirection,
                    sortBy, rating, pageRequest);
            String couponLabelValue = dbBo.getCouponLabelValue();
            productPage.getContent().forEach(product -> product.setCouponLabelValue(couponLabelValue));
            dbBo.setProductPage(productPage);
        }
        if (Coupon.DISCOUNT_TYPE_GIFT.equals(dbBo.getDiscountType())) {
            dbBo.setGiftList(findGift(dbBo.getId()));
        }
        /**
         * Ẩn các thuộc tính chỉ sử dụng cho giỏ hàng.
         */
        dbBo.setCartList(null);
        dbBo.setSelectedGift(null);
        return dbBo;
    }

    @Override
    public List<Product> findGift(Integer couponId) {
        return this.productDao.findAll((root, query, builder) -> {
            Root<ProductPrice> productPriceRoot = query.from(ProductPrice.class);
            query.multiselect(root, productPriceRoot);
            Root<CouponGiftLink> giftLinkRoot = query.from(CouponGiftLink.class);
            return builder.and(
                    builder.equal(root.get(Product._id), productPriceRoot.get(ProductPrice._productId)),
                    builder.equal(root.get(Product._id), giftLinkRoot.get(CouponGiftLink._productId)),
                    builder.lessThanOrEqualTo(productPriceRoot.get(ProductPrice._startDate), Instant.now()),
                    builder.greaterThan(productPriceRoot.get(ProductPrice._endDate), Instant.now()),
                    builder.equal(root.get(Product._status), Product.STATUS_ACTIVE),
                    builder.greaterThan(root.get(Product._amount), 0),
                    builder.equal(giftLinkRoot.get(CouponGiftLink._couponId), couponId)
            );
        });
    }

    private Page<Product> findProductOfCoupon(Integer couponId, Integer categoryId, List<Integer> brandIdList,
                                              String sortDirection, String sortBy, Integer rating, Pageable pageRequest) {
        Specification specification = productFilter(couponId, sortDirection, sortBy, rating);
        if (categoryId != null) {
            specification = specification.and((root, query, builder) -> builder.and(builder.equal(root.get(Product._productCategoryId), categoryId)));
        }
        if (brandIdList != null && brandIdList.size() > 0) {
            specification = specification.and((root, query, builder) -> builder.and(root.get(Product._brandId).in(brandIdList)));
        }
        return this.productDao.findAll(specification, pageRequest);
    }

    /**
     * Specification lấy sản phẩm của coupon
     */
    private Specification productFilter(Integer couponId, String sortDirection, String sortBy, Integer rating) {
        return (root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<CouponProductLink> productLinkRoot = query.from(CouponProductLink.class);
            predicates.add(builder.equal(root.get(Product._id), productLinkRoot.get(CouponProductLink._productId)));
            Root<Brand> brandRoot = query.from(Brand.class);
            predicates.add(builder.equal(root.get(Product._brandId), brandRoot.get(Brand._id)));
            Root<ProductPrice> priceRoot = query.from(ProductPrice.class);
            predicates.add(builder.equal(root.get(Product._id), priceRoot.get(ProductPrice._productId)));
            query.multiselect(root, priceRoot, brandRoot);
            if (sortDirection != null && Sort.Direction.ASC.equals(Sort.Direction.valueOf(sortDirection.toUpperCase()))) {
                if (Product.COMPARE_PRICE_FIELD.equals(sortBy)) {
                    query.orderBy(builder.asc(priceRoot.get(ProductPrice._priceSale)));
                }
            } else {
                if (Product.COMPARE_NEW_FIELD.equals(sortBy)) {
                    query.orderBy(builder.desc(root.get(Product._createdDate)));
                } else if (Product.COMPARE_PRICE_FIELD.equals(sortBy)) {
                    query.orderBy(builder.desc(priceRoot.get(ProductPrice._priceSale)));
                }
            }
            if (rating != null) {
                Subquery<Product> productSubquery = query.subquery(Product.class);
                Root<Product> productSubqueryRoot = productSubquery.from(Product.class);
                Root<ReviewSummary> reviewSummaryRoot = productSubquery.from(ReviewSummary.class);
                productSubquery.select(reviewSummaryRoot.get(ReviewSummary._ratingAverage)).where(
                        builder.and(
                                builder.equal(productSubqueryRoot.get(Product._id), reviewSummaryRoot.get(ReviewSummary._productId)),
                                builder.equal(root.get(Product._id), productSubqueryRoot.get(Product._id)),
                                builder.greaterThanOrEqualTo(reviewSummaryRoot.get(ReviewSummary._ratingAverage), rating)
                        ));
                productSubquery.select(reviewSummaryRoot.get(ReviewSummary._ratingAverage));
                predicates.add(builder.exists(productSubquery));
            }
            predicates.add(builder.lessThanOrEqualTo(priceRoot.get(ProductPrice._startDate), Instant.now()));
            predicates.add(builder.greaterThan(priceRoot.get(ProductPrice._endDate), Instant.now()));
            predicates.add(builder.equal(productLinkRoot.get(CouponProductLink._couponId), couponId));
            predicates.add(builder.equal(root.get(Product._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Product._status), Product.STATUS_ACTIVE));
            return builder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public Page<Coupon> findBySearchCouponActive(CouponSearchRequest model) {
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        return this.couponDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<Campaign> campaignRoot = query.from(Campaign.class);
            predicates.add(builder.equal(root.get(Coupon._campaignId), campaignRoot.get(Campaign._id)));
            Subquery<Long> subqueryProduct = query.subquery(Long.class);
            Root<CouponProductLink> couponProductLinkSubqueryRoot = subqueryProduct.from(CouponProductLink.class);
            subqueryProduct.select(builder.count(couponProductLinkSubqueryRoot.get(CouponProductLink._id))).where(
                    builder.equal(couponProductLinkSubqueryRoot.get(CouponProductLink._couponId), root.get(Coupon._id))
            );
            Subquery<Long> subqueryProductGift = query.subquery(Long.class);
            Root<CouponGiftLink> couponGiftLinkSubqueryRoot = subqueryProductGift.from(CouponGiftLink.class);
            subqueryProductGift.select(builder.count(couponGiftLinkSubqueryRoot.get(CouponGiftLink._id))).where(
                    builder.equal(couponGiftLinkSubqueryRoot.get(CouponGiftLink._couponId), root.get(Coupon._id))
            );
            query.multiselect(root, campaignRoot.get(Campaign._name), subqueryProduct.getSelection(), subqueryProductGift.getSelection());
            query.distinct(true);
            if (StringUtils.isNotEmpty(model.getName())) {
                predicates.add(builder.like(root.get(Coupon._name), "%" + model.getName() + "%"));
            }
            if (StringUtils.isNotEmpty(model.getCode())) {
                predicates.add(builder.equal(root.get(Coupon._code), model.getCode()));
            }
            if (model.getCampaignId() != null) {
                predicates.add(builder.equal(root.get(Coupon._campaignId), model.getCampaignId()));
            }
            if (model.getType() != null) {
                predicates.add(builder.equal(root.get(Coupon._type), model.getType()));
            }
            predicates.add(builder.equal(root.get(Coupon._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Coupon._status), Coupon.STATUS_ACTIVE));
            predicates.add(builder.lessThan(root.get(Coupon._couponUsed), root.get(Coupon._couponNumber)));
            predicates.add(
                    builder.and(
                            builder.lessThanOrEqualTo(root.get(Coupon._startTime), Instant.now()),
                            builder.greaterThanOrEqualTo(root.get(Coupon._endTime), Instant.now())
                    )
            );
            predicates.add(builder.greaterThan(subqueryProduct.getSelection(), 0L));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public Page<Coupon> findBySearch(CouponSearchRequest model) {
        int orgId = getOrgId();
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        return this.couponDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<Campaign> campaignRoot = query.from(Campaign.class);
            predicates.add(builder.equal(root.get(Coupon._campaignId), campaignRoot.get(Campaign._id)));
            Subquery<Long> subqueryProduct = query.subquery(Long.class);
            Root<CouponProductLink> couponProductLinkSubqueryRoot = subqueryProduct.from(CouponProductLink.class);
            subqueryProduct.select(builder.count(couponProductLinkSubqueryRoot.get(CouponProductLink._id))).where(
                    builder.equal(couponProductLinkSubqueryRoot.get(CouponProductLink._couponId), root.get(Coupon._id))
            );
            Subquery<Long> subqueryProductGift = query.subquery(Long.class);
            Root<CouponGiftLink> couponGiftLinkSubqueryRoot = subqueryProductGift.from(CouponGiftLink.class);
            subqueryProductGift.select(builder.count(couponGiftLinkSubqueryRoot.get(CouponGiftLink._id))).where(
                    builder.equal(couponGiftLinkSubqueryRoot.get(CouponGiftLink._couponId), root.get(Coupon._id))
            );
            query.multiselect(root, campaignRoot.get(Campaign._name), subqueryProduct.getSelection(), subqueryProductGift.getSelection());
            query.distinct(true);
            predicates.add(builder.equal(root.get(Coupon._orgId), orgId));
            if (StringUtils.isNotEmpty(model.getName())) {
                predicates.add(builder.like(root.get(Coupon._name), "%" + model.getName() + "%"));
            }
            if (StringUtils.isNotEmpty(model.getCode())) {
                predicates.add(builder.equal(root.get(Coupon._code), model.getCode()));
            }
            if (model.getCampaignId() != null) {
                predicates.add(builder.equal(root.get(Coupon._campaignId), model.getCampaignId()));
            }
            if (model.getType() != null) {
                predicates.add(builder.equal(root.get(Coupon._type), model.getType()));
            }
            predicates.add(builder.equal(root.get(Coupon._isTrash), model.getIsTrash()));
            if (model.getStatus() != null) {
                predicates.add(builder.equal(root.get(Coupon._status), model.getStatus()));
            }
            if (model.getStatusTime() != null) {
                predicates.add(builder.equal(root.get(Coupon._status), Coupon.STATUS_ACTIVE));
                //Coupon sắp diễn ra
                if (Coupon.STATUS_TIME_UPCOMING.equals(model.getStatusTime())) {
                    predicates.add(builder.greaterThan(root.get(Coupon._startTime), Instant.now()));
                    //Coupon đang diễn ra
                } else if (Coupon.STATUS_TIME_ONGOING.equals(model.getStatusTime())) {
                    predicates.add(builder.lessThanOrEqualTo(root.get(Coupon._startTime), Instant.now()));
                    predicates.add(builder.greaterThanOrEqualTo(root.get(Coupon._endTime), Instant.now()));
                } else {
                    //Coupon đã kết thúc
                    predicates.add(builder.lessThan(root.get(Coupon._endTime), Instant.now()));
                }
            }
            if (StringUtils.isNotEmpty(model.getProductName()) || StringUtils.isNotEmpty(model.getProductSku())) {
                Root<CouponProductLink> couponProductLinkRoot = query.from(CouponProductLink.class);
                Root<Product> productRoot = query.from(Product.class);
                predicates.add(builder.equal(root.get(Coupon._id), couponProductLinkRoot.get(CouponProductLink._couponId)));
                predicates.add(builder.equal(couponProductLinkRoot.get(CouponProductLink._productId), productRoot.get(Product._id)));
                if (StringUtils.isNotEmpty(model.getProductName())) {
                    predicates.add(builder.like(productRoot.get(Product._name), "%" + model.getProductName() + "%"));
                }
                if (StringUtils.isNotEmpty(model.getProductSku())) {
                    predicates.add(builder.equal(productRoot.get(Product._sku), model.getProductSku()));
                }
            }
            if (StringUtils.isNotEmpty(model.getProductNameGift()) || StringUtils.isNotEmpty(model.getProductSkuGift())) {
                Root<CouponGiftLink> couponGiftLinkRoot = query.from(CouponGiftLink.class);
                Root<Product> productGiftRoot = query.from(Product.class);
                predicates.add(builder.equal(root.get(Coupon._id), couponGiftLinkRoot.get(CouponGiftLink._couponId)));
                predicates.add(builder.equal(couponGiftLinkRoot.get(CouponGiftLink._productId), productGiftRoot.get(Product._id)));
                if (StringUtils.isNotEmpty(model.getProductNameGift())) {
                    predicates.add(builder.like(productGiftRoot.get(Product._name), "%" + model.getProductNameGift() + "%"));
                }
                if (StringUtils.isNotEmpty(model.getProductSkuGift())) {
                    predicates.add(builder.equal(productGiftRoot.get(Product._sku), model.getProductSkuGift()));
                }
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    private void updateProductOfCoupon(Integer couponId, List<Product> clientProductList) {
        CouponProductLink example = new CouponProductLink();
        example.setCouponId(couponId);
        List<CouponProductLink> dbList = this.couponProductLinkDao.findAll(Example.of(example));
        List<Integer> dbProductIdList = dbList.stream().map(CouponProductLink::getProductId).collect(Collectors.toList());
        ArrayList<CouponProductLink> clientList = new ArrayList<>();
        for (Product product : clientProductList) {
            if (!dbProductIdList.contains(product.getId())) {
                CouponProductLink couponProductLink = new CouponProductLink();
                couponProductLink.setCouponId(couponId);
                couponProductLink.setProductId(product.getId());
                clientList.add(couponProductLink);
            }

        }
        this.couponProductLinkDao.saveAll(clientList);
    }

    private void updateGiftOfCoupon(Integer couponId, List<Product> clientProductList) {
        CouponGiftLink example = new CouponGiftLink();
        example.setCouponId(couponId);
        List<CouponGiftLink> dbList = this.couponGiftLinkDao.findAll(Example.of(example));
        List<Integer> dbProductIdList = dbList.stream().map(CouponGiftLink::getProductId).collect(Collectors.toList());
        ArrayList<CouponGiftLink> clientList = new ArrayList<>();
        for (Product product : clientProductList) {
            if (!dbProductIdList.contains(product.getId())) {
                CouponGiftLink couponGiftLink = new CouponGiftLink();
                couponGiftLink.setCouponId(couponId);
                couponGiftLink.setProductId(product.getId());
                clientList.add(couponGiftLink);
            }
        }
        this.couponGiftLinkDao.saveAll(clientList);
    }
}

