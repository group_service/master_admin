package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.masterdata.bo.Payment;
import com.hebela.masterdata.dao.PaymentDao;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PaymentServiceImpl extends AbstractStatusService<Payment> implements PaymentService {
    private final PaymentDao paymentDao;

    public static final int PRIORITY_BEGIN = 0;

    public PaymentServiceImpl(PaymentDao paymentDao) {
        this.paymentDao = paymentDao;
    }

    @Override
    public List<Payment> find(Payment bo) {
        return paymentDao.findAll(Example.of(bo), Sort.by(Sort.Order.asc(Payment._priority), Sort.Order.desc(Payment._createdDate)));

    }

    @Override
    public Payment doInsert(Payment bo) {
        bo.setPriority(PRIORITY_BEGIN);
        return super.doInsert(bo);
    }
}

