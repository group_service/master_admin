package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Attribute;
import com.hebela.masterdata.bo.AttributeCategoryLink;
import com.hebela.masterdata.bo.AttributeData;
import com.hebela.masterdata.bo.Unit;
import com.hebela.masterdata.dao.AttributeCategoryLinkDao;
import com.hebela.masterdata.dao.AttributeDao;
import com.hebela.masterdata.dao.AttributeDataDao;
import com.hebela.masterdata.dao.UnitDao;
import com.hebela.masterdata.web.attribute.AttributeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttributeServiceImpl extends AbstractStatusService<Attribute> implements AttributeService {

    @Autowired
    private AttributeDao attributeDao;
    @Autowired
    private AttributeDataDao attributeDataDao;
    @Autowired
    private UnitDao unitDao;
    @Autowired
    private AttributeCategoryLinkDao attributeCategoryLinkDao;

    public static final int PRIORITY_BEGIN = 0;

    @Override
    public List<Attribute> find(Attribute bo) {
        return this.attributeDao.findAll(Example.of(bo), Sort.by(Sort.Order.asc(Attribute._priority), Sort.Order.desc(Attribute._createdDate)));
    }

    /*AdminUI: Trang danh sách thuộc tính*/
    @Override
    public List<Attribute> findBySearch(AttributeRequest attributeRequest) {
        if (attributeRequest.getAttribute() == null) {
            attributeRequest.setAttribute(new Attribute());
        }
        Attribute attribute = attributeRequest.getAttribute();
        List<Attribute> attributeList = attributeDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (attributeRequest.getProductCategoryId() != null) {
                Root<AttributeCategoryLink> attributeCategoryLinkRoot = query.from(AttributeCategoryLink.class);
                predicates.add(builder.equal(root.get(Attribute._id), attributeCategoryLinkRoot.get(AttributeCategoryLink._attributeId)));
                predicates.add(builder.equal(attributeCategoryLinkRoot.get(AttributeCategoryLink._productCategoryId), attributeRequest.getProductCategoryId()));
            }
            query.distinct(true);
            if (attribute.getName() != null) {
                predicates.add(builder.equal(root.get(Attribute._name), attribute.getName()));
            }
            if (attribute.getStatus() != null) {
                predicates.add(builder.equal(root.get(Attribute._status), attribute.getStatus()));
            }
            if (attribute.getIsTrash() != null) {
                predicates.add(builder.equal(root.get(Attribute._isTrash), attribute.getIsTrash()));
            }
            query.orderBy(builder.asc(root.get(Attribute._priority)), builder.desc(root.get(Attribute._createdDate)));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        });
        List<Integer> attributeIdList = attributeList.stream().map(Attribute::getId).collect(Collectors.toList());
        List<AttributeData> attributeDataList = attributeDataDao.findByAttributeIdIn(attributeIdList);
        List<AttributeCategoryLink> attributeCategoryLinkList = attributeCategoryLinkDao.findByAttributeIdIn(attributeIdList);
        for (Attribute _attribute : attributeList) {
            _attribute.setAmountData(attributeDataList.stream().filter(item -> item.getAttributeId().equals(_attribute.getId())).count());
            _attribute.setAmountCategory(attributeCategoryLinkList.stream().filter(item -> item.getAttributeId().equals(_attribute.getId())).count());
        }
        return attributeList;
    }

    /*
     * AdminUI: Trang thêm mới/cập nhật sản phẩm
     * Lấy danh sách thuộc tính khi chọn danh mục
     * Chỉ lấy các thuộc tính có data
     * */
    @Override
    public List<Attribute> findByCategory(Integer categoryId) {
        List<Attribute> attributeList = attributeDao.findByCategory(categoryId);
        if (attributeList.size() > 0) {
            List<Integer> attributeIdList = attributeList.stream().map(Attribute::getId).collect(Collectors.toList());
            List<AttributeData> attributeDataList = this.attributeDataDao.findByAttributeIdIn(attributeIdList);
            if (attributeDataList.size() > 0) {
                for (AttributeData item : attributeDataList) {
                    if (Attribute.TYPE_TEXT.equals(item.getType())) {
                        item.setName(item.getValueText());
                    } else if (Attribute.TYPE_NUMBER.equals(item.getType())) {
                        item.setName(item.getValueNumber() + " (" + item.getUnit() + ")");
                    }
                }
            }
            for (Attribute attribute : attributeList) {
                attribute.setAttributeDataList(attributeDataList.stream().filter(attributeData -> attributeData.getAttributeId().equals(attribute.getId())).collect(Collectors.toList()));
            }
        }
        return attributeList;
    }

    /*AdminUI: Update Group: Lấy danh sách attribute khi thêm mới sản phẩm vào group*/
    @Override
    public List<Attribute> findByProduct(Integer productId) {
        return attributeDao.findByProduct(productId);
    }

    @Override
    public List<Unit> findUnit() {
        return unitDao.findAll(Sort.by(Sort.Order.by(Unit._id)));
    }

    /*
     * AdminUI: Trang cập nhật thuộc tính
     * */
    @Override
    public Attribute get(Integer id) {
        Attribute dbAttribute = super.get(id);
        AttributeData attributeData = new AttributeData();
        attributeData.setAttributeId(id);
        List<AttributeData> attributeDataList = attributeDataDao.findAll(Example.of(attributeData));
        dbAttribute.setAttributeDataList(attributeDataList);
        AttributeCategoryLink attributeCategoryLink = new AttributeCategoryLink();
        attributeCategoryLink.setAttributeId(dbAttribute.getId());
        List<AttributeCategoryLink> dbLinkList = attributeCategoryLinkDao.findAll(Example.of(attributeCategoryLink));
        if (dbLinkList.size() > 0) {
            List<Integer> categoryIdList = dbLinkList.stream().map(AttributeCategoryLink::getProductCategoryId).collect(Collectors.toList());
            dbAttribute.setCategoryIdList(categoryIdList);
        }
        return dbAttribute;
    }

    /*
     * Thêm mới thuộc tính
     * Không thêm danh sách giá trị thuộc tính
     * Thêm link tới category
     * */
    @Override
    public Attribute doInsert(Attribute bo) {
        bo.setPriority(PRIORITY_BEGIN);
        bo.setCreatedBy(User.getContextId());
        Attribute dbBo = super.doInsert(bo);
        /*Insert AttributeCategoryLink*/
        if (bo.getCategoryIdList() != null && bo.getCategoryIdList().size() > 0) {
            for (Integer categoryId : bo.getCategoryIdList()) {
                AttributeCategoryLink attributeCategoryLink = new AttributeCategoryLink();
                attributeCategoryLink.setAttributeId(dbBo.getId());
                attributeCategoryLink.setProductCategoryId(categoryId);
                attributeCategoryLink.setCreatedBy(User.getContextId());
                attributeCategoryLinkDao.save(attributeCategoryLink);
            }
        }
        return bo;
    }

    /*
     * Cập nhật thuộc tính
     * Pre: Thuộc tính không bị hủy
     * Không cập nhật type, thứ tự ưu tiên và giá trị
     * Cập nhật link tới category
     * */
    @Override
    public Attribute doUpdate(Attribute bo) {
        Attribute dbAttribute = attributeDao.getOne(bo.getId());
        if (dbAttribute.getIsTrash().equals(Boolean.TRUE)) {
            throw new HebelaException("action.invalid");
        }
        dbAttribute.setName(bo.getName());
        dbAttribute.setSlug(bo.getSlug());
        dbAttribute.setModifiedBy(User.getContextId());
        attributeDao.save(dbAttribute);

        /*Cập nhật thay đổi bảng AttributeCategoryLink*/
        AttributeCategoryLink attributeCategoryLink = new AttributeCategoryLink();
        attributeCategoryLink.setAttributeId(dbAttribute.getId());
        List<AttributeCategoryLink> dbLinkList = attributeCategoryLinkDao.findAll(Example.of(attributeCategoryLink));
        List<AttributeCategoryLink> insertLinkList = new ArrayList<>();
        if (bo.getCategoryIdList() != null && bo.getCategoryIdList().size() > 0) {
            for (Integer categoryId : bo.getCategoryIdList()) {
                AttributeCategoryLink _attributeCategoryLink = new AttributeCategoryLink();
                _attributeCategoryLink.setAttributeId(dbAttribute.getId());
                _attributeCategoryLink.setProductCategoryId(categoryId);
                _attributeCategoryLink.setCreatedBy(User.getContextId());
                insertLinkList.add(_attributeCategoryLink);
            }
        }

        if (insertLinkList.size() == 0) {
            this.attributeCategoryLinkDao.deleteAll(dbLinkList);
        } else {
            for (Iterator<AttributeCategoryLink> insertIterator = insertLinkList.iterator(); insertIterator.hasNext(); ) {
                AttributeCategoryLink insertItem = insertIterator.next();
                for (Iterator<AttributeCategoryLink> removeIterator = dbLinkList.iterator(); removeIterator.hasNext(); ) {
                    AttributeCategoryLink removeItem = removeIterator.next();
                    if (removeItem.getProductCategoryId().equals(insertItem.getProductCategoryId())) {
                        removeIterator.remove();
                        insertIterator.remove();
                    }
                }
            }
            if (dbLinkList.size() > 0) {
                this.attributeCategoryLinkDao.deleteAll(dbLinkList);
            }
            if (insertLinkList.size() > 0) {
                this.attributeCategoryLinkDao.saveAll(insertLinkList);
            }
        }
        return bo;
    }

    /*
     * Thêm mới giá trị thuộc tính
     * Pre: Thuộc tính không bị hủy
     * */
    @Override
    public AttributeData doInsertAttributeData(AttributeData attributeData) {
        Attribute dbAttribute = attributeDao.getOne(attributeData.getAttributeId());
        if (dbAttribute.getIsTrash().equals(Boolean.TRUE)) {
            throw new HebelaException("action.invalid");
        }
        attributeData.setType(dbAttribute.getType());
        validateAttributeData(attributeData);
        attributeData.setCreatedBy(User.getContextId());
        attributeDataDao.save(attributeData);
        return attributeData;
    }

    /*
     * Cập nhật giá trị thuộc tính     *
     * Pre: Thuộc tính không bị hủy
     * Không cập nhật type
     * */
    @Override
    public AttributeData doUpdateAttributeData(AttributeData attributeData) {
        AttributeData dbAttributeData = attributeDataDao.getOne(attributeData.getId());
        Attribute dbAttribute = attributeDao.getOne(dbAttributeData.getAttributeId());
        if (dbAttribute.getIsTrash().equals(Boolean.TRUE)) {
            throw new HebelaException("action.invalid");
        }
        attributeData.setType(dbAttribute.getType());
        validateAttributeData(attributeData);

        dbAttributeData.setValueText(attributeData.getValueText());
        dbAttributeData.setValueNumber(attributeData.getValueNumber());
        dbAttributeData.setUnit(attributeData.getUnit());
        attributeData.setModifiedBy(User.getContextId());
        attributeDataDao.save(attributeData);
        return attributeData;
    }

    /*
     * Xóa giá trị thuộc tính     *
     * Pre: Thuộc tính không bị hủy
     * */
    @Override
    public AttributeData doDeleteAttributeData(AttributeData attributeData) {
        AttributeData dbAttributeData = attributeDataDao.getOne(attributeData.getId());
        Attribute dbAttribute = attributeDao.getOne(dbAttributeData.getAttributeId());
        if (dbAttribute.getIsTrash().equals(Boolean.TRUE)) {
            throw new HebelaException("action.invalid");
        }
        attributeDataDao.delete(dbAttributeData);
        return attributeData;
    }

    private void validateAttributeData(AttributeData attributeData) {
        if (Attribute.TYPE_TEXT.equals(attributeData.getType())) {
            if (attributeData.getValueText() == null || attributeData.getValueText().length() == 0) {
                throw new HebelaException("error.attribute.data.value.required");
            }
        } else if (Attribute.TYPE_NUMBER.equals(attributeData.getType())) {
            if (attributeData.getValueNumber() == null) {
                throw new HebelaException("error.attribute.data.value.required");
            }
            if (attributeData.getUnit() == null || attributeData.getUnit().length() == 0) {
                throw new HebelaException("error.attribute.data.unit.required");
            }
        } else {
            throw new HebelaException("action.invalid");
        }
    }
}

