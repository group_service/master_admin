package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Landing;

public interface LandingService extends StatusService<Landing> {
    Landing doActivate(Landing landing);

    Landing doDeactivate(Landing landing);
}

