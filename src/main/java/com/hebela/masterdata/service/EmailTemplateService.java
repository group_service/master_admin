package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.EmailTemplate;
import com.hebela.masterdata.web.emailTemplate.EmailTemplateRequest;
import org.springframework.data.domain.Page;

public interface EmailTemplateService extends Service<EmailTemplate, Integer> {

    Page<EmailTemplate> findBySearch(EmailTemplateRequest emailTemplateRequest);
}

