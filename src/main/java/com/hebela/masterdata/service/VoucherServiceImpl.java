package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Account;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.CartView;
import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.bo.extend.VoucherAccountExtend;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.enumeration.VoucherErrorEnum;
import com.hebela.masterdata.web.voucher.VoucherSearchRequest;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class VoucherServiceImpl extends AbstractStatusService<Voucher> implements VoucherService {
    private final VoucherAccountLinkDao voucherAccountLinkDao;
    private final AccountDao accountDao;
    private final VoucherDao voucherDao;
    private final CampaignDao campaignDao;
    private final VoucherGiftLinkDao voucherGiftLinkDao;
    private final ProductDao productDao;

    public VoucherServiceImpl(VoucherAccountLinkDao voucherAccountLinkDao, AccountDao accountDao, VoucherDao voucherDao,
                              CampaignDao campaignDao, VoucherGiftLinkDao voucherGiftLinkDao, ProductDao productDao) {
        this.voucherAccountLinkDao = voucherAccountLinkDao;
        this.accountDao = accountDao;
        this.voucherDao = voucherDao;
        this.campaignDao = campaignDao;
        this.voucherGiftLinkDao = voucherGiftLinkDao;
        this.productDao = productDao;
    }

    @Override
    public Voucher get(Integer key) {
        Voucher voucher = super.get(key);
        List<VoucherAccountExtend> accountList = this.accountDao.findAll((root, query, builder) -> {
            Root<VoucherAccountLink> linkRoot = query.from(VoucherAccountLink.class);
            return builder.and(
                    builder.equal(root.get(Account._id), linkRoot.get(VoucherAccountLink._accountId)),
                    builder.equal(linkRoot.get(VoucherAccountLink._voucherId), voucher.getId())
            );
        }).stream().map(VoucherAccountExtend::toExtend).collect(Collectors.toList());
        List<Product> productList = this.voucherGiftLinkDao.findProductGiftVoucher(voucher.getId());
        voucher.setAccountList(accountList);
        voucher.setGiftList(productList);
        return voucher;
    }

    @Override
    public Voucher doInsert(Voucher bo) {
        if (bo.getVoucherNumber() == null) {
            bo.setVoucherNumber(0);
        }
        if (bo.getStatus() == null) {
            bo.setStatus(Voucher.STATUS_INACTIVE);
        } else {
            List<Integer> productIdList = bo.getGiftList().stream().map(Product::getId).collect(Collectors.toList());
            Voucher voucherResponse = validProductForVoucher(productIdList);
            if (voucherResponse.getErrorList().size() > 0) {
                throw new HebelaException(voucherResponse.getErrorList().get(0).getMessage());
            }
        }

        if (bo.getGiftList().size() == 0 && bo.getDiscountType().equals(Voucher.DISCOUNT_TYPE_GIFT)) {
            throw new HebelaException("voucher.product.gift.invalid");
        }
        bo.setVoucherUsed(0);
        bo.setOrgId(User.getContext().getOrgId());
        if (bo.getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
            bo.setVoucherType(Voucher.VOUCHER_TYPE_ROOT);
        } else {
            bo.setVoucherType(Voucher.VOUCHER_TYPE_SHOP);
        }
        Voucher voucher = super.doInsert(bo);
        if (bo.getDiscountType().equals(Voucher.DISCOUNT_TYPE_GIFT)) {
            doUpdateProduct(voucher.getId(), bo.getGiftList());
        }
        if (Voucher.USER_TYPE_SPECIFIED.equals(voucher.getUserType())) {
            if (bo.getAccountList() != null && bo.getAccountList().size() > 0) {
                this.updateAccountOfVoucher(voucher.getId(), bo.getAccountList());
            } else {
                throw new HebelaException("voucher.account.list.missing");
            }
        }
        return voucher;
    }

    @Override
    public Voucher doRestore(Voucher bo) {
        Voucher voucher = super.get(bo.getId());
        Campaign campaign = campaignDao.findById(voucher.getCampaignId()).orElse(null);
        if (campaign == null || Boolean.TRUE.equals(campaign.getIsTrash())) {
            throw new HebelaException("campaign.not.exists");
        }
        return super.doRestore(bo);
    }

    @Override
    public Voucher doUpdate(Voucher bo) {
        Voucher dbBo = super.get(bo);
        bo.setVoucherUsed(dbBo.getVoucherUsed());
        bo.setVoucherType(dbBo.getVoucherType());
        bo.setOrgId(dbBo.getOrgId());
        Voucher voucher = super.doUpdate(bo);
        if (Voucher.USER_TYPE_SPECIFIED.equals(voucher.getUserType())) {
            if (bo.getAccountList() != null && bo.getAccountList().size() > 0) {
                VoucherAccountLink example = new VoucherAccountLink();
                example.setVoucherId(voucher.getId());
                List<VoucherAccountLink> dbLinkList = this.voucherAccountLinkDao.findAll(Example.of(example));
                List<VoucherAccountLink> clientLinkList = new ArrayList<>();
                for (VoucherAccountExtend account : bo.getAccountList()) {
                    VoucherAccountLink link = new VoucherAccountLink();
                    link.setVoucherId(voucher.getId());
                    link.setAccountId(account.getId());
                    link.setCreatedBy(User.getContextId());
                    clientLinkList.add(link);
                }
                this.voucherAccountLinkDao.saveAll(ListUtils.removeAll(clientLinkList, dbLinkList));
                this.voucherAccountLinkDao.deleteAll(ListUtils.removeAll(dbLinkList, clientLinkList));
            } else {
                throw new HebelaException("voucher.account.list.missing");
            }
        }
        return voucher;
    }

    @Override
    public Voucher doDelete(Voucher bo) {
        Voucher dbBo = super.get(bo);
        if (dbBo == null || dbBo.getVoucherUsed() != 0) {
            throw new HebelaException("voucher.can.not.detele");
        }
        return super.doDelete(bo);
    }

    /*
     * Kích hoạt voucher
     * voucher phải không hủy, ở trạng chái chưa kích hoạt
     * */
    @Override
    public Voucher doActivate(Voucher bo) {
        Voucher dbBo = super.get(bo.getId());
        if (dbBo.getStatus().equals(Voucher.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        VoucherGiftLink voucherGiftLink = new VoucherGiftLink();
        voucherGiftLink.setVoucherId(dbBo.getId());
        List<VoucherGiftLink> voucherGiftLinkList = this.voucherGiftLinkDao.findAll(Example.of(voucherGiftLink));
        if (voucherGiftLinkList.size() > 0) {
            List<Integer> productIdList = voucherGiftLinkList.stream().map(VoucherGiftLink::getProductId).collect(Collectors.toList());
            Voucher voucherResponse = validProductForVoucher(productIdList);
            if (voucherResponse.getErrorList().size() > 0) {
                dbBo.setErrorList(voucherResponse.getErrorList());
                return dbBo;
            }
        }
        dbBo.setStatus(Coupon.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.voucherDao.save(dbBo);
        return dbBo;
    }

    /*
     * Hủy kích hoạt voucher
     * Pre: voucher phải không hủy, ở trạng thái kích hoạt
     * */
    @Override
    public Voucher doDeactivate(Voucher bo) {
        Voucher dbBo = super.get(bo.getId());
        if (!dbBo.getStatus().equals(Voucher.STATUS_ACTIVE) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Voucher.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        this.voucherDao.save(dbBo);
        return dbBo;
    }

    @Override
    public Voucher doAddAccountOfVoucher(Voucher bo) {
        Voucher dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getAccountList() == null || bo.getAccountList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        this.updateAccountOfVoucher(bo.getId(), bo.getAccountList());
        return dbBo;
    }

    private Voucher validProductForVoucher(List<Integer> productGiftIdList) {
        Voucher voucher = new Voucher();
        Integer orgId = User.getContext().getOrgId();
        List<HebelaError> hebelaErrorList = new ArrayList<>();
        if (productGiftIdList == null || productGiftIdList.size() == 0) {
            return voucher;
        }
        //Lấy các sản phẩm quà tặng theo đang user đăng nhập
        Map<Integer, Product> productMap = this.productDao.findProductOfOrg(orgId, productGiftIdList)
                .stream()
                .collect(Collectors.toMap(Product::getId, p -> p));
        for (Integer productId : productGiftIdList) {

            Product productDB = productMap.get(productId);
            if (productDB.getType().equals(Product.TYPE_PRODUCT_IN_COMBO)) {
                hebelaErrorList.add(new HebelaError("Sản phẩm [SKU - " + productDB.getSku() + " ] là sản phẩm không bán lẻ nên không là quà tặng voucher"));
                continue;
            }
            //Kiểm tra trạng thái sản phẩm có phải đang bán không
            if (!productDB.getStatus().equals(Product.STATUS_ACTIVE)) {
                hebelaErrorList.add(new HebelaError("Sản phẩm [SKU - " + productDB.getSku() + " ] phải ở trạng thái đang bán"));
                continue;
            }
            //Kiểm tra tồn kho của sản phẩm
            if (productDB.getAmount() != null && (Integer.compare(productDB.getAmount(), 0) <= 0)) {
                hebelaErrorList.add(new HebelaError("Sản phẩm [SKU - " + productDB.getSku() + " ] hiện đang hết hàng"));
            }
        }
        voucher.setErrorList(hebelaErrorList);
        return voucher;
    }

    private void updateAccountOfVoucher(Integer voucherId, List<VoucherAccountExtend> clientAccountList) {
        VoucherAccountLink example = new VoucherAccountLink();
        example.setVoucherId(voucherId);
        List<VoucherAccountLink> dbList = this.voucherAccountLinkDao.findAll(Example.of(example));
        List<Integer> dbAccountIdList = dbList.stream().map(VoucherAccountLink::getAccountId).collect(Collectors.toList());
        List<VoucherAccountLink> clientList = new ArrayList<>();
        for (VoucherAccountExtend account : clientAccountList) {
            if (!dbAccountIdList.contains(account.getId())) {
                VoucherAccountLink voucherAccountLink = new VoucherAccountLink();
                voucherAccountLink.setVoucherId(voucherId);
                voucherAccountLink.setAccountId(account.getId());
                clientList.add(voucherAccountLink);
            }
        }
        this.voucherAccountLinkDao.saveAll(clientList);
    }

    /*
     * Remove account khỏi voucher
     * Pre:
     *      - voucher không được hủy và phải có thông tin account
     * */
    @Override
    public Voucher doRemoveAccountOfVoucher(Voucher bo) {
        Voucher dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getVoucherAccountExtend() == null) {
            throw new HebelaException("action.invalid");
        }
        VoucherAccountLink voucherAccountLink = new VoucherAccountLink();
        voucherAccountLink.setVoucherId(bo.getId());
        voucherAccountLink.setAccountId(bo.getVoucherAccountExtend().getId());
        List<VoucherAccountLink> voucherAccountLinkList = voucherAccountLinkDao.findAll(Example.of(voucherAccountLink));
        if (voucherAccountLinkList.size() > 0) {
            voucherAccountLinkDao.deleteAll(voucherAccountLinkList);
        }
        return bo;
    }

    public List<Account> findAccount(String keyword) {
        return this.accountDao.findAll((root, query, builder) -> builder.or(
                builder.like(root.get(Account._mobile), "%" + keyword + "%"),
                builder.like(root.get(Account._fullname), "%" + keyword + "%"),
                builder.like(root.get(Account._email), "%" + keyword + "%")
        ));
    }

    @Override
    public Page<Voucher> findBySearch(VoucherSearchRequest model) {
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        Instant endDate = model.getEndDate();

        return voucherDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<Campaign> campaignRoot = query.from(Campaign.class);
            predicates.add(builder.equal(root.get(Voucher._campaignId), campaignRoot.get(Campaign._id)));
            query.distinct(true);
            query.multiselect(root, campaignRoot.get(Campaign._name));
            predicates.add(builder.equal(root.get(Voucher._isTrash), model.getIsTrash()));
            if (model.getStatus() != null) {
                predicates.add(builder.equal(root.get(Voucher._status), model.getStatus()));
            }
            if (model.getStatusTime() != null) {
                predicates.add(builder.equal(root.get(Voucher._status), Voucher.STATUS_ACTIVE));
                //Voucher sắp diễn ra
                if (Voucher.STATUS_TIME_UPCOMING.equals(model.getStatusTime())) {
                    predicates.add(builder.greaterThan(root.get(Voucher._startDate), Instant.now()));
                    //Voucher đang diễn ra
                } else if (Voucher.STATUS_TIME_ONGOING.equals(model.getStatusTime())) {
                    predicates.add(builder.lessThanOrEqualTo(root.get(Voucher._startDate), Instant.now()));
                    predicates.add(builder.greaterThanOrEqualTo(root.get(Voucher._endDate), Instant.now()));
                } else {
                    //Voucher đã kết thúc
                    predicates.add(builder.lessThan(root.get(Voucher._endDate), Instant.now()));
                }
            }
            if (model.getStartDate() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Voucher._startDate), model.getStartDate()));
            }
            if (endDate != null) {
                predicates.add(builder.lessThan(root.get(Voucher._endDate), endDate.plus(1, ChronoUnit.DAYS)));
            }
            if (StringUtils.isNotEmpty(model.getName())) {
                predicates.add(builder.like(root.get(Voucher._name), "%" + model.getName() + "%"));
            }
            if (StringUtils.isNotEmpty(model.getVoucherValueText())) {
                predicates.add(builder.like(root.get(Voucher._voucherValueText), "%" + model.getVoucherValueText().trim() + "%"));
            }
            if (StringUtils.isNotEmpty(model.getCode())) {
                predicates.add(builder.like(root.get(Voucher._code), "%" + model.getCode() + "%"));
            }
            if (model.getCampaignId() != null) {
                predicates.add(builder.equal(root.get(Voucher._campaignId), model.getCampaignId()));
            }
            if (!User.getContext().getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
                predicates.add(builder.equal(root.get(Voucher._orgId), Account.getContext().getOrg().getId()));
            } else {
                if (model.getOrganizationId() != null) {
                    predicates.add(builder.equal(root.get(Voucher._orgId), model.getOrganizationId()));
                }
            }
            if (model.getVoucherStatusUseTypeList().size() == 1) {
                // Lượng voucher đã dùng hết
                if (Voucher.STATUS_USED.equals(model.getVoucherStatusUseTypeList().get(0))) {
                    predicates.add(builder.equal(root.get(Voucher._voucherUsed), root.get(Voucher._voucherNumber)));
                } else {
                    // Lượng voucher chưa dùng hết
                    predicates.add(builder.lessThan(root.get(Voucher._voucherUsed), root.get(Voucher._voucherNumber)));
                }
            }
            if (StringUtils.isNotEmpty(model.getMobile()) || StringUtils.isNotEmpty(model.getFullname())) {
                Root<Account> accountRoot = query.from(Account.class);
                Root<VoucherAccountLink> voucherAccountLinkRoot = query.from(VoucherAccountLink.class);
                predicates.add(builder.equal(root.get(Voucher._id), voucherAccountLinkRoot.get(VoucherAccountLink._voucherId)));
                predicates.add(builder.equal(accountRoot.get(Account._id), voucherAccountLinkRoot.get(VoucherAccountLink._accountId)));
                if (StringUtils.isNotEmpty(model.getMobile())) {
                    predicates.add(builder.like(accountRoot.get(Account._mobile), "%" + model.getMobile() + "%"));
                }
                if (StringUtils.isNotEmpty(model.getFullname())) {
                    predicates.add(builder.like(accountRoot.get(Account._fullname), "%" + model.getFullname() + "%"));
                }
            }
            query.orderBy(
                    builder.desc(root.get(Voucher._createdDate)),
                    builder.desc(root.get(Voucher._startDate)),
                    builder.desc(root.get(Voucher._endDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    /*
     * Tìm kiếm danh sách voucher theo account đang đăng nhập
     */
    @Override
    public List<Voucher> findByAccount(Integer platform, CartView cartView, Integer orgId) {
        if (!User.getContext().getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
            orgId = User.getContext().getOrgId();
        }
        return findVoucherActive(platform, cartView, orgId);
    }

    @Override
    public Voucher doRemoveProductGift(Voucher voucher) {
        Voucher voucherDB = super.get(voucher.getId());
        if (Boolean.TRUE.equals(voucherDB.getIsTrash() || voucher.getProduct() == null || voucher.getProduct().getId() == null)) {
            throw new HebelaException("action.invalid");
        }
        VoucherGiftLink voucherGiftLink = new VoucherGiftLink();
        voucherGiftLink.setVoucherId(voucher.getId());
        List<VoucherGiftLink> voucherGiftLinksList = this.voucherGiftLinkDao.findAll(Example.of(voucherGiftLink));
        if (voucherGiftLinksList.size() == 1) {
            throw new HebelaException("voucher.product.gift.invalid");
        }
        VoucherGiftLink voucherGiftLinkDB = voucherGiftLinksList
                .stream()
                .filter(item -> item.getProductId().equals(voucher.getProduct().getId()))
                .findFirst().orElse(null);
        if (voucherGiftLinkDB != null) {
            this.voucherGiftLinkDao.delete(voucherGiftLinkDB);
        }
        return voucher;
    }

    @Override
    public Voucher doAddProductGift(Voucher voucher) {
        Voucher voucherDB = super.get(voucher.getId());
        if (Boolean.TRUE.equals(voucherDB.getIsTrash()) || voucher.getGiftList() == null || voucher.getGiftList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        Voucher voucherValid = validProductForVoucher(voucher.getGiftList().stream().map(Product::getId).collect(Collectors.toList()));
        if (voucherValid.getErrorList().size() > 0) {
            throw new HebelaException(voucherValid.getErrorList().get(0).getMessage());
        }
        doUpdateProduct(voucherDB.getId(), voucher.getGiftList());
        return voucherDB;
    }

    @Override
    public VoucherGiftLink doChangeQuantityGift(Voucher bo) {
        VoucherGiftLink voucherGiftLinkExample = new VoucherGiftLink();
        voucherGiftLinkExample.setVoucherId(bo.getId());
        voucherGiftLinkExample.setProductId(bo.getProduct().getId());
        VoucherGiftLink voucherGiftLinkDB = this.voucherGiftLinkDao.findOne(Example.of(voucherGiftLinkExample)).orElse(null);
        if (voucherGiftLinkDB != null) {
            voucherGiftLinkDB.setProductQuantity(bo.getProduct().getQuantityInVoucherGift());
            VoucherGiftLink voucherGiftLink = this.voucherGiftLinkDao.save(voucherGiftLinkDB);
            return voucherGiftLink;
        }
        return voucherGiftLinkDB;
    }

    private void doUpdateProduct(Integer voucherId, List<Product> productList) {
        VoucherGiftLink example = new VoucherGiftLink();
        example.setVoucherId(voucherId);
        List<VoucherGiftLink> voucherGiftLinkListDB = this.voucherGiftLinkDao.findAll(Example.of(example));
        List<Integer> productIdList = voucherGiftLinkListDB.stream().map(VoucherGiftLink::getProductId).collect(Collectors.toList());
        List<VoucherGiftLink> clientList = new ArrayList<>();
        for (Product product : productList) {
            if (!productIdList.contains(product.getId())) {
                VoucherGiftLink voucherGiftLink = new VoucherGiftLink();
                voucherGiftLink.setVoucherId(voucherId);
                voucherGiftLink.setProductId(product.getId());
                voucherGiftLink.setProductQuantity(product.getQuantityInVoucherGift());
                clientList.add(voucherGiftLink);
            }
        }
        this.voucherGiftLinkDao.saveAll(clientList);
    }

    public List<Voucher> findVoucherActive(Integer platform, CartView view, Integer orgId) {
        List<Voucher> voucherList = this.voucherDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Subquery<VoucherAccountLink> subQuery = query.subquery(VoucherAccountLink.class);
            Root<VoucherAccountLink> accountLinkRoot = subQuery.from(VoucherAccountLink.class);
            subQuery.select(accountLinkRoot.get(VoucherAccountLink._voucherId))
                    .where(builder.and(
                            builder.equal(root.get(Voucher._id), accountLinkRoot.get(VoucherAccountLink._voucherId)),
                            builder.equal(accountLinkRoot.get(VoucherAccountLink._accountId), orgId)
                    ));
            predicates.add(builder.equal(root.get(Voucher._isTrash), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Voucher._isHidden), Boolean.FALSE));
            predicates.add(builder.equal(root.get(Voucher._status), Voucher.STATUS_ACTIVE));
            predicates.add(builder.isNotNull(root.get(Voucher._platform)));
            predicates.add(builder.isNotNull(root.get(Voucher._platformDisplay)));
            predicates.add(builder.equal(root.get(Voucher._orgId), orgId));
            predicates.add(builder.or(
                    builder.equal(root.get(Voucher._userType), Voucher.USER_TYPE_ALL),
                    builder.exists(subQuery)
            ));
            predicates.add(
                    builder.and(
                            builder.lessThan(root.get(Voucher._startDate), Instant.now()),
                            builder.greaterThan(root.get(Voucher._endDate), Instant.now())
                    )
            );
            query.orderBy(builder.asc(root.get(Voucher._name)));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        return checkPlatformVoucher(platform, voucherList, view);
    }

    public List<Voucher> checkPlatformVoucher(Integer platform, List<Voucher> voucherList, CartView cartView) {
        List<Voucher> vouchers = new ArrayList<>();
        if (voucherList.size() > 0) {
            for (Voucher voucher : voucherList) {
                if ((voucher.getPlatform() & platform) > 0) {
                    voucher.setIsEligible(true);
                    voucher.setVoucherHint(voucher.getDescription());
                    this.checkVoucher(voucher, cartView);
                    vouchers.add(voucher);
                } else {
                    voucher.setIsEligible(false);
                    this.setCartErrorList(voucher, platform);
                    if ((voucher.getPlatformDisplay() & platform) > 0) {
                        vouchers.add(voucher);
                    }
                }
            }
        }
        return vouchers;
    }

    private void setCartErrorList(Voucher voucher, Integer platform) {
        if ((voucher.getPlatform() & platform) == 0) {
            if ((voucher.getPlatform() & Platform.APP) > 0) {
                /*voucher chỉ áp dụng cho phiên bản app*/
                voucher.getCartErrorList().add(VoucherErrorEnum.platform_app.getValue());
                return;
            }
            if ((voucher.getPlatform() & Platform.WEB) > 0) {
                /*voucher chỉ áp dụng cho phiên bản website*/
                voucher.getCartErrorList().add(VoucherErrorEnum.platform_web.getValue());
                return;
            }
            if ((voucher.getPlatform() & Platform.WEB_MOBILE) > 0) {
                /*voucher chỉ áp dụng cho phiên bản website mobile*/
                voucher.getCartErrorList().add(VoucherErrorEnum.platform_web_mobile.getValue());
            }
        }
    }

    public void checkVoucher(Voucher voucher, CartView cartView) {
        if (Instant.now().isBefore(voucher.getStartDate())) {
            /* chưa đến thời gian kích hoạt*/
            voucher.getCartErrorList().add(VoucherErrorEnum.start_date_invalid.getValue());
            voucher.setVoucherHint(VoucherErrorEnum.start_date_invalid.getValue());
            voucher.setIsEligible(false);
            return;
        }
        if (Instant.now().isAfter(voucher.getEndDate())) {
            /* chưa đến thời gian kích hoạt*/
            voucher.getCartErrorList().add(VoucherErrorEnum.end_date_invalid.getValue());
            voucher.setVoucherHint(VoucherErrorEnum.end_date_invalid.getValue());
            voucher.setIsEligible(false);
            return;
        }
        if (voucher.getVoucherUsed() >= voucher.getVoucherNumber()) {
            /* voucher đã sử dụng hết lượt*/
            voucher.getCartErrorList().add(VoucherErrorEnum.order_remaining_invalid.getValue());
            voucher.setVoucherHint(VoucherErrorEnum.order_remaining_invalid.getValue());
            voucher.setIsEligible(false);
            return;
        }
        if (cartView != null) {
            if (voucher.getBuyMoneyMin() != null && cartView.getTotal() != null
                    && voucher.getBuyMoneyMin() > cartView.getTotal()) {
                /* chưa đạt giá trị đơn hàng tối thiểu */
                voucher.getCartErrorList().add(VoucherErrorEnum.min_order_invalid.getValue());
                voucher.setVoucherHint(VoucherErrorEnum.min_order_invalid.getValue());
                voucher.setIsEligible(false);
            }
        }
    }
}
