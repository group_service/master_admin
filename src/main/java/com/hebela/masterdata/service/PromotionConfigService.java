package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.PromotionConfig;

public interface PromotionConfigService extends Service<PromotionConfig, Integer> {
}

