package com.hebela.masterdata.service;

import com.hebela.core.AbstractBo;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.AdrProvince;
import com.hebela.masterdata.bo.ShippingFee;
import com.hebela.masterdata.bo.ShippingFeeLocation;
import com.hebela.masterdata.dao.AdrProvinceDao;
import com.hebela.masterdata.dao.ShippingFeeDao;
import com.hebela.masterdata.dao.ShippingFeeLocationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ShippingFeeServiceImpl extends BaseService<ShippingFee, Integer> implements ShippingFeeService {
    @Autowired
    private ShippingFeeLocationDao shippingFeeLocationDao;
    @Autowired
    private ShippingFeeDao shippingFeeDao;
    @Autowired
    private AdrProvinceDao adrProvinceDao;

    @Override
    public ShippingFee get(Integer id) {
        ShippingFee dbBo = super.get(id);
        List<AdrProvince> provinceList = this.adrProvinceDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            query.distinct(true);
            Root<ShippingFeeLocation> shippingFeeLocationRoot = query.from(ShippingFeeLocation.class);
            predicates.add(builder.equal(shippingFeeLocationRoot.get(ShippingFeeLocation._shippingFeeId), id));
            if (dbBo.getLocationType().equals(ShippingFee.LOCATION_TYPE_ALL)) {
                predicates.add(builder.equal(root.get(AdrProvince._id), shippingFeeLocationRoot.get(ShippingFeeLocation._provinceIdExclude)));
            } else if (dbBo.getLocationType().equals(ShippingFee.LOCATION_TYPE_PROVINCE)) {
                predicates.add(builder.equal(root.get(AdrProvince._id), shippingFeeLocationRoot.get(ShippingFeeLocation._provinceIdInclude)));
            }
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        });
        dbBo.setProvinceList(provinceList);
        return dbBo;
    }

    @Override
    public ShippingFee doInsert(ShippingFee bo) {
        bo.setShippingType(ShippingFee.SHIPPING_TYPE_HEBELA);
        if (bo.getMoneyMin() == null) {
            bo.setMoneyMin(ShippingFee.MONEY_MIN_DEFAULT);
        }
        if (bo.getMoneyMax() == null) {
            bo.setMoneyMax(ShippingFee.MONEY_MAX_DEFAULT);
        }
        if (bo.getLocationType() == null) {
            throw new HebelaException("data.input.invalid");
        }
        if (bo.getLocationType().equals(ShippingFee.LOCATION_TYPE_PROVINCE)
                && (bo.getProvinceList() == null || bo.getProvinceList().size() == 0)) {
            throw new HebelaException("data.input.invalid");
        }
        validateDataInput(bo);
        validateProvince(bo, false, null);
        ShippingFee dbBo = super.doInsert(bo);

        if (bo.getProvinceList() != null && bo.getProvinceList().size() > 0) {
            List<ShippingFeeLocation> shippingFeeLocationList = new ArrayList<>();
            for (AdrProvince province : bo.getProvinceList()) {
                ShippingFeeLocation shippingFeeLocation = new ShippingFeeLocation();
                shippingFeeLocation.setShippingFeeId(dbBo.getId());
                if (dbBo.getLocationType().equals(ShippingFee.LOCATION_TYPE_ALL)) {
                    shippingFeeLocation.setProvinceIdExclude(province.getId());
                } else {
                    shippingFeeLocation.setProvinceIdInclude(province.getId());
                }
                shippingFeeLocation.setCreatedBy(User.getContextId());
                shippingFeeLocationList.add(shippingFeeLocation);
            }
            shippingFeeLocationDao.saveAll(shippingFeeLocationList);
        }
        return bo;
    }

    @Override
    public ShippingFee doUpdate(ShippingFee bo) {
        ShippingFee dbBo = get(bo.getId());
        dbBo.setName(bo.getName());
        if (bo.getMoneyMin() == null) {
            bo.setMoneyMin(ShippingFee.MONEY_MIN_DEFAULT);
        }
        if (bo.getMoneyMax() == null) {
            bo.setMoneyMax(ShippingFee.MONEY_MAX_DEFAULT);
        }
        dbBo.setMoneyMin(bo.getMoneyMin());
        dbBo.setMoneyMax(bo.getMoneyMax());
        dbBo.setFee(bo.getFee());
        validateDataInput(dbBo);
        bo.setLocationType(dbBo.getLocationType());
        bo.setProvinceList(dbBo.getProvinceList());
        validateProvince(bo, false, dbBo.getId());
        dao.save(dbBo);
        return bo;
    }

    /**
     * Thêm Tỉnh/TP vào biểu phí
     */
    @Override
    public ShippingFee doAddProvince(ShippingFee bo) {
        if (bo.getProvinceList() == null || bo.getProvinceList().size() == 0) {
            throw new HebelaException("data.input.invalid");
        }
        ShippingFee dbBo = super.get(bo.getId());
        bo.setLocationType(dbBo.getLocationType());
        bo.setMoneyMin(dbBo.getMoneyMin());
        bo.setMoneyMax(dbBo.getMoneyMax());
        if (bo.getLocationType().equals(ShippingFee.LOCATION_TYPE_PROVINCE)) {
            validateProvince(bo, false, bo.getId());
        }
        List<ShippingFeeLocation> shippingFeeLocationList = new ArrayList<>();

        for (AdrProvince province : bo.getProvinceList()) {
            ShippingFeeLocation shippingFeeLocation = new ShippingFeeLocation();
            shippingFeeLocation.setShippingFeeId(dbBo.getId());
            if (dbBo.getLocationType().equals(ShippingFee.LOCATION_TYPE_ALL)) {
                shippingFeeLocation.setProvinceIdExclude(province.getId());
            } else {
                shippingFeeLocation.setProvinceIdInclude(province.getId());
            }
            shippingFeeLocation.setCreatedBy(User.getContextId());
            shippingFeeLocationList.add(shippingFeeLocation);
        }
        shippingFeeLocationDao.saveAll(shippingFeeLocationList);
        return bo;
    }

    /*
     * Loại bỏ Tỉnh/TP khỏi biểu phí
     * */
    @Override
    public ShippingFee doRemoveProvince(ShippingFee bo) {
        if (bo.getProvinceList() == null || bo.getProvinceList().size() == 0) {
            throw new HebelaException("data.input.invalid");
        }
        ShippingFee dbBo = super.get(bo.getId());
        if (dbBo.getLocationType().equals(ShippingFee.LOCATION_TYPE_ALL)) {
            bo.setLocationType(dbBo.getLocationType());
            validateProvince(bo, true, null);
        }
        List<ShippingFeeLocation> shippingFeeLocationList;
        for (AdrProvince province : bo.getProvinceList()) {
            ShippingFeeLocation shippingFeeLocation = new ShippingFeeLocation();
            shippingFeeLocation.setShippingFeeId(dbBo.getId());
            if (dbBo.getLocationType().equals(ShippingFee.LOCATION_TYPE_ALL)) {
                shippingFeeLocation.setProvinceIdExclude(province.getId());
            } else {
                shippingFeeLocation.setProvinceIdInclude(province.getId());
            }
            shippingFeeLocationList = shippingFeeLocationDao.findAll(Example.of(shippingFeeLocation));
            shippingFeeLocationDao.deleteAll(shippingFeeLocationList);
        }
        if (dbBo.getLocationType().equals(ShippingFee.LOCATION_TYPE_PROVINCE)) {
            ShippingFeeLocation shippingFeeLocation = new ShippingFeeLocation();
            shippingFeeLocation.setShippingFeeId(dbBo.getId());
            if (shippingFeeLocationDao.count(Example.of(shippingFeeLocation)) == 0) {
                throw new HebelaException("Danh sách Tỉnh/Thành phố không được trống");
            }

        }
        return bo;
    }

    private void validateDataInput(ShippingFee bo) {
        if (bo.getMoneyMin() < 0 || bo.getMoneyMin() > ShippingFee.MONEY_MAX_DEFAULT
                || bo.getMoneyMax() < 0 || bo.getMoneyMax() > ShippingFee.MONEY_MAX_DEFAULT
                || bo.getMoneyMin() > bo.getMoneyMax()) {
            throw new HebelaException("data.input.invalid");
        }
    }

    @Override
    public List<ShippingFee> find(ShippingFee bo) {
        List<ShippingFee> shippingFeeList = super.find(bo);
        List<ShippingFeeLocation> shippingFeeLocationList = shippingFeeLocationDao.findAll();
        Map<Integer, AdrProvince> provinceMap = this.adrProvinceDao.findAll().stream()
                .collect(Collectors.toMap(AdrProvince::getId, adrProvince -> adrProvince));
        for (ShippingFee shippingFee : shippingFeeList) {
            List<ShippingFeeLocation> shippingFeeLocations = shippingFeeLocationList.stream()
                    .filter(item -> item.getShippingFeeId().equals(shippingFee.getId()))
                    .collect(Collectors.toList());
            List<AdrProvince> provinceList = new ArrayList<>();
            for (ShippingFeeLocation location : shippingFeeLocations) {
                if (ShippingFee.LOCATION_TYPE_ALL.equals(shippingFee.getLocationType())) {
                    provinceList.add(provinceMap.get(location.getProvinceIdExclude()));
                } else {
                    provinceList.add(provinceMap.get(location.getProvinceIdInclude()));
                }
            }
            shippingFee.setProvinceList(provinceList);
        }
        return shippingFeeList;
    }

    private List<Integer> findProvinceIdAvailable(Integer moneyMin, Integer moneyMax, Integer shippingFeeIdDb) {
        List<Integer> includeList = shippingFeeDao.findProvinceInclude(moneyMin, moneyMax, shippingFeeIdDb);
        List<Integer> excludeList = shippingFeeDao.findProvinceExclude(moneyMin, moneyMax, shippingFeeIdDb);
        List<AdrProvince> provinceList = adrProvinceDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (includeList != null && includeList.size() > 0) {
                predicates.add(builder.and(root.get(AdrProvince._id).in(includeList).not()));
            }
            if (excludeList != null && excludeList.size() > 0) {
                predicates.add(builder.and(root.get(AdrProvince._id).in(excludeList)));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        return provinceList.stream().map(AdrProvince::getId).collect(Collectors.toList());
    }

    private void validateProvince(ShippingFee bo, boolean isRemove, Integer shippingFeeIdDb) {
        List<Integer> provinceIdList = bo.getProvinceList().stream().map(AbstractBo::getId).collect(Collectors.toList());
        List<Integer> provinceIdIncludeList;
        if (bo.getLocationType().equals(ShippingFee.LOCATION_TYPE_PROVINCE) || isRemove) {
            provinceIdIncludeList = provinceIdList;
        } else {
            provinceIdIncludeList = adrProvinceDao.findAll((root, query, builder) -> {
                final List<Predicate> predicates = new ArrayList<>();
                if (provinceIdList.size() > 0) {
                    predicates.add(builder.and(root.get(AdrProvince._id).in(provinceIdList).not()));
                }
                return builder.and(predicates.toArray(new Predicate[0]));
            }).stream().map(AdrProvince::getId).collect(Collectors.toList());
        }
        List<Integer> provinceIdAvailableList = findProvinceIdAvailable(bo.getMoneyMin(), bo.getMoneyMax(), shippingFeeIdDb);
        Integer provinceId = provinceIdIncludeList.stream().filter(item -> !provinceIdAvailableList.contains(item)).findFirst().orElse(null);
        if (provinceId != null) {
            List<ShippingFee> shippingFeeList = findShippingFree(provinceId, bo.getMoneyMin(), bo.getMoneyMax(), shippingFeeIdDb);
            ShippingFee shippingFeeExist = new ShippingFee();
            if (shippingFeeList != null && shippingFeeList.size() > 0) {
                shippingFeeExist = shippingFeeList.get(0);
            }
            AdrProvince dbProvince = adrProvinceDao.getOne(provinceId);
            throw new HebelaException("Trùng thành phố [" + dbProvince.getName() + "] với biểu phí [" + shippingFeeExist.getName() + "]");
        }
    }
}

