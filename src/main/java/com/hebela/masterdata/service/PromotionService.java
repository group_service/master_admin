package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Promotion;
import com.hebela.masterdata.web.promotionTimeFrame.PromotionTimeFrameRequest;
import com.hebela.masterdata.web.promotion.PromotionSearchRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PromotionService extends StatusService<Promotion> {

    Promotion doActivate(Promotion bo);

    Promotion doDeactivate(Promotion bo);

    Page<Promotion> findBySearch(PromotionSearchRequest model);

    Page<Promotion> findPromotionForShop(PromotionSearchRequest model);

    Promotion findPromotion(PromotionTimeFrameRequest model);

    Promotion findPromotionWithCategory(PromotionTimeFrameRequest model);

    List<Promotion> findPromotionActive();
}

