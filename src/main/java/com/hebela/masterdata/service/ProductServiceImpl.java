package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.obj.AttributeModel;
import com.hebela.masterdata.util.HtmlCleaner;
import com.hebela.masterdata.web.product.ProductSearchRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl extends AbstractStatusService<Product> implements ProductService {
    @Autowired
    private ProductDao productDao;
    @Autowired
    private AttributeDao attributeDao;
    @Autowired
    private CouponDao couponDao;
    @Autowired
    private CouponProductLinkDao couponProductLinkDao;
    @Autowired
    private CouponGiftLinkDao couponGiftLinkDao;
    @Autowired
    private ProductPriceDao productPriceDao;
    @Autowired
    private ProductGalleryDao productGalleryDao;
    @Autowired
    private BrandService brandService;
    @Autowired
    private GroupProductLinkDao groupProductDao;
    @Autowired
    private ProductAttributeLinkDao productAttributeLinkDao;
    @Autowired
    private FavoriteDao favoriteDao;
    @Autowired
    private CartDao cartDao;
    @Autowired
    private ProductGiftDao productGiftDao;
    @Autowired
    private ProductComboDao productComboDao;
    @Autowired
    private MetaConfigDao metaConfigDao;
    @Autowired
    private ProductGiftService productGiftService;
    @Autowired
    private ReviewSummaryDao reviewSummaryDao;
    @Autowired
    private SuperVoucherProductLinkDao superVoucherProductLinkDao;
    @Autowired
    private VoucherDao voucherDao;
    @Autowired
    private VoucherGiftLinkDao voucherGiftLinkDao;

    /*Get product for update*/
    @Override
    public Product get(Integer productId) {
        if (productId == null) {
            throw new HebelaException("error.bo.empty");
        } else {
            Product dbBo = this.dao.findById(productId).orElse(null);
            if (dbBo == null) {
                throw new HebelaException("product.not.exists");
            }
            dbBo.setBrand(brandService.get(dbBo.getBrandId()));
            ProductPrice productPrice = new ProductPrice();
            productPrice.setProductId(dbBo.getId());
            List<ProductPrice> productPriceList = this.productPriceDao.findAll(Example.of(productPrice), Sort.by(Sort.Order.asc(ProductPrice._createdDate)));
            productPriceList.forEach(ProductPrice::init);
            dbBo.setProductPrice(ProductPrice.calculateTimeline(productPriceList));
            dbBo.setProductPriceList(productPriceList);
            dbBo.setGalleryList(this.findGallery(dbBo.getId()));
            dbBo.setAttributeLinkList(this.productAttributeLinkDao.findAllByProductId(dbBo.getId()));
            Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                    MetaConfig.getMetaConfigExample(productId, MetaConfig.TYPE_PRODUCT)));
            dbMetaConfigOptional.ifPresent(dbBo::setMetaConfig);
            return dbBo;
        }
    }

    /*
     * Tạo mới sản phẩm
     * Nếu status = 1 thì tạo và Đăng bán luôn, ngược lại thì cập nhật trạng thái là "Nháp"
     * Giá sản phẩm được áp dụng ngày tự động từ min đến max của hệ thống
     * */
    @Override
    public Product doInsert(Product bo) {
        this.validatePriceProduct(bo);
        if (Product.STATUS_ACTIVE.equals(bo.getStatus())) {
            bo.setPublishedTime(Instant.now());
        } else {
            bo.setStatus(Product.STATUS_DRAFT);
        }
        bo.setEnableReviews(bo.getEnableReviews() != null ? bo.getEnableReviews() : Boolean.FALSE);
        bo.setAmount(0);
        Product dbBo = super.doInsert(bo);
        bo.getProductPrice().setProductId(dbBo.getId());
        bo.getProductPrice().setSku(dbBo.getSku());
        bo.getProductPrice().setStartDate(Instant.parse(ProductPrice.MIN_DATE));
        bo.getProductPrice().setEndDate(Instant.parse(ProductPrice.MAX_DATE));
        productPriceDao.save(bo.getProductPrice());
        List<ProductGallery> galleryList = this.doInsertProductGallery(bo.getId(), bo.getGalleryList());
        dbBo.setGalleryList(galleryList);
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setProductId(dbBo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_PRODUCT);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        this.insertAttribute(dbBo, bo.getAttributeLinkList());
        return dbBo;
    }

    /*
     * Cập nhật sản phẩm
     * Chỉ cập nhật thông tin, không thay đổi trạng thái
     * Khi trạng thái đang là "Đăng bán" thì không được cập nhật type từ sản phẩm kinh doanh sang quà tặng
     * Nếu trạng thái là "Nháp" thì cho phép cập nhật lại giá, ngược lại không cập nhật giá
     * */
    @Override
    public Product doUpdate(Product bo) {
        Product dbBo = super.get(bo.getId());
        dbBo.setName(bo.getName());
        dbBo.setSlug(bo.getSlug());
        if (!Organization.ROOT_ADMIN.getId().equals(dbBo.getOrgId())) {
            dbBo.setSku(bo.getSku());
        }
        dbBo.setPublishedCodeMoh(bo.getPublishedCodeMoh());
        dbBo.setBrandId(bo.getBrandId());
        dbBo.setProductCategoryId(bo.getProductCategoryId());
        dbBo.setMaxPurchaseQuantity(bo.getMaxPurchaseQuantity());
        this.updateAttribute(dbBo, bo.getAttributeLinkList());
        dbBo.setDisplayDiscountType(bo.getDisplayDiscountType());
        dbBo.setDescriptionSummary(bo.getDescriptionSummary());
        if (bo.getDescriptionUse() != null) {
            dbBo.setDescriptionUse(HtmlCleaner.clean(bo.getDescriptionUse()));
        } else {
            dbBo.setDescriptionUse(null);
        }
        if (bo.getDescriptionElement() != null) {
            dbBo.setDescriptionElement(HtmlCleaner.clean(bo.getDescriptionElement()));
        } else {
            dbBo.setDescriptionElement(null);
        }
        /*Document document = Jsoup.parse(bo.getDescriptionFull());
        for (Element element : document.getAllElements()) {
            if (element.tagName() != "p"
                    && element.tagName() != "h1"
                    && element.tagName() != "h2"
                    && element.tagName() != "h3"
                    && element.tagName() != "h4"
                    && element.tagName() != "h5"
                    && element.tagName() != "h6") {
                element.removeAttr("style");
                element.tagName("p");
            }
        }
        dbBo.setDescriptionFull(document.toString());*/
        // dbBo.setDescriptionFull(Jsoup.clean(bo.getDescriptionFull(), Whitelist.relaxed()));
        if (bo.getDescriptionFull() != null) {
            dbBo.setDescriptionFull(HtmlCleaner.clean(bo.getDescriptionFull()));
        } else {
            dbBo.setDescriptionFull(null);
        }
        if (Product.STATUS_DRAFT.equals(dbBo.getStatus())) {
            ProductPrice productPriceExample = new ProductPrice();
            productPriceExample.setProductId(dbBo.getId());
            Optional<ProductPrice> dbProductPriceOptional = this.productPriceDao.findOne(Example.of(productPriceExample));
            if (dbProductPriceOptional.isPresent()) {
                ProductPrice dbProductPrice = dbProductPriceOptional.get();
                dbProductPrice.setPriceRegular(bo.getProductPrice().getPriceRegular());
                dbProductPrice.setPriceSale(bo.getProductPrice().getPriceSale());
                dbProductPrice.setSku(bo.getSku());
                dbProductPrice.setModifiedBy(User.getContextId());
                this.productPriceDao.save(dbProductPrice);
                dbBo.setProductPrice(dbProductPrice);
            }
        }

        dbBo.setImageThumbnail(bo.getImageThumbnail());
        dbBo.setGalleryList(this.doUpdateProductGallery(bo.getId(), dbBo.getOrgId(), bo.getGalleryList()));
        dbBo.setVideoUrl(bo.getVideoUrl());

        if (Product.STATUS_ACTIVE.equals(dbBo.getStatus()) && Product.TYPE_GIFT.equals(bo.getType())) {
            /*Không cập nhật type*/
        } else {
            dbBo.setType(bo.getType());
        }

        dbBo.setPurchaseNote(bo.getPurchaseNote());
        dbBo.setEnableReviews(bo.getEnableReviews());

        dbBo.setModifiedBy(User.getContextId());
        this.dao.save(dbBo);

        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_PRODUCT, User.getContextId(), bo.getMetaConfig());
        }
        return dbBo;
    }

    /*
     * Đăng bán sản phẩm
     * Chuyển trạng thái từ "Nháp" hoặc "Ngừng bán" sang "Đăng bán"
     * Sản phẩm phải chưa đăng bán và không bị hủy
     */
    @Override
    public Product doActivate(Product bo) {
        Product dbBo = super.get(bo.getId());
        if (Product.STATUS_ACTIVE.equals(dbBo.getStatus()) || Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Product.STATUS_ACTIVE);
        dbBo.setPublishedTime(Instant.now());
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    /*
     * Ngừng bán sản phẩm
     * Pre: Sản phẩm phải ở trạng thái "Đăng bán"
     * Action: Chuyển trạng thái từ "Đăng bán" sang "Ngừng bán"
     * Post:
     * Xóa các liên kết với sản phẩm khác nếu nó là quà tặng
     * Hủy các combo chứa nó
     * Nếu sản phẩm có liên kết tới coupon thì deactivate coupon
     **/
    @Override
    public Product doDeactivate(Product bo) {
        Product dbBo = super.get(bo.getId());
        if (!Product.STATUS_ACTIVE.equals(dbBo.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(Product.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        /*Xóa liên kết quà tặng*/
        removeProductGift(bo.getId());
        /*Xóa liên kết yêu thích*/
        removeProductInFavorite(bo.getId());
        //Xóa sản phẩm liên kết với voucher shop
        removeProductInVoucher(bo.getId());
        /**Xoá sản phẩm khỏi giỏ hàng*/
        removeProductInCart(bo.getId());

        /*Ngừng bán các combo chứa nó*/
        List<ProductCombo> productComboList = productComboDao.findByProductId(dbBo.getId(), null, Boolean.FALSE, null);
        for (ProductCombo productCombo : productComboList) {
            productCombo.setStatus(ProductCombo.STATUS_INACTIVE);
            productCombo.setModifiedBy(User.getContextId());
            productComboDao.save(productCombo);
        }

        /*Xoá sản phẩm khỏi nhóm sản phẩm*/
        List<GroupProductLink> groupProductLinks = groupProductDao.findAll((root, query, builder) ->
                builder.equal(root.get(GroupProductLink._productId), bo.getId()));
        groupProductDao.deleteAll(groupProductLinks);

        /*Nếu sản phẩm có liên kết tới couponProduct thì xoá sản phẩm khỏi coupon*/
        List<Coupon> couponProductList = couponDao.findByProductId(bo.getId());
        if (couponProductList.size() > 0) {
            List<Integer> couponProductIdList = couponProductList.stream().map(Coupon::getId).collect(Collectors.toList());
            List<CouponProductLink> couponProductLinks = couponProductLinkDao.findAllByCouponIdsAndProductId(couponProductIdList, bo.getId());
            couponProductLinkDao.deleteAll(couponProductLinks);
        }

        /*Nếu sản phẩm có liên kết tới superVoucher thì xoá sản phẩm khỏi superVoucher*/
        SuperVoucherProductLink example = new SuperVoucherProductLink();
        example.setProductId(bo.getId());
        List<SuperVoucherProductLink> superVoucherProductLinks = superVoucherProductLinkDao.findAll(Example.of(example));
        if (superVoucherProductLinks.size() > 0) {
            superVoucherProductLinkDao.deleteAll(superVoucherProductLinks);
        }

        /* Nếu sản phẩm có liên kết tới couponGift thì xoá sản phẩm khỏi coupon
         * Pre
         *  Coupon có discountType = gift
         *  Nếu coupon ở trạng thái ACTIVE thì check còn đủ số lượng sản phẩm quà tặng không
         *  Nếu coupon ở trạng thái INACTIVE thì huỷ liên kết với coupon
         * */
        List<Coupon> couponGiftList = couponDao.findByProductGiftId(bo.getId());
        List<Integer> couponGiftIdList = new ArrayList<>();
        if (couponGiftList.size() > 0) {
            for (Coupon coupon : couponGiftList) {
                if (Coupon.DISCOUNT_TYPE_GIFT.equals(coupon.getDiscountType())) {
                    if (Coupon.STATUS_ACTIVE.equals(coupon.getStatus())) {
                        CouponGiftLink couponGiftLink = new CouponGiftLink();
                        couponGiftLink.setCouponId(coupon.getId());
                        List<CouponGiftLink> couponGiftLinkList = couponGiftLinkDao.findAll(Example.of(couponGiftLink));
                        if (couponGiftLinkList.size() <= coupon.getNumberOfGift()) {
                            coupon.setStatus(Coupon.STATUS_INACTIVE);
                            coupon.setModifiedBy(User.getContextId());
                            couponDao.save(coupon);
                        }
                        couponGiftIdList.add(coupon.getId());
                    } else {
                        couponGiftIdList.add(coupon.getId());
                    }
                }
            }
            if (couponGiftIdList.size() > 0) {
                List<CouponGiftLink> couponGiftLinks = couponGiftLinkDao.findAllByCouponIdsAndProductId(couponGiftIdList, bo.getId());
                couponGiftLinkDao.deleteAll(couponGiftLinks);
            }
        }
        return dbBo;
    }

    /*
     * Hủy sản phẩm
     * Pre: Sản phẩm ở trạng thái "Nháp" hoặc "Ngừng bán"
     * */
    @Override
    public Product doTrash(Product bo) {
        Product dbBo = super.get(bo.getId());
        if (!Product.STATUS_DRAFT.equals(dbBo.getStatus()) && !Product.STATUS_INACTIVE.equals(dbBo.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        /*Xóa liên kết yêu thích*/
        removeProductInFavorite(bo.getId());
        //Xóa sản phẩm liên kết với voucher shop
        removeProductInVoucher(bo.getId());
        dbBo.setIsTrash(Boolean.TRUE);
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    /*
     * Xóa vĩnh viễn sản phẩm
     * Pre: Sản phẩm đã bị hủy và có trạng thái "Nháp"
     * */
    @Override
    public Product doDelete(Product bo) {
        Product dbBo = super.get(bo.getId());
        if (!dbBo.getIsTrash().equals(Boolean.TRUE) || !dbBo.getStatus().equals(Product.STATUS_DRAFT)) {
            throw new HebelaException("action.invalid");
        }
        return super.doDelete(bo);
    }

    /*
     * Cập nhật giá sản phẩm chỉ áp dụng khi sản phẩm ở trạng thái "Đăng bán"
     * Nếu cập nhật giá current: Chỉ cho cập nhật giá niêm yết
     * Nếu cập nhật giá future: Cho phép cập nhật tất cả, thời gian bắt đầu lớn hơn hiện tại
     * Không được cập nhật giá past
     * Nếu thêm giá mới thời gian bắt đầu lớn hơn hiện tại
     */
    @Override
    public ProductPrice doUpdatePrice(ProductPrice productPrice) {
        if (productPrice.getProductId() == null) {
            throw new HebelaException("action.invalid");
        }
        Product dbProduct = super.get(productPrice.getProductId());
        dbProduct.setProductPrice(productPrice);
        this.validatePriceProduct(dbProduct);
        if (dbProduct.getIsTrash().equals(Boolean.TRUE) || !dbProduct.getStatus().equals(Product.STATUS_ACTIVE)) {
            throw new HebelaException("action.invalid");
        }
        ProductPrice example = new ProductPrice();
        example.setProductId(dbProduct.getId());
        List<ProductPrice> productPriceList = this.productPriceDao.findAll(Example.of(example), Sort.by(Sort.Order.asc(ProductPrice._createdDate)));
        ProductPrice productPriceCurrent = ProductPrice.calculateTimeline(productPriceList);
        if (productPrice.getId() != null) {
            Optional<ProductPrice> productPriceOptional = productPriceList.stream().filter(item -> item.getId().equals(productPrice.getId())).findFirst();
            if (productPriceOptional.isPresent()) {
                ProductPrice productPriceUpdate = productPriceOptional.get();
                if (ProductPrice.TIMELINE_PAST.equals(productPriceUpdate.getTimeline())) {
                    throw new HebelaException("action.invalid");
                } else if (ProductPrice.TIMELINE_CURRENT.equals(productPriceUpdate.getTimeline())) {
                    productPriceUpdate.setPriceRegular(productPrice.getPriceRegular());
                    productPriceUpdate.setModifiedBy(User.getContextId());
                    productPriceUpdate.init();
                    return productPriceDao.save(productPriceUpdate);
                } else if (ProductPrice.TIMELINE_FUTURE.equals(productPriceUpdate.getTimeline())) {
                    if (Instant.now().isAfter(productPrice.getStartDate())) {
                        throw new HebelaException("product.price.start.date.invalid");
                    }
                    productPriceCurrent.setEndDate(productPrice.getStartDate());
                    productPriceCurrent.setModifiedBy(User.getContextId());
                    productPriceDao.save(productPriceCurrent);
                    productPriceUpdate.setPriceRegular(productPrice.getPriceRegular());
                    productPriceUpdate.setPriceSale(productPrice.getPriceSale());
                    productPriceUpdate.setStartDate(productPrice.getStartDate());
                    productPriceUpdate.setModifiedBy(User.getContextId());
                    productPriceUpdate.init();
                    return productPriceDao.save(productPriceUpdate);
                }

            }
        } else {
            if (Instant.now().isAfter(productPrice.getStartDate()) ||
                    ProductPrice.TIMELINE_FUTURE.equals(productPriceList.get(productPriceList.size() - 1).getTimeline())) {
                throw new HebelaException("product.price.start.date.invalid");
            }
            productPriceCurrent.setEndDate(productPrice.getStartDate());
            productPriceCurrent.setModifiedBy(User.getContextId());
            productPriceDao.save(productPriceCurrent);
            productPrice.setSku(dbProduct.getSku());
            productPrice.setEndDate(Instant.parse(ProductPrice.MAX_DATE));
            productPrice.setCreatedBy(User.getContextId());
            productPrice.setTimeline(ProductPrice.TIMELINE_FUTURE);
            productPrice.init();
            productPriceDao.save(productPrice);
        }
        return productPrice;
    }

    /*
     * Xóa giá: Chỉ áp dụng cho sản phẩm đang đăng bán và muốn xóa giá tương lai
     * */
    @Override
    public ProductPrice doDeletePrice(ProductPrice productPrice) {
        ProductPrice dbPrice = productPriceDao.getOne(productPrice.getId());
        Product dbProduct = productDao.getOne(dbPrice.getProductId());
        if (dbProduct.getIsTrash().equals(Boolean.TRUE)
                || !dbProduct.getStatus().equals(Product.STATUS_ACTIVE)
                || !Instant.now().isBefore(dbPrice.getStartDate())) {
            throw new HebelaException("action.invalid");
        }
        ProductPrice example = new ProductPrice();
        example.setProductId(dbProduct.getId());
        List<ProductPrice> productPriceList = productPriceDao.findAll(Example.of(example), Sort.by(Sort.Order.asc(ProductPrice._createdDate)));
        ProductPrice productPriceCurrent = ProductPrice.calculateTimeline(productPriceList);
        productPriceCurrent.setEndDate(dbPrice.getEndDate());
        productPriceCurrent.setModifiedBy(User.getContextId());
        productPriceDao.save(productPriceCurrent);
        productPriceDao.delete(dbPrice);
        return productPrice;
    }

    /*Xóa liên kết quà tặng*/
    private void removeProductGift(Integer productId) {
        List<ProductGift> productGiftList = productGiftDao.findAll((root, query, builder) -> builder.or(
                builder.equal(root.get(ProductGift._productId), productId),
                builder.equal(root.get(ProductGift._productGiftId), productId)
        ));
        productGiftDao.deleteAll(productGiftList);
    }

    /*Xóa liên kết yêu thích*/
    private void removeProductInFavorite(Integer productId) {
        Favorite favorite = new Favorite();
        favorite.setProductId(productId);
        List<Favorite> favoriteList = favoriteDao.findAll(Example.of(favorite));
        favoriteDao.deleteAll(favoriteList);
    }

    private void removeProductInCart(Integer productId) {
        Cart cart = new Cart();
        cart.setProductId(productId);
        List<Cart> cartList = cartDao.findAll(Example.of(cart));
        cartDao.deleteAll(cartList);
    }

    private void removeProductInVoucher(Integer productId) {
        /* Xóa liên kết sản phẩm khỏi voucher*/
        List<Voucher> voucherProductList = this.voucherDao.findVoucherByProductId(productId);
        for (Voucher voucher : voucherProductList) {
            VoucherGiftLink voucherGiftLink = new VoucherGiftLink();
            voucherGiftLink.setVoucherId(voucher.getId());
            List<VoucherGiftLink> voucherProductGiftLink = this.voucherGiftLinkDao.findAll(Example.of(voucherGiftLink));
            List<Integer> voucherProductIdList = voucherProductList.stream().map(Voucher::getId).collect(Collectors.toList());
            List<VoucherGiftLink> voucherProductGiftLinkList = this.voucherGiftLinkDao.findVoucherGiftLinkByVoucherIdAndProductId(voucherProductIdList, productId);
            this.voucherGiftLinkDao.deleteAll(voucherProductGiftLinkList);
            // chuyển về trạng thái chưa kích hoạt khi sản phẩm bị ngừng bán hết
            if (voucherProductGiftLink.size() == 1) {
                voucher.setStatus(Voucher.STATUS_INACTIVE);
                this.voucherDao.save(voucher);
            }
        }
    }

    public List<ProductGallery> doInsertProductGallery(Integer productId, List<ProductGallery> insertList) {
        for (int i = 0; i < insertList.size(); i++) {
            ProductGallery gallery = insertList.get(i);
            gallery.setPriority(i + 1);
            if (gallery.getProductId() == null) {
                gallery.setProductId(productId);
            }
        }
        this.productGalleryDao.saveAll(insertList);
        return insertList;
    }

    public List<ProductGallery> doUpdateProductGallery(Integer productId, Integer orgId, List<ProductGallery> insertList) {
        insertList.forEach(item -> {
            if (item.getProductId() == null) {
                item.setProductId(productId);
                item.setOrgId(orgId);
            }
        });
        ArrayList<ProductGallery> result = new ArrayList<>();
        List<ProductGallery> removeList = this.productGalleryDao.findProductGalleriesByProductId(productId);
        for (Iterator<ProductGallery> insertIterator = insertList.iterator(); insertIterator.hasNext(); ) {
            ProductGallery insertItem = insertIterator.next();
            for (Iterator<ProductGallery> removeIterator = removeList.iterator(); removeIterator.hasNext(); ) {
                ProductGallery removeItem = removeIterator.next();
                if (removeItem.getId().equals(insertItem.getId()) && removeItem.getPriority().equals(insertItem.getPriority())) {
                    result.add(removeItem);
                    insertIterator.remove();
                    removeIterator.remove();
                }
            }
        }
        this.productGalleryDao.deleteAll(removeList);
        this.productGalleryDao.saveAll(insertList);
        result.addAll(insertList);
        return result;
    }


    /**
     * @param parent:                     Danh mục cha
     * @param productCategoryItemAllList: Tất cả danh mục
     * @param outputList:                 Danh sách trả về
     */
    private void _buildFlatChildren(ProductCategory parent, List<ProductCategory> productCategoryItemAllList,
                                    List<ProductCategory> outputList) {
        List<ProductCategory> children = productCategoryItemAllList.stream()
                .filter(item -> item.getParentId() != null && item.getParentId().equals(parent.getId()))
                .collect(Collectors.toList());
        if (children.size() > 0) {
            for (ProductCategory item : children) {
                outputList.add(item);
                _buildFlatChildren(item, productCategoryItemAllList, outputList);
            }
        }
    }

    @Override
    public List<ProductGallery> findGallery(Integer productId) {
        return this.productGalleryDao.findAll((root, query, builder) -> {
            query.orderBy(builder.asc(root.get(ProductGallery._priority)));
            return builder.and(
                    builder.equal(root.get(ProductGallery._productId), productId)
            );
        });
    }

    /**
     * - Tìm sản phẩm để gắn vào nhóm sản phẩm
     */
    @Override
    public List<Product> findByTerm(String keyword) {
        if (keyword != null) {
            return this.productDao.findAll((root, query, builder) -> {
                Predicate predicate =
                        builder.and(
                                builder.equal(root.get(Product._isTrash), Boolean.FALSE),
                                builder.equal(root.get(Product._status), Product.STATUS_ACTIVE),
                                builder.equal(root.get(Product._type), Product.TYPE_SELL),
                                builder.or(
                                        builder.like(root.get(Product._name), "%" + keyword + "%"),
                                        builder.like(root.get(Product._sku), "%" + keyword + "%")
                                )
                        );
                if (!Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
                    predicate = builder.and(predicate, builder.equal(root.get(Product._orgId), getOrgId()));
                }
                return predicate;
            });
        }
        return null;
    }

    @Override
    public Page<Product> findBySearch(ProductSearchRequest model) {
        Integer orgId = null;
        if (Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
            orgId = model.getOrgId();
        }
        if (orgId == null) {
            orgId = getOrgId();
        }
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        model.setKeyword(model.getKeyword() != null ? "%" + model.getKeyword() + "%" : "%%");

        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        List<Integer> statusList = model.getStatus() == null ? Arrays.asList(Product.STATUS_ACTIVE, Product.STATUS_INACTIVE, Product.STATUS_DRAFT)
                : Collections.singletonList(model.getStatus());
        return this.productDao.findBySearch(
                orgId,
                model.getKeyword(),
                model.getCategoryId(),
                model.getBrandId(),
                model.getType(),
                model.getIsTrash(),
                statusList,
                pageable
        );
    }

    @Override
    public List<Product> find(Product bo) {
        return super.find(bo);
    }

    private void insertAttribute(Product dbBo, List<ProductAttributeLink> attributeLinkList) {
        attributeLinkList.forEach(item -> {
            item.setProductId(dbBo.getId());
            item.setCreatedBy(User.getContextId());
        });
        this.productAttributeLinkDao.saveAll(attributeLinkList);

    }

    private void updateAttribute(Product dbBo, List<ProductAttributeLink> attributeLinkList) {
        attributeLinkList.forEach(item -> item.setProductId(dbBo.getId()));
        Map<Integer, Integer> clientAttributeMap = attributeLinkList.stream().collect(Collectors.toMap(ProductAttributeLink::getAttributeId, ProductAttributeLink::getAttributeDataId));
        List<Integer> clientAttributeIdList = attributeLinkList.stream().map(ProductAttributeLink::getAttributeId).collect(Collectors.toList());
        ProductAttributeLink productAttributeLink = new ProductAttributeLink();
        productAttributeLink.setProductId(dbBo.getId());
        List<ProductAttributeLink> dbAttributeList = this.productAttributeLinkDao.findAll(Example.of(productAttributeLink));
        List<Integer> dbAttributeIdList = dbAttributeList.stream().map(ProductAttributeLink::getAttributeId).collect(Collectors.toList());
        Map<Boolean, List<ProductAttributeLink>> clientPartitioningMap = attributeLinkList.stream().collect(Collectors.partitioningBy(attribute -> dbAttributeIdList.contains(attribute.getAttributeId())));
        Map<Boolean, List<ProductAttributeLink>> dbPartitioningMap = dbAttributeList.stream().collect(Collectors.partitioningBy(attribute -> clientAttributeIdList.contains(attribute.getAttributeId())));
        /*insert*/
        List<ProductAttributeLink> insertAttributeList = clientPartitioningMap.get(false);/*có ở client nhưng không có ở db */
        insertAttributeList.forEach(item -> item.setCreatedBy(User.getContextId()));
        this.productAttributeLinkDao.saveAll(insertAttributeList);
        /*update*/
        List<ProductAttributeLink> updateAttributeList = dbPartitioningMap.get(true);/*có ở client và có ở db */
        updateAttributeList.forEach(item -> {
            item.setAttributeDataId(clientAttributeMap.get(item.getAttributeId()));
            item.setModifiedBy(User.getContextId());
        });
        this.productAttributeLinkDao.saveAll(updateAttributeList);
        /*remove*/
        List<ProductAttributeLink> removeAttributeList = dbPartitioningMap.get(false);/*không có ở client nhưng có ở db */
        this.productAttributeLinkDao.deleteAll(removeAttributeList);
    }

    public Product getDetail(Integer productId) {
        Product dbBo = this.productDao.getProductAndGift(productId);
        /**
         * check sản phẩm tồn tại
         * */
        if (dbBo == null || !(Product.STATUS_ACTIVE.equals(dbBo.getStatus()) || Product.STATUS_INACTIVE.equals(dbBo.getStatus())
                || Product.TYPE_GIFT.equals(dbBo.getType()))) {
            throw new HebelaException("product.not.exists");
        }
        /** set thương hiệu **/
        Brand brand = null;
        if (dbBo.getBrandId() != null) {
            brand = brandService.get(dbBo.getBrandId());
            dbBo.setBrand(brand);
        }
        /** set thông tin review **/
        dbBo.setAmountTotalReview(0);
        dbBo.setRatingAverage(0F);
        ReviewSummary summary = reviewSummaryDao.findOne((root, query, builder) ->
                builder.and(builder.equal(root.get(ReviewSummary._productId), dbBo.getId()))).orElse(null);
        if (summary != null) {
            dbBo.setAmountTotalReview(summary.getAmountTotal());
            dbBo.setRatingAverage(summary.getRatingAverage());
        }
        dbBo.setGalleryList(this.findGallery(productId));
        dbBo.setGroupProductList(this.findAttributeGroup(dbBo.getGroupId()));
        /*Danh sách thuộc tính*/
        if (brand != null) {
            AttributeModel attributeModel = new AttributeModel();
            attributeModel.setAttributeName("Thương hiệu");
            attributeModel.setAttributeValue(brand.getName());
            dbBo.setAttributeModelList(new ArrayList<>());
            dbBo.getAttributeModelList().add(attributeModel);
        }
        List<AttributeModel> attributeModelList = productAttributeLinkDao.findAttributeModel(dbBo.getId());
        if (attributeModelList != null) {
            if (dbBo.getAttributeModelList() == null) {
                dbBo.setAttributeModelList(new ArrayList<>());
            }
            dbBo.getAttributeModelList().addAll(attributeModelList);
        }

        if (User.getContextId() != null) {
            dbBo.setIsFavorite(this.favoriteDao.findByAccountIdAndProductId(User.getContextId(), productId).size() > 0);
        }
        return dbBo;
    }

    private List<GroupProductLink> findAttributeGroup(Integer groupId) {
        List<Product> productList = groupProductDao.findProductByGroup(groupId);
        if (productList == null || productList.size() < 2) {
            return null;
        }
        List<Attribute> attributeList = attributeDao.findDistinctByGroup(groupId);
        if (attributeList != null && attributeList.size() > 0) {
            List<GroupProductLink> groupProductLinkList = new ArrayList<>();
            for (int i = 0; i < attributeList.size(); i++) {
                Attribute attribute = attributeList.get(i);
                GroupProductLink groupProductLink = new GroupProductLink();
                groupProductLink.setAttributeId(attribute.getId());
                groupProductLink.setAttributeName(attribute.getName());
                if (i == 0) {
                    groupProductLink.setDisplayType(GroupProductLink.DISPLAY_TYPE_IMAGE);
                } else {
                    if (Attribute.TYPE_TEXT.equals(attribute.getType())) {
                        groupProductLink.setDisplayType(GroupProductLink.DISPLAY_TYPE_NAME);
                    } else {
                        groupProductLink.setDisplayType(GroupProductLink.DISPLAY_TYPE_NAME | GroupProductLink.DISPLAY_TYPE_PRICE);
                    }
                }
                groupProductLink.setProductGroupModelList(groupProductDao.findProductGroupModel(groupId, attribute.getId()));
                groupProductLinkList.add(groupProductLink);
            }
            return groupProductLinkList;
        }
        return null;
    }

    private List<ProductAttributeLink> findAttribute(Integer attributeId, List<Integer> productAttributeLinkIdList) {
        return this.productAttributeLinkDao.findProductLink(attributeId, productAttributeLinkIdList);
    }

    /*
     * Group Detail: Add product: Lấy danh sách thuộc tính của một product
     * */
    @Override
    public List<ProductAttributeLink> findAttribute(Integer productId) {
        return productAttributeLinkDao.findAttribute(productId);
    }

    @Override
    public List<Product> findProductForOrder(String term) {
        List<Product> result = productDao.findProductForOrder(term);
        if (result == null || result.size() == 0) {
            return null;
        }
        result.forEach(product -> product.setGiftList(this.productGiftService.getDetailByProduct(product.getId())));
        return result;
    }

    @Override
    public void doSyncAmount(Product bo) {
        Product dbBo = productDao.findDistinctBySku(bo.getSku());
        dbBo = productDao.lockByIds(Collections.singleton(dbBo.getId())).get(0);
        // bo.getAmount là số lượng tương đối (tăng, giảm)
        dbBo.setAmount(dbBo.getAmount() + bo.getAmount());
        dbBo.setModifiedBy(User.getContextId());
        productDao.save(dbBo);
    }

    private void validatePriceProduct(Product bo) {
        if (bo.getProductPrice().getPriceRegular() == null) {
            bo.getProductPrice().setPriceRegular(0);
        }
        if (!Product.TYPE_PRODUCT_IN_COMBO.equals(bo.getType())) {
            if (bo.getProductPrice().getPriceSale() == null) {
                throw new HebelaException("product.priceSale.missing");
            }
        } else {
            if (bo.getProductPrice().getPriceSale() == null) {
                bo.getProductPrice().setPriceSale(0);
            }
        }
    }
}
