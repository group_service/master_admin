package com.hebela.masterdata.service;

import com.hebela.core.HebelaException;
import com.hebela.core.HebelaLogger;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.*;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.enumeration.VoucherErrorEnum;
import com.hebela.masterdata.web.cart.CartSearchRequest;
import com.hebela.masterdata.web.cart.cartview.CartViewRequest;
import com.hebela.masterdata.web.cart.checkout.CheckoutRequest;
import com.hebela.masterdata.web.cart.gift.SelectGiftRequest;
import com.hebela.util.BeanUtils;
import lombok.val;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.NumberFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl extends BaseService<Cart, Integer> implements CartService {
    @Value("${shipping.fee.default}")
    private Integer shippingFeeDefault;
    private final ProductComboDao productComboDao;
    private final CartDao cartDao;
    private final GiftAccountLinkDao giftAccountLinkDao;
    private final AccountAddressDao accountAddressDao;
    private final ProductDao productDao;
    private final FavoriteDao favoriteDao;
    private final ProductGiftDao productGiftDao;
    private final SellerDao sellerDao;
    private final VoucherDao voucherDao;
    private final OrderDao orderDao;
    private final VoucherGiftLinkDao voucherGiftLinkDao;

    public CartServiceImpl(OrderDao orderDao, ProductComboDao productComboDao, CartDao cartDao, GiftAccountLinkDao giftAccountLinkDao, AccountAddressDao accountAddressDao, ProductDao productDao, FavoriteDao favoriteDao, ProductGiftDao productGiftDao, SellerDao sellerDao, VoucherDao voucherDao, VoucherGiftLinkDao voucherGiftLinkDao) {
        this.productComboDao = productComboDao;
        this.cartDao = cartDao;
        this.giftAccountLinkDao = giftAccountLinkDao;
        this.accountAddressDao = accountAddressDao;
        this.productDao = productDao;
        this.favoriteDao = favoriteDao;
        this.productGiftDao = productGiftDao;
        this.sellerDao = sellerDao;
        this.voucherDao = voucherDao;
        this.orderDao = orderDao;
        this.voucherGiftLinkDao = voucherGiftLinkDao;
    }

    @Override
    public CartView view(CartViewRequest model) {
        if (model.getCartList() == null || model.getCartList().size() == 0) {
            return new CartView();
        }
        CartView cartView = buildCartView(model);
        List<Integer> productIds = model.getCartList().stream().map(Cart::getProductId).collect(Collectors.toList());
        List<Integer> comboIds = model.getCartList().stream().map(Cart::getComboId).collect(Collectors.toList());
        cartView.setFavorite(getFavoriteProduct(productIds), getFavoriteCombo(comboIds));
        return cartView;
    }

    private CartView buildCartView(CartViewRequest model) {
        CartView cartView = new CartView();
        Map<Integer, LinkedList<Coupon>> sellerMap = new HashMap<>();
        for (Cart cart : model.getCartList()) {
            // xử lý sản phẩm
            if (cart.getProductId() != null) {
                Product product = getProduct(cart);
                if (product == null) {
                    HebelaLogger.warn("product not found:" + cart.getProductId());
                    continue;
                }
                Integer currentCartCount = cartView.getProductAmountCountMap().computeIfAbsent(cart.getProductId(), k -> Integer.valueOf(0));
                if (product.getAmount() < (currentCartCount + cart.getCount())) {
                    int available = 1000000;
                    available = Integer.min(available, Integer.max(product.getAmount() - currentCartCount, Integer.valueOf(0)));
                    cart.setQuantityCartWarn("Còn " + available + " sản phẩm");
                    cartView.setCartInvalid(Boolean.TRUE);
                    cartView.setWarnMessage("Số lượng sản phẩm trong giỏ không hợp lệ, Quý khách vui lòng kiểm tra lại");
                }
                cartView.getProductMap().put(product.getId(), product);
                updateProductMap(cartView, product.getId(), cart.getCount());
                Coupon activeCoupon = getCouponActiveByProductId(cart.getProductId(), product.getOrgId(), model.getPlatform());
                cart.setProduct(product);
                if (Coupon.TYPE_BIG_AMOUNT_SAME.equals(activeCoupon.getType())) {
                    sellerMap.computeIfAbsent(product.getOrgId(), k -> new LinkedList<>());
                    LinkedList<Coupon> couponLinkedList = sellerMap.get(product.getOrgId());
                    Coupon coupon = BeanUtils.clone(activeCoupon);
                    coupon.getCartList().add(cart);
                    couponLinkedList.push(coupon);
                } else {
                    sellerMap.computeIfAbsent(product.getOrgId(), k -> new LinkedList<>());
                    LinkedList<Coupon> couponLinkedList = sellerMap.get(product.getOrgId());
                    boolean couponExist = Boolean.FALSE;
                    for (Coupon coupon : couponLinkedList) {
                        if (coupon.getId().equals(activeCoupon.getId())) {
                            couponExist = Boolean.TRUE;
                            coupon.getCartList().add(cart);
                        }
                    }
                    if (!couponExist) {
                        activeCoupon.getCartList().add(cart);
                        couponLinkedList.push(activeCoupon);
                    }
                }
                List<ProductGift> giftList = getGiftProductByProductId(cart.getProductId());
                for (Iterator<ProductGift> productGiftIterator = giftList.iterator(); productGiftIterator.hasNext(); ) {
                    ProductGift productGift = productGiftIterator.next();
                    Integer currentGiftCount = cartView.getProductAmountCountMap().computeIfAbsent(productGift.getProductGiftId(), k -> Integer.valueOf(0));
                    int availableOfGift = Integer.min(productGift.getProductAmount() - currentGiftCount, productGift.getNumberOfGift());
                    if (availableOfGift == 0) {
                        productGiftIterator.remove();
                    } else {
                        productGift.setAmount(availableOfGift);
                        updateProductMap(cartView, productGift.getProductGiftId(), availableOfGift);
                    }
                    productGift.setPriceSale(getPrice(productGift.getProductGiftId()).getPriceSale());
                }
                product.setGiftList(giftList);
            } else if (cart.getComboId() != null) {
                // xử lý sản phẩm trong combo
                ProductCombo productCombo = getProductCombo(cart);
                if (productCombo == null) {
                    HebelaLogger.warn("combo not found" + cart.getComboId());
                    continue;
                }
                cartView.getProductComboMap().put(productCombo.getId(), productCombo);
                Integer available = productCombo.getAmount() != null ? productCombo.getAmount() : 0;
                if (ProductCombo.TYPE_RETAIL.equals(productCombo.getType())) {
                    for (Product product : productCombo.getProductList()) {
                        Integer currentCartCount = cartView.getProductAmountCountMap().computeIfAbsent(product.getId(), k -> 0);
                        available = Integer.min(available, Integer.max(product.getAmount() - currentCartCount, 0));
                        updateProductMap(cartView, product.getId(), cart.getCount());
                    }
                }
                productCombo.setAmount(available);
                if (cart.getCount() > available) {
                    cart.setQuantityCartWarn("Còn " + available + " combo");
                    cartView.setCartInvalid(Boolean.TRUE);
                    cartView.setWarnMessage("Số lượng combo trong giỏ không hợp lệ, Quý khách vui lòng kiểm tra lại");
                }
                List<ProductGift> giftList = getGiftProductByComboId(cart.getComboId());
                for (Iterator<ProductGift> productGiftIterator = giftList.iterator(); productGiftIterator.hasNext(); ) {
                    ProductGift productGift = productGiftIterator.next();
                    Integer currentGiftCount = cartView.getProductAmountCountMap().computeIfAbsent(productGift.getProductGiftId(), k -> Integer.valueOf(0));
                    int availableOfGift = Integer.min(productGift.getProductAmount() - currentGiftCount, productGift.getNumberOfGift());
                    if (availableOfGift == 0) {
                        productGiftIterator.remove();
                    } else {
                        productGift.setAmount(availableOfGift);
                        updateProductMap(cartView, productGift.getProductGiftId(), availableOfGift);
                    }
                    productGift.setPriceSale(getPrice(productGift.getProductGiftId()).getPriceSale());
                }
                productCombo.setGiftList(giftList);
                cart.setProductCombo(productCombo);
                sellerMap.computeIfAbsent(productCombo.getOrgId(), k -> new LinkedList<>());
                LinkedList<Coupon> couponLinkedList = sellerMap.get(productCombo.getOrgId());
                if (couponLinkedList.size() == 0 || couponLinkedList.get(0).getId() != 0) {
                    Coupon defaultCoupon = new Coupon();
                    defaultCoupon.setId(0);
                    defaultCoupon.setType(Coupon.TYPE_NONE);
                    defaultCoupon.setCartList(new LinkedList<>());
                    couponLinkedList.addFirst(defaultCoupon);
                }
                couponLinkedList.get(0).getCartList().add(cart);
            }
            // xử lý quà gắn trực tiếp với coupon
            cartView.getCartIdList().add(cart.getId());
        }

        List<Seller> sellerList = new ArrayList<>();
        for (Integer orgId : sellerMap.keySet()) {
            Seller seller = sellerDao.findById(orgId).get();
            LinkedList<Coupon> couponLinkedList = sellerMap.get(orgId);
            couponLinkedList.sort((Comparator) (o1, o2) -> ((Coupon) o1).getId() > ((Coupon) o2).getId() ? -1 : 1);
            seller.setGroup(couponLinkedList);
            sellerList.add(seller);
        }
        cartView.setSellers(sellerList);
        // get voucher id by user
        calcPrice(cartView, model);
        return cartView;
    }

    private void updateProductMap(CartView cartView, Integer productId, Integer count) {
        cartView.getProductAmountCountMap().merge(productId, count, Integer::sum);
    }

    /**
     * Lấy sản phẩm kinh doanh theo id  chỉ sử dụng cho giỏ hàng
     * (không bao gồm giá) (type = 1, isTrash = false, status = 1)
     *
     * @return null nếu không tìm thấy
     */
    protected Product getProduct(Cart cart) {
        return this.productDao.findOne((root, query, builder) -> {
                    Root<Brand> brandRoot = query.from(Brand.class);
                    Root<ProductPrice> priceRoot = query.from(ProductPrice.class);
                    query.multiselect(root, priceRoot, brandRoot);
                    return builder.and(
                            builder.equal(root.get(Product._id), cart.getProductId()),
                            builder.equal(root.get(Product._id), priceRoot.get(ProductPrice._productId)),
                            builder.lessThanOrEqualTo(priceRoot.get(ProductPrice._startDate), Instant.now()),
                            builder.greaterThan(priceRoot.get(ProductPrice._endDate), Instant.now()),
                            builder.equal(root.get(Product._brandId), brandRoot.get(Brand._id)),
                            builder.or(
                                    builder.equal(root.get(Product._type), Product.TYPE_SELL),
                                    builder.equal(root.get(Product._type), Product.TYPE_PRODUCT_IN_COMBO)
                            ),
                            builder.equal(root.get(Product._isTrash), Boolean.FALSE),
                            builder.equal(root.get(Product._status), Product.STATUS_ACTIVE));
                }
        ).orElse(null);
    }

    /**
     * Lấy combo trong giỏ hàng theo id
     * - bao gồm sản phẩm bên trong
     */
    protected ProductCombo getProductCombo(Cart cart) {
        ProductCombo productCombo = this.productComboDao.findOne((root, query, builder) ->
                builder.and(
                        builder.equal(root.get(ProductCombo._status), ProductCombo.STATUS_ACTIVE),
                        builder.equal(root.get(ProductCombo._isTrash), Boolean.FALSE),
                        builder.equal(root.get(ProductCombo._id), cart.getComboId())
                )
        ).orElse(null);
        if (productCombo != null) {
            List<Product> productList = this.productDao.findAll((root, query, builder) -> {
                        Root<ProductComboLink> linkRoot = query.from(ProductComboLink.class);
                        Root<ProductPrice> priceRoot = query.from(ProductPrice.class);
                        query.multiselect(root, priceRoot);
                        return builder.and(
                                builder.equal(root.get(Product._id), linkRoot.get(ProductComboLink._productId)),
                                builder.equal(root.get(Product._id), priceRoot.get(ProductPrice._productId)),
                                builder.lessThanOrEqualTo(priceRoot.get(ProductPrice._startDate), Instant.now()),
                                builder.greaterThan(priceRoot.get(ProductPrice._endDate), Instant.now()),
                                builder.equal(linkRoot.get(ProductComboLink._comboId), cart.getComboId()),
                                builder.or(
                                        builder.equal(root.get(Product._type), Product.TYPE_SELL),
                                        builder.equal(root.get(Product._type), Product.TYPE_PRODUCT_IN_COMBO)
                                ),
                                builder.equal(root.get(Product._isTrash), Boolean.FALSE),
                                builder.equal(root.get(Product._status), Product.STATUS_ACTIVE)
                        );
                    }
            );
            productCombo.setCartProductList(productList);
        }
        return productCombo;
    }

    protected void calcPrice(CartView cartView, CartViewRequest model) {
        cartView.setTotal(0L);
        cartView.setSubTotal(0L);
        cartView.setCouponDiscount(0L);
        cartView.setVoucherDiscount(0L);
        Optional<Voucher> voucherSanOptional = model.getVoucherSelectedList().stream().filter(item -> Organization.ROOT_ACCOUNT.getId().equals(item.getOrgId())).findFirst();
        if (voucherSanOptional.isPresent()) {
            model.setVoucherCode(voucherSanOptional.get().getCode());
        } else {
            model.setVoucherCode(null);
        }
        for (Seller seller : cartView.getSellers()) {
            //set voucher cho từng shop
            Map<Integer, Voucher> voucherMap = model.getVoucherSelectedList().stream().collect(Collectors.toMap(Voucher::getOrgId, p -> p));
            seller.setVoucher(voucherMap.get(seller.getId()));
            seller.setTotal(0L);
            seller.setSubTotal(0L);
            seller.setCouponDiscount(0L);
            for (Coupon coupon : seller.getGroup()) {
                // sản phẩm ngoài coupon và combo
                if (Coupon.TYPE_NONE.equals(coupon.getType())) {
                    for (Cart cart : coupon.getCartList()) {
                        if (cart.getProduct() != null) {
                            long itemAmount = (long) cart.getCount() * (long) cart.getProduct().getProductPrice().getPriceSale();
                            cartView.addTotal(itemAmount).addSubTotal(itemAmount);
                            seller.addTotal(itemAmount).addSubTotal(itemAmount);
                        } else if (cart.getProductCombo() != null) {
                            long itemAmount = (long) cart.getCount() * (long) cart.getProductCombo().getPrice();
                            cartView.addTotal(itemAmount).addSubTotal(itemAmount);
                            seller.addTotal(itemAmount).addSubTotal(itemAmount);
                        }
                    }
                } else {
                    /** sản phẩm trong coupon */
                    long couponDiscount = 0;
                    long totalCount = 0;
                    long couponAmount = 0;
                    for (Cart cart : coupon.getCartList()) {
                        couponAmount += (long) cart.getCount() * (long) cart.getProduct().getProductPrice().getPriceSale();
                        totalCount += cart.getCount();
                    }
                    cartView.addSubTotal(couponAmount);
                    seller.addSubTotal(couponAmount);
                    if ((coupon.getBuyQuantityMin() != null && totalCount >= coupon.getBuyQuantityMin()) || (coupon.getBuyMoneyMin() != null && couponAmount >= coupon.getBuyMoneyMin())) {
                        if (Coupon.DISCOUNT_TYPE_PERCENT.equals(coupon.getDiscountType())) {
                            couponDiscount = couponAmount * coupon.getDiscountPercent() / 100;
                            if (coupon.getDiscountMoneyMax() != null && couponDiscount > coupon.getDiscountMoneyMax()) {
                                couponDiscount = coupon.getDiscountMoneyMax();
                            }
                            coupon.setConditionLabel("Tiết kiệm ");
                            coupon.setConditionLabelValue(formatMoney(couponDiscount));
                        } else if (Coupon.DISCOUNT_TYPE_MONEY.equals(coupon.getDiscountType())) {
                            couponDiscount = coupon.getDiscountMoney();
                            if (couponAmount < couponDiscount) {
                                couponDiscount = couponAmount;
                            }
                            coupon.setConditionLabel("Tiết kiệm ");
                            coupon.setConditionLabelValue(formatMoney(couponDiscount));
                        } else if (Coupon.DISCOUNT_TYPE_GIFT.equals(coupon.getDiscountType())) {
                            List<Product> selectedGiftList = findGiftSelected(model, coupon);
                            List<Product> availableGift = findGift(coupon.getId());
                            /* khách hàng chưa thực hiện hành động chọn quà */
                            if (selectedGiftList.size() == 0) {
                                for (int i = 0; i < coupon.getNumberOfGift(); i++) {
                                    if (i < availableGift.size()) {
                                        Product gift = BeanUtils.clone(availableGift.get(i));
                                        Integer currentCartCount = cartView.getProductAmountCountMap().computeIfAbsent(gift.getId(), k -> Integer.valueOf(0));
                                        int numberOfGift = Integer.min(gift.getAmount() - currentCartCount, 1);
                                        if (numberOfGift > 0) {
                                            updateProductMap(cartView, gift.getId(), numberOfGift);
                                            gift.setAmount(numberOfGift);
                                            selectedGiftList.add(gift);
                                        }
                                    }
                                }
                                coupon.setGiftBySystem(Boolean.TRUE);
                            } else {
                                for (Iterator<Product> iterator = selectedGiftList.iterator(); iterator.hasNext(); ) {
                                    Product gift = iterator.next();
                                    if (selectedGiftList.size() > coupon.getNumberOfGift()) {
                                        /* xoá quà khi giới hạn quà tặng bị thay đổi giảm*/
                                        iterator.remove();
                                    }
                                    Integer currentCartCount = cartView.getProductAmountCountMap().computeIfAbsent(gift.getId(), k -> Integer.valueOf(0));
                                    int numberOfGift = Integer.min(gift.getAmount() - currentCartCount, 1);
                                    if (numberOfGift == 0) {
                                        iterator.remove();
                                    }
                                }
                            }
                            coupon.setSelectedGift(selectedGiftList);
                            if (selectedGiftList.size() == 0) {
                                coupon.setConditionLabel("Chưa chọn quà tặng");
                                coupon.setConditionLabelValue("");
                            } else {
                                coupon.setConditionLabel("Đã chọn ");
                                coupon.setConditionLabelValue(selectedGiftList.size() + " quà");
                            }
                        } else if (Coupon.DISCOUNT_TYPE_GIFT_SAME.equals(coupon.getDiscountType())) {
                            List<Product> selectedGiftList = new ArrayList<>();
                            Product product = coupon.getCartList().get(0).getProduct();
                            Integer currentCartCount = cartView.getProductAmountCountMap().computeIfAbsent(product.getId(), k -> Integer.valueOf(0));
                            int numberOfGift = Integer.min(product.getAmount() - currentCartCount, coupon.getNumberOfGift());
                            if (numberOfGift > 0) {
                                Product gift = BeanUtils.clone(coupon.getCartList().get(0).getProduct());
                                gift.setAmount(numberOfGift);
                                selectedGiftList.add(gift);
                                coupon.setConditionLabel("Đã chọn ");
                                coupon.setConditionLabelValue(numberOfGift + " quà");
                            }
                            if (numberOfGift == 0) {
                                coupon.setConditionLabel("");
                                coupon.setConditionLabelValue("Đã hết sản phẩm để tặng quà");
                            }
                            coupon.setSelectedGift(selectedGiftList);
                        }
                    }
                    /** set text cho coupon chưa đủ điều kiện áp dụng*/
                    else {
                        this.setTextHighLineForCoupon(coupon, totalCount, couponAmount);
                    }
                    cartView.addTotal(couponAmount - couponDiscount).addCouponDiscount(couponDiscount);
                    seller.addTotal(couponAmount - couponDiscount).addCouponDiscount(couponDiscount);
                }
            }
        }
        //set giam gia theo voucher shop
        for (Seller seller : cartView.getSellers()) {
            if (seller.getVoucher() != null) {
                this.voucherForShop(cartView, seller, seller.getVoucher().getCode(), model.getPlatform());
            }
        }
        Long totalDiscountShop = cartView.getSellers().stream().mapToLong(Seller::getVoucherDiscount).sum();

        if (model.getVoucherCode() != null) {
            Optional<Voucher> voucherOptional = voucherDao.findOne(
                    (root, query, builder) -> builder.and(
                            builder.equal(root.get(Voucher._code), model.getVoucherCode()),
                            builder.equal(root.get(Voucher._isTrash), Boolean.FALSE)
                    )
            );
            if (voucherOptional.isPresent()) {
                Voucher voucher = voucherOptional.get();
                Integer platform = model.getPlatform();
                voucher.setIsEligible((voucher.getPlatform() & platform) > 0);
                if (!voucher.getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
                    cartView.getSellers().stream().filter(item -> item.getId().equals(voucher.getOrgId())).findFirst().get().setVoucher(voucher);
                }
                this.checkVoucher(platform, voucher, cartView);

                //set giam gia voucher san
                if (voucher.getIsEligible() && voucher.getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
                    cartView.setVoucher(voucher);
                    long voucherDiscount = 0;
                    if (Voucher.DISCOUNT_TYPE_PERCENT.equals(voucher.getDiscountType())) {
                        // giam gia theo % tong phai tra - giam gia coupon - tong giam gia voucher
                        voucherDiscount = (cartView.getSubTotal() - cartView.getCouponDiscount() - totalDiscountShop) * voucher.getDiscountPercent() / 100;
                        if (voucher.getDiscountMoneyMax() != null && voucherDiscount > voucher.getDiscountMoneyMax()) {
                            voucherDiscount = voucher.getDiscountMoneyMax();
                        }
                    } else if (Voucher.DISCOUNT_TYPE_MONEY.equals(voucher.getDiscountType())) {
                        voucherDiscount = voucher.getDiscountMoney();
                    }
                    cartView.setVoucherDiscount(voucherDiscount < cartView.getTotal() ? voucherDiscount : cartView.getTotal());
                    cartView.setTotal(Long.max((cartView.getTotal() - voucherDiscount), 0));
                    /*tiến hành áp dụng voucher theo shop*/
                    for (Seller seller : cartView.getSellers()) {
                        double sellerPercentOfMainOrder = ((seller.getSubTotal() - seller.getCouponDiscount()) * 1.0) / (cartView.getSubTotal() - cartView.getCouponDiscount());
                        long sellerVoucherDiscount = (long) (sellerPercentOfMainOrder * cartView.getVoucherDiscount());
                        seller.setVoucherDiscount(sellerVoucherDiscount);
                    }
                }
            } else {
                Voucher voucher = new Voucher();
                voucher.getCartErrorList().add(VoucherErrorEnum.voucher_not_exits.getValue());
                cartView.setVoucher(voucher);
            }
        }
        cartView.setTotalVoucherDiscountShop(totalDiscountShop);
    }

    // set giảm giá voucher từng shop
    private void voucherForShop(CartView cartView, Seller seller, String voucherCode, Integer platform) {
        Optional<Voucher> voucherOptional = voucherDao.findOne(
                (root, query, builder) -> builder.and(
                        builder.equal(root.get(Voucher._code), voucherCode),
                        builder.equal(root.get(Voucher._isTrash), Boolean.FALSE)
                )
        );
        if (voucherOptional.isPresent()) {
            Voucher voucher = voucherOptional.get();
            voucher.setIsEligible((voucher.getPlatform() & platform) > 0);
            this.checkVoucher(platform, voucher, cartView);
            if (voucher.getIsEligible()) {
                long voucherDiscount = 0;
                if (Voucher.DISCOUNT_TYPE_PERCENT.equals(voucher.getDiscountType())) {
                    voucherDiscount = (cartView.getSubTotal() - cartView.getCouponDiscount()) * voucher.getDiscountPercent() / 100;
                    if (voucher.getDiscountMoneyMax() != null && voucherDiscount > voucher.getDiscountMoneyMax()) {
                        voucherDiscount = voucher.getDiscountMoneyMax();
                    }
                } else if (Voucher.DISCOUNT_TYPE_MONEY.equals(voucher.getDiscountType())) {
                    voucherDiscount = voucher.getDiscountMoney();
                } else if (Voucher.DISCOUNT_TYPE_GIFT.equals(voucher.getDiscountType())) {
                    VoucherGiftLink voucherGiftLink = new VoucherGiftLink();
                    voucherGiftLink.setVoucherId(voucher.getId());
                    List<VoucherGiftLink> voucherGiftLinkList = this.voucherGiftLinkDao.findAll(Example.of(voucherGiftLink));
                    Map<Integer, Integer> mapMountQuantity = voucherGiftLinkList.stream().collect(Collectors.toMap(VoucherGiftLink::getProductId, VoucherGiftLink::getProductQuantity));
                    List<Integer> productGiftIdList = voucherGiftLinkList.stream().map(VoucherGiftLinkBase::getProductId).collect(Collectors.toList());
                    List<Product> productList = this.productDao.getGiftByIds(productGiftIdList);
                    List<ProductGift> productGiftList = new ArrayList<>();
                    for (Product product : productList) {
                        productGiftList.add(this.convertProductToProductGift(product, mapMountQuantity.get(product.getId())));
                    }
                    seller.getProductVoucherGiftList().addAll(productGiftList);
                }
                seller.setVoucherDiscount(voucherDiscount);
                seller.setTotal(seller.getTotal() - voucherDiscount);
            }
        }
    }

    private ProductGift convertProductToProductGift(Product product, Integer amount) {
        ProductGift productGift = new ProductGift();
        productGift.setImageThumbnail(product.getImageThumbnail());
        productGift.setName(product.getName());
        productGift.setProductId(product.getId());
        productGift.setAmount(amount);
        return productGift;
    }

    /**
     * @param coupon       coupon cần set textNormal và text highline
     * @param totalCount   tổng số sản phẩm trong cart
     * @param couponAmount tổng số tiền trong cart
     */
    public void setTextHighLineForCoupon(Coupon coupon, long totalCount, long couponAmount) {
        /**
         *  Giảm theo %
         * */
        if (Coupon.DISCOUNT_TYPE_PERCENT.equals(coupon.getDiscountType())) {
            /**
             *  Số lượng tối thiểu
             * */
            if (coupon.getBuyQuantityMin() != null && totalCount < coupon.getBuyQuantityMin()) {
                coupon.setConditionLabel("Mua thêm " + (coupon.getBuyQuantityMin() - totalCount)
                        + " sản phẩm để được giảm ");
                coupon.setConditionLabelValue(coupon.getDiscountPercent() + "%");
            }
            /**
             *  Số tiền tối thiểu
             * */
            else if (coupon.getBuyMoneyMin() != null && couponAmount < coupon.getBuyMoneyMin()) {
                coupon.setConditionLabel("Mua thêm " + formatMoney(coupon.getBuyMoneyMin() - couponAmount) + " để được giảm ");
                coupon.setConditionLabelValue(coupon.getDiscountPercent() + "%");
            }
            /**
             *  Giảm theo số tiền
             * */
        } else if (Coupon.DISCOUNT_TYPE_MONEY.equals(coupon.getDiscountType())) {
            /**
             *  Số lượng tối thiểu
             * */
            if (coupon.getBuyQuantityMin() != null && totalCount < coupon.getBuyQuantityMin()) {
                coupon.setConditionLabel("Mua thêm " + (coupon.getBuyQuantityMin() - totalCount) + " sản phẩm để được giảm ");
                coupon.setConditionLabelValue(formatMoney(coupon.getDiscountMoney()));
            }
            /**
             *  Số tiền tối thiểu
             * */
            else if (coupon.getBuyMoneyMin() != null && couponAmount < coupon.getBuyMoneyMin()) {
                coupon.setConditionLabel("Mua thêm " + formatMoney(coupon.getBuyMoneyMin() - couponAmount) + " để được giảm ");
                coupon.setConditionLabelValue(formatMoney(coupon.getDiscountMoney()));
            }
            /**
             *  Tặng quà
             * */
        } else if (Coupon.DISCOUNT_TYPE_GIFT.equals(coupon.getDiscountType())) {
            /**
             *  Số lượng tối thiểu
             * */
            if (coupon.getBuyQuantityMin() != null && totalCount < coupon.getBuyQuantityMin()) {
                coupon.setConditionLabel("Mua thêm " + (coupon.getBuyQuantityMin() - totalCount) + " sản phẩm để được nhận ");
                coupon.setConditionLabelValue(coupon.getNumberOfGift() + " quà");
            }
            /**
             *  Số tiền tối thiểu
             * */
            else if (coupon.getBuyMoneyMin() != null && couponAmount < coupon.getBuyMoneyMin()) {
                coupon.setConditionLabel("Mua thêm " + formatMoney(coupon.getBuyMoneyMin() - couponAmount) + " để được nhận ");
                coupon.setConditionLabelValue(coupon.getNumberOfGift() + " quà");
            }
        } else if (Coupon.DISCOUNT_TYPE_GIFT_SAME.equals(coupon.getDiscountType())) {
            /**
             *  Số lượng tối thiểu
             * */
            if (coupon.getBuyQuantityMin() != null && totalCount < coupon.getBuyQuantityMin()) {
                coupon.setConditionLabel("Mua thêm " + (coupon.getBuyQuantityMin() - totalCount) + " sản phẩm để được nhận ");
                coupon.setConditionLabelValue(coupon.getNumberOfGift() + " quà");
            }
        }
    }

    public String formatMoney(long money) {
        NumberFormat vnFormat = NumberFormat.getInstance(new Locale("vi", "VN"));
        return vnFormat.format(money) + "đ";
    }

    /**
     * Lấy quà có thể tặng cho người dùng
     */
    private List<Product> findGift(Integer couponId) {
        return this.productDao.findAll((root, query, builder) -> {
            Root<CouponGiftLink> giftLinkRoot = query.from(CouponGiftLink.class);
            return builder.and(
                    builder.equal(root.get(Product._id), giftLinkRoot.get(CouponGiftLink._productId)),
                    builder.equal(root.get(Product._status), Product.STATUS_ACTIVE),
                    builder.greaterThan(root.get(Product._amount), 0),
                    builder.equal(giftLinkRoot.get(CouponGiftLink._couponId), couponId)
            );
        });
    }

    /**
     * Lấy quà trong coupon của người dùng
     *
     * @param couponId:  id của coupon
     * @param accountId: id của người dùng
     */
    protected List<Product> findCouponAccountGift(Integer couponId, Integer accountId, List productIdList) {
        return this.productDao.findAll(((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<GiftAccountLink> giftAccountRoot = query.from(GiftAccountLink.class);
            predicates.add(builder.equal(root.get(Product._id), giftAccountRoot.get(GiftAccountLink._productGiftId)));
            predicates.add(builder.equal(giftAccountRoot.get(GiftAccountLink._couponId), couponId));
            predicates.add(builder.equal(giftAccountRoot.get(GiftAccountLink._accountId), accountId));
            if (productIdList.size() > 0) {
                predicates.add(giftAccountRoot.get(GiftAccountLink._productId).in(productIdList));
            }
            predicates.add(builder.equal(giftAccountRoot.get(GiftAccountLink._deselected), Boolean.FALSE));
            return builder.and(predicates.toArray(new Predicate[0]));
        }));
    }

    /**
     * Lấy quà mà người dùng đã bỏ chọn
     */
    private List<GiftAccountLink> findDeselected(Integer couponId, Integer accountId, List productIdList) {
        return this.giftAccountLinkDao.findAll(((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(GiftAccountLink._couponId), couponId));
            predicates.add(builder.equal(root.get(GiftAccountLink._accountId), accountId));
            if (productIdList.size() > 0) {
                predicates.add(root.get(GiftAccountLink._productId).in(productIdList));
            }
            predicates.add(builder.equal(root.get(GiftAccountLink._deselected), Boolean.TRUE));
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        }));
    }

    /**
     * Lấy quà mà người dùng chọn
     */
    private List<Product> findGiftSelected(CartViewRequest model, Coupon coupon) {
        List<Product> giftList = new ArrayList<>();
        if (model.getGiftSelectedList() != null) {
            List<Integer> productGiftId = new ArrayList<>();
            for (SelectGiftRequest giftRequest : model.getGiftSelectedList()) {
                if (Coupon.TYPE_BIG_AMOUNT_SAME.equals(coupon.getType())) {
                    if (coupon.getCartList().get(0).getProduct().getId().equals(giftRequest.getProductId())) {
                        productGiftId.addAll(giftRequest.getGiftIdList());
                    }
                } else {
                    productGiftId.addAll(giftRequest.getGiftIdList());
                }
            }
            giftList.addAll(productDao.findAllById(productGiftId));
        }
        return giftList;
    }

    protected List<Integer> getFavoriteCombo(List<Integer> comboIdList) {
        if (comboIdList.size() == 0) {
            return Lists.newArrayList();
        }
        return this.favoriteDao.isFavoriteCombo(User.getContextId(), comboIdList);
    }

    protected List<Integer> getFavoriteProduct(List<Integer> productIdList) {
        if (productIdList.size() == 0) {
            return Lists.newArrayList();
        }
        return this.favoriteDao.isFavorite(User.getContextId(), productIdList);
    }

    protected List<ProductGift> getGiftProductByProductId(Integer productId) {
        return this.productGiftDao.findAll((root, query, builder) -> {
            Root<Product> productRoot = query.from(Product.class);
            query.multiselect(
                    root.get(ProductGift._productGiftId),
                    root.get(ProductGift._productId),
                    root.get(ProductGift._productComboId),
                    root.get(ProductGift._numberOfGift),
                    productRoot.get(Product._name),
                    productRoot.get(Product._imageThumbnail),
                    productRoot.get(Product._amount),
                    productRoot.get(Product._slug),
                    productRoot.get(Product._sku)
            );
            return builder.and(
                    builder.equal(root.get(ProductGift._productGiftId), productRoot.get(Product._id)),
                    builder.equal(root.get(ProductGift._productId), productId),
                    builder.greaterThan(productRoot.get(Product._amount), 0)
            );
        });
    }

    private ProductPrice getPrice(Integer productId) {
        Optional<ProductPrice> priceOptional = this.productPriceDao.findOne((root, query, builder) -> builder.and(
                builder.equal(root.get(ProductPrice._productId), productId),
                builder.lessThanOrEqualTo(root.get(ProductPrice._startDate), Instant.now()),
                builder.greaterThan(root.get(ProductPrice._endDate), Instant.now())
        ));
        return priceOptional.orElse(null);
    }

    protected List<ProductGift> getGiftProductByComboId(Integer comboId) {
        return this.productGiftDao.findAll((root, query, builder) -> {
            Root<Product> productRoot = query.from(Product.class);
            query.multiselect(
                    root.get(ProductGift._productGiftId),
                    root.get(ProductGift._productId),
                    root.get(ProductGift._productComboId),
                    root.get(ProductGift._numberOfGift),
                    productRoot.get(Product._name),
                    productRoot.get(Product._imageThumbnail),
                    productRoot.get(Product._amount),
                    productRoot.get(Product._slug),
                    productRoot.get(Product._sku)
            );
            return builder.and(
                    builder.equal(root.get(ProductGift._productGiftId), productRoot.get(Product._id)),
                    builder.equal(root.get(ProductGift._productComboId), comboId),
                    builder.greaterThan(productRoot.get(Product._amount), 0)
            );
        });
    }

    @Override
    public Cart doRemove(Cart cart, Integer accountId) {
        if (accountId == null) {
            return null;
        }
        Cart dbCart = getOptional(cart, accountId);
        if (dbCart.getId() != null) {
            cartDao.delete(dbCart);
        }
        if (dbCart.getProductId() != null) {
            dbCart.setProduct(productDao.getDetail(dbCart.getProductId()));
        }
        if (dbCart.getComboId() != null) {
            dbCart.setProductCombo(productComboDao.findById(dbCart.getComboId()).get());
        }
        return dbCart;
    }

    private Cart getOptional(Cart cart, Integer accountId) {
        if (cart.getProductId() != null && cart.getComboId() != null) {
            throw new HebelaException("order.data.invalid");
        }
        Cart dbCart = Cart.newInstanceByAccountId(accountId);
        dbCart.setProductId(cart.getProductId());
        dbCart.setComboId(cart.getComboId());
        dbCart.setIsByAdmin(cart.getIsByAdmin());
        return cartDao.findOne(Example.of(dbCart)).orElse(dbCart);
    }

    @Override
    public Checkout checkout(CheckoutRequest model) {
        if (model.getCartList().size() == 0) return null;
        Checkout checkout = this.view(model).toCheckout();
        /**validate sản phẩm ngừng bán -  hết hàng*/
        if (model.getAccountAddressId() == null) {
            AccountAddress accountAddress = this.accountAddressDao.findByAccountIdAndIsDefaultTrue(User.getContextId());
            checkout.setAccountAddress(accountAddress);
        } else {
            AccountAddress accountAddress = this.accountAddressDao.findById(model.getAccountAddressId()).orElse(null);
            if (accountAddress == null) {
                /*địa chỉ bị xóa thì lấy lại địa chỉ mặc định*/
                accountAddress = this.accountAddressDao.findByAccountIdAndIsDefaultTrue(User.getContextId());
            }
            checkout.setAccountAddress(accountAddress);
        }
        if (checkout.getAccountAddress() != null) {
            checkout.setShippingFee(0);
            for (Seller seller : checkout.getSellers()) {
                val provinceId = checkout.getAccountAddress().getProvinceId();
                seller.setShippingFee(calcShippingFree(provinceId, seller.getTotal()));
                checkout.addShippingFee(seller.getShippingFee());
                seller.setTotal(seller.getTotal() + seller.getShippingFee());
            }
            checkout.setTotal(checkout.getTotal() + checkout.getShippingFee() - checkout.getTotalVoucherDiscountShop());
        } else {
            checkout.setShippingFee(null);
        }
        return checkout;
    }

    @Override
    public Page<CartAccount> findBySearch(CartSearchRequest cartSearchRequest) {

        Pageable pageable = PageRequest.of(cartSearchRequest.getPage(), cartSearchRequest.getSize());
        if (cartSearchRequest.getCartItemSku() != null && cartSearchRequest.getCartItemSku().length() == 0) {
            cartSearchRequest.setCartItemSku(null);
        }
        Page<CartAccount> cartAccountPage = this.cartDao.findCartAccount(
                cartSearchRequest.getCartItemKeyword(),
                cartSearchRequest.getCartItemSku(),
                cartSearchRequest.getAccountKeyword(),
                pageable);
        if (cartAccountPage.getContent().size() > 0) {
            List<Cart> cartList = cartDao.findCartByAccountList(cartAccountPage.getContent().stream().map(item -> item.getAccount().getId()).collect(Collectors.toList()));
            if (cartList.size() > 0) {
                List<Integer> productIdList = cartList.stream().filter(cart -> cart.getProduct() != null).map(cart -> cart.getProduct().getId()).collect(Collectors.toList());
                List<Product> productList = null;
                if (productIdList.size() > 0) {
                    productList = productDao.findProductWithPrice(productIdList);
                }
                for (CartAccount cartAccount : cartAccountPage.getContent()) {
                    cartAccount.setOrderedCount(cartDao.countOrderByAccount(cartAccount.getAccount().getId()));
                    cartAccount.setCartList(new ArrayList<>());
                    Integer totalMoney = 0;
                    for (Cart cart : cartList) {
                        if (cartAccount.getAccount().getId().equals(cart.getAccountId())) {
                            cartAccount.getCartList().add(cart);
                            if (cart.getProductId() != null) {
                                Product product = productList.stream().filter(item -> item.getId().equals(cart.getProductId())).findFirst().orElse(null);
                                if (product != null) {
                                    cart.setProduct(product);
                                    totalMoney += cart.getCount() * product.getProductPrice().getPriceSale();
                                }
                            } else if (cart.getComboId() != null) {
                                totalMoney += cart.getProductCombo().getPrice();
                            }
                        }
                    }
                    cartAccount.setTotalMoney(totalMoney);
                }
            }
        }

        return cartAccountPage;

//        Page<Integer> accountIdsAndCountRecord = this.cartDao.findCartDistinct(cartSearchRequest.getCartItemKeyword(),
//                cartSearchRequest.getCartItemSku(),
//                cartSearchRequest.getAccountKeyword(), pageable);
//
//        List<Cart> results = accountIdsAndCountRecord.getContent().size() != 0 ? this.cartDao.findBySearch(accountIdsAndCountRecord.getContent(),
//                cartSearchRequest.getCartItemKeyword(),
//                cartSearchRequest.getCartItemSku(),
//                cartSearchRequest.getAccountKeyword()) : new ArrayList<>();
//        List<Cart> carts = results;
//        int size = results.size();
//
//        // gộp tên sản phẩm và tên combo vào 1 list
//        for (int i = 0; i < size; i++) {
//            List<CartItemResponse> cartItemRespons = new ArrayList<>();
//            for (int j = 0; j < size; j++) {
//                if (carts.get(i).getAccountId().intValue() == carts.get(j).getAccountId().intValue()) {
//                    CartItemResponse cartItemResponse = new CartItemResponse();
//                    if (carts.get(j).getProduct() != null) {
//                        cartItemResponse.setSku(carts.get(j).getProduct().getSku());
//                        cartItemResponse.setName(carts.get(j).getProduct().getName());
//                    } else if (carts.get(j).getProductCombo() != null) {
//                        cartItemResponse.setSku(carts.get(j).getProductCombo().getSku());
//                        cartItemResponse.setName(carts.get(j).getProductCombo().getName());
//                    }
//                    cartItemResponse.setPrice(carts.get(j).getPrice());
//                    cartItemResponse.setCount(carts.get(j).getCount());
//                    cartItemRespons.add(cartItemResponse);
//                }
//            }
//            Cart cart = results.get(i);
//            Order order = new Order();
//            order.setAccountId(cart.getAccountId());
//            cart.setCountOrder(this.orderDao.count(Example.of(order)));
//            cart.setCartItemResponseList(cartItemRespons);
//        }
//        List<Cart> cartWithoutDuplicate = withoutDuplicate(results);
//        List<Cart> resultsResponse = cartWithoutDuplicate;
//        totalPrice(results);
//        // lọc theo số lượng đơn
//        if (cartSearchRequest.isMoreZero() && !cartSearchRequest.isZero()) {
//            resultsResponse = cartWithoutDuplicate
//                    .stream()
//                    .filter(item -> item.getCountOrder() != 0)
//                    .collect(Collectors.toList());
//        } else if (!cartSearchRequest.isMoreZero() && cartSearchRequest.isZero()) {
//            resultsResponse = cartWithoutDuplicate
//                    .stream()
//                    .filter(item -> item.getCountOrder() == 0)
//                    .collect(Collectors.toList());
//        }
//        return new PageImpl<>(resultsResponse, accountIdsAndCountRecord.getPageable(), accountIdsAndCountRecord.getTotalElements());
    }

//    private void totalPrice(List<Cart> carts) {
//        for (Cart cart : carts) {
//            int total = 0;
//            for (CartItemResponse cartItemResponse : cart.getCartItemResponseList()) {
//                total += cartItemResponse.getPrice() * cartItemResponse.getCount();
//            }
//            cart.setTotalMoneyInCart(total);
//        }
//    }

//    private List<Cart> withoutDuplicate(List<Cart> results) {
//        List<Cart> cartWithoutDuplicate = new ArrayList<>();
//        for (int i = 0; i < results.size(); i++) {
//            if (i == 0) {
//                cartWithoutDuplicate.add(results.get(i));
//            } else {
//                boolean tag = true;
//                Cart cart = results.get(i);
//                for (int j = 0; j < cartWithoutDuplicate.size(); j++) {
//                    if (cart.getAccountId().intValue() == cartWithoutDuplicate.get(j).getAccountId().intValue()) {
//                        tag = false;
//                        break;
//                    }
//                }
//                if (tag) {
//                    cartWithoutDuplicate.add(results.get(i));
//                }
//            }
//        }
//        return cartWithoutDuplicate;
//    }

    private Integer calcShippingFree(Integer provinceId, Long money) {
        List<ShippingFee> shippingFeeList = findShippingFree(provinceId, money.intValue(), money.intValue(), null);
        if (shippingFeeList != null && shippingFeeList.size() > 0) {
            return shippingFeeList.get(0).getFee();
        }
        return shippingFeeDefault;
    }

    @Override
    public CartView doAdd(CartViewRequest model) {
        Cart cart = model.getCartList().get(0);
        Cart dbCart = getOptional(cart, model.getAccountId());
        Integer amount;
        if (dbCart.getProductId() != null) {
            amount = productDao.getOne(dbCart.getProductId()).getAmount();
        } else {
            ProductCombo combo = productComboDao.getOne(dbCart.getComboId());
            amount = combo.getAmount() != null ? combo.getAmount() : 0;
            if (ProductCombo.TYPE_RETAIL.equals(combo.getType())) {
                List<Product> productList = productComboDao.findProduct(dbCart.getComboId());
                for (Product product : productList) {
                    amount = Integer.min(amount, product.getAmount());
                }
            }
        }
        if (dbCart.getId() != null) {
            if (dbCart.getCount() + cart.getCount() > amount) {
                throw new HebelaException("product.quantity.invalid");
            }
            dbCart.setCount(dbCart.getCount() + cart.getCount());
            dbCart.setModifiedBy(User.getContextId());
            cartDao.save(dbCart);
        } else {
            if (cart.getCount() > amount) {
                throw new HebelaException("product.quantity.invalid");
            }
            dbCart.setCount(cart.getCount());
            dbCart.setCreatedBy(User.getContextId());
            cartDao.save(dbCart);
        }
        return null;
    }

    @Override
    public CartView doUpdate(CartViewRequest model) {
        List<Cart> dbCartList = find(Cart.newInstanceByAccountId(model.getAccountId()));
        Cart cart = model.getCartList().get(0);
        model.setCartList(dbCartList);
        Cart dbCart = getOptional(cart, model.getAccountId());
        if (dbCart.getId() != null) {
            dbCart.setCount(cart.getCount());
            dbCart.setModifiedBy(User.getContextId());
            cartDao.save(dbCart);
        } else {
            dbCart.setCount(cart.getCount());
            dbCart.setCreatedBy(User.getContextId());
            cartDao.save(dbCart);
        }
        return view(model);
    }

    public void checkVoucher(Integer platform, Voucher voucher, CartView cartView) {
        if ((voucher.getPlatform() & platform) == 0) {
            if ((voucher.getPlatform() & Platform.APP) > 0) {
                /*voucher chỉ áp dụng cho phiên bản app*/
                voucher.getCartErrorList().add(VoucherErrorEnum.platform_app.getValue());
                voucher.setVoucherHint(VoucherErrorEnum.platform_app.getValue());
                voucher.setIsEligible(false);
                return;
            }
            if ((voucher.getPlatform() & Platform.WEB) > 0) {
                /*voucher chỉ áp dụng cho phiên bản website*/
                voucher.getCartErrorList().add(VoucherErrorEnum.platform_web.getValue());
                voucher.setVoucherHint(VoucherErrorEnum.platform_web.getValue());
                voucher.setIsEligible(false);
                return;
            }
            if ((voucher.getPlatform() & Platform.WEB_MOBILE) > 0) {
                /*voucher chỉ áp dụng cho phiên bản website mobile*/
                voucher.getCartErrorList().add(VoucherErrorEnum.platform_web_mobile.getValue());
                voucher.setVoucherHint(VoucherErrorEnum.platform_web_mobile.getValue());
                voucher.setIsEligible(false);
                return;
            }
        }
        if (Instant.now().isBefore(voucher.getStartDate())) {
            /* chưa đến thời gian kích hoạt*/
            voucher.getCartErrorList().add(VoucherErrorEnum.start_date_invalid.getValue());
            voucher.setVoucherHint(VoucherErrorEnum.start_date_invalid.getValue());
            voucher.setIsEligible(false);
            return;
        }
        if (Instant.now().isAfter(voucher.getEndDate())) {
            /* chưa đến thời gian kích hoạt*/
            voucher.getCartErrorList().add(VoucherErrorEnum.end_date_invalid.getValue());
            voucher.setVoucherHint(VoucherErrorEnum.end_date_invalid.getValue());
            voucher.setIsEligible(false);
            return;
        }
        if (voucher.getVoucherUsed() >= voucher.getVoucherNumber()) {
            /* voucher đã sử dụng hết lượt*/
            voucher.getCartErrorList().add(VoucherErrorEnum.order_remaining_invalid.getValue());
            voucher.setVoucherHint(VoucherErrorEnum.order_remaining_invalid.getValue());
            voucher.setIsEligible(false);
            return;
        }
        if (cartView != null) {
            if (voucher.getBuyMoneyMin() != null && voucher.getBuyMoneyMin() > cartView.getTotal()) {
                /* chưa đạt giá trị đơn hàng tối thiểu */
                voucher.getCartErrorList().add(VoucherErrorEnum.min_order_invalid.getValue());
                voucher.setVoucherHint(VoucherErrorEnum.min_order_invalid.getValue());
                voucher.setIsEligible(false);
            }
        }

    }
}

