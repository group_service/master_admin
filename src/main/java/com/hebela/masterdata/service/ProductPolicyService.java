package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCategory;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.bo.ProductPolicy;



public interface ProductPolicyService extends StatusService<ProductPolicy> {

    Product doLinkProduct(Integer productPolicyId, Product bo);

    ProductCategory doLinkProductCategory(Integer productPolicyId, ProductCategory bo);

    void doUnlinkProductCategory(Integer productPolicyId, ProductCategory bo);

    void doUnlinkProduct(Integer productPolicyId, Product bo);

    ProductCombo doLinkProductCombo(Integer productPolicyId, ProductCombo bo);

    void doUnlinkProductCombo(Integer productPolicyId, ProductCombo bo);
}

