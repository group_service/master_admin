package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.Favorite;

import java.util.List;

public interface FavoriteService extends Service<Favorite, Integer> {
}

