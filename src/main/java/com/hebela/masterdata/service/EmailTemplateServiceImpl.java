package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.masterdata.bo.EmailTemplate;
import com.hebela.masterdata.bo.Voucher;
import com.hebela.masterdata.dao.EmailTemplateDao;
import com.hebela.masterdata.web.emailTemplate.EmailTemplateRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


@Service
public class EmailTemplateServiceImpl extends AbstractService<EmailTemplate, Integer> implements EmailTemplateService {

    @Autowired
    private EmailTemplateDao emailTemplateDao;

    @Override
    public EmailTemplate get(Integer key) {
        return super.get(key);
    }

    @Override
    public EmailTemplate doInsert(EmailTemplate bo) {
        bo.setCreatedDate(Instant.now());
        bo.setModifiedDate(Instant.now());
        return super.doInsert(bo);
    }

    @Override
    public EmailTemplate doUpdate(EmailTemplate bo) {
        bo.setModifiedDate(Instant.now());
        return super.doUpdate(bo);
    }

    @Override
    public EmailTemplate doDelete(EmailTemplate bo) {
        return super.doDelete(bo);
    }

    @Override
    public Page<EmailTemplate> findBySearch(EmailTemplateRequest model) {
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());

        return this.emailTemplateDao.findAll(((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();

            if (model.getFromDate() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(EmailTemplate._createdDate), model.getFromDate()));
            }
            if (model.getToDate() != null) {
                predicates.add(builder.lessThan(root.get(EmailTemplate._createdDate), model.getToDate().plus(1, ChronoUnit.DAYS)));
            }
            if (model.getType() != null) {
                predicates.add(builder.equal(root.get(EmailTemplate._type), model.getType()));
            }
            if (StringUtils.isNotEmpty(model.getKeyword())) {
                predicates.add(builder.or(
                        (builder.like(root.get(EmailTemplate._name), "%" + model.getKeyword() + "%")),
                        (builder.like(root.get(EmailTemplate._subject), "%" + model.getKeyword() + "%")))
                );
            }

            query.orderBy(builder.desc(root.get(EmailTemplate._modifiedDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }), pageable);
    }
}

