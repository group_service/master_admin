package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Review;
import com.hebela.masterdata.bo.ReviewSummary;
import com.hebela.masterdata.web.review.ReviewSearchRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ReviewService extends StatusService<Review> {
    Review doApprove(Review bo);

    Page<Review> findBySearch(ReviewSearchRequest dto, Pageable pageable);

    Review getReviewDetail(Integer id);
}
