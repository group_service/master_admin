package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.ObjectImage;
import com.hebela.masterdata.dao.ObjectImageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class ObjectImageServiceImpl extends AbstractService<ObjectImage, Integer> implements ObjectImageService {
    @Autowired
    ObjectImageDao objectImageDao;

    @Override
    public List<ObjectImage> doAddObjectImage(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId) {
        this.validateData(objectImageList, objectClass, objectClassId);
        for (ObjectImage objectImageClient : objectImageList) {
            objectImageClient.setObjectClass(objectClass);
            objectImageClient.setObjectClassId(objectClassId);
            objectImageDao.customUpdate(null, User.getContextId(), objectImageClient);
        }
        return objectImageList;
    }

    @Override
    public List<ObjectImage> doUpdateObjectImage(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId) {
        this.validateData(objectImageList, objectClass, objectClassId);
        Map<Integer, ObjectImage> imageClientMap = objectImageList.stream().collect(Collectors.toMap(ObjectImage::getId, image -> image));
        List<Integer> objectImageIds = objectImageList.stream().map(ObjectImage::getId).collect(Collectors.toList());
        List<ObjectImage> objectImageListDb = this.findObjectImage(objectImageIds, objectClass, objectClassId);
        for (ObjectImage objectImageDb : objectImageListDb) {
            objectImageDao.customUpdate(objectImageDb, User.getContextId(), imageClientMap.get(objectImageDb.getId()));

        }
        return objectImageList;
    }

    @Override
    public List<ObjectImage> doUpdateObjectImagePriority(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId) {
        this.validateData(objectImageList, objectClass, objectClassId);
        Map<Integer, Integer> priorityClientMap = objectImageList.stream().collect(Collectors.toMap(ObjectImage::getId, ObjectImage::getPriority));
        List<Integer> objectImageIds = objectImageList.stream().map(ObjectImage::getId).collect(Collectors.toList());
        List<ObjectImage> objectImageListDb = this.findObjectImage(objectImageIds, objectClass, objectClassId);
        for (ObjectImage objectImageDb : objectImageListDb) {
            objectImageDb.setPriority(priorityClientMap.get(objectImageDb.getId()));
            objectImageDb.setModifiedBy(User.getContextId());
        }
        objectImageDao.saveAll(objectImageListDb);

        return objectImageList;
    }

    @Override
    public List<ObjectImage> doDeleteObjectImage(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId) {
        this.validateData(objectImageList, objectClass, objectClassId);
        List<Integer> objectImageIds = objectImageList.stream().map(ObjectImage::getId).collect(Collectors.toList());
        List<ObjectImage> objectImageListDb = this.findObjectImage(objectImageIds, objectClass, objectClassId);
        objectImageDao.deleteAll(objectImageListDb);
        return objectImageList;
    }

    @Override
    public List<ObjectImage> findObjectImage(List<Integer> objectImageIds, String objectClass, Integer objectClassId) {
        return objectImageDao.findAll((root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get(ObjectImage._objectClass), objectClass));
            predicates.add(builder.equal(root.get(ObjectImage._objectClassId), objectClassId));
            if (objectImageIds != null && objectImageIds.size() > 0) {
                predicates.add(root.get(ObjectImage._id).in(objectImageIds));
            }
            query.orderBy(
                    builder.asc(root.get(ObjectImage._priority)),
                    builder.asc(root.get(ObjectImage._createdDate))
            );
            return builder.and(predicates.toArray(new Predicate[0]));
        });
    }

    private void validateData(List<ObjectImage> objectImageList, String objectClass, Integer objectClassId) {
        if (objectImageList == null || objectImageList.size() == 0 || objectClass == null || objectClassId == null) {
            throw new HebelaException("object.image.invalid");
        }
    }
}

