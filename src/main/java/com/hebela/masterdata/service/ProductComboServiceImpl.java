package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import com.hebela.masterdata.web.combo.ProductComboSearchRequest;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductComboServiceImpl extends AbstractStatusService<ProductCombo> implements ProductComboService {
    @Autowired
    private ProductComboDao productComboDao;
    @Autowired
    private ProductComboLinkDao productComboLinkDao;
    @Autowired
    private ProductCategoryDao productCategoryDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private ProductComboGalleryDao productComboGalleryDao;
    @Autowired
    private FavoriteDao favoriteDao;
    @Autowired
    private MetaConfigDao metaConfigDao;
    @Autowired
    private ProductGiftDao productGiftDao;
    @Autowired
    private CartDao cartDao;
    @Autowired
    private MenuItemDao menuItemDao;
    @Autowired
    private RecommendationLinkDao recommendationLinkDao;

    public static final int pageSize = 10;

    @Override
    public ProductCombo get(Integer comboId) {
        ProductCombo productCombo = productComboDao.findById(comboId).orElse(null);
        if (productCombo == null) {
            throw new HebelaException("combo.obj.missing");
        }
        List<Product> productList = productComboDao.findProductInCombo(comboId);
        productCombo.setProductList(productList);
        productCombo.setGalleryList(this.findGallery(Collections.singletonList(comboId)));
        if (User.getContextId() != null) {
            productCombo.setIsFavorite(this.favoriteDao.findByAccountIdAndComboId(User.getContextId(), comboId).size() > 0);
        }

        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(comboId, MetaConfig.TYPE_COMBO)));
        dbMetaConfigOptional.ifPresent(productCombo::setMetaConfig);
        return productCombo;
    }

    @Override
    public List<ProductCombo> find(ProductCombo bo) {
        List<ProductCombo> productComboList = this.productComboDao.findAll(Example.of(bo),
                Sort.by(Sort.Order.asc(ProductCombo._priority), Sort.Order.desc(ProductCombo._createdDate)));
        for (ProductCombo productCombo : productComboList) {
            productCombo.setProductList(this.productComboDao.findProduct(productCombo.getId()));
            if (productCombo.getProductCategoryId() != null) {
                productCombo.setProductCategoryName(this.productCategoryDao.getOne(productCombo.getProductCategoryId()).getName());
            }
        }
        return productComboList;
    }

    public void addProducts(Map<Integer, ProductCombo> productComboMap, List<Integer> productComboIdList) {
        if (productComboMap.size() == 0 || productComboIdList.size() == 0) {
            return;
        }
        List<Product> productList = productComboDao.findAllProduct(productComboIdList);
        for (Integer comboId : productComboMap.keySet()) {
            List<Product> productInCombo = productList.stream().filter(product -> product.getComboId().equals(comboId)).collect(Collectors.toList());
            ProductCombo productCombo = productComboMap.get(comboId);
            productCombo.setProductList(productInCombo);
            getAmountCombo(productCombo, productInCombo);
        }
    }

    private List<ProductComboGallery> doInsertGallery(Integer comboId, List<ProductComboGallery> insertList) {
        for (int i = 0; i < insertList.size(); i++) {
            ProductComboGallery gallery = insertList.get(i);
            gallery.setPriority(i + 1);
            if (gallery.getProductComboId() == null) {
                gallery.setProductComboId(comboId);
            }
        }
        this.productComboGalleryDao.saveAll(insertList);
        return insertList;
    }

    private List<ProductComboGallery> doUpdateGallery(Integer comboId, List<ProductComboGallery> insertList) {
        insertList.forEach(item -> {
            if (item.getProductComboId() == null) {
                item.setProductComboId(comboId);
            }
        });
        ArrayList<ProductComboGallery> result = new ArrayList<>();
        val example = new ProductComboGallery();
        example.setProductComboId(comboId);
        List<ProductComboGallery> removeList = this.productComboGalleryDao.findAll(Example.of(example));
        for (Iterator<ProductComboGallery> insertIterator = insertList.iterator(); insertIterator.hasNext(); ) {
            ProductComboGallery insertItem = insertIterator.next();
            for (Iterator<ProductComboGallery> removeIterator = removeList.iterator(); removeIterator.hasNext(); ) {
                ProductComboGallery removeItem = removeIterator.next();
                if (removeItem.getId().equals(insertItem.getId()) && removeItem.getPriority().equals(insertItem.getPriority())) {
                    result.add(removeItem);
                    insertIterator.remove();
                    removeIterator.remove();
                }
            }
        }
        this.productComboGalleryDao.deleteAll(removeList);
        this.productComboGalleryDao.saveAll(insertList);
        result.addAll(insertList);
        return result;
    }

    /*
     * Tạo mới combo
     * Nếu đăng bán luôn thì số lượng sản phẩm liên kết phải lớn hơn 1
     * */
    @Override
    public ProductCombo doInsert(ProductCombo bo) {
        this.validatePriceProductCombo(bo);
        if (!ProductCombo.STATUS_ACTIVE.equals(bo.getStatus())) {
            bo.setStatus(ProductCombo.STATUS_DRAFT);
        }
        bo.setPriority(0);
        ProductCombo dbBo = super.doInsert(bo);
        for (Product product : bo.getProductList()) {
            ProductComboLink productComboLink = new ProductComboLink();
            productComboLink.setComboId(dbBo.getId());
            productComboLink.setProductId(product.getId());
            productComboLink.setQuantity(product.getQuantityInCombo());
            this.productComboLinkDao.save(productComboLink);
        }
        if (bo.getGalleryList() != null) {
            bo.getGalleryList().forEach(item -> {
                item.setProductComboId(bo.getId());
                if (item.getPriority() == null) {
                    item.setPriority(0);
                }
            });
            dbBo.setGalleryList(this.doInsertGallery(bo.getId(), bo.getGalleryList()));
        }
        checkComboValid(dbBo, ProductCombo.STATUS_ACTIVE.equals(bo.getStatus()));
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setComboId(dbBo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_COMBO);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        return dbBo;
    }

    /*
     * Cập nhật combo
     * Pre: Combo không bị hủy
     * Chỉ cập nhật thông tin combo, trạng thái và sản phẩm gắn với combo không bị thay đổi
     * */
    @Override
    public ProductCombo doUpdate(ProductCombo bo) {
        this.validatePriceProductCombo(bo);
        ProductCombo dbBo = super.get(bo);
        if (Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        checkComboValid(bo, true);
        dbBo.setName(bo.getName());
        dbBo.setSlug(bo.getSlug());
        dbBo.setSku(bo.getSku());
        dbBo.setBarcode(bo.getBarcode());
        dbBo.setProductCategoryId(bo.getProductCategoryId());
        dbBo.setPrice(bo.getPrice());
        dbBo.setPriceRegular(bo.getPriceRegular());
        dbBo.setImageThumbnail(bo.getImageThumbnail());
        dbBo.setDescriptionSummary(bo.getDescriptionSummary());
        dbBo.setDescriptionFull(bo.getDescriptionFull());
        dbBo.setAmount(bo.getAmount());
        dbBo.setModifiedBy(User.getContextId());
        dbBo.setDisplayDiscountType(bo.getDisplayDiscountType());
        super.doUpdate(dbBo);
        bo.getGalleryList().forEach(item -> {
            item.setProductComboId(bo.getId());
            if (item.getPriority() == null) {
                item.setPriority(0);
            }
        });
        dbBo.setGalleryList(this.doUpdateGallery(bo.getId(), bo.getGalleryList()));
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_COMBO, User.getContextId(), bo.getMetaConfig());
        }
        return dbBo;
    }

    @Override
    public ProductCombo doAddProduct(ProductCombo bo) {
        ProductCombo dbCombo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbCombo.getIsTrash()) || ProductCombo.STATUS_ACTIVE.equals(dbCombo.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        List<ProductComboLink> insertList = new ArrayList<>();
        bo.getProductList().forEach(item -> {
            ProductComboLink productComboLink = new ProductComboLink();
            productComboLink.setComboId(dbCombo.getId());
            productComboLink.setProductId(item.getId());
            productComboLink.setQuantity(1);
            insertList.add(productComboLink);
        });
        updateProductOfCombo(dbCombo.getId(), insertList);
        this.checkComboValid(dbCombo, false);
        return dbCombo;
    }

    private void updateProductOfCombo(Integer comboId, List<ProductComboLink> clientProductList) {
        List<ProductComboLink> dbRecommendationLinks = getRecommendationLinkList(comboId);
        List<Integer> dbProductIdList = dbRecommendationLinks.stream().map(ProductComboLink::getProductId).collect(Collectors.toList());
        for (ProductComboLink item : clientProductList) {
            if (!dbProductIdList.contains(item.getProductId())) {
                this.productComboLinkDao.save(item);
            }
        }
    }

    private List<ProductComboLink> getRecommendationLinkList(Integer comboId) {
        ProductComboLink example = new ProductComboLink();
        example.setComboId(comboId);
        return this.productComboLinkDao.findAll(Example.of(example));
    }

    @Override
    public ProductCombo doRemoveProduct(ProductCombo bo) {
        ProductCombo dbCombo = super.get(bo.getId());
        if (ProductCombo.STATUS_ACTIVE.equals(dbCombo.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        ProductComboLink productComboLink = new ProductComboLink();
        productComboLink.setComboId(bo.getId());
        productComboLink.setProductId(bo.getProduct().getId());
        List<ProductComboLink> productComboLinkList = productComboLinkDao.findAll(Example.of(productComboLink));
        if (productComboLinkList.size() > 0) {
            productComboLinkDao.deleteAll(productComboLinkList);
        }
        this.checkComboValid(dbCombo, true);
        return dbCombo;
    }

    /*
     * Đăng bán combo
     * Chuyển trạng thái từ "Nháp"/"Ngừng bán" sang "Đăng bán"
     * Combo phải không bị hủy và ở trạng thái "Nháp"
     * Các sản phẩm trong combo phải là loại kinh doanh và đang đăng bán
     * Số sản phẩm gắn với combo phải lớn hơn 1
     */
    @Override
    public ProductCombo doActivate(ProductCombo bo) {
        ProductCombo dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || ProductCombo.STATUS_ACTIVE.equals(dbBo.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        checkComboValid(dbBo, true);
        dbBo.setStatus(ProductCombo.STATUS_ACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        dao.save(dbBo);
        return dbBo;
    }

    /*
     * Ngừng bán combo
     * Pre: combo phải ở trạng thái "Đăng bán"
     * Action: Chuyển trạng thái từ "Đăng bán" sang "Ngừng bán"
     * Post:
     * Xóa liên kết quà tặng mới combo đó đang có quà tặng
     * Xóa liên kết yêu thích
     **/
    @Override
    public ProductCombo doDeactivate(ProductCombo bo) {
        ProductCombo dbBo = super.get(bo.getId());
        if (!Product.STATUS_ACTIVE.equals(dbBo.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        dbBo.setStatus(ProductCombo.STATUS_INACTIVE);
        dbBo.setModifiedBy(User.getContextId());
        dbBo.setModifiedDate(Instant.now());
        dao.save(dbBo);
        /*Xóa liên kết quà tặng*/
        removeProductGift(bo.getId());
        /*Xóa liên kết yêu thích*/
        removeComboInFavorite(bo.getId());
        /** Xoá khỏi giỏ hàng*/
        removeCart(bo.getId());
        return dbBo;
    }

    /*
     * Hủy combo
     * Pre: combo ở trạng thái "Nháp" hoặc "Ngừng bán"
     * */
    @Override
    public ProductCombo doTrash(ProductCombo bo) {
        ProductCombo dbBo = super.get(bo.getId());
        if (!ProductCombo.STATUS_DRAFT.equals(dbBo.getStatus()) && !ProductCombo.STATUS_INACTIVE.equals(dbBo.getStatus())) {
            throw new HebelaException("action.invalid");
        }
        /*Xóa liên kết yêu thích*/
        removeComboInFavorite(bo.getId());
//        /*Xóa liên kết quà tặng*/
//        removeProductGift(bo.getId());
//        /*Xóa liên kết banner*/
//        removeBannerItem(bo.getId());
//        /*Xóa liên kết menuItem*/
//        removeMenuItem(bo.getId());
//        /*Xóa liên kết recommendation*/
//        removeRecommendationLink(bo.getId());
        return super.doTrash(bo);
    }

    /*Xóa liên kết yêu thích*/
    private void removeComboInFavorite(Integer comboId) {
        Favorite favorite = new Favorite();
        favorite.setComboId(comboId);
        List<Favorite> favoriteList = favoriteDao.findAll(Example.of(favorite));
        favoriteDao.deleteAll(favoriteList);
    }

    /*Xóa liên kết quà tặng*/
    private void removeProductGift(Integer comboId) {
        List<ProductGift> productGiftList = productGiftDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(ProductGift._productComboId), comboId)
        ));
        productGiftDao.deleteAll(productGiftList);
    }

    private void removeCart(Integer comboId) {
        Cart cart = new Cart();
        cart.setComboId(comboId);
        List<Cart> cartList = cartDao.findAll(Example.of(cart));
        cartDao.deleteAll(cartList);
    }

//    /*Xóa liên kết banner*/
//    private void removeBannerItem(Integer comboId) {
//        List<BannerItem> bannerItems = bannerItemDao.findAll((root, query, builder) -> builder.and(
//                builder.equal(root.get(BannerItem._comboId), comboId)
//        ));
//        bannerItemDao.deleteAll(bannerItems);
//    }
//
//    /*Xóa liên kết menuItem*/
//    private void removeMenuItem(Integer comboId) {
//        List<MenuItem> menuItems = menuItemDao.findAll((root, query, builder) -> builder.and(
//                builder.equal(root.get(MenuItem._productComboId), comboId)
//        ));
//        menuItemDao.deleteAll(menuItems);
//    }
//
//    /*Xóa liên kết recommendation*/
//    private void removeRecommendationLink(Integer comboId) {
//        List<RecommendationLink> recommendationLinks = recommendationLinkDao.findAll((root, query, builder) -> builder.and(
//                builder.equal(root.get(RecommendationLink._comboId), comboId)
//        ));
//        recommendationLinkDao.deleteAll(recommendationLinks);
//    }

    /*
     * Khôi phục combo
     * Pre: Combo phải đang bị hủy và các sản phẩm trong combo phải là loại kinh doanh và đang đăng bán
     * Post: Khôi phục combo sẽ có trạng thái "Nháp"
     * */
    @Override
    public ProductCombo doRestore(ProductCombo bo) {
        ProductCombo dbBo = super.get(bo);
        /* check sản phẩm trong thùng rác hoặc trạng thái nháp*/
        checkComboValid(dbBo, false);
        dbBo.setPriority(0);
        dbBo.setIsTrash(Boolean.FALSE);
        dbBo.setStatus(ProductCombo.STATUS_DRAFT);
        dbBo.setModifiedBy(User.getContextId());
        return dao.save(dbBo);
    }

    @Override
    public List<ProductCombo> findByTerm(String term) {
        List<ProductCombo> comboList = productComboDao.findByTerm(User.getContext().getOrgId(), term);
        if (comboList != null && comboList.size() > 0) {
            Map<Integer, ProductCombo> comboMap = comboList.stream().collect(Collectors.toMap(ProductCombo::getId, combo -> combo));
            List<Integer> comboIds = new ArrayList<>();
            comboList.forEach(combo -> comboIds.add(combo.getId()));
            this.addProducts(comboMap, comboIds);
            return comboList.stream().filter(combo -> combo.getAmount() > 0).collect(Collectors.toList());
        }
        return null;
    }

    public List<ProductComboGallery> findGallery(List<Integer> productComboIdList) {
        List<ProductComboGallery> productComboGalleryList = this.productComboGalleryDao.findAllByProductComboIdIn(productComboIdList);
        productComboGalleryList.sort(Comparator.comparing(ProductComboGallery::getPriority));
        return productComboGalleryList;
    }

    /**
     * kết quả phương thức phụ thuộc orgId
     */
    @Override
    public List<Product> findProductForCombo(Integer comboId, String query) {
        return this.productDao.findProductForCombo(getOrgId(), query, comboId);
    }

    @Override
    public List<Product> findProductInCombo(Integer comboId) {
        return this.productComboDao.findProduct(comboId);
    }

    /*
     * Tìm kiếm combo
     * Tham số: Tên combo hoặc sản phẩm, sku
     * */
    @Override
    public Page<ProductCombo> findBySearch(ProductComboSearchRequest model) {
        Integer orgId = null;
        if (Organization.ROOT_ACCOUNT.getId().equals(User.getContext().getOrgId())) {
            orgId = model.getOrgId();
        }
        if (orgId == null) {
            orgId = getOrgId();
        }
        if (model.getPage() == null || model.getPage() < 0) {
            model.setPage(0);
        }
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        List<Integer> statusList = model.getStatus() == null ? Arrays.asList(ProductCombo.STATUS_ACTIVE, ProductCombo.STATUS_INACTIVE, ProductCombo.STATUS_DRAFT)
                : Collections.singletonList(model.getStatus());
        Page<ProductCombo> productComboList = this.productComboDao.findBySearch(
                orgId,
                model.getKeyword(),
                model.getSku(),
                model.getBarcode(),
                model.getIsTrash(),
                model.getType(),
                model.getCategoryId(),
                statusList,
                pageable
        );
        if (productComboList != null && productComboList.getContent().size() > 0) {
            for (ProductCombo productCombo : productComboList.getContent()) {
                List<Product> products = productComboDao.findProduct(productCombo.getId());
                if (!productCombo.getType().equals(ProductCombo.TYPE_FOR_PACKAGE)) {
                    Integer priceRegular = products.stream().mapToInt(item -> {
                        return Math.max(item.getProductPrice().getPriceRegular(), item.getProductPrice().getPriceSale()) * item.getQuantityInCombo();
                    }).sum();
                    productCombo.setPriceRegular(priceRegular);
                }
                productCombo.setProductList(products);
                if (productCombo.getProductCategoryId() != null) {
                    productCombo.setProductCategoryName(this.productCategoryDao.getOne(productCombo.getProductCategoryId()).getName());
                }
            }
        }
//        List<Integer> productComboIdList = null;
//        if (model.getSku() != null) {
//            Product product = new Product();
//            product.setSku(model.getSku());
//            List<Product> productList = this.productService.find(product);
//            if (productList.size() == 0) {
//                return this.productComboDao.findByProductId(null, model.getIsTrash(), null);
//            } else {
//                productComboIdList = this.productComboDao.findByProductId(productList.get(0).getId(), model.getIsTrash(), null)
//                        .stream().map(ProductCombo::getId).collect(Collectors.toList());
//            }
//        }
//        ProductCombo productCombo = new ProductCombo();
//        productCombo.copyData(model);
//        if (productComboIdList != null) {
//            List<Integer> finalProductComboIdList = productComboIdList;
//            return this.find(productCombo).stream().filter(item -> finalProductComboIdList.contains(item.getId())).collect(Collectors.toList());
//        }
//        return this.find(productCombo);
        return productComboList;
    }

    /*
     * Kiểm tra xem combo có hợp lệ để active được không
     * Sản phẩm trong combo phải hợp lệ: Không bị hủy | Trạng thái "Đăng bán" | Loại: không là quà tặng
     * Số lượng sản phẩm hợp lệ trong combo phải lớn hơn 1
     * */
    private void checkComboValid(ProductCombo dbBo, boolean checkCount) {
        List<Product> productList = productDao.findAll((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            Root<ProductComboLink> comboLinkRoot = query.from(ProductComboLink.class);
            predicates.add(builder.equal(root.get(Product._id), comboLinkRoot.get(ProductComboLink._productId)));
            query.distinct(true);
            predicates.add(builder.equal(comboLinkRoot.get(ProductComboLink._comboId), dbBo.getId()));
            return builder.and(predicates.toArray(new Predicate[0]));
        });
        for (Product product : productList) {
            if (Boolean.TRUE.equals(product.getIsTrash()) || !Product.STATUS_ACTIVE.equals(product.getStatus())
                    || Product.TYPE_GIFT.equals(product.getType())) {
                throw new HebelaException("combo.product.invalid");
            }
            if (ProductCombo.TYPE_RETAIL.equals(dbBo.getType()) && !Product.TYPE_SELL.equals(product.getType())) {
                throw new HebelaException("combo.product.invalid");
            }
        }

        if (checkCount) {
            Integer total = this.productComboLinkDao.sumQuantityComBo(dbBo.getId());
            if (total == null || total < 2) {
                throw new HebelaException("combo.product.amount.invalid");
            }
        }
    }

    private void validatePriceProductCombo(ProductCombo bo) {
        if (bo.getPrice() == null) {
            throw new HebelaException("product.priceSale.missing");
        }
        if (ProductCombo.TYPE_RETAIL.equals(bo.getType())) {
            bo.setPriceRegular(0);
        }
        if (ProductCombo.TYPE_FOR_PACKAGE.equals(bo.getType())) {
            if (bo.getPriceRegular() == null) {
                throw new HebelaException("product.priceRegular.missing");
            }
        }
    }

    @Override
    public void doSyncAmount(ProductCombo bo) {
        ProductCombo dbBo = productComboDao.findDistinctBySku(bo.getSku());
        dbBo = productComboDao.lockByIds(Collections.singleton(dbBo.getId())).get(0);
        // bo.getAmount là số lượng tương đối (tăng, giảm)
        dbBo.setAmount(dbBo.getAmount() + bo.getAmount());
        dbBo.setModifiedBy(User.getContextId());
        productComboDao.save(dbBo);
    }

    @Override
    public ProductCombo doUpdateQuantityInCombo(ProductCombo bo) {

        Product product = bo.getProduct();
        ProductComboLink productComboLink = new ProductComboLink();
        productComboLink.setComboId(bo.getId());
        productComboLink.setProductId(bo.getProduct().getId());
        ProductComboLink productComboLinkDB = productComboLinkDao.findOne(Example.of(productComboLink)).orElse(null);
        if (productComboLinkDB == null) {
            throw new HebelaException("data.input.invalid");
        }
        productComboLinkDB.setQuantity(product.getQuantityInCombo());
        if (product.getQuantityInCombo() < 1) {
            throw new HebelaException("combo.product.amount.invalid");
        }
        this.productComboLinkDao.save(productComboLinkDB);
        return bo;
    }

    private void getAmountCombo(ProductCombo combo, List<Product> productInComboList) {
        int available = combo.getAmount() != null ? combo.getAmount() : 0;
        if (ProductCombo.TYPE_RETAIL.equals(combo.getType())) {
            for (Product product : productInComboList) {
                available = Integer.min(available, product.getAmount());
            }
        }
        combo.setAmount(available);
    }
}
