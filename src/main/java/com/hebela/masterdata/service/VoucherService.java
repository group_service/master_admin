package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.core.bo.Account;
import com.hebela.masterdata.bo.Voucher;
import com.hebela.masterdata.bo.VoucherGiftLink;
import com.hebela.masterdata.bo.extend.CartView;
import com.hebela.masterdata.web.voucher.VoucherSearchRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface VoucherService extends StatusService<Voucher> {
    List<Account> findAccount(String keyword);

    Voucher doActivate(Voucher bo);

    Voucher doDeactivate(Voucher bo);

    Page<Voucher> findBySearch(VoucherSearchRequest bo);

    Voucher doAddAccountOfVoucher(Voucher voucher);

    Voucher doRemoveAccountOfVoucher(Voucher voucher);

    List<Voucher> findByAccount(Integer platform, CartView view, Integer orgID);

    Voucher doRemoveProductGift(Voucher voucher);

    Voucher doAddProductGift(Voucher voucher);

    VoucherGiftLink doChangeQuantityGift(Voucher voucher);
}

