package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.web.combo.ProductComboSearchRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductComboService extends StatusService<ProductCombo> {

    ProductCombo doActivate(ProductCombo productCombo);

    ProductCombo doDeactivate(ProductCombo productCombo);

    ProductCombo doAddProduct(ProductCombo bo);

    ProductCombo doRemoveProduct(ProductCombo bo);

    Page<ProductCombo> findBySearch(ProductComboSearchRequest model);

    List<Product> findProductForCombo(Integer comboId, String name);

    List<Product> findProductInCombo(Integer comboId);

    List<ProductCombo> findByTerm(String term);

    void doSyncAmount(ProductCombo o);

    ProductCombo doUpdateQuantityInCombo(ProductCombo productCombo);
}

