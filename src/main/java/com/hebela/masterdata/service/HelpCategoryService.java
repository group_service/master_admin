package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.HelpCategory;

public interface HelpCategoryService extends StatusService<HelpCategory> {

}

