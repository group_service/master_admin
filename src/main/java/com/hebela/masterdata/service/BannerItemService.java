package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.BannerItem;

import java.util.List;

public interface BannerItemService extends StatusService<BannerItem> {
}
