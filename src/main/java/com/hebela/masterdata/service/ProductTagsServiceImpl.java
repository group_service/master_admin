package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.masterdata.bo.ProductTags;
import com.hebela.masterdata.bo.ProductTagsBase;
import com.hebela.masterdata.dao.ProductTagsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductTagsServiceImpl extends AbstractStatusService<ProductTags> implements ProductTagsService {
    @Autowired
    private ProductTagsDao productTagsDao;

    @Override
    public List<ProductTags> findWithExtends(ProductTags productTags) {
        return productTagsDao.findWithExtends(productTags.getStatus() != null ? productTags.getStatus() : 1, productTags.getIsTrash() != null ? productTags.getIsTrash() : false);
    }
}

