package com.hebela.masterdata.service;

import com.hebela.core.Service;
import com.hebela.masterdata.bo.Attribute;
import com.hebela.masterdata.bo.ProductCategory;

import java.util.List;

public interface ProductCategoryService extends Service<ProductCategory, Integer> {
    List<ProductCategory> findByTerm(String term);

    ProductCategory getDetail(Integer categoryId);

    ProductCategory doTrash(ProductCategory bo);

    ProductCategory doSetDefault(ProductCategory bo);

    List<ProductCategory> findByKeyword(ProductCategory bo);

    List<ProductCategory> findTree(String type);

    List<ProductCategory> findFlat(String type);
}
