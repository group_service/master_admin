package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.core.bo.Account;
import com.hebela.core.obj.StatusInfo;
import com.hebela.masterdata.bo.Order;
import com.hebela.masterdata.bo.OrderRoot;
import com.hebela.masterdata.bo.extend.Checkout;
import com.hebela.masterdata.web.order.AdminOrderFilterRequest;
import com.hebela.masterdata.web.order.CancelOrderRequest;
import com.hebela.masterdata.web.order.ShopViewOrderResponse;
import com.hebela.masterdata.web.order.insert.OrderRootResponse;
import com.hebela.masterdata.web.order.merchant.MerchantOrderUpdateRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OrderService extends StatusService<Order> {
    StatusInfo merchantStatusInfo();

    Page<Order> findMerchantOrder(AdminOrderFilterRequest model);

    Page<Order> findHebelaOrder(AdminOrderFilterRequest model);

    OrderRoot getOrderToRefund(String orderRootCode, Integer accountId);

    OrderRoot doMerchantUpdateStatus(MerchantOrderUpdateRequest model);

    void doRefund(OrderRoot orderRoot);

    ShopViewOrderResponse detail(String code);

    Order getByCode(String code, Integer accountId);

    Order doCancel(CancelOrderRequest model, Integer accountId);

    List<Account> findAccount(String keyword);

    OrderRoot doInsert(List<Order> orderList, Checkout checkout);

    Order doSyncStatus(Order bo);

    void doUpdatePayment(OrderRoot o);

    OrderRootResponse doCloneOrder(Order order);

    Order doRefundDone(String orderCode);

    Order getByCode(String code);

    Long countOrderByOrderRoot(Integer orderRootId);

    Order doUpdateRefundSuccess(Order order, OrderRoot orderRoot);

    List<Order> publish(List<String> orderCodeList);

    Order publish(String orderCode);
}

