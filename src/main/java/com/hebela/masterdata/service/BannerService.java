package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.Banner;

public interface BannerService extends StatusService<Banner> {
}
