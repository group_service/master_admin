package com.hebela.masterdata.service;

import com.hebela.core.AbstractService;
import com.hebela.masterdata.bo.EmailConfig;
import com.hebela.masterdata.bo.EmailTemplate;
import com.hebela.masterdata.dao.EmailConfigDao;
import com.hebela.masterdata.dao.EmailTemplateDao;
import com.hebela.masterdata.web.emailTemplateConfig.EmailConfigRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


@Service
public class EmailConfigServiceImpl extends AbstractService<EmailConfig, Integer> implements EmailConfigService {

    @Autowired
    private EmailConfigDao emailTemplateConfigDao;
    @Autowired
    private EmailTemplateDao emailTemplateDao;

    @Override
    public EmailConfig get(Integer key) {
        EmailConfig emailConfig = super.get(key);
        EmailTemplate emailTemplate = this.emailTemplateDao.getEmailTemplateById(emailConfig.getEmailTemplateId());
        emailConfig.setEmailTemplate(emailTemplate);
        return emailConfig;
    }

    @Override
    public EmailConfig doInsert(EmailConfig bo) {
        bo.setStatus(EmailConfig.DEACTIVE);
        bo.setCreatedDate(Instant.now());
        bo.setModifiedDate(Instant.now());
        return super.doInsert(bo);
    }

    @Override
    public EmailConfig doUpdate(EmailConfig bo) {
        EmailConfig emailConfigDb = super.get(bo.getId());
        emailConfigDb.setNameConfig(bo.getNameConfig());
        emailConfigDb.setMailTo(bo.getMailTo());
        emailConfigDb.setEventType(bo.getEventType());
        emailConfigDb.setEmailTemplateId(bo.getEmailTemplateId());
        emailConfigDb.setModifiedDate(Instant.now());
        emailConfigDb.setNameConfig(bo.getNameConfig());
        return super.doUpdate(emailConfigDb);
    }

    @Override
    public EmailConfig doDelete(EmailConfig bo) {
        return super.doDelete(bo);
    }

    @Override
    public Page<EmailConfig> findBySearch(EmailConfigRequest model) {
        Pageable pageable = PageRequest.of(model.getPage(), model.getSize());
        Page<EmailConfig> emailTemplateConfigPage = this.emailTemplateConfigDao.findAll(((root, query, builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (model.getStatus() != null) {
                predicates.add(builder.equal(root.get(EmailConfig._status), model.getStatus()));
            }
            if (model.getEventType() != null) {
                predicates.add(builder.equal(root.get(EmailConfig._eventType), model.getEventType()));
            }
            if (model.getFromDate() != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(EmailConfig._createdDate), model.getFromDate()));
            }
            if (model.getToDate() != null) {
                predicates.add(builder.lessThan(root.get(EmailConfig._createdDate), model.getFromDate().plus(1, ChronoUnit.DAYS)));
            }
            if (StringUtils.isNotEmpty(model.getNameConfig())) {
                predicates.add(builder.like(root.get(EmailConfig._nameConfig), "%" + model.getNameConfig() + "%"));
            }
            query.orderBy(builder.desc(root.get(EmailConfig._modifiedDate)));
            return builder.and(predicates.toArray(new Predicate[0]));
        }), pageable);


        return new PageImpl<>(emailTemplateConfigPage.getContent(), pageable, emailTemplateConfigPage.getTotalElements());
    }

    @Override
    public EmailConfig doActive(EmailConfig emailTemplateConfig) {
        EmailConfig emailTemplateConfigDB = super.get(emailTemplateConfig.getId());
        emailTemplateConfigDB.setStatus(EmailConfig.ACTIVE);
        return this.emailTemplateConfigDao.save(emailTemplateConfigDB);
    }

    @Override
    public EmailConfig doDeActive(EmailConfig emailTemplateConfig) {
        EmailConfig emailTemplateConfigDB = super.get(emailTemplateConfig.getId());
        emailTemplateConfigDB.setStatus(EmailConfig.DEACTIVE);
        return this.emailTemplateConfigDao.save(emailTemplateConfigDB);
    }
}

