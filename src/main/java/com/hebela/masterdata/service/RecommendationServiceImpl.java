package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.HebelaException;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RecommendationServiceImpl extends AbstractStatusService<Recommendation> implements RecommendationService {
    private final ProductDao productDao;
    private final RecommendationDao recommendationDao;
    private final ProductComboDao productComboDao;
    private final ProductCategoryDao productCategoryDao;
    private final RecommendationLinkDao recommendationLinkDao;
    private final MetaConfigDao metaConfigDao;
    private final CouponDao couponDao;
    private final BrandDao brandDao;
    private final ObjectLinkDao objectLinkDao;

    public RecommendationServiceImpl(ProductDao productDao, RecommendationDao recommendationDao, ProductComboDao productComboDao, ProductCategoryDao productCategoryDao, RecommendationLinkDao recommendationLinkDao, MetaConfigDao metaConfigDao, CouponDao couponDao, BrandDao brandDao, ObjectLinkDao objectLinkDao) {
        this.productDao = productDao;
        this.recommendationDao = recommendationDao;
        this.productComboDao = productComboDao;
        this.productCategoryDao = productCategoryDao;
        this.recommendationLinkDao = recommendationLinkDao;
        this.metaConfigDao = metaConfigDao;
        this.couponDao = couponDao;
        this.brandDao = brandDao;
        this.objectLinkDao = objectLinkDao;
    }

    @Override
    public Recommendation get(Integer key) {
        Recommendation dbBo = super.get(key);
        if (Recommendation.TYPE_PRODUCT.equals(dbBo.getType())) {
            dbBo.setProductList(this.productDao.findByRecommendationId(dbBo.getId()));
        } else if (Recommendation.TYPE_COMBO.equals(dbBo.getType())) {
            dbBo.setProductComboList(this.productComboDao.findByRecommendationId(dbBo.getId()));
        } else if (Recommendation.TYPE_CATEGORY.equals(dbBo.getType())) {
            dbBo.setProductCategoryList(this.productCategoryDao.findByRecommendationId(dbBo.getId()));
        } else if (Recommendation.TYPE_COUPON.equals(dbBo.getType())) {
            List<Coupon> couponList = this.couponDao.findByRecommendationIdForAdmin(dbBo.getId());
            Map<Integer, Integer> mapDisplayType = recommendationLinkDao.findAll((root, query, builder) -> builder.and(
                    builder.equal(root.get(RecommendationLink._recommendationId), dbBo.getId())))
                    .stream().collect(Collectors.toMap(RecommendationLink::getObjectId, RecommendationLink::getDisplayType));
            List<Coupon> primaryList = new ArrayList<>();
            List<Coupon> secondaryList = new ArrayList<>();
            for (Coupon coupon : couponList) {
                Integer displayType = mapDisplayType.get(coupon.getId());
                if (displayType != null) {
                    coupon.setDisplayType(displayType);
                    if (Coupon.DISPLAY_TYPE_LEFT.equals(displayType)) {
                        primaryList.add(coupon);
                    } else {
                        secondaryList.add(coupon);
                    }
                }
            }
            dbBo.setPrimaryList(primaryList);
            dbBo.setSecondaryList(secondaryList);
        } else if (Recommendation.TYPE_BRAND.equals(dbBo.getType())) {
            dbBo.setBrandList(brandDao.findByRecommendationId(dbBo.getId()));
        }
        dbBo.setObjectLink(objectLinkDao.getObjectLink(ObjectLink.OBJ_CLASS_RECOMMENDATION, dbBo.getId()).orElse(null));
        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(key, MetaConfig.TYPE_RECOMMENDATION)));
        dbMetaConfigOptional.ifPresent(dbBo::setMetaConfig);
        return dbBo;
    }

    @Override
    public List<Recommendation> findByTerm(String term) {
        return this.recommendationDao.findByTerm(term);
    }

    @Override
    public List<Recommendation> findWithExtends(Recommendation bo) {
        List<Recommendation> recommendationList = recommendationDao.findWithExtends(bo.getStatus() != null ? bo.getStatus() : 1,
                bo.getIsTrash() != null ? bo.getIsTrash() : false);
        List<Integer> allComboId = productComboDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(ProductCombo._isTrash), Boolean.FALSE),
                builder.equal(root.get(ProductCombo._status), ProductCombo.STATUS_ACTIVE)))
                .stream().map(ProductCombo::getId).collect(Collectors.toList());
        List<Integer> allCategoryId = productCategoryDao.findAll((root, query, builder) ->
                builder.equal(root.get(ProductCategory._isTrash), Boolean.FALSE))
                .stream().map(ProductCategory::getId).collect(Collectors.toList());
        List<Integer> allProductId = productDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(Product._isTrash), Boolean.FALSE),
                builder.equal(root.get(Product._type), Product.TYPE_SELL),
                builder.equal(root.get(Product._status), Product.STATUS_ACTIVE)))
                .stream().map(Product::getId).collect(Collectors.toList());
        List<Integer> allCouponId = couponDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(Coupon._isTrash), Boolean.FALSE)))
                .stream().map(Coupon::getId).collect(Collectors.toList());
        List<Integer> allBrandId = brandDao.findAll((root, query, builder) ->
                builder.equal(root.get(Brand._isTrash), Boolean.FALSE))
                .stream().map(Brand::getId).collect(Collectors.toList());
        List<Integer> recommendationIdList = recommendationList.stream().map(Recommendation::getId).collect(Collectors.toList());
        List<RecommendationLink> recommendationLinks = recommendationLinkDao.findAll((root, query, builder) ->
                root.get(RecommendationLink._recommendationId).in(recommendationIdList));
        for (Recommendation recommendation : recommendationList) {
            String type = recommendation.getType();
            List<RecommendationLink> links = recommendationLinks.stream()
                    .filter(link -> link.getRecommendationId().equals(recommendation.getId())).collect(Collectors.toList());
            int itemCount = 0;
            for (RecommendationLink recommendationLink : links) {
                Integer objectId = recommendationLink.getObjectId();
                if (Recommendation.TYPE_PRODUCT.equals(type)) {
                    if (allProductId.contains(objectId)) {
                        itemCount++;
                    }
                } else if (Recommendation.TYPE_COMBO.equals(type)) {
                    if (allComboId.contains(objectId)) {
                        itemCount++;
                    }
                } else if (Recommendation.TYPE_CATEGORY.equals(type)) {
                    if (allCategoryId.contains(objectId)) {
                        itemCount++;
                    }
                } else if (Recommendation.TYPE_COUPON.equals(type)) {
                    if (allCouponId.contains(objectId)) {
                        itemCount++;
                    }
                } else if (Recommendation.TYPE_BRAND.equals(type)) {
                    if (allBrandId.contains(objectId)) {
                        itemCount++;
                    }
                }
            }
            recommendation.setItemCount(itemCount);
        }
        return recommendationList;

    }

    private void addProducts(Map<Integer, ProductCategory> productCategoryMap) {
        productCategoryMap.forEach((categoryId, productCategory) -> {
            List<Integer> categoryIdList = this.findFlatChildren(categoryId).stream().map(ProductCategory::getId).collect(Collectors.toList());
            categoryIdList.add(categoryId);
            if (categoryIdList.size() > 0) {
                List<Product> productPage = this.productDao.findByFilter(null, categoryIdList, PageRequest.of(0, Recommendation.PAGE_SIZE_DEFAULT))
                        .getContent().stream().filter(product -> product.getAmount() > 0).collect(Collectors.toList());
//                setReviewSummary(productPage);
                if (productCategory.getProductList() == null) {
                    productCategory.setProductList(new ArrayList<>());
                }
                productCategory.getProductList().addAll(productPage);
            }
        });
    }

    private List<ProductCategory> findFlatChildren(Integer productCategoryId) {
        List<ProductCategory> productCategoryList = this.productCategoryDao.findAll()
                .stream().filter(item -> item.getIsTrash().equals(Boolean.FALSE)).collect(Collectors.toList());
        ProductCategory productCategory = new ProductCategory();
        productCategory.setId(productCategoryId);
        List<ProductCategory> result = new ArrayList<>();
        this._buildFlatChildren(productCategory, productCategoryList, result);
        return result;
    }

    /**
     * @param parent:                     Danh mục cha
     * @param productCategoryItemAllList: Tất cả danh mục
     * @param outputList:                 Danh sách trả về
     */
    private void _buildFlatChildren(ProductCategory parent, List<ProductCategory> productCategoryItemAllList, List<ProductCategory> outputList) {
        List<ProductCategory> children = productCategoryItemAllList.stream().filter(item -> item.getParentId() != null && item.getParentId().equals(parent.getId())).collect(Collectors.toList());
        if (children.size() > 0) {
            for (ProductCategory item : children) {
                outputList.add(item);
                _buildFlatChildren(item, productCategoryItemAllList, outputList);
            }
        }
    }

    @Override
    public List<Recommendation> findRecommendHasObjectLink() {
        List<ObjectLink> objectLinks = objectLinkDao.findAll((root, query, builder) -> builder.and(
                builder.equal(root.get(ObjectLink._objectClass), ObjectLink.OBJ_CLASS_RECOMMENDATION))
        );
        if (objectLinks.size() > 0) {
            List<Integer> recommendationIdList = objectLinks.stream().map(ObjectLink::getObjectClassId).collect(Collectors.toList());
            return recommendationDao.findAll((root, query, builder) -> builder.and(
                    (root.get(Recommendation._id).in(recommendationIdList)),
                    builder.equal(root.get(Recommendation._isTrash), Boolean.FALSE))
            );
        }
        return null;
    }

    @Override
    public Recommendation doInsert(Recommendation bo) {
        this.validateData(bo);
        bo.setPriority(1);
        Recommendation dbBo = super.doInsert(bo);
        bo.setId(dbBo.getId());

        this.doAddObject(bo);
        objectLinkDao.customUpdate(ObjectLink.OBJ_CLASS_RECOMMENDATION, dbBo.getId(), User.getContextId(), bo.getObjectLink());
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setRecommendationId(bo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_RECOMMENDATION);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        return dbBo;
    }

    @Override
    public Recommendation doUpdate(Recommendation bo) {
        this.validateData(bo);
        Recommendation dbBo = super.get(bo.getId());
        bo.setType(dbBo.getType());
        bo.setPriority(dbBo.getPriority());
        Recommendation result = super.doUpdate(bo);
        objectLinkDao.customUpdate(ObjectLink.OBJ_CLASS_RECOMMENDATION, dbBo.getId(), User.getContextId(), bo.getObjectLink());
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(dbBo.getId(), MetaConfig.TYPE_RECOMMENDATION, User.getContextId(), bo.getMetaConfig());
        }
        return result;
    }

    private void validateData(Recommendation bo) {
        if (StringUtils.isEmpty(bo.getName()) || bo.getObjectLink() == null) {
            throw new HebelaException("action.invalid");
        }
        String objectType = bo.getObjectLink().getObjectType();
        if (StringUtils.isEmpty(objectType)) {
            throw new HebelaException("objectLink.objectId.is.null");
        } else {
            if (!ObjectLink.TYPE_CUSTOM_LINK.equals(objectType) && !ObjectLink.TYPE_ALL_BRAND.equals(objectType)
                    && !ObjectLink.TYPE_ALL_COUPON.equals(objectType) && !ObjectLink.TYPE_RECOMMENDATION.equals(objectType)
                    && !ObjectLink.TYPE_CART.equals(objectType)) {
                if (bo.getObjectLink().getObjectId() == null) {
                    throw new HebelaException("objectLink.objectId.is.null");
                }
            }
        }
    }

    @Override
    public Recommendation doPriorityObject(Recommendation bo) {
        Recommendation dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getObjectList() == null || bo.getObjectList().size() == 0) {
            throw new HebelaException("action.invalid");
        }

        String objectClass = ObjectLink.getObjectClass(dbBo.getType());
        List<RecommendationLink> dbRecommendationLinks = getRecommendationLinkList(dbBo.getId(), objectClass);
        Map<Integer, Integer> priorityMap = bo.getObjectList().stream().collect(Collectors.toMap(ObjectLink::getObjectId, ObjectLink::getPriority));

        for (RecommendationLink item : dbRecommendationLinks) {
            Integer priority = priorityMap.get(item.getObjectId());
            if (priority != null) {
                item.setPriority(priority);
                item.setModifiedBy(User.getContextId());
            }
        }
        this.recommendationLinkDao.saveAll(dbRecommendationLinks);
        return dbBo;
    }

    @Override
    public Recommendation doAddObject(Recommendation bo) {
        Recommendation dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash()) || bo.getObjectList() == null || bo.getObjectList().size() == 0) {
            throw new HebelaException("action.invalid");
        }
        List<RecommendationLink> insertList = new ArrayList<>();
        String objectClass = ObjectLink.getObjectClass(dbBo.getType());
        for (int i = 0; i < bo.getObjectList().size(); i++) {
            ObjectLink item = bo.getObjectList().get(i);
            RecommendationLink recommendationLink = new RecommendationLink();
            recommendationLink.setRecommendationId(bo.getId());
            recommendationLink.setObjectId(item.getObjectId());
            recommendationLink.setObjectClass(objectClass);
            recommendationLink.setCreatedBy(User.getContextId());
            if (ObjectLink.OBJ_CLASS_COUPON.equals(objectClass)) {
                recommendationLink.setDisplayType(item.getDisplayType() != null ? item.getDisplayType() : Coupon.DISPLAY_TYPE_LEFT);
            }
            recommendationLink.setPriority(i + 1);
            insertList.add(recommendationLink);
        }
        updateObjectOfRecommendation(dbBo.getId(), objectClass, insertList);
        return dbBo;
    }

    private void updateObjectOfRecommendation(Integer recommendationId, String objectClass, List<RecommendationLink> insertList) {
        List<RecommendationLink> dbRecommendationLinks = getRecommendationLinkList(recommendationId, objectClass);
        // check số lượng liên kết với loại = coupon và displayType
        if (ObjectLink.OBJ_CLASS_COUPON.equals(objectClass)) {
            this.validateCouponList(dbRecommendationLinks, insertList);
        }
        List<Integer> dbObjectIdList = dbRecommendationLinks.stream().map(RecommendationLink::getObjectId).collect(Collectors.toList());
        for (RecommendationLink item : insertList) {
            if (!dbObjectIdList.contains(item.getObjectId())) {
                if (dbRecommendationLinks.size() > 0) {
                    if (ObjectLink.OBJ_CLASS_COUPON.equals(objectClass)) {
                        int priorityMax = dbRecommendationLinks.stream()
                                .filter(recommendLink -> item.getDisplayType().equals(recommendLink.getDisplayType()))
                                .mapToInt(RecommendationLinkBase::getPriority)
                                .summaryStatistics().getMax();
                        priorityMax = Math.max(priorityMax, 0);
                        if (priorityMax > 0) {
                            item.setPriority(item.getPriority() + priorityMax);
                        }
                    } else {
                        item.setPriority(dbRecommendationLinks.size() + 1);
                    }
                }
                this.recommendationLinkDao.save(item);
            }
        }
    }

    private void validateCouponList(List<RecommendationLink> dbRecommendationLinks, List<RecommendationLink> insertList) {
        if (dbRecommendationLinks.size() == 0) {
            long countCouponLeft = insertList.stream()
                    .filter(recommendLink -> Coupon.DISPLAY_TYPE_LEFT.equals(recommendLink.getDisplayType())).count();
            long countCouponRight = insertList.stream()
                    .filter(recommendLink -> Coupon.DISPLAY_TYPE_RIGHT.equals(recommendLink.getDisplayType())).count();
            if (countCouponLeft > 5) {
                throw new HebelaException("recommendation.coupon.display.left.max.invalid");
            } else if (countCouponRight > 12) {
                throw new HebelaException("recommendation.coupon.display.right.max.invalid");
            }
        } else {
            Integer displayType = insertList.get(0).getDisplayType();
            long countCoupon = dbRecommendationLinks.stream()
                    .filter(recommendLink -> displayType.equals(recommendLink.getDisplayType())).count();
            long sumCouponList = insertList.size() + countCoupon;
            if (Coupon.DISPLAY_TYPE_LEFT.equals(displayType) && sumCouponList > 5) {
                throw new HebelaException("recommendation.coupon.display.left.max.invalid");
            } else if (Coupon.DISPLAY_TYPE_RIGHT.equals(displayType) && sumCouponList > 12) {
                throw new HebelaException("recommendation.coupon.display.right.max.invalid");
            }
        }
    }

    private List<RecommendationLink> getRecommendationLinkList(Integer recommendationId, String objectClass) {
        RecommendationLink example = new RecommendationLink();
        example.setRecommendationId(recommendationId);
        if (objectClass != null) {
            example.setObjectClass(objectClass);
        }
        return this.recommendationLinkDao.findAll(Example.of(example));
    }

    @Override
    public Recommendation doRemoveObject(Recommendation bo) {
        String objectClass = ObjectLink.getObjectClass(bo.getType());
        if (objectClass == null || bo.getId() == null || bo.getObjectId() == null) {
            throw new HebelaException("action.invalid");
        }
        Recommendation dbBo = super.get(bo.getId());
        if (Boolean.TRUE.equals(dbBo.getIsTrash())) {
            throw new HebelaException("action.invalid");
        }
        RecommendationLink recommendationLink = new RecommendationLink();
        recommendationLink.setRecommendationId(bo.getId());
        recommendationLink.setObjectId(bo.getObjectId());
        recommendationLink.setObjectClass(objectClass);
        List<RecommendationLink> recommendationLinks = recommendationLinkDao.findAll(Example.of(recommendationLink));
        if (recommendationLinks.size() > 0) {
            recommendationLinkDao.deleteAll(recommendationLinks);
        }
        return dbBo;
    }
}

