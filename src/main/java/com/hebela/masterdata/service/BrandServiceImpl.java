package com.hebela.masterdata.service;

import com.hebela.core.AbstractStatusService;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Brand;
import com.hebela.masterdata.bo.MetaConfig;
import com.hebela.masterdata.dao.BrandDao;
import com.hebela.masterdata.dao.MetaConfigDao;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl extends AbstractStatusService<Brand> implements BrandService {
    private final BrandDao brandDao;
    private final MetaConfigDao metaConfigDao;

    public BrandServiceImpl(BrandDao brandDao, MetaConfigDao metaConfigDao) {
        this.brandDao = brandDao;
        this.metaConfigDao = metaConfigDao;
    }

    @Override
    public Brand doInsert(Brand bo) {
        bo.setPriority(1);
        Brand dbBo = super.doInsert(bo);
        /*Meta SEO*/
        if (!MetaConfig.isClearMetaConfig(bo.getMetaConfig())) {
            bo.getMetaConfig().setBrandId(dbBo.getId());
            bo.getMetaConfig().setType(MetaConfig.TYPE_BRAND);
            bo.getMetaConfig().setCreatedBy(User.getContextId());
            metaConfigDao.save(bo.getMetaConfig());
        }
        return dbBo;
    }

    @Override
    public List<Brand> findWithExtends(Brand brand) {
        return brandDao.findWithExtends(
                brand.getStatus() != null ? brand.getStatus() : 1,
                brand.getIsTrash() != null ? brand.getIsTrash() : false);
    }

    @Override
    public List<Brand> find(Brand bo) {
        return brandDao.findAll(Example.of(bo), Sort.by(Sort.Order.asc(Brand._name)));
    }

    @Override
    public Brand get(Integer key) {
        Brand dbBo = super.get(key);
        Optional<MetaConfig> dbMetaConfigOptional = metaConfigDao.findOne(Example.of(
                MetaConfig.getMetaConfigExample(key, MetaConfig.TYPE_BRAND)));
        dbMetaConfigOptional.ifPresent(dbBo::setMetaConfig);
        return dbBo;
    }

    @Override
    public Brand doUpdate(Brand bo) {
        if (bo.getMetaConfig() != null) {
            metaConfigDao.customUpdate(bo.getId(), MetaConfig.TYPE_BRAND, User.getContextId(), bo.getMetaConfig());
        }
        return super.doUpdate(bo);
    }

}

