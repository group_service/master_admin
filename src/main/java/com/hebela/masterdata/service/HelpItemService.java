package com.hebela.masterdata.service;

import com.hebela.core.StatusService;
import com.hebela.masterdata.bo.HelpItem;

public interface HelpItemService extends StatusService<HelpItem> {

}

