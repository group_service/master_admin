package com.hebela.masterdata.util;

import com.hebela.masterdata.bo.Order;
import com.hebela.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderUtil {

    public static String getRandomCode() {
        Date date = new Date();
        return StringUtils.getRandomCode(2, Order.CHAR_CODE) +
                new SimpleDateFormat("dd").format(date) +
                StringUtils.getRandomCode(1, Order.CHAR_CODE) +
                new SimpleDateFormat("MM").format(date) +
                StringUtils.getRandomCode(1, Order.CHAR_CODE) +
                new SimpleDateFormat("yy").format(date) +
                StringUtils.getRandomCode(2, Order.CHAR_CODE);
    }

}
