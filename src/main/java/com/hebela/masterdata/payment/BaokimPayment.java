package com.hebela.masterdata.payment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.hebela.core.HebelaException;
import com.hebela.core.HebelaLogger;
import com.hebela.masterdata.bo.OrderRoot;
import com.hebela.masterdata.bo.extend.OnlinePayment;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

@Component
public class BaokimPayment {
    @Value("${baokim.endpoint}")
    private String endpoint;
    @Value("${baokim.api.sendOrder}")
    private String apiSendOrder;
    @Value("${baokim.api.detailOrder}")
    private String apiDetailOrder;
    @Value("${baokim.api.cancelOrder}")
    private String apiCancelOrder;
    @Value("${baokim.api.refund}")
    private String apiRefund;
    @Value("${baokim.urlSuccess}")
    private String urlSuccess;
    @Value("${baokim.urlFail}")
    private String urlFail;
    @Value("${baokim.webhooks}")
    private String webhooks;
    @Value("${baokim.apiKey}")
    private String apiKey;
    @Value("${baokim.apiSecret}")
    private String apiSecret;
    @Value("${baokim.merchantId}")
    private Integer merchantId;
    @Value("${baokim.tokenExpired}")
    private Long tokenExpired;
    @Value("${baokim.customerEmail}")
    private String customerEmail;
    @Value("${baokim.customerPhone}")
    private String customerPhone;
    @Value("${baokim.customerAddress}")
    private String customerAddress;
    @Value("${spring.profiles.active}")
    protected String env;

    public BaokimPayment() {
    }

    public BaokimPayment(String apiKey, String apiSecret, Long tokenExpired) {
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
        this.tokenExpired = tokenExpired;
    }

    private String genJwtToken() {
        Instant now = Instant.now();
        Date expirationDate = Date.from(now.plus(tokenExpired, ChronoUnit.MINUTES));
        SecretKey key = Keys.hmacShaKeyFor(apiSecret.getBytes(StandardCharsets.UTF_8));
        String jws = Jwts.builder()
                .setHeaderParam("alg", "HS256")
                .setHeaderParam("typ", "JWT")
                .setIssuedAt(Date.from(now)) //iat
                .setId(String.valueOf(UUID.randomUUID())) //jti
                .setIssuer(apiKey)//iss
                .setNotBefore(Date.from(now)) //nbf
                .setExpiration(expirationDate) //exp
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
        return jws;
    }

    public OnlinePayment sendOrder(OrderRoot orderRoot) {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        JsonObject payload = new JsonObject();
        String mrcOrderId = orderRoot.getMrcOrderId();
        if (!env.equals("prod")) {
            mrcOrderId = env + "-" + mrcOrderId;
        }
        payload.addProperty("mrc_order_id", mrcOrderId);
        payload.addProperty("total_amount", orderRoot.getTotal());
        payload.addProperty("description", "Thanh toán cho đơn hàng " + orderRoot.getOrderRootCode());
        payload.addProperty("url_success", urlSuccess + "/" + orderRoot.getOrderList().get(0).getCode());
        payload.addProperty("merchant_id", merchantId);
        payload.addProperty("url_detail", urlFail);
        payload.addProperty("webhooks", webhooks);
        payload.addProperty("customer_email", customerEmail);
        payload.addProperty("customer_phone", customerPhone);
        payload.addProperty("customer_name", orderRoot.getOrderList().get(0).getAccountAddress().getFullname());
        payload.addProperty("customer_address", customerAddress);

        okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, payload.toString());
        Request baokimRequest = new Request.Builder()
                .url(endpoint + apiSendOrder + "?jwt=" + genJwtToken())
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        try {
            String response = client.newCall(baokimRequest).execute().body().string();
            HebelaLogger.info(response);
            Gson gson = new Gson();
            BaokimResponse baokimResponse = gson.fromJson(response, BaokimResponse.class);
            return OnlinePayment.builder()
                    .status(OnlinePayment.STATUS_SUCCESS)
                    .paymentUrl(baokimResponse.getData().getPayment_url())
                    .build();
        } catch (Exception e) {
            HebelaLogger.warn(e);
            return OnlinePayment.builder()
                    .status(OnlinePayment.STATUS_FAILED)
                    .message("Không thể kết nối với cổng thanh toán")
                    .build();
        }
    }


    public BaokimResponse refund(OrderRoot orderRoot) {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        JsonObject payload = new JsonObject();
        payload.addProperty("txn_id", orderRoot.getTxnId());
        payload.addProperty("description", "Hoàn tiền cho đơn hàng " + orderRoot.getOrderRootCode());

        okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, payload.toString());
        String url = endpoint + apiRefund;
        HebelaLogger.info("refund with url" + url);
        Request baokimRequest = new Request.Builder()
                .url(url + "?jwt=" + genJwtToken())
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        okhttp3.Response response = null;
        try {
            response = client.newCall(baokimRequest).execute();
            Gson gson = new Gson();
            if (response.code() == 200) {
                return gson.fromJson(response.body().string(), BaokimResponse.class);
            } else {
                throw new HebelaException("baokim.refund.failed");
            }
        } catch (Exception e) {
            throw new HebelaException("baokim.refund.failed");
        }
    }

    public static void main(String[] args) {
        BaokimPayment baokimPayment = new BaokimPayment("","", 60L);
        System.out.println(baokimPayment.genJwtToken());
    }
}
