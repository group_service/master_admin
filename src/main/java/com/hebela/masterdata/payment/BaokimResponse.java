package com.hebela.masterdata.payment;



import com.google.gson.Gson;

import java.util.List;

public class BaokimResponse {
    private Integer code;
    private List<String> message;
    private Integer count;
    private BaokimDataResponse data;

    public BaokimResponse() {
    }

    public BaokimResponse(Integer code, List<String> message, Integer count, BaokimDataResponse data) {
        this.code = code;
        this.message = message;
        this.count = count;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BaokimDataResponse getData() {
        return data;
    }

    public void setData(String baokimDataString) {
        BaokimDataResponse baokimDataResponse = new Gson().fromJson(baokimDataString, BaokimDataResponse.class);
        this.data = baokimDataResponse;
    }
}
