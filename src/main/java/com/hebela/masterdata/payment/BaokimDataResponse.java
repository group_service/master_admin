package com.hebela.masterdata.payment;

public class BaokimDataResponse {
    private Integer order_id;
    private String redirect_url;
    private String payment_url;

    public BaokimDataResponse() {
    }

    public BaokimDataResponse(Integer order_id, String redirect_url, String payment_url) {
        this.order_id = order_id;
        this.redirect_url = redirect_url;
        this.payment_url = payment_url;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    public String getPayment_url() {
        return payment_url;
    }

    public void setPayment_url(String payment_url) {
        this.payment_url = payment_url;
    }
}
