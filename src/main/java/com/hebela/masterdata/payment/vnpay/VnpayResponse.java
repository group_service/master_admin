package com.hebela.masterdata.payment.vnpay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VnpayResponse {
    private String RspCode = "00";
    private String Message = "Confirm Success";
}
