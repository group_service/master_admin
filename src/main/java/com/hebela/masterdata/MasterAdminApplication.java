package com.hebela.masterdata;

import com.hebela.SessionRedisConfig;
import com.hebela.Swagger2Config;
import com.hebela.core.HebelaJpaRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication(scanBasePackages = "com.hebela")
@EnableJpaRepositories(basePackages = "com.hebela", repositoryBaseClass = HebelaJpaRepository.class)
@Import({SessionRedisConfig.class, Swagger2Config.class, KafkaConfig.class})
@ImportResource({"classpath*:spring-master-admin.xml"})
@EnableWebSecurity
public class MasterAdminApplication extends SpringBootServletInitializer {
    private static ApplicationContext applicationContext;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MasterAdminApplication.class);
    }

    public static void main(String[] args) {
        applicationContext = SpringApplication.run(MasterAdminApplication.class, args);
    }

    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }
}
