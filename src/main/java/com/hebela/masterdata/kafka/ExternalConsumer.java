package com.hebela.masterdata.kafka;

import com.hebela.core.HebelaLogger;
import com.hebela.kafka.KafkaConsumer;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.dao.ErpSyncLogDao;
import com.hebela.masterdata.service.OrderService;
import com.hebela.masterdata.service.ProductComboService;
import com.hebela.masterdata.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ExternalConsumer extends KafkaConsumer {
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductComboService productComboService;
    @Autowired
    private ErpSyncLogDao erpSyncLogDao;

    @KafkaListener(topics = "Ext-Order-${spring.profiles.active}", concurrency = "${kafka.product-consumer-concurrency}", groupId = "odoo")
    public void odooOrder(@Payload String orderJson, @Headers Map<String, ?> headers) throws Exception {
        // đã giao 16
        if (headers.get(ACTION_KEY).equals("odoo")) {
            Order order = objectMapper.readValue(orderJson, Order.class);
            orderService.doSyncStatus(order);
            if (!"prod".equals(env)) {
                ErpSyncLog erpSyncLog = new ErpSyncLog();
                erpSyncLog.setTopic("Ext-Order-" + env);
                erpSyncLog.setType(ErpSyncLog.SYNC_STATUS_ORDER);
                erpSyncLog.setObjJson(orderJson);
                erpSyncLog.setOrderCode(order.getCode());
                erpSyncLogDao.save(erpSyncLog);
            }
        }
    }

    @KafkaListener(topics = "Ext-Product-${spring.profiles.active}", concurrency = "${kafka.product-consumer-concurrency}", groupId = "${spring.profiles.active}")
    public void odooProduct(@Payload String productJson, @Headers Map<String, ?> headers) throws Exception {
        HebelaLogger.info(productJson);
        consumeWithContext(productJson, headers, Product.class, o -> {
            if (o.getSku().startsWith(ProductCombo.TYPE_PACKAGE_PREFIX)) {
                ProductCombo comboPackage = new ProductCombo();
                comboPackage.setSku(o.getSku());
                comboPackage.setAmount(o.getAmount());
                productComboService.doSyncAmount(comboPackage);
            } else {
                if (!"prod".equals(env)) {
                    ErpSyncLog erpSyncLog = new ErpSyncLog();
                    erpSyncLog.setTopic("Ext-Product-" + env);
                    erpSyncLog.setType(ErpSyncLog.SYNC_PRODUCT_AMOUNT);
                    erpSyncLog.setObjJson(productJson);
                    erpSyncLogDao.save(erpSyncLog);
                }
                productService.doSyncAmount(o);
            }
        });
    }

    @KafkaListener(topics = "Ext-OrderRoot-${spring.profiles.active}", concurrency = "${kafka.product-consumer-concurrency}", groupId = "paymentOrderRoot")
    public void paymentOrderRoot(@Payload String orderRootJson, @Headers Map<String, ?> headers) throws Exception {
        consumeWithContext(orderRootJson, headers, OrderRoot.class, orderService::doUpdatePayment);
    }
}
