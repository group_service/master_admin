package com.hebela.masterdata.obj;

import com.hebela.hbm.BaseJsonType;
import com.hebela.masterdata.bo.extend.Seller;

public class JsonOrderSellerType extends BaseJsonType {
    @Override
    protected boolean equalsObject(Object x, Object y) {
        return ((Seller) x).equalsData(y);
    }

    @Override
    public Class returnedClass() {
        return Seller.class;
    }
}
