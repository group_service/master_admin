package com.hebela.masterdata.obj;

public class RoleModel {
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_ROOT = "ROOT";
    public static final String ROLE_PUBLISHER_ADMIN = "PUBLISHER_ADMIN";
    public static final String ROLE_PUBLISHER_PAGE = "PUBLISHER_PAGE";
}
