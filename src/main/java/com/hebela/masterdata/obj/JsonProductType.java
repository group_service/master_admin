package com.hebela.masterdata.obj;

import com.hebela.hbm.BaseJsonType;
import com.hebela.masterdata.bo.Product;

public class JsonProductType extends BaseJsonType {
    @Override
    protected boolean equalsObject(Object x, Object y) {
        return ((Product) x).equalsData(y);
    }

    @Override
    public Class returnedClass() {
        return Product.class;
    }
}
