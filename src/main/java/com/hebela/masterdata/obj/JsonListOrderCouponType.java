package com.hebela.masterdata.obj;

import com.google.gson.reflect.TypeToken;
import com.hebela.hbm.JsonListType;
import com.hebela.masterdata.bo.Coupon;
import com.hebela.util.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class JsonListOrderCouponType extends JsonListType {

    @Override
    protected Object fromJson(String jsonString) {
        LinkedList<Coupon> couponList = new LinkedList<>();
        for (Coupon coupon : (List<Coupon>) StringUtils.fromJson(jsonString, this.returnedType())) {
            couponList.add(coupon);
        }
        return couponList;
    }

    @Override
    protected String toJson(Object value) {
        LinkedList<Coupon> couponList = new LinkedList<>();
        for (Coupon coupon : (List<Coupon>) value) {
            couponList.add(coupon);
        }
        return StringUtils.toJson(couponList);
    }

    @Override
    public Type returnedType() {
        return new TypeToken<Collection<Coupon>>() {
        }.getType();
    }

    @Override
    protected boolean equalsElement(Object x, Object y) {
        return x.equals(y);
    }
}
