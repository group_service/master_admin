package com.hebela.masterdata.obj;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

@Setter
@Getter
@NoArgsConstructor
public class ComboFilterRequest extends HebelaPage {
    private Integer productId;
    private Integer categoryId;
    private String sortBy;
    private String sortDirection;
}
