package com.hebela.masterdata.obj;

import com.hebela.masterdata.bo.Attribute;
import com.hebela.masterdata.bo.AttributeData;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class AttributeModel {
    private Integer productId;
    private String attributeName;
    private String attributeValue;

    public AttributeModel(){

    }
    /*Lấy danh sách thuộc tính tại trang chi tiết sản phẩm*/
    public AttributeModel(Integer productId, String attributeName, AttributeData attributeData) {
        this.productId = productId;
        this.attributeName = attributeName;
        if (Attribute.TYPE_TEXT.equals(attributeData.getType())) {
            this.attributeValue = attributeData.getValueText();
        } else if (Attribute.TYPE_NUMBER.equals(attributeData.getType())) {
            this.attributeValue = attributeData.getValueNumber() + " " + attributeData.getUnit();
        }
    }
}


