package com.hebela.masterdata.obj;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class RecommendationFilterRequest extends HebelaPage {
    private Integer recommendationId;
    private Integer productCategoryId;
    private String sortBy;
    private String sortDirection;
}
