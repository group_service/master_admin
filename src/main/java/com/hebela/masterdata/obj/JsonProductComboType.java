package com.hebela.masterdata.obj;

import com.hebela.hbm.BaseJsonType;
import com.hebela.masterdata.bo.ProductCombo;

public class JsonProductComboType extends BaseJsonType {
    @Override
    protected boolean equalsObject(Object x, Object y) {
        return ((ProductCombo) x).equalsData(y);
    }

    @Override
    public Class returnedClass() {
        return ProductCombo.class;
    }
}
