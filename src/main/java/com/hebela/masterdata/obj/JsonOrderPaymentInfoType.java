package com.hebela.masterdata.obj;

import com.hebela.hbm.BaseJsonType;
import com.hebela.masterdata.bo.Payment;

public class JsonOrderPaymentInfoType extends BaseJsonType {
    @Override
    protected boolean equalsObject(Object x, Object y) {
        return ((Payment) x).equalsData(y);
    }

    @Override
    public Class returnedClass() {
        return Payment.class;
    }
}
