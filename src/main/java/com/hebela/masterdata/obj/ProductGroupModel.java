package com.hebela.masterdata.obj;

import com.hebela.masterdata.bo.Attribute;
import com.hebela.masterdata.bo.AttributeData;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ProductGroupModel {
    private Integer productId;
    private String slug;
    private String productName;
    private String productImageThumbnail;
    private Integer priceSale;
    private String attributeValue;

    public ProductGroupModel(Integer productId, String slug, String productName, String productImageThumbnail, Number priceSale, AttributeData attributeData) {
        this.productId = productId;
        this.slug = slug;
        this.productName = productName;
        this.productImageThumbnail = productImageThumbnail;
        this.priceSale = priceSale.intValue();
        if (Attribute.TYPE_TEXT.equals(attributeData.getType())) {
            this.attributeValue = attributeData.getValueText();
        } else if (Attribute.TYPE_NUMBER.equals(attributeData.getType())) {
            this.attributeValue = attributeData.getValueNumber() + " " + attributeData.getUnit();
        }
    }
}


