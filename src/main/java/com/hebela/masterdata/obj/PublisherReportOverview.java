package com.hebela.masterdata.obj;

import com.hebela.masterdata.bo.Publisher;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PublisherReportOverview {
    private Publisher publisher;
    private long countClick;
    private long countOrderTotal;
    private long moneyTotal;
    private long countOrderInprogress;
    private long moneyInprogress;
    private long countOrderCompleted;
    private long moneyCompleted;
    private long countOrderCancel;
    private long moneyCancel;

}
