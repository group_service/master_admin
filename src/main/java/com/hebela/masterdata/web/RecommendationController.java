package com.hebela.masterdata.web;

import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.Recommendation;
import com.hebela.masterdata.service.ProductCategoryService;
import com.hebela.masterdata.service.ProductComboService;
import com.hebela.masterdata.service.ProductService;
import com.hebela.masterdata.service.RecommendationService;
import com.hebela.web.AbstractStatusController;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/recommendation")
@ResponseBody
public class RecommendationController extends AbstractStatusController<Recommendation> {
    private final RecommendationService recommendationService;
    private final ProductService productService;
    private final ProductComboService productComboService;
    private final ProductCategoryService productCategoryService;

    public RecommendationController(RecommendationService recommendationService, ProductService productService, ProductComboService productComboService, ProductCategoryService productCategoryService) {
        this.recommendationService = recommendationService;
        this.productService = productService;
        this.productComboService = productComboService;
        this.productCategoryService = productCategoryService;
    }

    @PostMapping("/addObject")
    Recommendation addObject(@RequestBody Recommendation bo) {
        return this.recommendationService.doAddObject(bo);
    }

    @PostMapping("/removeObject")
    Recommendation removeObject(@RequestBody Recommendation bo) {
        return this.recommendationService.doRemoveObject(bo);
    }

    @PostMapping("/priorityObject")
    Recommendation priorityObject(@RequestBody Recommendation bo) {
        return this.recommendationService.doPriorityObject(bo);
    }

    @PostMapping({"/findWithExtends"})
    public List<Recommendation> findWithExtends(@RequestBody @Nullable Recommendation bo) {
        return this.recommendationService.findWithExtends(bo);
    }

    @PutMapping("/priority")
    public List<Recommendation> priority(@RequestBody List<Recommendation> recommendationList) {
        return recommendationService.doPriority(recommendationList);
    }

    @PostMapping("/findItemLink")
    public Object findItemLink(@RequestParam String type, @RequestParam String term) {
        if (Recommendation.TYPE_PRODUCT.equals(type)) {
            return this.productService.findByTerm(term);
        } else if (Recommendation.TYPE_COMBO.equals(type)) {
            return this.productComboService.findByTerm(term);
        } else {
            return this.productCategoryService.findByTerm(term);
        }
    }

    @PostMapping("/findByTerm")
    public List<Recommendation> findByTerm(@RequestParam String term) {
        return this.recommendationService.findByTerm(term);
    }

    @PostMapping("/findRecommendHasObjectLink")
    public List<Recommendation> findRecommendHasObjectLink() {
        return this.recommendationService.findRecommendHasObjectLink();
    }
}

