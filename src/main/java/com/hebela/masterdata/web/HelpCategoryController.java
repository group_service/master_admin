package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.HelpCategory;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/help-category")
@ResponseBody
public class HelpCategoryController extends AbstractStatusController<HelpCategory> {

    @PutMapping("/priority")
    public List<HelpCategory> doPriority(@RequestBody List<HelpCategory> productCategories) {
        return this.service.doPriority(productCategories);
    }
}

