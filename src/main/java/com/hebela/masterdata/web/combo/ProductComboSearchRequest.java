package com.hebela.masterdata.web.combo;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProductComboSearchRequest extends HebelaPage {
    private Integer orgId;
    private String keyword;
    private Integer categoryId;
    private String sku;
    private String barcode;
    private Integer status;
    private Boolean isTrash = Boolean.FALSE;
    private Integer type;
}
