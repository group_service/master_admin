package com.hebela.masterdata.web.combo;

import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.service.ProductComboService;
import com.hebela.web.AbstractStatusController;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product-combo")
@ResponseBody
public class ProductComboController extends AbstractStatusController<ProductCombo> {

    private final ProductComboService productComboService;

    public ProductComboController(ProductComboService productComboService) {
        this.productComboService = productComboService;
    }

    @PostMapping("/activate")
    public ProductCombo activate(@RequestBody ProductCombo productCombo) {
        return this.productComboService.doActivate(productCombo);
    }

    @PostMapping("/deactivate")
    public ProductCombo deactivate(@RequestBody ProductCombo productCombo) {
        return this.productComboService.doDeactivate(productCombo);
    }

    @PostMapping("/addProduct")
    public ProductCombo addProduct(@RequestBody ProductCombo bo) {
        return this.productComboService.doAddProduct(bo);
    }

    @PostMapping("/removeProduct")
    public ProductCombo removeProduct(@RequestBody ProductCombo bo) {
        return this.productComboService.doRemoveProduct(bo);
    }

    @PostMapping("/findBySearch")
    public Page<ProductCombo> findBySearch(@RequestBody ProductComboSearchRequest model) {
        return this.productComboService.findBySearch(model);
    }

    @PostMapping("/findProductForCombo/{comboId}")
    public List<Product> findProductForCombo(@PathVariable Integer comboId, @RequestBody Product bo) {
        return this.productComboService.findProductForCombo(comboId, bo.getName());
    }

    @PostMapping("/findProductInCombo/{comboId}")
    public List<Product> findProductInCombo(@PathVariable Integer comboId) {
        return this.productComboService.findProductInCombo(comboId);
    }

    @PostMapping("/priority")
    public List<ProductCombo> priority(@RequestBody List<ProductCombo> productComboList) {
        return this.productComboService.doPriority(productComboList);
    }

    @PostMapping("/findByTerm")
    public List<ProductCombo> findByTerm(@RequestParam String term) {
        return this.productComboService.findByTerm(term);
    }
    @PostMapping("/updateQuantityInCombo")
    public ProductCombo updateQuantityInCombo(@RequestBody ProductCombo bo) {
        return productComboService.doUpdateQuantityInCombo(bo);
    }
}

