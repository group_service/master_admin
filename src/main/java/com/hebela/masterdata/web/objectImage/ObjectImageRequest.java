package com.hebela.masterdata.web.objectImage;

import com.hebela.masterdata.bo.ObjectImage;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ObjectImageRequest {
    private List<ObjectImage> objectImageList;
    private String objectClass;
    private Integer objectClassId;
}
