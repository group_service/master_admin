package com.hebela.masterdata.web.objectImage;

import com.hebela.masterdata.bo.ObjectImage;
import com.hebela.masterdata.service.ObjectImageService;
import com.hebela.web.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/object-image")
@ResponseBody
public class ObjectImageController extends AbstractController<ObjectImage> {
    @Autowired
    ObjectImageService objectImageService;

    @PostMapping(value = {"/promotionCategory/findObjectImage", "/promotionTimeFrame/findObjectImage"})
    public List<ObjectImage> findObjectImage(@RequestBody ObjectImageRequest bo) {
        return this.objectImageService.findObjectImage(null, bo.getObjectClass(), bo.getObjectClassId());
    }

    @PostMapping(value = {"/promotionCategory/addImage", "/promotionTimeFrame/addImage"})
    public List<ObjectImage> addImage(@RequestBody ObjectImageRequest bo) {
        return this.objectImageService.doAddObjectImage(bo.getObjectImageList(), bo.getObjectClass(), bo.getObjectClassId());
    }

    @PostMapping(value = {"/promotionCategory/updateImage", "/promotionTimeFrame/updateImage"})
    public List<ObjectImage> updateImage(@RequestBody ObjectImageRequest bo) {
        return this.objectImageService.doUpdateObjectImage(bo.getObjectImageList(), bo.getObjectClass(), bo.getObjectClassId());
    }

    @PostMapping(value = {"/promotionCategory/updatePriorityImage", "/promotionTimeFrame/updatePriorityImage"})
    public List<ObjectImage> updatePriorityImage(@RequestBody ObjectImageRequest bo) {
        return this.objectImageService.doUpdateObjectImagePriority(bo.getObjectImageList(), bo.getObjectClass(), bo.getObjectClassId());
    }

    @PostMapping(value = {"/promotionCategory/deleteImage", "/promotionTimeFrame/deleteImage"})
    public List<ObjectImage> deleteImage(@RequestBody ObjectImageRequest bo) {
        return this.objectImageService.doDeleteObjectImage(bo.getObjectImageList(), bo.getObjectClass(), bo.getObjectClassId());
    }
}

