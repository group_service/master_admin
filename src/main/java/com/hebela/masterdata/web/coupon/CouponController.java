package com.hebela.masterdata.web.coupon;

import com.hebela.masterdata.bo.Coupon;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.service.CouponService;
import com.hebela.web.AbstractStatusController;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/coupon")
@ResponseBody
public class CouponController extends AbstractStatusController<Coupon> {
    private final CouponService couponService;

    public CouponController(CouponService couponService) {
        this.couponService = couponService;
    }

    @PostMapping("/activate")
    Coupon activate(@RequestBody Coupon bo) {
        return this.couponService.doActivate(bo);
    }

    @PostMapping("/deactivate")
    Coupon deactivate(@RequestBody Coupon bo) {
        return this.couponService.doDeactivate(bo);
    }

    @PostMapping("/addProduct")
    Coupon addProduct(@RequestBody Coupon bo) {
        return this.couponService.doAddProduct(bo);
    }

    @PostMapping("/removeProduct")
    Coupon removeProduct(@RequestBody Coupon bo) {
        return this.couponService.doRemoveProduct(bo);
    }

    @PostMapping("/addProductGift")
    Coupon addProductGift(@RequestBody Coupon bo) {
        return this.couponService.doAddProductGift(bo);
    }

    @PostMapping("/removeProductGift")
    Coupon removeProductGift(@RequestBody Coupon bo) {
        return this.couponService.doRemoveProductGift(bo);
    }

    @PostMapping("/findBySearch")
    Page<Coupon> findBySearch(@RequestBody CouponSearchRequest bo) {
        return this.couponService.findBySearch(bo);
    }

    @PostMapping("/findBySearchCouponActive")
    Page<Coupon> findBySearchCouponActive(@RequestBody CouponSearchRequest bo) {
        return this.couponService.findBySearchCouponActive(bo);
    }

    @ApiOperation("Tìm tất cả quà tặng khả dụng của coupon")
    @PostMapping("/findGift")
    List<Product> findGift(@RequestParam Integer couponId) {
        return this.couponService.findGift(couponId);
    }
}

