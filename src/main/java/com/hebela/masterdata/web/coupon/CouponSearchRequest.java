package com.hebela.masterdata.web.coupon;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CouponSearchRequest extends HebelaPage {
    private String name;
    private String code;
    private Integer campaignId;
    private String type;
    private Integer status;
    private Boolean isTrash = Boolean.FALSE;
    private String productName;
    private String productSku;
    private String productNameGift;
    private String productSkuGift;
    private Integer statusTime;
}
