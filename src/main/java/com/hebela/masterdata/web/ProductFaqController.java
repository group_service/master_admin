package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.ProductFaq;
import com.hebela.masterdata.service.ProductFaqService;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product-faq")
@ResponseBody
public class ProductFaqController extends AbstractStatusController<ProductFaq> {
    private final ProductFaqService productFaqService;

    public ProductFaqController(ProductFaqService productFaqService) {
        this.productFaqService = productFaqService;
    }

    @PostMapping("/addProduct")
    ProductFaq addProduct(@RequestBody ProductFaq bo) {
        return this.productFaqService.doAddProduct(bo);
    }

    @PostMapping("/removeProduct")
    ProductFaq removeProduct(@RequestBody ProductFaq bo) {
        return this.productFaqService.doRemoveProduct(bo);
    }

    @PostMapping("/addCombo")
    ProductFaq addCombo(@RequestBody ProductFaq bo) {
        return this.productFaqService.doAddCombo(bo);
    }

    @PostMapping("/removeCombo")
    ProductFaq removeCombo(@RequestBody ProductFaq bo) {
        return this.productFaqService.doRemoveCombo(bo);
    }

    @PutMapping("/priority")
    public List<ProductFaq> priority(@RequestBody List<ProductFaq> productFaqList) {
        return productFaqService.doPriority(productFaqList);
    }

}

