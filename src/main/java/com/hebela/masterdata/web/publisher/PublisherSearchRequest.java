package com.hebela.masterdata.web.publisher;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class PublisherSearchRequest extends HebelaPage {
    private Integer publisherId;
    private String keyword;
    private Integer type;
    private Integer adminId;
    private String affCode;
    private Instant fromDate;
    private Instant toDate;
    private Integer status;
    private Boolean isTrash = Boolean.FALSE;

}