package com.hebela.masterdata.web.publisher;

import com.hebela.masterdata.bo.Publisher;
import com.hebela.masterdata.obj.PublisherReportOverview;
import com.hebela.masterdata.service.PublisherService;
import com.hebela.web.AbstractStatusController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/publisher")
@ResponseBody
public class PublisherController extends AbstractStatusController<Publisher> {
    @Autowired
    PublisherService publisherService;

    @PostMapping("/findPublisher")
    public Page<Publisher> findPublisher(@RequestBody PublisherSearchRequest model) {
        return this.publisherService.findPublisher(model);
    }

    @PostMapping("/getReportOverview")
    public PublisherReportOverview getReportOverview(@RequestBody PublisherSearchRequest model) {
        return this.publisherService.getReportOverview(model);
    }
}

