package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.BannerItem;
import com.hebela.masterdata.service.BannerItemService;
import com.hebela.web.AbstractStatusController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/banner-item")
@ResponseBody
public class BannerItemController extends AbstractStatusController<BannerItem> {
    @Autowired
    BannerItemService bannerItemService;

    @Override
    @PostMapping({"/find"})
    public List<BannerItem> find(@RequestBody @Nullable BannerItem bo) {
        return this.bannerItemService.find(bo);
    }

    @PutMapping("/priority")
    public List<BannerItem> doPriority(@RequestBody List<BannerItem> bannerItemList) {
        return bannerItemService.doPriority(bannerItemList);
    }
}
