package com.hebela.masterdata.web.attribute;

import com.hebela.masterdata.bo.Attribute;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AttributeRequest {
    private Attribute attribute;
    private Integer productCategoryId;
}
