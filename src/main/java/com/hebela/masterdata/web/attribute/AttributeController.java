package com.hebela.masterdata.web.attribute;

import com.hebela.masterdata.bo.Attribute;
import com.hebela.masterdata.bo.AttributeData;
import com.hebela.masterdata.bo.Unit;
import com.hebela.masterdata.service.AttributeService;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/attribute")
@ResponseBody
public class AttributeController extends AbstractStatusController<Attribute> {
    private final AttributeService attributeService;

    public AttributeController(AttributeService attributeService) {
        this.attributeService = attributeService;
    }

    @PutMapping("/priority")
    public List<Attribute> priority(@RequestBody List<Attribute> attributeList) {
        return attributeService.doPriority(attributeList);
    }

    @PostMapping("/findBySearch")
    public List<Attribute> findBySearch(@RequestBody AttributeRequest attributeRequest) {
        return attributeService.findBySearch(attributeRequest);
    }

    @PostMapping("/findByCategory")
    public List<Attribute> findByCategory(@RequestBody Integer categoryId) {
        return attributeService.findByCategory(categoryId);
    }

    @PostMapping("/findByProduct")
    public List<Attribute> findByProduct(@RequestBody Integer productId) {
        return attributeService.findByProduct(productId);
    }

    @PostMapping("/findUnit")
    public List<Unit> findUnit() {
        return this.attributeService.findUnit();
    }

    @PostMapping("/insertAttributeData")
    public AttributeData insertAttributeData(@RequestBody AttributeData bo) {
        return this.attributeService.doInsertAttributeData(bo);
    }

    @PostMapping("/updateAttributeData")
    public AttributeData updateAttributeData(@RequestBody AttributeData bo) {
        return this.attributeService.doUpdateAttributeData(bo);
    }

    @PostMapping("/deleteAttributeData")
    public AttributeData deleteAttributeData(@RequestBody AttributeData bo) {
        return this.attributeService.doDeleteAttributeData(bo);
    }

}

