package com.hebela.masterdata.web.promotion;

import com.hebela.masterdata.bo.Promotion;
import com.hebela.masterdata.service.PromotionService;
import com.hebela.masterdata.web.promotionTimeFrame.PromotionTimeFrameRequest;
import com.hebela.web.AbstractStatusController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/promotion")
@ResponseBody
public class PromotionController extends AbstractStatusController<Promotion> {
    @Autowired
    PromotionService promotionService;

    @PostMapping("/activate")
    Promotion activate(@RequestBody Promotion bo) {
        return this.promotionService.doActivate(bo);
    }

    @PostMapping("/deactivate")
    Promotion deactivate(@RequestBody Promotion bo) {
        return this.promotionService.doDeactivate(bo);
    }

    @PostMapping("/findBySearch")
    Page<Promotion> findBySearch(@RequestBody PromotionSearchRequest bo) {
        return this.promotionService.findBySearch(bo);
    }

    @PostMapping("/findPromotion")
    Promotion findPromotion(@RequestBody PromotionTimeFrameRequest bo) {
        return this.promotionService.findPromotion(bo);
    }

    @PostMapping("/findPromotionActive")
    List<Promotion> findPromotionActive() {
        return this.promotionService.findPromotionActive();
    }

    @PostMapping("/findPromotionWithCategory")
    Promotion findPromotionWithCategory(@RequestBody PromotionTimeFrameRequest bo) {
        return this.promotionService.findPromotionWithCategory(bo);
    }

    @PostMapping("/findPromotionForShop")
    Page<Promotion> findPromotionForShop(@RequestBody PromotionSearchRequest bo) {
        return this.promotionService.findPromotionForShop(bo);
    }

}

