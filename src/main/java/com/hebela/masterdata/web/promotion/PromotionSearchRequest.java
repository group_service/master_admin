package com.hebela.masterdata.web.promotion;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
@NoArgsConstructor
public class PromotionSearchRequest extends HebelaPage {
    private String promotionName;
    private String keyword;
    private Integer status;
    private Boolean isTrash = false;
    private Instant startTime;
    private Instant endTime;
    private Integer type;
    private int typeTime;
}
