package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Group;
import com.hebela.masterdata.service.GroupService;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/group")
@ResponseBody
public class GroupController extends AbstractStatusController<Group> {

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping("/getDetail/{id}")
    public Group getDetail(@PathVariable Integer id) {
        return this.groupService.getDetail(id);
    }
}

