package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCategory;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.bo.ProductPolicy;
import com.hebela.masterdata.service.ProductPolicyService;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product-policy")
@ResponseBody
public class ProductPolicyController extends AbstractStatusController<ProductPolicy> {
    private final ProductPolicyService productPolicyService;

    public ProductPolicyController(ProductPolicyService productPolicyService) {
        this.productPolicyService = productPolicyService;
    }

    @PutMapping("/priority")
    public List<ProductPolicy> priority(@RequestBody List<ProductPolicy> productPolicyList) {
        return productPolicyService.doPriority(productPolicyList);
    }

    @PostMapping("/linkProduct/{productPolicyId}")
    public Product linkProduct(@PathVariable Integer productPolicyId, @RequestBody Product bo) {
        return this.productPolicyService.doLinkProduct(productPolicyId, bo);
    }

    @PostMapping("/unlinkProduct/{productPolicyId}")
    public void unlinkProduct(@PathVariable Integer productPolicyId, @RequestBody Product bo) {
        this.productPolicyService.doUnlinkProduct(productPolicyId, bo);
    }

    @PostMapping("/linkProductCategory/{productPolicyId}")
    public ProductCategory linkProductCategory(@PathVariable Integer productPolicyId, @RequestBody ProductCategory bo) {
        return this.productPolicyService.doLinkProductCategory(productPolicyId, bo);
    }

    @PostMapping("/unlinkProductCategory/{productPolicyId}")
    public void unlinkProductCategory(@PathVariable Integer productPolicyId, @RequestBody ProductCategory bo) {
        this.productPolicyService.doUnlinkProductCategory(productPolicyId, bo);
    }

    @PostMapping("/linkProductCombo/{productPolicyId}")
    public ProductCombo linkProductCombo(@PathVariable Integer productPolicyId, @RequestBody ProductCombo bo) {
        return this.productPolicyService.doLinkProductCombo(productPolicyId, bo);
    }

    @PostMapping("/unlinkProductCombo/{productPolicyId}")
    public void unlinkProductCombo(@PathVariable Integer productPolicyId, @RequestBody ProductCombo bo) {
        this.productPolicyService.doUnlinkProductCombo(productPolicyId, bo);
    }
}

