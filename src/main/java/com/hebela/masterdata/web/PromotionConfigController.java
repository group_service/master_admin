package com.hebela.masterdata.web;
import com.hebela.masterdata.bo.PromotionConfig;
import com.hebela.web.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/promotion-config")
@ResponseBody
public class PromotionConfigController extends AbstractController<PromotionConfig> {
}

