package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.ProductTags;
import com.hebela.masterdata.service.ProductTagsService;
import com.hebela.web.AbstractStatusController;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product-tags")
@ResponseBody
public class ProductTagsController extends AbstractStatusController<ProductTags> {
    private final ProductTagsService productTagsService;

    public ProductTagsController(ProductTagsService productTagsService) {
        this.productTagsService = productTagsService;
    }

    @PostMapping({"/findWithExtends"})
    public List<ProductTags> findWithExtends(@RequestBody @Nullable ProductTags bo) {
        return this.productTagsService.findWithExtends(bo);
    }

    @PutMapping("/priority")
    public List<ProductTags> priority(@RequestBody List<ProductTags> productTagsList) {
        return productTagsService.doPriority(productTagsList);
    }
}

