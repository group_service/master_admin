package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.ShippingFee;
import com.hebela.masterdata.service.ShippingFeeService;
import com.hebela.web.AbstractController;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/shipping-fee")
@ResponseBody
public class ShippingFeeController extends AbstractController<ShippingFee> {
    private final ShippingFeeService shippingFeeService;

    public ShippingFeeController(ShippingFeeService shippingFeeService) {
        this.shippingFeeService = shippingFeeService;
    }

    @PostMapping("/find")
    public List<ShippingFee> find(@RequestBody @Nullable ShippingFee bo) {
        return this.shippingFeeService.find(bo);
    }

    @PostMapping("/addProvince")
    public ShippingFee addProvince(@RequestBody @Nullable ShippingFee bo) {
        return this.shippingFeeService.doAddProvince(bo);
    }

    @PostMapping("/removeProvince")
    public ShippingFee removeProvince(@RequestBody ShippingFee bo) {
        return this.shippingFeeService.doRemoveProvince(bo);
    }
}

