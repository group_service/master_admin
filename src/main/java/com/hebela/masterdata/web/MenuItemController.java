package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.MenuItem;
import com.hebela.masterdata.service.MenuItemService;
import com.hebela.web.AbstractStatusController;
import lombok.extern.log4j.Log4j2;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/menu-item")
@ResponseBody
@Log4j2
public class MenuItemController extends AbstractStatusController<MenuItem> {
    private final MenuItemService menuItemService;

    public MenuItemController(MenuItemService menuItemService) {
        this.menuItemService = menuItemService;
    }

    @Override
    @PostMapping("/find")
    public List<MenuItem> find(@RequestBody @Nullable MenuItem bo) {
        return this.menuItemService.find(bo);
    }

    @PostMapping("/findTreeFlat")
    public List<MenuItem> findTreeFlat(@RequestBody @Nullable MenuItem bo) {
        return this.menuItemService.findTreeFlat(bo);
    }

    @PutMapping("/priority")
    public List<MenuItem> priority(@RequestBody List<MenuItem> menuItemList) {
        return menuItemService.doPriority(menuItemList);
    }
}
