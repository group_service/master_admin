package com.hebela.masterdata.web.feedback;

import com.hebela.masterdata.bo.Feedback;
import com.hebela.masterdata.service.FeedbackService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/feedback")
@ResponseBody
public class FeedbackController {
    @Value("${page.size}")
    protected int pageSize;
    protected final FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @PostMapping("/find")
    public List<Feedback> find(@RequestBody @Nullable Feedback bo) {
        return feedbackService.find(bo);
    }

    @PostMapping("/find/{page}")
    public Page<Feedback> findPage(@PathVariable Integer page, @RequestBody @Nullable Feedback bo) {
        if (page == null || page < 0) {
            page = 0;
        }
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, Feedback._createdDate);
        return feedbackService.findFeedBack(bo, pageable);
    }

    @GetMapping("/get/{id}")
    public Feedback get(@PathVariable Integer id) {
        return feedbackService.get(id);
    }
}

