package com.hebela.masterdata.web.emailTemplate;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class EmailTemplateRequest extends HebelaPage {

    private Instant fromDate;
    private Instant toDate;
    private String keyword;
    private Integer type;
}
