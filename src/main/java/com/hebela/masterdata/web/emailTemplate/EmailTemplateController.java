package com.hebela.masterdata.web.emailTemplate;

import com.hebela.masterdata.bo.EmailTemplate;
import com.hebela.masterdata.service.EmailTemplateService;
import com.hebela.web.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/email-template")
@ResponseBody
public class EmailTemplateController extends AbstractController<EmailTemplate> {

    @Autowired
    private EmailTemplateService emailTemplateService;

    @PostMapping("/findBySearch")
    public Page<EmailTemplate> findBySearch(@RequestBody EmailTemplateRequest emailTemplateRequest) {
        return emailTemplateService.findBySearch(emailTemplateRequest);
    }
}

