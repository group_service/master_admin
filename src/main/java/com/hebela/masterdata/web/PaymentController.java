package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Payment;
import com.hebela.masterdata.service.PaymentService;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/payment")
@ResponseBody
public class PaymentController extends AbstractStatusController<Payment> {
    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PutMapping("/priority")
    public List<Payment> priority(@RequestBody List<Payment> paymentList) {
        return paymentService.doPriority(paymentList);
    }
}

