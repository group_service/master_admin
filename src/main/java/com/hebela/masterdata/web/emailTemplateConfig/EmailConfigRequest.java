package com.hebela.masterdata.web.emailTemplateConfig;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class EmailConfigRequest extends HebelaPage {
    private String nameConfig;
    private Integer eventType;
    private Integer status;
    private Instant fromDate;
    private Instant toDate;

}
