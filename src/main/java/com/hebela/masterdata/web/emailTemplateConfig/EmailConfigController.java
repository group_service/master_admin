package com.hebela.masterdata.web.emailTemplateConfig;

import com.hebela.masterdata.bo.EmailConfig;
import com.hebela.masterdata.service.EmailConfigService;
import com.hebela.web.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/email-template-config")
public class EmailConfigController extends AbstractController<EmailConfig> {

    @Autowired
    private EmailConfigService emailConfigService;

    @PostMapping("/findBySearch")
    public Page<EmailConfig> findBySearch(@RequestBody EmailConfigRequest model) {
        return emailConfigService.findBySearch(model);
    }

    @PostMapping("/active")
    public EmailConfig active(@RequestBody EmailConfig model) {
        return emailConfigService.doActive(model);
    }

    @PostMapping("/deActive")
    public EmailConfig deActive(@RequestBody EmailConfig model) {
        return emailConfigService.doDeActive(model);
    }
}
