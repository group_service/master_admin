package com.hebela.masterdata.web.product;

import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductAttributeLink;
import com.hebela.masterdata.bo.ProductGallery;
import com.hebela.masterdata.bo.ProductPrice;
import com.hebela.masterdata.service.ProductGiftService;
import com.hebela.masterdata.service.ProductService;
import com.hebela.web.AbstractStatusController;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product")
@ResponseBody
public class ProductController extends AbstractStatusController<Product> {
    private final ProductService productService;
    private final ProductGiftService productGiftService;

    public ProductController(ProductService productService, ProductGiftService productGiftService) {
        this.productService = productService;
        this.productGiftService = productGiftService;
    }

    @PostMapping("/activate")
    public Product activate(@RequestBody Product product) {
        return this.productService.doActivate(product);
    }

    @PostMapping("/deactivate")
    public Product deactivate(@RequestBody Product product) {
        return this.productService.doDeactivate(product);
    }

    @PostMapping("/updatePrice")
    public ProductPrice updateprice(@RequestBody ProductPrice productPrice) {
        return this.productService.doUpdatePrice(productPrice);
    }

    @PostMapping("/deletePrice")
    public ProductPrice deletePrice(@RequestBody ProductPrice productPrice) {
        return this.productService.doDeletePrice(productPrice);
    }

    @GetMapping("/findGallery/{productId}")
    public List<ProductGallery> findGallery(@PathVariable Integer productId) {
        return this.productService.findGallery(productId);
    }

    @PostMapping("/findBySearch")
    public Page<Product> findBySearch(@RequestBody ProductSearchRequest model) {
        return this.productService.findBySearch(model);
    }

    @PostMapping("/findByTerm")
    public List<Product> findByTerm(@RequestParam String term) {
        return this.productService.findByTerm(term);
    }

    @ApiOperation(value = "Lấy chi tiết sản phẩm")
    @PostMapping("/getDetail/{id}")
    public Product getDetail(@PathVariable Integer id) {
        Product detail = this.productService.getDetail(id);
        detail.setGiftList(this.productGiftService.getDetailByProduct(id));
        return detail;
    }

    @PostMapping("/findAttribute/{id}")
    public List<ProductAttributeLink> findAttribute(@PathVariable Integer id) {
        return this.productService.findAttribute(id);
    }

    @PostMapping("/findProductForOrder")
    public List<Product> findProductForOrder(@RequestParam String term) {
        return this.productService.findProductForOrder(term);
    }
}

