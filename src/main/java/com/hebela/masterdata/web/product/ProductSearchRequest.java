package com.hebela.masterdata.web.product;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProductSearchRequest extends HebelaPage {
    private Integer orgId;
    private String keyword;
    private Integer categoryId;
    private Integer brandId;
    private Integer type;
    private Integer status;
    private Boolean isTrash = Boolean.FALSE;
}
