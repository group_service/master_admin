package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.PageConfig;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/page-config")
@ResponseBody
public class PageConfigController extends AbstractStatusController<PageConfig> {
}
