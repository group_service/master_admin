package com.hebela.masterdata.web.promotionTimeFrame;

import com.hebela.annotation.DTO;
import com.hebela.masterdata.bo.PromotionTimeFrame;
import com.hebela.masterdata.service.PromotionTimeFrameService;
import com.hebela.web.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/promotion-time-frame")
@ResponseBody
public class PromotionTimeFrameController extends AbstractController<PromotionTimeFrame> {
    @Autowired
    PromotionTimeFrameService promotionTimeFrameService;

    @PostMapping({"/insertTimeFrame"})
    public PromotionTimeFrame insertTimeFame(@RequestBody @DTO(PromotionTimeFrameDTO.class) PromotionTimeFrame bo) {
        return this.promotionTimeFrameService.doInsert(bo);
    }

    @PostMapping({"/updateTimeFrame/{id}"})
    public PromotionTimeFrame updateTimeFame(@PathVariable Integer id, @RequestBody @DTO(PromotionTimeFrameDTO.class) PromotionTimeFrame bo) {
        bo.setId(id);
        return this.promotionTimeFrameService.doUpdate(bo);
    }

    @GetMapping("/getDetail/{id}")
    PromotionTimeFrame getDetail(@PathVariable Integer id) {
        return this.promotionTimeFrameService.getDetail(id);
    }

    @PostMapping("/activate")
    PromotionTimeFrame activate(@RequestBody PromotionTimeFrame bo) {
        return this.promotionTimeFrameService.doActivate(bo);
    }

    @PostMapping("/deactivate")
    PromotionTimeFrame deactivate(@RequestBody PromotionTimeFrame bo) {
        return this.promotionTimeFrameService.doDeactivate(bo);
    }

    @PostMapping("/findPromotionTimeFrame")
    Page<PromotionTimeFrame> findPromotionTimeFrame(@RequestBody PromotionTimeFrameRequest bo) {
        return this.promotionTimeFrameService.findPromotionTimeFrame(bo);
    }
}
