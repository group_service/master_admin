package com.hebela.masterdata.web.promotionTimeFrame;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDate;

@Getter
@Setter
public class PromotionTimeFrameDTO {
    protected Integer id;
    protected Integer promotionId;
    protected Integer platform;
    @JsonFormat(
            pattern = "yyyy-MM-dd"
    )
    @JsonDeserialize(
            using = LocalDateDeserializer.class
    )
    @JsonSerialize(
            using = LocalDateSerializer.class
    )
    protected LocalDate day;
    protected Instant startTime;
    protected Instant endTime;
    protected Integer status;
    protected Integer createdBy;
    protected Instant createdDate;
    protected Integer modifiedBy;
    protected Instant modifiedDate;
}
