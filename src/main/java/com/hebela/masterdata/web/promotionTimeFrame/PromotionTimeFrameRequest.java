package com.hebela.masterdata.web.promotionTimeFrame;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class PromotionTimeFrameRequest extends HebelaPage {
    private Integer promotionId;
    private Boolean isTrash = false;
    private Integer status;
    private Instant startTime;
    private Instant endTime;
    private Integer type;
    private int typeTime;
}
