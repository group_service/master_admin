package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.HelpItem;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/help-item")
@ResponseBody
public class HelpItemController extends AbstractStatusController<HelpItem> {

    @PostMapping("/priority")
    public List<HelpItem> priority(@RequestBody List<HelpItem> productComboList) {
        return this.service.doPriority(productComboList);
    }
}

