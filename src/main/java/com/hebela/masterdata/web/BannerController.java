package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Banner;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/banner")
@ResponseBody
public class BannerController extends AbstractStatusController<Banner> {
}

