package com.hebela.masterdata.web.voucher;

import com.hebela.masterdata.bo.SuperVoucher;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Builder
@Getter
public class SuperVoucherResponse {
    private Integer id;
    private String name;
    private String description;
    private String detail;
    private Integer platform;
    private String voucherLabel;
    private String voucherValueText;
    private String discountType;
    private Integer discountPercent;
    private Integer discountMoneyMax;
    private Integer discountMoney;
    private Instant startDate;
    private Instant endDate;

    public static SuperVoucherResponse toResponse(SuperVoucher superVoucher) {
        if (superVoucher == null) {
            return null;
        }
        return SuperVoucherResponse.builder()
                .id(superVoucher.getId())
                .name(superVoucher.getName())
                .description(superVoucher.getDescription())
                .detail(superVoucher.getDetail())
                .platform(superVoucher.getPlatform())
                .voucherLabel(superVoucher.getVoucherLabel())
                .voucherValueText(superVoucher.getVoucherValueText())
                .discountType(superVoucher.getDiscountType())
                .discountPercent(superVoucher.getDiscountPercent())
                .discountMoneyMax(superVoucher.getDiscountMoneyMax())
                .discountMoney(superVoucher.getDiscountMoney())
                .startDate(superVoucher.getStartDate())
                .endDate(superVoucher.getEndDate())
                .build();
    }
}
