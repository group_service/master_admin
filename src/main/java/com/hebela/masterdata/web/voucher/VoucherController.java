package com.hebela.masterdata.web.voucher;

import com.hebela.core.obj.StatusInfo;
import com.hebela.masterdata.bo.Voucher;
import com.hebela.masterdata.bo.VoucherGiftLink;
import com.hebela.masterdata.bo.extend.CartView;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.service.CartService;
import com.hebela.masterdata.service.VoucherService;
import com.hebela.masterdata.web.cart.cartview.CartViewRequest;
import com.hebela.masterdata.web.cart.checkout.CheckoutRequest;
import com.hebela.masterdata.web.order.OrderAccountResponse;
import com.hebela.web.AbstractStatusController;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/voucher")
@ResponseBody
public class VoucherController extends AbstractStatusController<Voucher> {

    private final VoucherService voucherService;
    private final CartService cartService;

    public VoucherController(VoucherService voucherService, CartService cartService) {
        this.voucherService = voucherService;
        this.cartService = cartService;
    }

    @PostMapping("/activate")
    Voucher activate(@RequestBody Voucher bo) {
        return this.voucherService.doActivate(bo);
    }

    @PostMapping("/deactivate")
    Voucher deactivate(@RequestBody Voucher bo) {
        return this.voucherService.doDeactivate(bo);
    }

    @PostMapping("/findAccount")
    public List<OrderAccountResponse> findAccount(@RequestParam String keyword) {
        return this.voucherService.findAccount(keyword).stream().map(OrderAccountResponse::toResponse).collect(Collectors.toList());
    }

    @PostMapping("/findBySearch")
    Page<Voucher> findBySearch(@RequestBody VoucherSearchRequest bo) {
        return this.voucherService.findBySearch(bo);
    }

    @PostMapping("/addAccountOfVoucher")
    public Voucher addAccountOfVoucher(@RequestBody Voucher voucher) {
        return this.voucherService.doAddAccountOfVoucher(voucher);
    }

    @PostMapping("/removeAccountOfVoucher")
    public Voucher removeAccountOfVoucher(@RequestBody Voucher voucher) {
        return this.voucherService.doRemoveAccountOfVoucher(voucher);
    }

    @PostMapping("/findByAccount")
    public List<Voucher> findByAccount(HttpServletRequest request, @RequestBody CheckoutRequest bo) {
        Integer platform = Platform.getPatform(request);
        CartViewRequest cartViewRequest = new CartViewRequest();
        cartViewRequest.setPlatform(platform);
        cartViewRequest.setCartList(bo.getCartList());
        CartView view = this.cartService.view(cartViewRequest);
        return this.voucherService.findByAccount(platform, view, bo.getOrgId());
    }

    @GetMapping("/statusInfo/{orgId}")
    public StatusInfo getStatusInfor(@PathVariable Integer orgId) {
        return this.voucherService.statusInfo(orgId);
    }

    @PostMapping("/removeProductGift")
    public Voucher doRemoveProductGift(@RequestBody Voucher voucher) {
        return this.voucherService.doRemoveProductGift(voucher);
    }

    @PostMapping("/addProductGift")
    public Voucher addProductGift(@RequestBody Voucher voucher) {
        return this.voucherService.doAddProductGift(voucher);
    }

    @PostMapping("/changeQuantityProductGift")
    public VoucherGiftLink changeQuantityGift(@RequestBody Voucher voucher) {
        return this.voucherService.doChangeQuantityGift(voucher);
    }
}

