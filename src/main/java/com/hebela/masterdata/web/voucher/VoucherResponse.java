package com.hebela.masterdata.web.voucher;

import com.hebela.masterdata.bo.Voucher;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Builder
@Getter
public class VoucherResponse {
    private Integer id;
    private String name;
    private String description;
    private String detail;
    private Integer platform;
    private String voucherLabel;
    private String voucherValueText;
    private String discountType;
    private Integer buyMoneyMin;
    private Integer discountPercent;
    private Integer discountMoneyMax;
    private Integer discountMoney;
    private Instant startDate;
    private Instant endDate;

    public static VoucherResponse toResponse(Voucher voucher) {
        if (voucher == null) {
            return null;
        }
        return VoucherResponse.builder()
                .id(voucher.getId())
                .name(voucher.getName())
                .description(voucher.getDescription())
                .detail(voucher.getDetail())
                .platform(voucher.getPlatform())
                .voucherLabel(voucher.getVoucherLabel())
                .voucherValueText(voucher.getVoucherValueText())
                .discountType(voucher.getDiscountType())
                .buyMoneyMin(voucher.getBuyMoneyMin())
                .discountPercent(voucher.getDiscountPercent())
                .discountMoneyMax(voucher.getDiscountMoneyMax())
                .discountMoney(voucher.getDiscountMoney())
                .startDate(voucher.getStartDate())
                .endDate(voucher.getEndDate())
                .build();
    }
}
