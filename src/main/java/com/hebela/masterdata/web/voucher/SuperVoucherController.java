package com.hebela.masterdata.web.voucher;

import com.hebela.masterdata.bo.SuperVoucher;
import com.hebela.masterdata.bo.extend.CartView;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.service.CartService;
import com.hebela.masterdata.service.SuperVoucherService;
import com.hebela.masterdata.web.cart.cartview.CartViewRequest;
import com.hebela.masterdata.web.cart.checkout.CheckoutRequest;
import com.hebela.masterdata.web.order.OrderAccountResponse;
import com.hebela.web.AbstractStatusController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/super-voucher")
@ResponseBody
public class SuperVoucherController extends AbstractStatusController<SuperVoucher> {

    @Autowired
    private SuperVoucherService superVoucherService;
    @Autowired
    private CartService cartService;

    @PostMapping("/activate")
    SuperVoucher activate(@RequestBody SuperVoucher bo) {
        return this.superVoucherService.doActivate(bo);
    }

    @PostMapping("/deactivate")
    SuperVoucher deactivate(@RequestBody SuperVoucher bo) {
        return this.superVoucherService.doDeactivate(bo);
    }

    @PostMapping("/findAccount")
    public List<OrderAccountResponse> findAccount(@RequestParam String keyword) {
        return this.superVoucherService.findAccount(keyword).stream().map(OrderAccountResponse::toResponse).collect(Collectors.toList());
    }

    @PostMapping("/findBySearch")
    Page<SuperVoucher> findBySearch(@RequestBody VoucherSearchRequest bo) {
        return this.superVoucherService.findBySearch(bo);
    }

    @PostMapping("/addAccountOfVoucher")
    public SuperVoucher addAccountOfVoucher(@RequestBody SuperVoucher superVoucher) {
        return this.superVoucherService.doAddAccountOfVoucher(superVoucher);
    }

    @PostMapping("/removeAccountOfVoucher")
    public SuperVoucher removeAccountOfVoucher(@RequestBody SuperVoucher superVoucher) {
        return this.superVoucherService.doRemoveAccountOfVoucher(superVoucher);
    }

    @PostMapping("/findByAccount")
    public List<SuperVoucher> findByAccount(HttpServletRequest request, @RequestBody CheckoutRequest bo) {
        Integer platform = Platform.getPatform(request);
        CartViewRequest cartViewRequest = new CartViewRequest();
        cartViewRequest.setPlatform(platform);
        cartViewRequest.setCartList(bo.getCartList());
        CartView view = this.cartService.view(cartViewRequest);
        return this.superVoucherService.findByAccount(platform, view);
    }

    @PostMapping("/addProduct")
    SuperVoucher addProduct(@RequestBody SuperVoucher bo) {
        return this.superVoucherService.doAddProduct(bo);
    }

    @PostMapping("/removeProduct")
    SuperVoucher removeProduct(@RequestBody SuperVoucher bo) {
        return this.superVoucherService.doRemoveProduct(bo);
    }

    @PostMapping("/updateFixedPrice")
    SuperVoucher updateFixedPrice(@RequestBody SuperVoucher bo) {
        return this.superVoucherService.doUpdateFixedPrice(bo);
    }
}

