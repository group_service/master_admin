package com.hebela.masterdata.web.voucher;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.compress.utils.Lists;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
public class VoucherSearchRequest extends HebelaPage {
    private String name;
    private String code;
    private Integer campaignId;
    private Integer status;
    private Boolean isTrash = Boolean.FALSE;
    private Instant startDate;
    private Instant endDate;
    private String fullname;
    private String mobile;
    private Integer organizationId;
    private Integer statusTime;
    private String voucherValueText;
    private List<Integer> voucherStatusUseTypeList = Lists.newArrayList();
}
