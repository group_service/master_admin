package com.hebela.masterdata.web;

import com.hebela.core.HebelaException;
import com.hebela.core.bo.Account;
import com.hebela.masterdata.bo.Order;
import com.hebela.masterdata.bo.OrderRoot;
import com.hebela.masterdata.bo.OrderTransaction;
import com.hebela.masterdata.bo.Payment;
import com.hebela.masterdata.dao.OrderRootDao;
import com.hebela.masterdata.payment.vnpay.ConfigPaymentVNPay;
import com.hebela.masterdata.payment.vnpay.VnpayResponse;
import com.hebela.masterdata.service.OrderService;
import com.hebela.masterdata.service.OrderTransactionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/vnpay")
public class VnpayController {

    @Value("${vnpay.vnpHashSecret}")
    public String vnpHashSecret;
    @Value("${vnpay.vnpTmnCode}")
    public String vnpTmnCode;
    @Value("${vnpay.vnpVersion}")
    public String vnpVersion;
    @Value("${vnpay.vnpCommand}")
    public String vnpCommand;
    @Value("${vnpay.vnpCreateBy}")
    public String vnpCreateBy;
    @Value("${vnpay.vnpTransactionType}")
    public String vnpTransactionType;
    @Value("${vnpay.refund.orderInfo}")
    public String orderInfo;
    @Value("${vnpay.vnpApiUrl}")
    public String vnpApiUrl;
    @Value("${vnpay.ipServer}")
    public String ipServer;
    @Value("${spring.profiles.active}")
    protected String env;

    @Autowired
    private ConfigPaymentVNPay configPaymentVNPay;
    @Autowired
    private OrderRootDao orderRootDao;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderTransactionServiceImpl orderTransactionService;

    private final String PATTERN_FORMAT = "yyyyMMddHHmmss";

    @PostMapping("/refund/{orderCode}")
    public VnpayResponse refund(HttpServletRequest request, @PathVariable String orderCode) throws Exception {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_FORMAT)
                    .withZone(ZoneId.of("Asia/Ho_Chi_Minh"));
            Order order = this.orderService.getByCode(orderCode);
            if (order == null ||
                    Payment.TYPE_COD.equals(order.getPaymentMethod()) ||
                    !Payment.PAYMENT_STATUS_WAITE_REFUND.equals(order.getPaymentStatus())) {
                throw new HebelaException("action.invalid");
            }
            Optional<OrderRoot> optionalOrderRoot = this.orderRootDao.findById(order.getOrderRootId());
            if (!optionalOrderRoot.isPresent()) {
                throw new HebelaException("action.invalid");
            }
            Order orderExample = new Order();
            orderExample.setOrderRootId(order.getOrderRootId());
            Long numberOrder = this.orderService.countOrderByOrderRoot(order.getOrderRootId());
            if (numberOrder > 1) {
                throw new HebelaException("vnpay.refund.fail");
            }

            OrderRoot orderRoot = optionalOrderRoot.get();

            String vnpTransDate = formatter.format(orderRoot.getTxnExecuteDate());
            String createDate = formatter.format(Instant.now());
            String vnpTxnRef = orderRoot.getMrcOrderId();
            String vnpOrderInfo = orderInfo + " " + orderCode;
            Map<String, String> vnp_Params = new HashMap<>();
            if (!env.equals("prod")) {
                vnpTxnRef = env + "-" + orderRoot.getMrcOrderId();
            }
            vnp_Params.put("vnp_Version", vnpVersion);
            vnp_Params.put("vnp_Command", vnpCommand);
            vnp_Params.put("vnp_TmnCode", vnpTmnCode);
            vnp_Params.put("vnp_Amount", String.valueOf(orderRoot.getTotal() * 100));
            vnp_Params.put("vnp_TxnRef", vnpTxnRef);
            vnp_Params.put("vnp_OrderInfo", vnpOrderInfo);
            vnp_Params.put("vnp_TransDate", vnpTransDate);
            vnp_Params.put("vnp_IpAddr", ConfigPaymentVNPay.getIpAddress(request));
            vnp_Params.put("vnp_CreateBy", vnpCreateBy);
            vnp_Params.put("vnp_TransactionType", vnpTransactionType);
            vnp_Params.put("vnp_CreateDate", createDate);

            List<String> fieldNames = new ArrayList<>(vnp_Params.keySet());
            Collections.sort(fieldNames);
            StringBuilder hashData = new StringBuilder();
            StringBuilder query = new StringBuilder();
            Iterator<String> itr = fieldNames.iterator();
            try {
                while (itr.hasNext()) {
                    String fieldName = itr.next();
                    String fieldValue = vnp_Params.get(fieldName);
                    if ((fieldValue != null) && (fieldValue.length() > 0)) {
                        //Build hash data
                        hashData.append(fieldName);
                        hashData.append('=');
                        hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                        //Build query
                        query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                        query.append('=');
                        query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));

                        if (itr.hasNext()) {
                            query.append('&');
                            hashData.append('&');
                        }
                    }
                }
            } catch (Exception e) {

            }
            String queryUrl = query.toString();
            String vnp_SecureHash = configPaymentVNPay.hmacSHA512(vnpHashSecret, hashData.toString());
            queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
            String paymentUrl = vnpApiUrl + "?" + queryUrl;

            URL url = new URL(paymentUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String Rsp = response.toString();
            String respDecode = URLDecoder.decode(Rsp, "UTF-8");
            String[] responseData = respDecode.split("&|\\=");
            int i = 0;
            String vnpTransactionNo = this.getTransactionNo(respDecode);
            List<String> responseVNPAY = Arrays.asList(responseData);
            for (String item : responseVNPAY) {
                if (item.equals(ConfigPaymentVNPay.VNPAY_PAYMENT_SUCCESS)) {
                    i++;
                } else if (item.equals("SUCCESS")) {
                    i++;
                }
                if (i == 2) {
                    break;
                }
            }
            if (i == 2) {
                //hoan tien thanh cong
                order.setPaymentStatus(Payment.PAYMENT_STATUS_REFUND);
                orderRoot.setPaymentStatus(Payment.PAYMENT_STATUS_REFUND);
                orderService.doUpdateRefundSuccess(order, orderRoot);
                OrderTransaction orderTransaction = this.toModel(order, orderRoot, vnpTransactionNo);
                this.orderTransactionService.doInsert(orderTransaction);
                return new VnpayResponse();
            }
        } catch (Exception e) {
            throw new HebelaException("action.invalid");
        }
        return null;
    }

    private OrderTransaction toModel(Order order, OrderRoot orderRoot, String vnpTransactionNo) {
        OrderTransaction orderTransaction = new OrderTransaction();
        orderTransaction.setOrderCode(order.getCode());
        orderTransaction.setOrderRootCode(orderRoot.getOrderRootCode());
        orderTransaction.setAmount(orderRoot.getTotal());
        orderTransaction.setRefCode(vnpTransactionNo);
        orderTransaction.setType(OrderTransaction.ORDER_TRANSACTION_TYPE_REFUND);
        orderTransaction.setExecutionTime(Instant.now());
        orderTransaction.setCreatedDate(Instant.now());
        orderTransaction.setPaymentMethod(Payment.TYPE_ONLINE_VNPAY);
        orderTransaction.setExecutorId(Account.getContextId());
        return orderTransaction;
    }

    private String getTransactionNo(String response) {
        Pattern pattern = Pattern.compile("vnp_TransactionNo=(.*)&vnp_TransactionStatus");
        Matcher matcher = pattern.matcher(response);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
