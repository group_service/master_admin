package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.PromotionCategory;
import com.hebela.masterdata.service.PromotionCategoryService;
import com.hebela.web.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/promotion-category")
@ResponseBody
public class PromotionCategoryController extends AbstractController<PromotionCategory> {
    @Autowired
    PromotionCategoryService promotionCategoryService;

    @PostMapping("/activate")
    PromotionCategory activate(@RequestBody PromotionCategory bo) {
        return this.promotionCategoryService.doActivate(bo);
    }

    @PostMapping("/deactivate")
    PromotionCategory deactivate(@RequestBody PromotionCategory bo) {
        return this.promotionCategoryService.doDeactivate(bo);
    }

    @PutMapping("/priority")
    public List<PromotionCategory> priority(@RequestBody List<PromotionCategory> promotionCategories) {
        return promotionCategoryService.doPriority(promotionCategories);
    }
}

