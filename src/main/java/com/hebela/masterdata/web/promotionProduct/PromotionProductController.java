package com.hebela.masterdata.web.promotionProduct;

import com.hebela.masterdata.bo.PromotionProduct;
import com.hebela.masterdata.service.PromotionProductService;
import com.hebela.web.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/promotion-product")
@ResponseBody
public class PromotionProductController extends AbstractController<PromotionProduct> {
    @Autowired
    PromotionProductService promotionProductService;

    @PutMapping("/priority")
    public List<PromotionProduct> priority(@RequestBody List<PromotionProduct> promotionProducts) {
        return this.promotionProductService.doPriority(promotionProducts);
    }

    @PostMapping("/insertAll")
    PromotionProduct insertAll(@RequestBody List<PromotionProduct> promotionProducts) {
        return this.promotionProductService.doInsertAll(promotionProducts);
    }

    @PostMapping("/updateAll")
    PromotionProduct updateAll(@RequestBody List<PromotionProduct> promotionProducts) {
        return this.promotionProductService.doUpdateAll(promotionProducts);
    }

    @PostMapping("/activate")
    PromotionProduct activate(@RequestBody PromotionProduct bo) {
        return this.promotionProductService.doActivate(bo);
    }

    @PostMapping("/deactivate")
    PromotionProduct deactivate(@RequestBody PromotionProduct bo) {
        return this.promotionProductService.doDeactivate(bo);
    }

    @PostMapping("/findBySearch")
    public List<PromotionProduct> findPromotionProduct(@RequestBody PromotionProductRequest model) {
        return this.promotionProductService.findBySearch(model);
    }

    @PostMapping("/findPromotionProduct/{categoryId}")
    public List<PromotionProduct> findPromotionProduct(@PathVariable Integer categoryId) {
        return this.promotionProductService.findPromotionProduct(categoryId);
    }

}

