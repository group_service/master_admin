package com.hebela.masterdata.web.promotionProduct;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class PromotionProductRequest {
    @NonNull
    private Integer timeFrameId;
    private Integer categoryId;
    private Integer status;
    private Integer type;
    private String keyword;
}
