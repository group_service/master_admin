package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Campaign;
import com.hebela.masterdata.service.CampaignService;
import com.hebela.web.AbstractStatusController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/campaign")
@ResponseBody
public class CampaignController extends AbstractStatusController<Campaign> {

    @Autowired
    private CampaignService campaignService;

    // lấy tất cả chiến dịch của sàn và shop
    @PostMapping("/findAll")
    public List<Campaign> findAllCampaign(@RequestBody Campaign campaign) {
        return campaignService.findAll(campaign);
    }

    // lấy tất cả chiến dịch của shop
    @PostMapping("/findAllShop")
    public List<Campaign> findAllShop(@RequestBody Campaign campaign) {
        return campaignService.findAllShop(campaign);
    }
}

