package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.ProductCategory;
import com.hebela.masterdata.service.ProductCategoryService;
import com.hebela.web.AbstractStatusController;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product-category")
@ResponseBody
public class ProductCategoryController extends AbstractStatusController<ProductCategory> {

    private final ProductCategoryService productCategoryService;

    public ProductCategoryController(ProductCategoryService productCategoryService) {
        this.productCategoryService = productCategoryService;
    }

    @PostMapping("/getDetail")
    public ProductCategory getDetail(@RequestBody Integer categoryId) {
        return this.productCategoryService.getDetail(categoryId);
    }

    @PostMapping("/priority")
    public void doPriority(@RequestBody List<ProductCategory> productCategories) {
        this.productCategoryService.doPriority(productCategories);
    }

    @PostMapping("/findByTerm")
    public List<ProductCategory> findByTerm(@RequestParam String term) {
        return this.productCategoryService.findByTerm(term);
    }

    @PostMapping("/findFlat")
    public List<ProductCategory> findFlat(@RequestBody @Nullable String type) {
        return this.productCategoryService.findFlat(type);
    }

    @PostMapping("/findTree")
    public List<ProductCategory> findTree(@RequestBody @Nullable String type) {
        return this.productCategoryService.findTree(type);
    }

    @PostMapping("/findByKeyword")
    public List<ProductCategory> findByKeyword(@RequestBody @Nullable ProductCategory bo) {
        return this.productCategoryService.findByKeyword(bo);
    }

    @PostMapping("/inserts")
    public ProductCategory doInsert(@RequestBody ProductCategory bo) {
        return this.productCategoryService.doInsert(bo);
    }

    @PostMapping("/setDefault")
    public ProductCategory setDefault(@RequestBody ProductCategory bo) {
        return this.productCategoryService.doSetDefault(bo);
    }

    @PostMapping("/trash/{id}")
    public ProductCategory trash(@PathVariable Integer id, @RequestBody ProductCategory bo) {
        return this.productCategoryService.doTrash(bo);
    }

//    @PostMapping("/attribute/{id}")
//    List<Attribute> getAttribute(@PathVariable Integer id) {
//        return this.productCategoryService.getAttribute(id);
//    }

//    private void buildCategoryTreeView(ProductCategory productCategory,CategoryTreeViewResponse treeViewResponse){
//        treeViewResponse.setText(productCategory.getName());
//        treeViewResponse.setValue(productCategory.getId());
//        treeViewResponse.setChildren(productCategory.getChildren());
//        if(productCategory.getChildren() != null && productCategory.getChildren().size()>0){
//            for(ProductCategory item: productCategory.getChildren()){
//                buildCategoryTreeView(item,treeViewResponse);
//            }
//        }else{
//
//        }
//    }
}
