package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Landing;
import com.hebela.masterdata.service.LandingService;
import com.hebela.web.AbstractStatusController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/landing")
@ResponseBody
public class LandingController extends AbstractStatusController<Landing> {
    private final LandingService landingService;

    public LandingController(LandingService landingService) {
        this.landingService = landingService;
    }

    @PostMapping("/activate")
    public Landing activate(@RequestBody Landing landing) {
        return this.landingService.doActivate(landing);
    }

    @PostMapping("/deactivate")
    public Landing deactivate(@RequestBody Landing landing) {
        return this.landingService.doDeactivate(landing);
    }
}

