package com.hebela.masterdata.web.cart;

import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.Checkout;
import com.hebela.masterdata.bo.extend.Seller;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Builder
public class AdminCheckoutResponse {
    private List<AdminCouponResponse> groups;
    private Integer shippingFee;
    private Long couponDiscount;
    private Long subTotal;
    private Long total;

    public static AdminCheckoutResponse toResponse(Checkout checkout) {
        if (checkout == null) {
            return null;
        }
        Seller hebelaSeller = checkout.getSellers().get(0);
        return AdminCheckoutResponse.builder()
                .groups(hebelaSeller.getGroup().stream().map(AdminCouponResponse::toResponse).collect(Collectors.toList()))
                .subTotal(hebelaSeller.getSubTotal())
                .couponDiscount(checkout.getCouponDiscount())
                .total(hebelaSeller.getTotal())
                .shippingFee(hebelaSeller.getShippingFee())
                .build();
    }
}

@Builder
class AdminCouponResponse {
    private Integer id;
    private String name;
    private Integer numberOfGift;
    private String type;
    private String couponLabel;
    private String couponValue;
    private String benefitType;
    private Integer discountPercent;
    private Integer price;
    private Integer discountAmount;
    private Integer quantity;
    private List<AdminCartResponse> cartList;

    public static AdminCouponResponse toResponse(Coupon coupon) {
        return AdminCouponResponse.builder()
                .id(coupon.getId())
                .name(coupon.getName())
                .type(coupon.getType())
                .benefitType(coupon.getDiscountType())
                .couponLabel(coupon.getCouponLabel())
                .numberOfGift(coupon.getNumberOfGift())
                .couponValue(coupon.getCouponLabelValue())
                .discountAmount(coupon.getDiscountMoney())
                .discountPercent(coupon.getDiscountPercent())
                .price(coupon.getBuyMoneyMin())
                .quantity(coupon.getBuyQuantityMin())
                .cartList(coupon.getCartList().stream().map(AdminCartResponse::toResponse).collect(Collectors.toList()))
                .build();
    }
}

@Builder
class AdminCartResponse {

    private Integer id;
    private Integer productId;
    private AdminProductResponse product;
    private Integer comboId;
    private AdminComboResponse productCombo;
    private Integer count;

    public static AdminCartResponse toResponse(Cart cart) {
        return AdminCartResponse.builder()
                .id(cart.getId())
                .productId(cart.getProductId())
                .product(AdminProductResponse.toResponse(cart.getProduct()))
                .comboId(cart.getComboId())
                .productCombo(AdminComboResponse.toResponse(cart.getProductCombo()))
                .count(cart.getCount())
                .build();
    }
}

@Builder
@Getter
class AdminProductResponse {
    private Integer id;
    private String name;
    private String imageThumbnail;
    private ProductPrice productPrice;
    private Boolean isFavorite;
    private List<ProductGift> giftList;

    public static AdminProductResponse toResponse(Product product) {
        if (product == null) {
            return null;
        }
        AdminProductResponse response = AdminProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .imageThumbnail(product.getImageThumbnail())
                .productPrice(product.getProductPrice())
                .isFavorite(product.getIsFavorite())
                .giftList(product.getGiftList())
                .build();
        if (response.getProductPrice() != null && response.getProductPrice().getPriceRegular() != null && response.getProductPrice().getPriceSale() != null) {
            int discount = response.getProductPrice().getPriceRegular() - response.getProductPrice().getPriceSale();
            int regular = response.getProductPrice().getPriceRegular();
            response.getProductPrice().setSalePercent(Double.valueOf(discount) / Double.valueOf(regular));
        }

        return response;
    }
}

@Builder
class AdminComboResponse {
    private Integer id;
    private String name;
    private String imageThumbnail;
    private Integer price;
    private Integer priceRegular;
    private Boolean isFavorite;
    private List<AdminProductResponse> productList;

    public static AdminComboResponse toResponse(ProductCombo combo) {
        if (combo == null) {
            return null;
        }
        return AdminComboResponse.builder()
                .id(combo.getId())
                .name(combo.getName())
                .imageThumbnail(combo.getImageThumbnail())
                .price(combo.getPrice())
                .priceRegular(combo.getPriceRegular())
                .isFavorite(combo.getIsFavorite())
                .productList(combo.getProductList().stream().map(AdminProductResponse::toResponse).collect(Collectors.toList()))
                .build();
    }
}
