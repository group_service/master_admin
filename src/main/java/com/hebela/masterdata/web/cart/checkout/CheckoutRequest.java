package com.hebela.masterdata.web.cart.checkout;

import com.hebela.masterdata.web.cart.cartview.CartViewRequest;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CheckoutRequest extends CartViewRequest {
    private Integer paymentMethod;
    private Integer accountAddressId;
    private Integer accountId;
    private Integer orgId;
    private Integer paymentStatus;
}
