package com.hebela.masterdata.web.cart.gift;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class SelectGiftRequest {
    private Integer productId;
    private Integer couponId;
    private List<Integer> giftIdList;
}
