package com.hebela.masterdata.web.cart.checkout;

import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.Checkout;
import com.hebela.masterdata.bo.extend.Seller;
import lombok.Builder;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@Getter
public class CheckoutResponse {
    private LinkedList<SellerResponse> sellers;
    private Long couponDiscount;
    private VoucherResponse voucher;
    private Long voucherDiscount;
    private Long total;
    private Long subTotal;
    private Integer shippingFee;
    private Integer paymentMethod;
    private AccountAddress accountAddress;
    private String warnMessage;
    private Long totalVoucherDiscountShop;

    public static CheckoutResponse toResponse(Checkout checkout) {
        if (checkout == null) {
            return null;
        }
        return CheckoutResponse.builder()
                .sellers(checkout.getSellers().stream().map(SellerResponse::toResponse).collect(Collectors.toCollection(LinkedList::new)))
                .couponDiscount(checkout.getCouponDiscount())
                .voucher(VoucherResponse.toResponse(checkout.getVoucher()))
                .voucherDiscount(checkout.getVoucherDiscount())
                .subTotal(checkout.getSubTotal())
                .total(checkout.getTotal())
                .shippingFee(checkout.getShippingFee())
                .paymentMethod(checkout.getPaymentMethod())
                .accountAddress(checkout.getAccountAddress())
                .totalVoucherDiscountShop(checkout.getTotalVoucherDiscountShop())
                .warnMessage(checkout.getWarnMessage())
                .build();
    }
}


@Builder
class VoucherResponse {
    private Integer id;
    private String code;
    private String name;
    private String discountType;
    private Integer discountMoney;
    private Integer discountPercent;
    private List<String> cartErrorList;
    private Boolean isEligible;
    private String voucherHint;
    private Integer orgId;

    public static VoucherResponse toResponse(Voucher voucher) {
        if (voucher == null) {
            return null;
        }
        return VoucherResponse.builder()
                .id(voucher.getId())
                .code(voucher.getCode())
                .name(voucher.getName())
                .discountType(voucher.getDiscountType())
                .discountMoney(voucher.getDiscountMoney())
                .discountPercent(voucher.getDiscountPercent())
                .cartErrorList(voucher.getCartErrorList())
                .isEligible(voucher.getIsEligible())
                .voucherHint(voucher.getVoucherHint())
                .orgId(voucher.getOrgId())
                .build();
    }
}

@Builder
class SellerResponse {
    private LinkedList<CouponResponse> groups;
    private Integer id;
    private String name;
    private String logo;
    private Long subTotal;
    private Integer shippingFee;
    private Long couponDiscount;
    private Long voucherDiscount;
    private Long total;
    private Long voucherShopDiscount = 0L;
    private List<ProductGift> productVoucherGiftList;
    private VoucherResponse voucher;

    public static SellerResponse toResponse(Seller seller) {
        return SellerResponse.builder()
                .groups(seller.getGroup().stream().map(CouponResponse::toResponse).collect(Collectors.toCollection(LinkedList::new)))
                .productVoucherGiftList(seller.getProductVoucherGiftList())
                .id(seller.getId())
                .name(seller.getName())
                .logo(seller.getLogo())
                .subTotal(seller.getSubTotal())
                .shippingFee(seller.getShippingFee())
                .couponDiscount(seller.getCouponDiscount())
                .voucherDiscount(seller.getVoucherDiscount())
                .voucher(VoucherResponse.toResponse(seller.getVoucher()))
                .total(seller.getTotal())
                .build();
    }
}

@Builder
class CouponResponse {
    private Integer id;
    private String name;
    private Integer numberOfGift;
    private String type;
    private String couponLabel;
    private String couponLabelValue;
    private String discountType;
    private Integer discountPercent;
    private Integer discountMoney;
    private Integer buyQuantityMin;
    private Integer buyMoneyMin;
    private String conditionLabel;
    private String conditionLabelValue;
    LinkedList<CartResponse> cartList;
    List<ProductResponse> selectedGift;


    public static CouponResponse toResponse(Coupon coupon) {
        return CouponResponse.builder()
                .id(coupon.getId())
                .name(coupon.getName())
                .type(coupon.getType())
                .discountType(coupon.getDiscountType())
                .couponLabel(coupon.getCouponLabel())
                .numberOfGift(coupon.getNumberOfGift())
                .couponLabelValue(coupon.getCouponLabelValue())
                .discountMoney(coupon.getDiscountMoney())
                .discountPercent(coupon.getDiscountPercent())
                .buyQuantityMin(coupon.getBuyQuantityMin())
                .buyMoneyMin(coupon.getBuyMoneyMin())
                .conditionLabel(coupon.getConditionLabel())
                .conditionLabelValue(coupon.getConditionLabelValue())
                .cartList(coupon.getCartList().stream().map(CartResponse::toResponse).collect(Collectors.toCollection(LinkedList::new)))
                .selectedGift(coupon.getSelectedGift().stream().map(ProductResponse::toResponse).collect(Collectors.toList()))
                .build();
    }
}

@Builder
class CartResponse {
    private Integer id;
    private Integer productId;
    private ProductResponse product;
    private Integer comboId;
    private ComboResponse productCombo;
    private Integer count;
    private String quantityCartWarn;

    public static CartResponse toResponse(Cart cart) {
        return CartResponse.builder()
                .id(cart.getId())
                .productId(cart.getProductId())
                .product(ProductResponse.toResponse(cart.getProduct()))
                .comboId(cart.getComboId())
                .productCombo(ComboResponse.toResponse(cart.getProductCombo()))
                .count(cart.getCount())
                .quantityCartWarn(cart.getQuantityCartWarn())
                .build();
    }
}

@Builder
class ComboResponse {
    private Integer id;
    private String name;
    private String sku;
    private String imageThumbnail;
    private Integer price;
    private Integer priceRegular;
    private Integer amount;
    private Boolean isFavorite;
    private List<ProductResponse> productList;
    private List<ProductGift> giftList;
    private String slug;

    public static ComboResponse toResponse(ProductCombo combo) {
        if (combo == null) {
            return null;
        }
        return ComboResponse.builder()
                .id(combo.getId())
                .name(combo.getName())
                .sku(combo.getSku())
                .imageThumbnail(combo.getImageThumbnail())
                .price(combo.getPrice())
                .priceRegular(combo.getPriceRegular())
                .amount(combo.getAmount())
                .isFavorite(combo.getIsFavorite())
                .productList(combo.getProductList().stream().map(ProductResponse::toResponse).collect(Collectors.toList()))
                .giftList(combo.getGiftList())
                .slug(combo.getSlug())
                .build();
    }
}

@Builder
@Getter
class ProductResponse {
    private Integer id;
    private String name;
    private String sku;
    private String imageThumbnail;
    private ProductPrice productPrice;
    private Integer amount;
    private Boolean isFavorite;
    private List<ProductGift> giftList;
    private Brand brand;
    private String slug;

    public static final ProductResponse toResponse(Product product) {
        if (product == null) {
            return null;
        }
        ProductResponse response = ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .sku(product.getSku())
                .imageThumbnail(product.getImageThumbnail())
                .productPrice(product.getProductPrice())
                .amount(product.getAmount())
                .isFavorite(product.getIsFavorite())
                .giftList(product.getGiftList())
                .brand(product.getBrand())
                .slug(product.getSlug())
                .build();
        if (response.getProductPrice() != null && response.getProductPrice().getPriceRegular() != null && response.getProductPrice().getPriceSale() != null) {
            int discount = response.getProductPrice().getPriceRegular() - response.getProductPrice().getPriceSale();
            int regular = response.getProductPrice().getPriceRegular();
            response.getProductPrice().setSalePercent(Double.valueOf(discount) / Double.valueOf(regular));
        }

        return response;
    }
}
