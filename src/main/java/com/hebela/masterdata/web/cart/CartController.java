package com.hebela.masterdata.web.cart;

import com.hebela.core.HebelaException;
import com.hebela.masterdata.bo.Cart;
import com.hebela.masterdata.bo.extend.CartAccount;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.service.CartService;
import com.hebela.masterdata.web.cart.checkout.CheckoutRequest;
import com.hebela.masterdata.web.cart.checkout.CheckoutResponse;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/cart")
@ResponseBody
public class CartController {
    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

//    @PostMapping("/add")
//    public synchronized CartView add(HttpServletRequest request, @RequestBody Cart cart) {
//        Integer platform = Platform.getPatform(request);
//        cart.setIsByAdmin(Boolean.TRUE);
//        CartViewRequest model = new CartViewRequest();
//        model.setPlatform(platform);
//        model.setCartList(Arrays.asList(cart));
//        model.setAccountId(cart.getAccountId());
//        return cart.getAccountId() == null ? null : cartService.doAdd(model);
//    }

//    @PostMapping("/remove")
//    public Cart remove(@RequestBody Cart cart) {
//        return cart.getAccountId() == null ? null : ignoreId(cartService.doRemove(cart, cart.getAccountId()));
//    }
//
//    @PostMapping("/update")
//    public CartView update(HttpServletRequest request, @RequestBody Cart cart) {
//        CartViewRequest cartViewRequest = new CartViewRequest();
//        cartViewRequest.setPlatform(Platform.getPatform(request));
//        cart.setIsByAdmin(Boolean.TRUE);
//        cartViewRequest.setAccountId(cart.getAccountId());
//        cartViewRequest.setCartList(Arrays.asList(cart));
//        return cart.getAccountId() == null ? null : cartService.doUpdate(cartViewRequest);
//    }

//    @PostMapping("/checkout")
//    public AdminCheckoutResponse checkout(HttpServletRequest request, @RequestBody CheckoutRequest bo) {
//        if (bo.getAccountId() == null) {
//            throw new HebelaException("order.user.missing");
//        }
//        Cart cart = Cart.newInstanceByAccountId(bo.getAccountId());
//        cart.setIsByAdmin(Boolean.TRUE);
//        CheckoutRequest checkoutRequest = new CheckoutRequest();
//        checkoutRequest.setPlatform(Platform.getPatform(request));
//        checkoutRequest.setCartList(cartService.find(cart));
//        checkoutRequest.setPaymentMethod(bo.getPaymentMethod());
//        checkoutRequest.setAccountAddressId(bo.getAccountAddressId());
//        return AdminCheckoutResponse.toResponse(this.cartService.checkout(checkoutRequest));
//    }

    @PostMapping("/checkout")
    public CheckoutResponse checkout(HttpServletRequest request, @RequestBody CheckoutRequest bo) {
        if (bo.getAccountId() == null) {
            throw new HebelaException("order.user.missing");
        }
        CheckoutRequest checkoutRequest = new CheckoutRequest();
        checkoutRequest.setPlatform(Platform.getPatform(request));
        // sắp xếp giỏ hàng, xếp combo lên đầu
        bo.getCartList().sort(new Comparator<Cart>() {
            @Override
            public int compare(Cart o1, Cart o2) {
                if (o1.getProductId() == null) {
                    return -1;
                }
                if (o2.getProductId() == null) {
                    return 1;
                }
                return o2.getProductId().compareTo(o1.getProductId());
            }
        });
        checkoutRequest.setCartList(bo.getCartList());
        checkoutRequest.setPaymentMethod(bo.getPaymentMethod());
        checkoutRequest.setAccountAddressId(bo.getAccountAddressId());
        checkoutRequest.setVoucherCode(bo.getVoucherCode());
        checkoutRequest.setGiftSelectedList(bo.getGiftSelectedList());
        checkoutRequest.setVoucherSelectedList(bo.getVoucherSelectedList());
        return CheckoutResponse.toResponse(this.cartService.checkout(checkoutRequest));
    }

    @PostMapping("/findBySearch")
    public Page<CartAccount> findBySearch(@RequestBody CartSearchRequest model) {
        Page<CartAccount> cartAccountPage = this.cartService.findBySearch(model);
        return cartAccountPage;
    }

    private List<Cart> ignoreId(List<Cart> cartList) {
        if (cartList == null || cartList.size() == 0) {
            return cartList;
        }
        cartList.forEach(this::ignoreId);
        return cartList;
    }

    private Cart ignoreId(Cart cart) {
        cart.setId(null);
        cart.setCreatedBy(null);
        cart.setModifiedBy(null);
        return cart;
    }
}
