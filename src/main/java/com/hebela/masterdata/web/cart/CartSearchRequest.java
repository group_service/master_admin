package com.hebela.masterdata.web.cart;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CartSearchRequest extends HebelaPage {
    private String cartItemKeyword;
    private String cartItemSku;
    private String accountKeyword;
}
