package com.hebela.masterdata.web.cart.cartview;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hebela.masterdata.bo.Cart;
import com.hebela.masterdata.bo.ProductGift;
import com.hebela.masterdata.bo.Voucher;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.web.cart.gift.SelectGiftRequest;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.compress.utils.Lists;

import java.util.List;

@Setter
@Getter
public class CartViewRequest {
    private String voucherCode;
    private List<Cart> cartList;
    @JsonIgnore
    private Integer platform = Platform.WEB;
    @JsonIgnore
    private Integer accountId;
    private List<SelectGiftRequest> giftSelectedList;
    private List<Voucher> voucherSelectedList = Lists.newArrayList();
    private List<ProductGift> productGiftVoucher;
}
