package com.hebela.masterdata.web.gift;

import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.bo.ProductGift;
import com.hebela.masterdata.service.ProductGiftService;
import com.hebela.web.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product-gift")
@ResponseBody
public class ProductGiftController extends AbstractController<ProductGift> {

    private final ProductGiftService productGiftService;

    public ProductGiftController(ProductGiftService productGiftService) {
        this.productGiftService = productGiftService;
    }

    @PostMapping("/priority")
    public void doPriority(@RequestBody List<ProductGift> productCategories) {
        this.productGiftService.doPriority(productCategories);
    }

    @PostMapping("/findProductForGift")
    public List<Product> findProductForGift(@RequestParam String term, @RequestParam Integer productId,
                                            @RequestParam Integer typeProduct) {
        return this.productGiftService.findProductForGift(term, productId, typeProduct);
    }

    @PostMapping("/findComboForGift")
    public List<ProductCombo> findComboForGift() {
        return this.productGiftService.findComboForGift();
    }

    @PostMapping("/getDetail")
    public ProductGiftResponse getDetail(@RequestParam Integer productGiftId) {
        return this.productGiftService.getDetail(productGiftId);
    }

    @PostMapping("/updateGift")
    public ProductGift update(@RequestBody ProductGift bo) {
        return this.productGiftService.doUpdate(bo);
    }
}

