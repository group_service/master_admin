package com.hebela.masterdata.web.gift;

import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.bo.ProductGift;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProductGiftResponse {
    private Integer id;
    private Integer productId;
    private Integer productComboId;
    private Product product;
    private ProductCombo productCombo;
    private List<ProductGift> productGiftList;
}
