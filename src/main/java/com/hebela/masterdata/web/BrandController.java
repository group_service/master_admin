package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Brand;
import com.hebela.masterdata.service.BrandService;
import com.hebela.web.AbstractStatusController;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/brand")
@ResponseBody
public class BrandController extends AbstractStatusController<Brand> {
    private final BrandService brandService;

    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @PostMapping({"/findWithExtends"})
    public List<Brand> findWithExtends(@RequestBody @Nullable Brand bo) {
        return this.brandService.findWithExtends(bo);
    }

    @PutMapping("/priority")
    public List<Brand> priority(@RequestBody List<Brand> brandList) {
        return brandService.doPriority(brandList);
    }

}

