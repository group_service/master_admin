package com.hebela.masterdata.web;

import com.hebela.masterdata.bo.Menu;
import com.hebela.web.AbstractStatusController;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/menu")
@ResponseBody
@Log4j2
public class MenuController extends AbstractStatusController<Menu> {
}


