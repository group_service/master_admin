package com.hebela.masterdata.web.order.merchant;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MerchantOrderUpdateRequest {
    private Integer status;
    private String code;
}
