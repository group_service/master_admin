package com.hebela.masterdata.web.order;

import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.AccountAddress;
import com.hebela.masterdata.bo.Order;
import com.hebela.masterdata.bo.OrderItem;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Builder
public class ShopViewOrderResponse {
    private String code;
    private Integer paymentMethod;
    private Integer paymentStatus;
    private Integer status;
    private Integer shippingFee;
    private Long couponDiscount;
    private Long voucherDiscount;
    private Long subTotal;
    private Long total;
    private List<OrderItemResponse> orderItemList;
    private AccountAddress accountAddress;
    private Long amountPrepay;
    private String codeOrderOld;
    private String note;
    private String description;
    private Instant createDate;
    private String voucherCode;
    private OrderAccountResponse account;
    private Boolean isOrderUnion;

    public static ShopViewOrderResponse toResponse(Order order, List<OrderItem> orderItemList) {
        if (order == null) {
            return null;
        }
        if (!User.getContext().getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
            order.setAccountAddress(null);
        }
        return ShopViewOrderResponse.builder()
                .code(order.getCode())
                .status(order.getStatus())
                .paymentMethod(order.getPaymentMethod())
                .paymentStatus(order.getPaymentStatus())
                .shippingFee(order.getShippingFee())
                .subTotal(order.getSubTotal())
                .total(order.getTotal())
                .couponDiscount(order.getCouponDiscount())
                .voucherDiscount(order.getVoucherDiscount())
                .orderItemList(orderItemList.stream().map(OrderItemResponse::toResponse).collect(Collectors.toList()))
                .accountAddress(order.getAccountAddress())
                .amountPrepay(order.getAmountPrepay())
                .codeOrderOld(order.getCodeOrderOld())
                .note(order.getNote())
                .description(order.getDescription())
                .voucherCode(order.getVoucherCode())
                .createDate(order.getCreatedDate())
                .account(order.getAccountResponse())
                .isOrderUnion(order.getIsOrderUnion())
                .build();
    }
}

@Builder
@Getter
class OrderItemResponse {
    private Integer type;
    private String name;
    private String imageThumbnail;
    private Integer price;
    private Integer count;
    private String sku;
    private Integer productId;
    private Integer comboId;

    public static OrderItemResponse toResponse(OrderItem orderItem) {
        if (orderItem == null) {
            return null;
        }
        if (!OrderItem.TYPE_COMBO.equals(orderItem.getType())) {
            return OrderItemResponse.builder()
                    .type(orderItem.getType())
                    .name(orderItem.getProduct().getName())
                    .imageThumbnail(orderItem.getProduct().getImageThumbnail())
                    .price(orderItem.getPrice())
                    .sku(orderItem.getProduct().getSku())
                    .count(orderItem.getCount())
                    .productId(orderItem.getProductId())
                    .build();
        } else {
            return OrderItemResponse.builder()
                    .type(orderItem.getType())
                    .sku(orderItem.getCombo().getSku())
                    .name(orderItem.getCombo().getName())
                    .imageThumbnail(orderItem.getCombo().getImageThumbnail())
                    .price(orderItem.getCombo().getPrice())
                    .count(orderItem.getCount())
                    .comboId(orderItem.getComboId())
                    .build();
        }
    }


}
