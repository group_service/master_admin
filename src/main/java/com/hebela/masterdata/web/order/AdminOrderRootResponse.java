package com.hebela.masterdata.web.order;

import com.hebela.masterdata.bo.OrderRoot;
import lombok.Builder;

import java.util.List;
import java.util.stream.Collectors;

@Builder
public class AdminOrderRootResponse {
    private Long total;
    private List<OrderResponse> orderList;

    public static AdminOrderRootResponse toResponse(OrderRoot orderRoot) {
        return AdminOrderRootResponse.builder()
                .total(orderRoot.getTotal())
                .orderList(orderRoot.getOrderList().stream().map(order -> OrderResponse.toResponse(order)).collect(Collectors.toList()))
                .build();
    }
}
