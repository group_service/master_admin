package com.hebela.masterdata.web.order;

import com.hebela.core.bo.Account;
import com.hebela.masterdata.util.AccountUtil;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class OrderAccountResponse {
    private Integer id;
    private String mobile;
    private String email;
    private String fullname;
    private String avatarUrl;

    public static OrderAccountResponse toResponse(Account account) {
        return OrderAccountResponse.builder()
                .id(account.getId())
                .mobile(AccountUtil.hideMobileFromAccount(account.getMobile()))
                .email(account.getEmail())
                .fullname(account.getFullname())
                .avatarUrl(account.getAvatarUrl())
                .build();
    }
}
