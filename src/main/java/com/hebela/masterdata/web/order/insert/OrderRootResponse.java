package com.hebela.masterdata.web.order.insert;

import com.hebela.masterdata.bo.OrderRoot;
import com.hebela.masterdata.bo.extend.OnlinePayment;
import com.hebela.masterdata.web.order.OrderResponse;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Getter
public class OrderRootResponse {
    private Long total;
    private Integer shippingFee;
    private Long subTotal;
    private Long couponDiscount;
    private Long voucherDiscount;
    private String orderRootCode;
    private String mrcOrderId;
    private List<OrderResponse> orderList;
    private OnlinePayment onlinePayment;

    public static OrderRootResponse toResponse(OrderRoot orderRoot, OnlinePayment onlinePayment) {
        return OrderRootResponse.builder()
                .total(orderRoot.getTotal())
                .shippingFee(orderRoot.getShippingFee())
                .subTotal(orderRoot.getSubTotal())
                .couponDiscount(orderRoot.getCouponDiscount())
                .voucherDiscount(orderRoot.getVoucherDiscount())
                .orderRootCode(orderRoot.getOrderRootCode())
                .mrcOrderId(orderRoot.getMrcOrderId())
                .orderList(orderRoot.getOrderList().stream().map(OrderResponse::toResponse).collect(Collectors.toList()))
                .onlinePayment(onlinePayment)
                .build();
    }
}
