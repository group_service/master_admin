package com.hebela.masterdata.web.order.insert;

import com.hebela.masterdata.bo.Order;
import com.hebela.masterdata.web.cart.cartview.CartViewRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class InsertOrderRequest extends CartViewRequest {
    private String voucherCode;
    private Integer paymentStatus;
    private List<Order> orderList;
}
