package com.hebela.masterdata.web.order;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CancelOrderRequest {
    private String code;
    private String cancelReason;
}
