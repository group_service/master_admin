package com.hebela.masterdata.web.order;

import com.hebela.core.HebelaException;
import com.hebela.core.bo.Organization;
import com.hebela.core.bo.User;
import com.hebela.core.obj.StatusInfo;
import com.hebela.kafka.InternalPublisher;
import com.hebela.masterdata.bo.Cart;
import com.hebela.masterdata.bo.Order;
import com.hebela.masterdata.bo.OrderRoot;
import com.hebela.masterdata.bo.Payment;
import com.hebela.masterdata.bo.extend.Checkout;
import com.hebela.masterdata.bo.extend.Platform;
import com.hebela.masterdata.payment.BaokimPayment;
import com.hebela.masterdata.payment.BaokimResponse;
import com.hebela.masterdata.service.CartService;
import com.hebela.masterdata.service.OrderService;
import com.hebela.masterdata.web.cart.checkout.CheckoutRequest;
import com.hebela.masterdata.web.order.insert.InsertOrderRequest;
import com.hebela.masterdata.web.order.insert.OrderRootResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/order")
@ResponseBody
public class OrderController {

    @Value("${hebela.shop.id}")
    private Integer hebelaShopId;
    private final BaokimPayment baokimPayment;
    private final OrderService orderService;
    private final CartService cartService;
    private final InternalPublisher internalPublisher;

    public OrderController(BaokimPayment baokimPayment, OrderService orderService, CartService cartService, InternalPublisher internalPublisher) {
        this.baokimPayment = baokimPayment;
        this.orderService = orderService;
        this.cartService = cartService;
        this.internalPublisher = internalPublisher;
    }

    @GetMapping("/statusInfo")
    public StatusInfo statusInfo() {
        return this.orderService.statusInfo();
    }

    @PostMapping("/insertOrderByAmin")
    public OrderRootResponse insert(HttpServletRequest request, @RequestBody InsertOrderRequest dto) {
        if (!User.getContext().getOrgId().equals(Organization.ROOT_ACCOUNT.getId())) {
            throw new HebelaException("action.invalid");
        }
        if (dto.getOrderList() == null || dto.getOrderList().size() == 0) {
            throw new HebelaException("order.list.invalid");
        }
        Integer platform = Platform.getPatform(request);
        for (Order order : dto.getOrderList()) {
            order.setSaleChannel(Order.SALE_CHANNEL_ADMIN);
            order.setPaymentMethod(Payment.TYPE_COD);
        }
        CheckoutRequest checkoutRequest = new CheckoutRequest();
        checkoutRequest.setVoucherCode(dto.getVoucherCode());
        // sắp xếp giỏ hàng, xếp combo lên đầu
        dto.getCartList().sort(new Comparator<Cart>() {
            @Override
            public int compare(Cart o1, Cart o2) {
                if (o1.getProductId() == null) {
                    return -1;
                }
                if (o2.getProductId() == null) {
                    return 1;
                }
                return o2.getProductId().compareTo(o1.getProductId());
            }
        });
        checkoutRequest.setCartList(dto.getCartList());
        checkoutRequest.setAccountAddressId(dto.getOrderList().get(0).getAccountAddressId());
        checkoutRequest.setPaymentMethod(dto.getOrderList().get(0).getPaymentMethod());
        checkoutRequest.setPlatform(platform);
        checkoutRequest.setGiftSelectedList(dto.getGiftSelectedList());
        checkoutRequest.setVoucherSelectedList(dto.getVoucherSelectedList());
        Checkout checkout = cartService.checkout(checkoutRequest);
        if (checkout == null) {
            return null;
        }
        if (checkout.getCartInvalid()) {
            throw new HebelaException("cart.quantity.invalid");
        }
        if (checkout.getVoucher() != null) {
            if (checkout.getVoucher().getCartErrorList().size() > 0) {
                throw new HebelaException("voucher.apply.failed");
            }
        }
        checkout.setVoucherSelectedList(checkoutRequest.getVoucherSelectedList());
        checkout.setPaymentStatus(dto.getPaymentStatus());
        OrderRoot orderRoot = orderService.doInsert(dto.getOrderList(), checkout);
        return OrderRootResponse.toResponse(orderRoot, null);
    }

    @PostMapping("/find")
    public Page<Order> find(@RequestBody AdminOrderFilterRequest model) {
        return orderService.findHebelaOrder(model);
    }

    @GetMapping("/get/{code}")
    public AdminOrderResponse get(@PathVariable String code) {
        return AdminOrderResponse.toResponse(orderService.getByCode(code, null));
    }

    @PostMapping("/cancel")
    public Order cancel(@RequestBody AdminCancelOrderRequest model) {
        return model.getAccountId() == null ? null : orderService.doCancel(model, model.getAccountId());
    }

    @PostMapping("/findAccount")
    public List<OrderAccountResponse> findAccount(@RequestParam String keyword) {
        return this.orderService.findAccount(keyword).stream().map(OrderAccountResponse::toResponse).collect(Collectors.toList());
    }

    @PostMapping("/refund")
    public void refund(@RequestBody RefundRequest dto) {
        OrderRoot orderRoot = orderService.getOrderToRefund(dto.getOrderRootCode(), dto.getAccountId());
        BaokimResponse refund = baokimPayment.refund(orderRoot);
        if (refund != null) {
            orderService.doRefund(orderRoot);
        }
    }

    /*@PostMapping("/publish")
    public List<Order> publish(@RequestBody List<Order> newOrderList) {
        for (Order dbOrder : newOrderList) {
            internalPublisher.send(InternalPublisher.ACTION_CREATE, dbOrder);
        }
        return newOrderList;
    }*/
    @PostMapping("/publish")
    public List<Order> publishPost(@RequestBody List<String> orderCodeList) {
        return orderService.publish(orderCodeList);
    }

    @GetMapping("/publish/{orderCode}")
    public Order publishGet(@PathVariable String orderCode) {
        return orderService.publish(orderCode);
    }
}

