package com.hebela.masterdata.web.order;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AdminCancelOrderRequest extends CancelOrderRequest {
    private Integer accountId;
}
