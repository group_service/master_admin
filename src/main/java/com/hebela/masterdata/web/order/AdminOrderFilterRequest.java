package com.hebela.masterdata.web.order;

import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Setter
@Getter
public class AdminOrderFilterRequest extends HebelaPage {
    private String keyWord;
    private Instant toDate;
    private Instant fromDate;
    private Integer status;
    private Integer merchantId;
    private Integer saleChannel;
    private Integer paymentStatus;
    private List<Integer> statusList;
}
