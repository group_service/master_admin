package com.hebela.masterdata.web.order.merchant;

import com.hebela.core.bo.User;
import com.hebela.core.obj.StatusInfo;
import com.hebela.masterdata.bo.Order;
import com.hebela.masterdata.bo.OrderRoot;
import com.hebela.masterdata.payment.BaokimPayment;
import com.hebela.masterdata.service.OrderService;
import com.hebela.masterdata.web.order.AdminOrderFilterRequest;
import com.hebela.masterdata.web.order.ShopViewOrderResponse;
import com.hebela.masterdata.web.order.insert.OrderRootResponse;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/merchant-order")
@ResponseBody
public class MerchantOrderController {
    private final OrderService orderService;
    private final BaokimPayment baokimPayment;

    public MerchantOrderController(OrderService orderService, BaokimPayment baokimPayment) {
        this.orderService = orderService;
        this.baokimPayment = baokimPayment;
    }

    @GetMapping("/statusInfo")
    public StatusInfo statusInfo() {
        return this.orderService.merchantStatusInfo();
    }

    @PostMapping("/find")
    public Page<Order> findByMerchant(@RequestBody AdminOrderFilterRequest model) {
        model.setMerchantId(User.getContext().getOrgId());
        return orderService.findMerchantOrder(model);
    }

    @GetMapping("/detail/{code}")
    public ShopViewOrderResponse detail(@PathVariable String code) {
        return orderService.detail(code);
    }

    @PostMapping("/cloneOrder")
    public OrderRootResponse cloneOrder(@RequestBody Order order) {
        return orderService.doCloneOrder(order);
    }

    @PostMapping("/refundDone/{orderCode}")
    public Order refundDone(@PathVariable String orderCode) {
        return orderService.doRefundDone(orderCode);
    }

    @PostMapping("/updateStatus")
    public void updateStatus(@RequestBody MerchantOrderUpdateRequest model) {
        OrderRoot orderRoot = orderService.doMerchantUpdateStatus(model);
        if (orderRoot != null) {
            orderRoot = orderService.getOrderToRefund(orderRoot.getOrderRootCode(), orderRoot.getAccountId());
            baokimPayment.refund(orderRoot);
            orderService.doRefund(orderRoot);
        }
    }

}
