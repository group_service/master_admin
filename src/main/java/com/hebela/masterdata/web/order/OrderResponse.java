package com.hebela.masterdata.web.order;

import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.bo.extend.Seller;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Dùng chung trang danh sách và tạo mới
 */
@Builder
@Getter
public class OrderResponse {
    private SellerResponse seller;
    private Long couponDiscount;
    private Long voucherDiscount;
    private String note;
    private Long total;
    private Long subTotal;
    private Integer shippingFee;
    private Integer paymentMethod;
    private AccountAddress accountAddress;
    private String code;
    private Instant paymentDeadline;
    private Instant createdDate;
    private Integer status;
    private PaymentResponse payment;
    private Boolean canReview;

    public static OrderResponse toResponse(Order order) {
        if (order == null) {
            return null;
        }
        return OrderResponse.builder()
                .seller(SellerResponse.toResponse(order.getSeller()))
                .couponDiscount(order.getCouponDiscount())
                .voucherDiscount(order.getVoucherDiscount())
                .note(order.getNote())
                .subTotal(order.getSubTotal())
                .total(order.getTotal())
                .shippingFee(order.getShippingFee())
                .paymentMethod(order.getPaymentMethod())
                .accountAddress(order.getAccountAddress())
                .code(order.getCode())
                .paymentDeadline(order.getPaymentDeadline())
                .createdDate(order.getCreatedDate())
                .status(order.getStatus())
                .canReview((order.getHasReview() == null || !order.getHasReview()) && Order.STATUS_COMPLETED.equals(order.getStatus()) && Instant.now().isBefore(order.getCreatedDate().plus(10, ChronoUnit.DAYS)))
                .payment(PaymentResponse.toResponse(order.getPaymentInfo()))
                .build();
    }
}

@Builder
class PaymentResponse {
    private String name;
    private Integer method;

    public static PaymentResponse toResponse(Payment payment) {
        if (payment == null) {
            return null;
        }
        return PaymentResponse.builder()
                .name(payment.getName())
                .method(payment.getMethod())
                .build();
    }
}

@Builder
class SellerResponse {
    private LinkedList<CouponResponse> groups;
    private Integer id;
    private String name;
    private String logo;
    private Long subTotal;
    private Long couponDiscount;
    private Long total;
    private Integer shippingFee;

    public static SellerResponse toResponse(Seller seller) {
        if (seller == null) {
            return null;
        }
        return SellerResponse.builder()
                .groups(seller.getGroup().stream().map(CouponResponse::toResponse).collect(Collectors.toCollection(LinkedList::new)))
                .id(seller.getId())
                .name(seller.getName())
                .logo(seller.getLogo())
                .subTotal(seller.getSubTotal())
                .couponDiscount(seller.getCouponDiscount())
                .shippingFee(seller.getShippingFee())
                .total(seller.getTotal())
                .build();
    }

}

@Builder
class CouponResponse {
    private Integer id;
    private String name;
    private Integer numberOfGift;
    private String type;
    private String couponLabel;
    private String couponLabelValue;
    private String discountType;
    private Integer discountPercent;
    private Integer buyMoneyMin;
    private Integer discountMoney;
    private Integer buyQuantityMin;
    private String conditionLabel;
    private String conditionLabelValue;
    LinkedList<CartResponse> cartList;
    List<Product> giftList;
    List<Product> selectedGift;


    public static CouponResponse toResponse(Coupon coupon) {
        return CouponResponse.builder()
                .id(coupon.getId())
                .name(coupon.getName())
                .type(coupon.getType())
                .couponLabel(coupon.getCouponLabel())
                .discountType(coupon.getDiscountType())
                .numberOfGift(coupon.getNumberOfGift())
                .couponLabelValue(coupon.getCouponLabelValue())
                .discountMoney(coupon.getDiscountMoney())
                .discountPercent(coupon.getDiscountPercent())
                .buyMoneyMin(coupon.getBuyMoneyMin())
                .buyQuantityMin(coupon.getBuyQuantityMin())
                .conditionLabel(coupon.getConditionLabel())
                .conditionLabelValue(coupon.getConditionLabelValue())
                .cartList(coupon.getCartList().stream().map(CartResponse::toResponse).collect(Collectors.toCollection(LinkedList::new)))
                .giftList(coupon.getGiftList())
                .selectedGift(coupon.getSelectedGift())
                .build();
    }
}

@Builder
class CartResponse {
    private Integer id;
    private Integer productId;
    private ProductResponse product;
    private Integer comboId;
    private ComboResponse productCombo;
    private Integer count;

    public static CartResponse toResponse(Cart cart) {
        return CartResponse.builder()
                .id(cart.getId())
                .productId(cart.getProductId())
                .product(ProductResponse.toResponse(cart.getProduct()))
                .comboId(cart.getComboId())
                .productCombo(ComboResponse.toResponse(cart.getProductCombo()))
                .count(cart.getCount())
                .build();
    }
}

@Builder
class ComboResponse {
    private Integer id;
    private String name;
    private String imageThumbnail;
    private Integer price;
    private Integer amount;
    private Boolean isFavorite;
    private List<ProductResponse> productList;
    private List<ProductGift> giftList;
    private String slug;
    private String sku;

    public static ComboResponse toResponse(ProductCombo combo) {
        if (combo == null) {
            return null;
        }
        return ComboResponse.builder()
                .id(combo.getId())
                .name(combo.getName())
                .imageThumbnail(combo.getImageThumbnail())
                .price(combo.getPrice())
                .amount(combo.getAmount())
                .isFavorite(combo.getIsFavorite())
                .productList(combo.getProductList().stream().map(ProductResponse::toResponse).collect(Collectors.toList()))
                .giftList(combo.getGiftList())
                .slug(combo.getSlug())
                .sku(combo.getSku())
                .build();
    }
}

@Builder
@Getter
class ProductResponse {
    private Integer id;
    private String name;
    private String imageThumbnail;
    private ProductPrice productPrice;
    private Integer amount;
    private Boolean isFavorite;
    private List<ProductGift> giftList;
    private Brand brand;
    private String slug;
    private String sku;

    public static ProductResponse toResponse(Product product) {
        if (product == null) {
            return null;
        }
        ProductResponse response = ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .imageThumbnail(product.getImageThumbnail())
                .productPrice(product.getProductPrice())
                .amount(product.getAmount())
                .isFavorite(product.getIsFavorite())
                .giftList(product.getGiftList())
                .brand(product.getBrand())
                .slug(product.getSlug())
                .sku(product.getSku())
                .build();
        if (response.getProductPrice() != null && response.getProductPrice().getPriceRegular() != null && response.getProductPrice().getPriceSale() != null) {
            int discount = response.getProductPrice().getPriceRegular() - response.getProductPrice().getPriceSale();
            int regular = response.getProductPrice().getPriceRegular();
            response.getProductPrice().setSalePercent((double) discount / (double) regular);
        }
        return response;
    }
}
