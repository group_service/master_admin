package com.hebela.masterdata.web.order;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RefundRequest {
    private String orderRootCode;
    private Integer accountId;
}
