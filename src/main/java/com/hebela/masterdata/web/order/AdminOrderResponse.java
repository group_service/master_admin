package com.hebela.masterdata.web.order;

import com.hebela.core.bo.Account;
import com.hebela.masterdata.bo.*;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Dùng chung trang danh sách và tạo mới
 */
@Builder
@Getter
public class AdminOrderResponse {
    private LinkedList<AdminCouponResponse> groups;
    private Long couponDiscount;
    private Integer voucherDiscount;
    private Long total;
    private Long subTotal;
    private Integer shippingFee;
    private Integer paymentMethod;
    private AccountAddress accountAddress;
    private String code;
    private Integer status;
    private Instant createdDate;
    private String note;
    private String description;
    private OrderAccountResponse account;

    public static AdminOrderResponse toResponse(Order order) {
        return AdminOrderResponse.builder()
                .groups(order.getSeller().getGroup().stream().map(AdminCouponResponse::toResponse).collect(Collectors.toCollection(LinkedList::new)))
                .couponDiscount(order.getCouponDiscount())
                .subTotal(order.getSubTotal())
                .total(order.getTotal())
                .shippingFee(order.getShippingFee())
                .paymentMethod(order.getPaymentMethod())
                .accountAddress(order.getAccountAddress())
                .code(order.getCode())
                .status(order.getStatus())
                .createdDate(order.getCreatedDate())
                .note(order.getNote())
                .description(order.getDescription())
                .account(order.getAccountResponse())
                .build();
    }
}

@Builder
class AdminCouponResponse {
    private Integer id;
    private String name;
    private Integer numberOfGift;
    private String type;
    private String couponLabel;
    private String couponLabelValue;
    private String discountType;
    private Integer discountPercent;
    private Integer buyMoneyMin;
    private Integer discountMoney;
    private Integer buyQuantityMin;
    LinkedList<AdminCartResponse> cartList;
    List<Product> giftList;
    List<Product> selectedGift;


    public static AdminCouponResponse toResponse(Coupon coupon) {
        return AdminCouponResponse.builder()
                .id(coupon.getId())
                .name(coupon.getName())
                .type(coupon.getType())
                .couponLabel(coupon.getCouponLabel())
                .discountType(coupon.getDiscountType())
                .numberOfGift(coupon.getNumberOfGift())
                .couponLabelValue(coupon.getCouponLabelValue())
                .discountMoney(coupon.getDiscountMoney())
                .discountPercent(coupon.getDiscountPercent())
                .buyMoneyMin(coupon.getBuyMoneyMin())
                .buyQuantityMin(coupon.getBuyQuantityMin())
                .cartList(coupon.getCartList().stream().map(cart -> AdminCartResponse.toResponse(cart)).collect(Collectors.toCollection(LinkedList::new)))
                .giftList(coupon.getGiftList())
                .selectedGift(coupon.getSelectedGift())
                .build();
    }
}

@Builder
class AdminCartResponse {
    private Integer id;
    private Integer productId;
    private AdminProductResponse product;
    private Integer comboId;
    private AdminComboResponse productCombo;
    private Integer count;

    public static AdminCartResponse toResponse(Cart cart) {
        return AdminCartResponse.builder()
                .id(cart.getId())
                .productId(cart.getProductId())
                .product(AdminProductResponse.toResponse(cart.getProduct()))
                .comboId(cart.getComboId())
                .productCombo(AdminComboResponse.toResponse(cart.getProductCombo()))
                .count(cart.getCount())
                .build();
    }
}

@Builder
class AdminComboResponse {
    private Integer id;
    private String name;
    private String imageThumbnail;
    private Integer price;
    private Boolean isFavorite;
    private List<AdminProductResponse> productList;

    public static AdminComboResponse toResponse(ProductCombo combo) {
        if (combo == null) {
            return null;
        }
        return AdminComboResponse.builder()
                .id(combo.getId())
                .name(combo.getName())
                .imageThumbnail(combo.getImageThumbnail())
                .price(combo.getPrice())
                .isFavorite(combo.getIsFavorite())
                .productList(combo.getProductList().stream().map(product -> AdminProductResponse.toResponse(product)).collect(Collectors.toList()))
                .build();
    }
}

@Builder
@Getter
class AdminProductResponse {
    private Integer id;
    private String name;
    private String imageThumbnail;
    private ProductPrice productPrice;
    private Boolean isFavorite;
    private List<ProductGift> giftList;

    public static final AdminProductResponse toResponse(Product product) {
        if (product == null) {
            return null;
        }
        AdminProductResponse response = AdminProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .imageThumbnail(product.getImageThumbnail())
                .productPrice(product.getProductPrice())
                .isFavorite(product.getIsFavorite())
                .giftList(product.getGiftList())
                .build();
        if (response.getProductPrice() != null && response.getProductPrice().getPriceRegular() != null && response.getProductPrice().getPriceSale() != null) {
            int discount = response.getProductPrice().getPriceRegular() - response.getProductPrice().getPriceSale();
            int regular = response.getProductPrice().getPriceRegular();
            response.getProductPrice().setSalePercent(Double.valueOf(discount) / Double.valueOf(regular));
        }
        return response;
    }
}
