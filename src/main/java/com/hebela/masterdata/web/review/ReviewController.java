package com.hebela.masterdata.web.review;

import com.hebela.masterdata.bo.Review;
import com.hebela.masterdata.service.ReviewService;
import com.hebela.web.AbstractStatusController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/review")
@ResponseBody
public class ReviewController extends AbstractStatusController<Review> {

    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @PostMapping("/approveReview/{id}")
    public Review approveReview(@PathVariable Integer id, @RequestBody Review bo) {
        bo.setId(id);
        return reviewService.doApprove(bo);
    }

    @PostMapping("/findBySearch")
    public Page<Review> findBySearch(@RequestBody @Valid ReviewSearchRequest dto) {
        Pageable pageable = PageRequest.of(dto.getPage(), dto.getSize(), Sort.Direction.DESC, Review._createdDate);
        return reviewService.findBySearch(dto, pageable);
    }

    @GetMapping("/getReviewDetail/{id}")
    public Review getReviewDetail(@PathVariable Integer id) {
        return reviewService.getReviewDetail(id);
    }
}

