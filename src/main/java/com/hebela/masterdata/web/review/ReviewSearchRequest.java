package com.hebela.masterdata.web.review;

import com.hebela.masterdata.bo.Review;
import com.hebela.masterdata.obj.HebelaPage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
@NoArgsConstructor
public class ReviewSearchRequest extends HebelaPage {
    private Integer orgId;
    private Integer productId;
    private Review review;
    private String keyword;
    private Instant toDate;
    private Instant fromDate;
    private Integer rating;
    private String sortBy;
    private Integer status;
    private Boolean isTrash;
}
