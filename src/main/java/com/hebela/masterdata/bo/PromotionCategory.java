package com.hebela.masterdata.bo;

import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.util.PriorityObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * PromotionCategory generated by hbm2java
 */
@Getter
@Setter
public class PromotionCategory extends PromotionCategoryBase implements PriorityObject {
    public static final Integer STATUS_ACTIVE = 1;
    public static final Integer STATUS_INACTIVE = 2;
    private PromotionConfig promotionConfig;
    private PromotionTimeFrame promotionTimeFrame;
    private List<ObjectImage> objectImageList;
    private List<HebelaError> errorList;

    public PromotionCategory() {
    }

    public PromotionCategory(Integer id) {
        this.id = id;
    }
}

