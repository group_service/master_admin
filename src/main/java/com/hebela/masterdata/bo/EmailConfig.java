package com.hebela.masterdata.bo;

import lombok.Getter;
import lombok.Setter;


/**
 * EmailTemplateConfig generated by hbm2java
 */
@Getter
@Setter
public class EmailConfig extends EmailConfigBase {

    public final static Integer DEACTIVE = 1;
    public final static Integer ACTIVE = 2;
    public final static Integer CANCEL = 4;
    public EmailTemplate emailTemplate;

    public EmailConfig() {
    }

    public EmailConfig(Integer id) {
        this.id = id;
    }
}

