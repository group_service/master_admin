package com.hebela.masterdata.bo;

import com.hebela.core.HebelaException;
import com.hebela.core.bo.Account;
import com.hebela.masterdata.bo.extend.Seller;
import com.hebela.masterdata.web.order.OrderAccountResponse;
import com.hebela.util.BeanUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Order generated by hbm2java
 */
@Setter
@Getter
public class Order extends OrderBase {
    public static final String CHAR_CODE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public static final Integer SALE_CHANNEL_APP = 1;
    public static final Integer SALE_CHANNEL_WEB = 2;
    public static final Integer SALE_CHANNEL_ADMIN = 4;
    public static final Integer STATUS_CANCEL_ENABLE = STATUS_PENDING;
    public static final Integer STATUS_ORDER_REFUND = 64;
    private Seller seller;
    private Integer sellerId;
    private Integer amount;
    private String imageThumbnail;
    private List<OrderItem> orderItemList;
    private Account account;
    private OrderAccountResponse accountResponse;
    private Payment paymentInfo;
    private LinkedList<Coupon> promotionInfo;
    private Seller sellerInfo;
    private AccountAddress accountAddress;
    private String codeOrderOld;
    private String paymentRef;
    private String voucherCode;
    private Boolean isOrderUnion;

    public Order() {
    }

    public Order(Order order, OrderRoot orderRoot) {
        copyData(order);
        this.createdDate = order.getCreatedDate();
        this.voucherCode = orderRoot.voucherCode;
    }

    public Order(Order order) {
        this.copyData(order);
    }

    public Order(Integer id) {
        this.id = id;
    }

    public Payment getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(Payment paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public LinkedList<Coupon> getPromotionInfo() {
        return promotionInfo;
    }

    public void setPromotionInfo(LinkedList<Coupon> promotionInfo) {
        this.promotionInfo = promotionInfo;
    }

    public Seller getSellerInfo() {
        return sellerInfo;
    }

    public void setSellerInfo(Seller sellerInfo) {
        this.sellerInfo = sellerInfo;
    }

    public AccountAddress getAccountAddress() {
        return accountAddress;
    }

    public void setAccountAddress(AccountAddress accountAddress) {
        this.accountAddress = accountAddress;
    }

    @Override
    public boolean equalsData(Object object) {
        Order bo = (Order) object;
        if (!BeanUtils.nullSafeEquals(paymentInfo, bo.paymentInfo)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(promotionInfo, bo.promotionInfo)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(sellerInfo, bo.sellerInfo)) {
            return false;
        }
        if (!BeanUtils.nullSafeEquals(accountAddress, bo.accountAddress)) {
            return false;
        }
        return true;
    }

    @Override
    public void copyData(Object object) {
        if (!(object instanceof Order)) {
            throw new HebelaException("error.instanceof");
        }
        if (this == object) {
            return;
        }
        Order bo = (Order) object;
        super.copyData(bo);
        paymentInfo = bo.paymentInfo;
        promotionInfo = bo.promotionInfo;
        sellerInfo = bo.sellerInfo;
        accountAddress = bo.accountAddress;
    }

    public void buildSeller(List<OrderItem> orderItemList) {
        List<OrderItem> sortedOrderItemList = orderItemList.stream().sorted(Comparator.comparing(OrderItem::getType)).collect(Collectors.toList());
        this.seller = BeanUtils.clone(this.getSellerInfo());
        seller.setGroup(BeanUtils.clone(this.getPromotionInfo()));
        this.setPromotionInfo(null);
        for (OrderItem orderItem : sortedOrderItemList) {
            if (OrderItem.TYPE_PRODUCT.equals(orderItem.getType()) || OrderItem.TYPE_COMBO.equals(orderItem.getType())) {
                seller.getGroup().get(orderItem.getCouponIndex()).getCartList().add(orderItem.toCart());
            }
            if (OrderItem.TYPE_PRODUCT_BY_COMBO_RETAIL.equals(orderItem.getType()) || OrderItem.TYPE_PRODUCT_BY_COMBO_PACKAGE.equals(orderItem.getType())) {
                seller.getGroup().get(orderItem.getCouponIndex()).getCartList()
                        .get(orderItem.getCartIndex()).getProductCombo().getProductList().add(orderItem.toProduct());
            }
            if (OrderItem.TYPE_COUPON_GIFT.equals(orderItem.getType())) {
                seller.getGroup().get(orderItem.getCouponIndex()).getGiftList().add(orderItem.getProduct());
            }
            if (OrderItem.TYPE_GIFT.equals(orderItem.getType())) {
                Product product = seller.getGroup().get(orderItem.getCouponIndex()).getCartList()
                        .get(orderItem.getCartIndex()).getProduct();
                if (product != null) {
                    product.getGiftList().add(orderItem.toProductGift());
                } else {
                    seller.getGroup().get(orderItem.getCouponIndex()).getCartList()
                            .get(orderItem.getCartIndex()).getProductCombo().getGiftList().add(orderItem.toProductGift());
                }
            }
        }
    }
}

