package com.hebela.masterdata.bo;

import com.hebela.masterdata.obj.ProductGroupModel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class GroupProductLink extends GroupProductLinkBase {
    public static final Integer DISPLAY_TYPE_IMAGE = 1;//Hiển thị ảnh
    public static final Integer DISPLAY_TYPE_NAME = 2;//Hiển thị giá trị thuộc tính
    public static final Integer DISPLAY_TYPE_PRICE = 4;//Hiển thị giá (thường kèm với giá trị thuộc tính)
    private String attributeName;
    private Integer displayType;
    private List<ProductGroupModel> productGroupModelList;

    public GroupProductLink() {
    }

    public GroupProductLink(Integer id) {
        this.id = id;
    }
}

