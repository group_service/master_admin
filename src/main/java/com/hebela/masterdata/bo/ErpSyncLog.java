package com.hebela.masterdata.bo;

public class ErpSyncLog extends ErpSyncLogBase {
    public static final String CREATE_ORDER = "create_order";
    public static final String SYNC_STATUS_ORDER = "sync_status_order";
    public static final String SYNC_PRODUCT_AMOUNT = "sync_product_amount";
}
