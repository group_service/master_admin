package com.hebela.masterdata.bo;

import com.hebela.masterdata.bo.extend.HebelaError;
import com.hebela.masterdata.bo.extend.VoucherAccountExtend;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.compress.utils.Lists;

import java.util.List;

/**
 * Voucher generated by hbm2java
 */
@Setter
@Getter
public class SuperVoucher extends SuperVoucherBase implements Comparable<SuperVoucher> {
    public static final String USER_TYPE_ALL = "all";
    public static final String USER_TYPE_SPECIFIED = "specified";
    public static final String DISCOUNT_TYPE_PERCENT = "percent";
    public static final String DISCOUNT_TYPE_MONEY = "money";
    public static final String FIXED_MONEY = "fixedMoney";
    public static final Integer STATUS_ACTIVE = 1;
    public static final Integer STATUS_INACTIVE = 2;
    //Sắp diễn ra
    public static final Integer STATUS_TIME_UPCOMING = 1;
    // Đang diễn ra
    public static final Integer STATUS_TIME_ONGOING = 2;
    // Đã kết thúc
    public static final Integer STATUS_TIME_FINISHED = 3;
    // Chừa dùng hết
    public static final Integer STATUS_USING = 1;
    // Đã dùng hết
    public static final Integer STATUS_USED = 2;
    private String campaignName;
    private List<VoucherAccountExtend> accountList;
    private VoucherAccountExtend voucherAccountExtend;
    private Boolean isEligible; // voucher có đủ điều kiện áp dụng không
    private List<HebelaError> errorList;
    private Product product;
    private List<Product> productList;

    /**
     * nếu isPresent là true thì voucher có lỗi
     */
    private List<String> cartErrorList = Lists.newArrayList();

    public SuperVoucher() {
    }

    public SuperVoucher(Integer id) {
        this.id = id;
    }

    public SuperVoucher(SuperVoucher superVoucher, String campaignName) {
        copyData(superVoucher);
        this.campaignName = campaignName;
    }

    @Override
    public int compareTo(SuperVoucher o) {
        int errorSize = o.getCartErrorList().size();
        int thisErroSize = this.getCartErrorList().size();
        return errorSize > thisErroSize ? -1 : 1;
    }
}

