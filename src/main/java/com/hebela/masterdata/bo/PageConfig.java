package com.hebela.masterdata.bo;

/**
 * PageConfig generated by hbm2java
 */
public class PageConfig extends PageConfigBase {
    public static final Integer STATUS_ACTIVE = 1; // Hoạt động
    public static final Integer STATUS_INACTIVE = 1 << 1; // Tạm dừng

    public static final String CODE_MESSAGE_TOP = "messageTop";
    public static final String CODE_HOTLINE = "hotline";
    public static final String CODE_SEARCH_KEYWORD_SUGGEST = "searchKeywordSuggest";
    public static final String CODE_APP_ANDROID_URL = "appAndroidUrl";
    public static final String CODE_APP_IOS_URL = "appIosUrl";
    public static final String CODE_APP_QR_IMAGE = "appQRImage";
    public static final String CODE_APP_PROMOTION_TITLE = "appPromotionTitle";
    public static final String CODE_APP_PROMOTION_MESSAGE = "appPromotionMessage";
    public static final String CODE_APP_PROMOTION_TOP_IMAGE = "appPromotionTopImage";
    public static final String CODE_APP_PROMOTION_POPUP_IMAGE = "appPromotionPopupImage";
    public static final String CODE_LOGO = "logo";

    public static final String CODE_SOCIAL_FACEBOOK_URL = "socialFacebookUrl";
    public static final String CODE_SOCIAL_GOOGLE_URL = "socialGoogleUrl";
    public static final String CODE_SOCIAL_YOUTUBE_URL = "socialYoutubeUrl";
    public static final String CODE_SOCIAL_TIKTOK_URL = "socialTikTokUrl";
    public static final String CODE_SOCIAL_TWITTER_URL = "socialTwitterUrl";
    public static final String CODE_SOCIAL_LINKEDIN_URL = "socialLinkedInUrl";
    public static final String CODE_SOCIAL_PRINTEREST_URL = "socialPinterestUrl";
    public static final String CODE_SOCIAL_ZALO_URL = "socialZaloUrl";
    public static final String CODE_SOCIAL_WHATSAPP_URL = "socialWhatsappUrl";
    public static final String CODE_SOCIAL_VIBER_URL = "socialViberUrl";
    public static final String CODE_SOCIAL_TELEGRAM_URL = "socialTelegramUrl";
    public static final String CODE_SOCIAL_INSTAGRAM_URL = "socialInstagramUrl";

    public static final String CODE_META_SITE_NAME = "metaSiteName";
    public static final String CODE_META_FB_APP_ID = "metaFbAppId";
    public static final String CODE_META_TITLE = "metaTitle";
    public static final String CODE_META_KEYWORDS = "metaKeywords";
    public static final String CODE_META_DESCRIPTION = "metaDescription";
    public static final String CODE_META_IMAGE = "metaImage";
    public static final String CODE_META_IMAGE_ALT = "metaImageAlt";

    public static final String CONFIG_TYPE_STORE = "store";
    public static final String CONFIG_TYPE_SEO = "seo";

    public PageConfig() {
    }

    public PageConfig(Integer id) {
        this.id = id;
    }
}

