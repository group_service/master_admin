package com.hebela.masterdata.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Landing generated by hbm2java
 */
@Getter
@Setter
public class Landing extends LandingBase {
    public static final Integer STATUS_ACTIVE = 1;
    public static final Integer STATUS_INACTIVE = 2;
    private MetaConfig metaConfig;

    public Landing() {
    }

    public Landing(Integer id) {
        this.id = id;
    }
}

