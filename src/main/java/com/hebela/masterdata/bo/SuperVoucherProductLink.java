package com.hebela.masterdata.bo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SuperVoucherProductLink extends SuperVoucherProductLinkBase {
    private String productName;
    private String imageThumbnail;
    private String sku;
    private Integer priceSale;

    public SuperVoucherProductLink() {
    }
}
