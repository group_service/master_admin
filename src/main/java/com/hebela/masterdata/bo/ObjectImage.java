package com.hebela.masterdata.bo;

/**
 * ObjectImage generated by hbm2java
 */
public class ObjectImage extends ObjectImageBase {
    public static final String OBJ_CLASS_PROMOTION_CATEGORY = PromotionCategory.class.getSimpleName();
    public static final String OBJ_CLASS_PROMOTION_TIME_FRAME = PromotionTimeFrame.class.getSimpleName();

    public ObjectImage() {
    }

    public ObjectImage(Integer id) {
        this.id = id;
    }
}

