package com.hebela.masterdata.bo.extend;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderExtend {
    private Integer orderId;
    private String orderCode;
    private Long orderValue;
    private Long coinCommission;
    private Boolean isGiveCoinCommission = Boolean.FALSE;
}
