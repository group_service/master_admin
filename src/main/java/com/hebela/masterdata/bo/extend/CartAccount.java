package com.hebela.masterdata.bo.extend;

import com.hebela.core.bo.Account;
import com.hebela.masterdata.bo.Cart;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Setter
@Getter
public class CartAccount {
    private Account account;
    private List<Cart> cartList;
    private Integer totalMoney;
    private Integer orderedCount;

    public CartAccount() {
    }

    public CartAccount(Integer accountId, String accountFullname) {
        this.account = new Account();
        this.account.setId(accountId);
        this.account.setFullname(accountFullname);
    }
}
