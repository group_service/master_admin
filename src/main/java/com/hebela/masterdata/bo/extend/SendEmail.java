package com.hebela.masterdata.bo.extend;

import com.hebela.core.Bo;
import com.hebela.core.bo.Account;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
public class SendEmail implements Bo<Integer> {

    //    'Khi có Publisher đăng ký'
    public final static Integer PUBLISHER_REGISTER = 1;
    //    'Khi duyệt thông tin thanh toán'
    public final static Integer UPDATE_PUBLISHER_PAYMENT_INFO = 2;
    //    'Khi duyệt thông tin thanh toán'
    public final static Integer APPROVE_PAYMENT_INFO = 3;
    //    'Khi không duyệt thông tin thanh toán'
    public final static Integer NOT_APPROVE_PAYMENT_INFO = 4;
    //    'Khi gửi report doanh thu tháng'
    public final static Integer REPORT_EVERY_MONTH = 5;
    //    'Khi duyệt hoa hồng'
    public final static Integer APPROVE_COMMISSION = 6;
    //    'Khi gửi yêu cầu rút tiền'
    public final static Integer WITHDRAW_MONEY_REQUEST = 7;
    //     'Khi gửi yêu cầu chuyển tiền'
    public final static Integer TRANSFER_MONEY_REQUEST = 8;
    //     'Khi hoàn thành giao dịch'
    public final static Integer TRANSACTION_DONE = 9;
    private Account account;
    private String email;
    private Integer type;

    @Override
    public Integer getId() {
        return null;
    }

    @Override
    public Integer getCreatedBy() {
        return null;
    }

    @Override
    public Instant getCreatedDate() {
        return null;
    }

    @Override
    public Integer getModifiedBy() {
        return null;
    }

    @Override
    public Instant getModifiedDate() {
        return null;
    }

    @Override
    public boolean equalsKey(Object o) {
        return false;
    }

    @Override
    public boolean equalsData(Object o) {
        return false;
    }

    @Override
    public void copyData(Object o) {

    }

    @Override
    public String getProperty(String s) {
        return null;
    }

    @Override
    public String getField(String s) {
        return null;
    }
}
