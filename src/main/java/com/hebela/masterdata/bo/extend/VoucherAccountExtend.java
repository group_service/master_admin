package com.hebela.masterdata.bo.extend;

import com.hebela.core.bo.Account;
import com.hebela.masterdata.util.AccountUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class VoucherAccountExtend {
    private Integer id;
    private String mobile;
    private String fullname;
    private String avatarUrl;

    public static VoucherAccountExtend toExtend(Account account) {
        VoucherAccountExtend accountExtend = new VoucherAccountExtend();
        accountExtend.setId(account.getId());
        accountExtend.setFullname(account.getFullname());
        accountExtend.setAvatarUrl(account.getAvatarUrl());
        accountExtend.setMobile(AccountUtil.hideMobileFromAccount(account.getMobile()));
        return accountExtend;
    }
}
