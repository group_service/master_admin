package com.hebela.masterdata.bo.extend;

import com.hebela.masterdata.bo.Coupon;
import com.hebela.masterdata.bo.ProductGift;
import com.hebela.masterdata.bo.Voucher;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.compress.utils.Lists;

import java.util.LinkedList;
import java.util.List;

@Setter
@Getter
public class Seller extends SellerBase {
    private LinkedList<Coupon> group;
    private Integer shippingFee;
    private Long total;
    private Long subTotal;
    private Long couponDiscount = 0L;
    private Long voucherDiscount = 0L;
    private Voucher voucher;
    private List<ProductGift> productVoucherGiftList = Lists.newArrayList();

    public Seller addTotal(Long add) {
        this.total += add;
        return this;
    }

    public Seller addSubTotal(Long add) {
        this.subTotal += add;
        return this;
    }

    public Seller addCouponDiscount(Long add) {
        this.couponDiscount += add;
        return this;
    }
}
