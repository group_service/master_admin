package com.hebela.masterdata.bo.extend;

import com.hebela.core.bo.Organization;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SellerBase extends Organization {
    private String logo;
}
