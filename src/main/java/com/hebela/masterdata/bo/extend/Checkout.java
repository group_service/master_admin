package com.hebela.masterdata.bo.extend;

import com.hebela.masterdata.bo.AccountAddress;
import com.hebela.masterdata.bo.Voucher;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.compress.utils.Lists;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Checkout extends CartView {
    Integer shippingFee;
    Integer paymentMethod;
    private Integer paymentStatus;
    AccountAddress accountAddress;
    List<Voucher> voucherSelectedList = Lists.newArrayList();

    public Checkout addShippingFee(Integer shippingFee) {
        this.shippingFee += shippingFee;
        return this;
    }
}
