package com.hebela.masterdata.bo.extend;

import com.hebela.core.bo.User;
import com.hebela.masterdata.bo.*;
import com.hebela.masterdata.util.ListProcess;
import com.hebela.util.BeanUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.compress.utils.Lists;

import java.util.*;

@Setter
@Getter
public class CartView {
    private Map<Integer, Integer> productAmountCountMap = new HashMap<>();
    private Map<Integer, Product> productMap = new HashMap<>();
    private Map<Integer, ProductCombo> productComboMap = new HashMap<>();
    private List<Seller> sellers = new ArrayList<>();
    private List<Integer> cartIdList = new ArrayList<>();
    private Voucher voucher;
    private Long couponDiscount;
    private Long voucherDiscount;
    private Long totalVoucherDiscountShop = 0l;
    private Boolean cartInvalid = Boolean.FALSE;
    private String warnMessage;
    /**
     * Tổng số tiền khách hàng phải trả sau khi trừ coupon, voucher
     */
    private Long total;
    /**
     * Tổng giá trị tiền hàng tính theo giá bán
     */
    private Long subTotal;

    public CartView() {
    }

    public Checkout toCheckout() {
        Checkout checkout = new Checkout();
        checkout.setSellers(this.sellers);
        checkout.setVoucher(this.voucher);
        checkout.setVoucherDiscount(this.voucherDiscount);
        checkout.setCouponDiscount(this.couponDiscount);
        checkout.setTotal(this.total);
        checkout.setSubTotal(this.subTotal);
        checkout.setCartIdList(this.cartIdList);
        checkout.setCartInvalid(this.cartInvalid);
        checkout.setWarnMessage(this.warnMessage);
        checkout.setTotalVoucherDiscountShop(this.totalVoucherDiscountShop);
        return checkout;
    }

    public void setFavorite(List<Integer> favoriteProduct, List<Integer> favoriteCombo) {
        this.sellers.forEach(
                seller -> seller.getGroup().forEach(
                        coupon -> coupon.getCartList().forEach(cart -> {
                            if (cart.getProductId() != null && favoriteProduct.contains(cart.getProductId())) {
                                cart.getProduct().setIsFavorite(Boolean.TRUE);
                            }
                            if (cart.getComboId() != null && favoriteCombo.contains(cart.getComboId())) {
                                cart.getProductCombo().setIsFavorite(Boolean.TRUE);
                            }
                        })
                )
        );
    }

    /**
     * Thêm quà ngoài coupon vào sản phẩm, combo
     */
    public void setProductGift(List<ProductGift> productGiftList, List<ProductGift> productComboGiftList) {
        this.sellers.forEach(seller -> seller.getGroup().forEach(coupon -> coupon.getCartList().forEach(cart -> {
                    if (cart.getProductId() != null) {
                        List<ProductGift> productGifts = ListProcess.filterByField(productGiftList, ProductGiftBase.class, ProductGift._productId, cart.getProductId());
                        cart.getProduct().setGiftList(productGifts);
                    }
                    if (cart.getComboId() != null) {
                        List<ProductGift> productGifts = ListProcess.filterByField(productComboGiftList, ProductGiftBase.class, ProductGift._productComboId, cart.getComboId());
                        cart.getProductCombo().setGiftList(productGifts);
                    }
                })
                )
        );
    }

    /**
     * Lấy danh sách sản phẩm  để tạo đơn hàng
     * Note: sử dụng for để lấy index
     * xử lý key trùng đối với các sản phẩm xuất hiện trong combo
     */
    public List<OrderItem> getOrderItems(Seller seller) {
        ArrayList<OrderItem> result = Lists.newArrayList();
        Map<String, OrderItem> resultMap = new HashMap<>();
        final String PREFIX_PRODUCT = "P";
        final String PREFIX_PRODUCT_IN_COMBO = "PIC";
        final String PREFIX_GIFT_IN_PRODUCT = "GIP";
        final String PREFIX_GIFT_IN_COMBO = "GIB";
        final String PREFIX_GIFT_IN_COUPON = "GIC";
        /**
         * xử lý coupon
         * */
        for (int i = 0; i < seller.getGroup().size(); i++) {
            Coupon coupon = seller.getGroup().get(i);
            for (int j = 0; j < coupon.getCartList().size(); j++) {
                Cart cart = coupon.getCartList().get(j);
                /** xử lý sản phẩm*/
                if (cart.getProductId() != null) {
                    OrderItem orderItem = resultMap.get(PREFIX_PRODUCT + cart.getProductId());
                    if (orderItem == null) {
                        orderItem = new OrderItem();
                        resultMap.put(PREFIX_PRODUCT + cart.getProductId(), orderItem);
                        orderItem.setCouponId(coupon.getId());
                        orderItem.setProductId(cart.getProductId());
                        Product product = BeanUtils.clone(cart.getProduct());
                        product.setGiftList(new ArrayList<>());
                        orderItem.setProduct(product);
                        orderItem.setPrice(cart.getProduct().getProductPrice().getPriceSale());
                        orderItem.setCount(cart.getCount());
                        orderItem.setType(OrderItem.TYPE_PRODUCT);
                        orderItem.setCouponIndex(i);
                        orderItem.setCartId(cart.getId());
                        orderItem.setCreatedBy(User.getContextId());
                    } else {
                        orderItem.setCount(orderItem.getCount() + cart.getCount());
                    }
                } else if (cart.getComboId() != null) {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setCouponId(coupon.getId());
                    ProductCombo combo = BeanUtils.clone(cart.getProductCombo());
                    combo.setProductList(new ArrayList<>());
                    combo.setGiftList(new ArrayList<>());
                    orderItem.setComboId(cart.getComboId());
                    orderItem.setCombo(combo);
                    orderItem.setPrice(combo.getPrice());
                    orderItem.setCount(cart.getCount());
                    orderItem.setType(OrderItem.TYPE_COMBO);
                    orderItem.setCouponIndex(i);
                    orderItem.setCartId(cart.getId());
                    orderItem.setCreatedBy(User.getContextId());
                    result.add(orderItem);
                }
                /** Xử lý sản phẩm trong combo */
                if (cart.getComboId() != null) {
                    ProductCombo combo = BeanUtils.clone(cart.getProductCombo());
                    for (Product product : combo.getProductList()) {
                        OrderItem productByComboItem = resultMap.get(PREFIX_PRODUCT_IN_COMBO + j + product.getId());
                        if (productByComboItem == null) {
                            productByComboItem = new OrderItem();
                            resultMap.put(PREFIX_PRODUCT_IN_COMBO + j + product.getId(), productByComboItem);
                            productByComboItem.setCouponId(coupon.getId());
                            productByComboItem.setProduct(product);
                            productByComboItem.setProductId(product.getId());
                            productByComboItem.setPrice(product.getProductPrice().getPriceSale());
                            productByComboItem.setType(ProductCombo.TYPE_FOR_PACKAGE.equals(
                                    combo.getType()) ? OrderItem.TYPE_PRODUCT_BY_COMBO_PACKAGE : OrderItem.TYPE_PRODUCT_BY_COMBO_RETAIL);
                            productByComboItem.setCouponIndex(i);
                            productByComboItem.setCartIndex(j);
                            productByComboItem.setCartId(cart.getId());
                            productByComboItem.setCount(cart.getCount());
                        } else {
                            productByComboItem.setCount(productByComboItem.getCount() + cart.getCount());
                        }
                    }
                }
                /**
                 * xử lý quà tặng gắn trực tiếp sản phẩm/combo
                 * */
                if (cart.getProductId() != null) {
                    for (ProductGift productGift : cart.getProduct().getGiftList()) {
                        OrderItem gift = resultMap.get(PREFIX_GIFT_IN_PRODUCT + productGift.getProductGiftId());
                        if (gift == null) {
                            gift = new OrderItem();
                            resultMap.put(PREFIX_GIFT_IN_PRODUCT + productGift.getProductGiftId(), gift);
                            gift.setCouponId(coupon.getId());
                            gift.setProduct(productGift.toProduct());
                            gift.setPrice(productGift.getPriceSale());
                            gift.setProductId(productGift.getProductGiftId());
                            gift.setType(OrderItem.TYPE_GIFT);
                            gift.setCouponIndex(i);
                            gift.setCartIndex(j);
                            gift.setCartId(cart.getId());
                            gift.setCount(Integer.min(productGift.getNumberOfGift(), productGift.getAmount()));
                        } else {
                            gift.setCount(gift.getCount() + Integer.min(productGift.getNumberOfGift(), productGift.getAmount()));
                        }
                    }
                } else if (cart.getComboId() != null) {
                    for (ProductGift productGift : cart.getProductCombo().getGiftList()) {
                        OrderItem gift = resultMap.get(PREFIX_GIFT_IN_COMBO + productGift.getProductGiftId());
                        if (gift == null) {
                            gift = new OrderItem();
                            resultMap.put(PREFIX_GIFT_IN_COMBO + productGift.getProductGiftId(), gift);
                            gift.setCouponId(coupon.getId());
                            gift.setProduct(productGift.toProduct());
                            gift.setProductId(productGift.getProductGiftId());
                            gift.setPrice(productGift.getPriceSale());
                            gift.setType(OrderItem.TYPE_GIFT);
                            gift.setCount(Integer.min(productGift.getNumberOfGift(), productGift.getAmount()));
                            gift.setCartId(cart.getId());
                            gift.setCouponIndex(i);
                            gift.setCartIndex(j);
                        } else {
                            gift.setCount(gift.getCount() + Integer.min(productGift.getNumberOfGift(), productGift.getAmount()));
                        }
                    }
                }
            }
            /** Xử lý coupon*/
            for (int j = 0; j < coupon.getSelectedGift().size(); j++) {
                Product gift = coupon.getSelectedGift().get(j);
                OrderItem orderItem = resultMap.get(PREFIX_GIFT_IN_COUPON + gift.getId());
                if (orderItem == null) {
                    orderItem = new OrderItem();
                    resultMap.put(PREFIX_GIFT_IN_COUPON + gift.getId(), orderItem);
                    orderItem.setCouponId(coupon.getId());
                    orderItem.setProductId(gift.getId());
                    orderItem.setProduct(gift);
                    /** tặng bao nhiêu quà */
                    if (Coupon.DISCOUNT_TYPE_GIFT_SAME.equals(coupon.getDiscountType())) {
                        orderItem.setCount(coupon.getNumberOfGift());
                    } else {
                        orderItem.setCount(1);
                    }
                    orderItem.setType(OrderItem.TYPE_COUPON_GIFT);
                    orderItem.setPrice(gift.getProductPrice() != null ? gift.getProductPrice().getPriceSale() : null);
                    orderItem.setCouponIndex(i);
                    orderItem.setCartIndex(j);
                    orderItem.setCartId(gift.getId());
                    orderItem.setCreatedBy(User.getContextId());
                } else {
                    /* tặng bao nhiêu quà */
                    if (Coupon.DISCOUNT_TYPE_GIFT_SAME.equals(coupon.getDiscountType())) {
                        orderItem.setCount(orderItem.getCount() + coupon.getNumberOfGift());
                    } else {
                        orderItem.setCount(orderItem.getCount() + 1);
                    }
                }
            }
        }
        result.addAll(resultMap.values());
        return result;
    }

    public LinkedList<Coupon> getCouponInfo(Seller seller) {
        /** không nên sử dụng stream để clone đối tượng, sử dụng BeanUtils để clone từng đối tượng thành phần */
        LinkedList<Coupon> result = new LinkedList<>();
        seller.getGroup().forEach(item -> {
            Coupon coupon = BeanUtils.clone(item);
            coupon.setCartList(new LinkedList<>());
            coupon.setGiftList(new ArrayList<>());
            coupon.setConditionLabelValue(null);
            coupon.setConditionLabel(null);
            result.add(coupon);
        });
        return result;
    }

    public CartView addTotal(Long add) {
        this.total += add;
        return this;
    }

    public CartView addSubTotal(Long add) {
        this.subTotal += add;
        return this;
    }

    public CartView addCouponDiscount(Long add) {
        this.couponDiscount += add;
        return this;
    }

    public Seller buildSellerInfo(Seller seller) {
        Seller sellerInfo = BeanUtils.clone(seller);
        sellerInfo.setGroup(new LinkedList<>());
        return sellerInfo;
    }
}
