package com.hebela.masterdata.bo.extend;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class OnlinePayment {

    public static final Integer STATUS_FAILED = 0;
    public static final Integer STATUS_SUCCESS = 1;

    private String message;
    private Integer status;
    private String paymentUrl;
}
