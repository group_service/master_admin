package com.hebela.masterdata.bo;

import com.hebela.util.PriorityObject;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
public class HelpItem extends HelpItemBase implements PriorityObject {

    private String categoryName;
    private MetaConfig metaConfig;

    public HelpItem() {
    }

    public HelpItem(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public HelpItem(HelpItem hi, String categoryName) {
        this.copyData(hi);
        this.categoryName = categoryName;
    }

    public HelpItem(Integer id) {
        this.id = id;
    }

    public HelpItem(Integer id, String name, String slug, Instant modifiedDate) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.modifiedDate = modifiedDate;
    }
}

