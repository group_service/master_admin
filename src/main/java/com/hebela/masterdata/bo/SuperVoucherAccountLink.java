package com.hebela.masterdata.bo;

public class SuperVoucherAccountLink extends SuperVoucherAccountLinkBase {
    public SuperVoucherAccountLink() {
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        return this.equalsData(object);
    }
}
