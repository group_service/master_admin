package com.hebela.masterdata.bo;

import com.hebela.util.PriorityObject;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Setter
@Getter
public class ProductCategory extends ProductCategoryBase implements PriorityObject {
    public static final String TYPE_COMBO = "combo";

    private List<ProductCategory> children;
    private Integer productCount;
    private Integer attributeCount;
    private List<Product> productList;
    private List<Attribute> attributeList;
    private String parentName;
    private MetaConfig metaConfig;
    private List<Integer> attributeIdList;

    public ProductCategory() {
    }

    public ProductCategory(Integer id) {
        this.id = id;
    }

    public ProductCategory(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public ProductCategory(ProductCategory productCategory, String name, Integer priority) {
        copyData(productCategory);
        if (priority != null) {
            this.priority = priority;
        }
    }

    public ProductCategory(ProductCategory productCategory, Number productCount) {
        copyData(productCategory);
        this.productCount = productCount.intValue();
    }

    public ProductCategory(ProductCategory productCategory, Number productCount, Number attributeCount) {
        copyData(productCategory);
        this.productCount = productCount.intValue();
        this.attributeCount = attributeCount.intValue();
    }

    public ProductCategory(Integer id, String name, String slug, Instant modifiedDate, String imageThumbnail, String imageFeature, String type) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.modifiedDate = modifiedDate;
        this.imageThumbnail = imageThumbnail;
        this.imageFeature = imageFeature;
        this.type = type;
    }
}
