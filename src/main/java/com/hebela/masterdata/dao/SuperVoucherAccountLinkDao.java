package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.SuperVoucherAccountLink;
import org.springframework.stereotype.Repository;

@Repository
public interface
SuperVoucherAccountLinkDao extends Dao<SuperVoucherAccountLink, Integer> {
}
