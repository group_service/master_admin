package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.VoucherGiftLink;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoucherGiftLinkDao extends Dao<VoucherGiftLink, Integer> {

    @Query("SELECT NEW Product(p,vgl,pp) FROM VoucherGiftLink vgl" +
            " INNER JOIN Product p ON vgl.productId = p.id" +
            " INNER JOIN ProductPrice pp ON p.id = pp.productId " +
            " WHERE vgl.voucherId = :voucherId " +
            "AND pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME")
    List<Product> findProductGiftVoucher(Integer voucherId);

    @Query("SELECT vgl FROM VoucherGiftLink vgl" +
            " WHERE vgl.voucherId IN :voucherIdList " +
            "AND vgl.productId = :productId")
    List<VoucherGiftLink> findVoucherGiftLinkByVoucherIdAndProductId(List<Integer> voucherIdList, Integer productId);
}

