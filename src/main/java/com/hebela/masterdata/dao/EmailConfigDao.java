package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.EmailConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailConfigDao extends Dao<EmailConfig, Integer> {
}

