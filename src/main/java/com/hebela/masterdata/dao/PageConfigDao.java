package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.PageConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface PageConfigDao extends Dao<PageConfig, Integer> {
}

