package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductFaqLink;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductFaqLinkDao extends Dao<ProductFaqLink, Integer> {
}

