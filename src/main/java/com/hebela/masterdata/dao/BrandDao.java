package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Brand;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandDao extends StatusDao<Brand> {

    @Query("select new Brand(b, (select count(p) from Product p where p.brandId = b.id))" +
            " from Brand b" +
            " where b.status = :status AND b.isTrash = :isTrash" +
            " order by b.name")
    List<Brand> findWithExtends(Integer status, Boolean isTrash);

    @Query("select distinct new Brand(b, rl.priority) from Brand b " +
            "join RecommendationLink rl on b.id =  rl.objectId " +
            "where b.isTrash = false " +
            "and rl.recommendationId = :recommendationId " +
            "and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_BRAND " +
            "order by rl.priority, rl.createdDate")
    List<Brand> findByRecommendationId(Integer recommendationId);
}

