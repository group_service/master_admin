package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ShippingFee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShippingFeeDao extends Dao<ShippingFee, Integer> {

    @Query("select distinct sf from ShippingFee sf " +
            "left join ShippingFeeLocation sfl on sf.id = sfl.shippingFeeId " +
            "where ((sf.moneyMin >= :moneyMin and sf.moneyMin < :moneyMax) or (sf.moneyMin <= :moneyMin and sf.moneyMax > :moneyMin)) " +
            "and (:shippingFeeId is null or sf.id <> :shippingFeeId) " +
            "and sf.locationType = com.hebela.masterdata.bo.ShippingFee.LOCATION_TYPE_PROVINCE " +
            "and sfl.provinceIdInclude = :provinceId")
    List<ShippingFee> findShippingFeeByInclude(Integer provinceId, Integer moneyMin, Integer moneyMax, Integer shippingFeeId);

    @Query("select distinct sf from ShippingFee sf " +
            "left join ShippingFeeLocation sfl on sf.id = sfl.shippingFeeId " +
            "where ((sf.moneyMin >= :moneyMin and sf.moneyMin < :moneyMax) or (sf.moneyMin <= :moneyMin and sf.moneyMax > :moneyMin)) " +
            "and (:shippingFeeId is null or sf.id <> :shippingFeeId) " +
            "and sf.locationType = com.hebela.masterdata.bo.ShippingFee.LOCATION_TYPE_ALL " +
            "and sfl.provinceIdExclude is null")
    List<ShippingFee> findShippingFeeByExcludeAll(Integer moneyMin, Integer moneyMax, Integer shippingFeeId);

    @Query("select distinct sf.id from ShippingFee sf " +
            "left join ShippingFeeLocation sfl on sf.id = sfl.shippingFeeId " +
            "where ((sf.moneyMin >= :moneyMin and sf.moneyMin < :moneyMax) or (sf.moneyMin <= :moneyMin and sf.moneyMax > :moneyMin)) " +
            "and (:shippingFeeId is null or sf.id <> :shippingFeeId) " +
            "and sf.locationType = com.hebela.masterdata.bo.ShippingFee.LOCATION_TYPE_ALL " +
            "and sfl.provinceIdExclude = :provinceId")
    List<Integer> findShippingFeeIdExclude(Integer provinceId, Integer moneyMin, Integer moneyMax, Integer shippingFeeId);

    @Query("select distinct sf from ShippingFee sf " +
            "where ((sf.moneyMin >= :moneyMin and sf.moneyMin < :moneyMax) or (sf.moneyMin <= :moneyMin and sf.moneyMax > :moneyMin)) " +
            "and (:shippingFeeId is null or sf.id <> :shippingFeeId) " +
            "and sf.id not in :shippingFeeIdList " +
            "and sf.locationType = com.hebela.masterdata.bo.ShippingFee.LOCATION_TYPE_ALL")
    List<ShippingFee> findShippingFeeByNotInExclude(List<Integer> shippingFeeIdList, Integer moneyMin, Integer moneyMax, Integer shippingFeeId);

    @Query("select distinct sf from ShippingFee sf " +
            "where ((sf.moneyMin >= :moneyMin and sf.moneyMin < :moneyMax) or (sf.moneyMin <= :moneyMin and sf.moneyMax > :moneyMin)) " +
            "and (:shippingFeeId is null or sf.id <> :shippingFeeId) " +
            "and sf.locationType = com.hebela.masterdata.bo.ShippingFee.LOCATION_TYPE_ALL")
    List<ShippingFee> findShippingFeeByExclude(Integer moneyMin, Integer moneyMax, Integer shippingFeeId);


    @Query("select distinct sfl.provinceIdInclude from ShippingFee sf " +
            "left join ShippingFeeLocation sfl on sf.id = sfl.shippingFeeId " +
            "where sf.locationType = com.hebela.masterdata.bo.ShippingFee.LOCATION_TYPE_PROVINCE " +
            "and (:shippingFeeId is null or sf.id <> :shippingFeeId) " +
            "and ((sf.moneyMin >= :moneyMin and sf.moneyMin < :moneyMax) or (sf.moneyMin <= :moneyMin and sf.moneyMax > :moneyMin))")
    List<Integer> findProvinceInclude(Integer moneyMin, Integer moneyMax, Integer shippingFeeId);

    @Query("select distinct sfl.provinceIdExclude from ShippingFee sf " +
            "left join ShippingFeeLocation sfl on sf.id = sfl.shippingFeeId " +
            "where sf.locationType = com.hebela.masterdata.bo.ShippingFee.LOCATION_TYPE_ALL " +
            "and (:shippingFeeId is null or sf.id <> :shippingFeeId) " +
            "and ((sf.moneyMin >= :moneyMin and sf.moneyMin < :moneyMax) or (sf.moneyMin <= :moneyMin and sf.moneyMax > :moneyMin))")
    List<Integer> findProvinceExclude(Integer moneyMin, Integer moneyMax, Integer shippingFeeId);

}

