package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.CouponGiftLink;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CouponGiftLinkDao extends Dao<CouponGiftLink, Integer> {

    @Query("select cgl from CouponGiftLink cgl where cgl.couponId in (:couponId) and cgl.productId = :productId")
    List<CouponGiftLink> findAllByCouponIdsAndProductId(List<Integer> couponId, Integer productId);
}

