package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Review;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewDao extends StatusDao<Review> {

    @Query("select new Review(r, a.fullname, a.avatarUrl) from Review r " +
            " join Account a on r.accountId = a.id " +
            " where r.id = :reviewId")
    Review getReviewById(Integer reviewId);
}

