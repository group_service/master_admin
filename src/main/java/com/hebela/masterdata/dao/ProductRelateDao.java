package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductRelate;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRelateDao extends Dao<ProductRelate, Integer> {
}

