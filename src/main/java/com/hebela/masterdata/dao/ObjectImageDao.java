package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ObjectImage;
import org.springframework.stereotype.Repository;

@Repository
public interface ObjectImageDao extends ObjectImageSaveDao, Dao<ObjectImage, Integer> {
}

