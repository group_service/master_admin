package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.OrderTransaction;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderTransactionDao extends Dao<OrderTransaction, Integer> {
}
