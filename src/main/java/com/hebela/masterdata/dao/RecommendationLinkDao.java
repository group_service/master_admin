package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.RecommendationLink;
import org.springframework.stereotype.Repository;

@Repository
public interface RecommendationLinkDao extends Dao<RecommendationLink, Integer> {
}

