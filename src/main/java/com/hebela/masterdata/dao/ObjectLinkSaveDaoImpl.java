package com.hebela.masterdata.dao;

import com.hebela.core.HebelaException;
import com.hebela.masterdata.bo.ObjectLink;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

public class ObjectLinkSaveDaoImpl implements ObjectLinkSaveDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void customUpdate(String objectClass, Integer objectClassId, Integer userId, ObjectLink objectLinkForClient) {
        ObjectLink objectLinkDb = getObjectLink(objectClass, objectClassId).orElse(null);
        if (objectLinkDb != null) {
            if (this.isClearObjectLink(objectLinkForClient)) {
                // delete objectLink
                entityManager.remove(objectLinkDb);
            } else {
                // update objectLink
                objectLinkDb.setObjectType(objectLinkForClient.getObjectType());
                objectLinkDb.setObjectId(objectLinkForClient.getObjectId());
                objectLinkDb.setUrl(null);
                objectLinkDb.setTarget(objectLinkForClient.getTarget() != null ? objectLinkForClient.getTarget() : ObjectLink.TARGET_IN_TAB);
                setObjectIdByObjectType(objectLinkForClient.getObjectType(), objectLinkForClient.getUrl(), objectLinkDb);
                objectLinkDb.setModifiedBy(userId);
                entityManager.merge(objectLinkDb);
            }
        } else {
            // insert objectLink
            ObjectLink objectLink = new ObjectLink();
            objectLink.setObjectClass(objectClass);
            objectLink.setObjectClassId(objectClassId);
            String objectType = objectLinkForClient.getObjectType();
            objectLink.setObjectType(objectType);
            objectLink.setObjectId(objectLinkForClient.getObjectId());
            setObjectIdByObjectType(objectType, objectLinkForClient.getUrl(), objectLink);
            objectLink.setTarget(objectLinkForClient.getTarget() != null ? objectLinkForClient.getTarget() : ObjectLink.TARGET_IN_TAB);
            objectLink.setCreatedBy(userId);
            entityManager.persist(objectLink);
        }
    }

    private boolean isClearObjectLink(ObjectLink bo) {
        if (bo == null) {
            return true;
        }
        return bo.getObjectType() == null && bo.getObjectId() == null;
    }

    private void setObjectIdByObjectType(String objectType, String url, ObjectLink objectLinkSave) {
        if (ObjectLink.TYPE_CUSTOM_LINK.equals(objectType)) {
            objectLinkSave.setUrl(url);
            objectLinkSave.setObjectId(null);
        }
        String objectClass = objectLinkSave.getObjectClass();
        if (ObjectLink.TYPE_ALL_BRAND.equals(objectType) || ObjectLink.TYPE_ALL_COUPON.equals(objectType)) {
            objectLinkSave.setObjectId(null);
        } else {
            if (ObjectLink.TYPE_RECOMMENDATION.equals(objectType)) {
                if (!ObjectLink.OBJ_CLASS_BANNER_ITEM.equals(objectClass) && !ObjectLink.OBJ_CLASS_MENUITEM.equals(objectClass)) {
                    objectLinkSave.setObjectId(null);
                }
            }
        }
    }

    @Override
    public Optional<ObjectLink> getObjectLink(String objectClass, Integer objectClassId) {
        if (objectClass == null || objectClassId == null) {
            throw new HebelaException("object.link.missing.objectClass");
        }
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ObjectLink> query = builder.createQuery(ObjectLink.class);
        Root<ObjectLink> root = query.from(ObjectLink.class);
        query.select(root);
        query.where(
                builder.and(
                        builder.equal(root.get(ObjectLink._objectClass), objectClass),
                        builder.equal(root.get(ObjectLink._objectClassId), objectClassId))
        );
        try {
            return Optional.of(entityManager.createQuery(query).getSingleResult());
        } catch (NoResultException var3) {
            return Optional.empty();
        }
    }
}
