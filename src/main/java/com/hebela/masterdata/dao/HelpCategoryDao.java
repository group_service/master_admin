package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.HelpCategory;
import org.springframework.stereotype.Repository;

@Repository
public interface HelpCategoryDao extends StatusDao<HelpCategory> {
}

