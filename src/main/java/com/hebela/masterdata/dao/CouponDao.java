package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.Coupon;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
public interface CouponDao extends Dao<Coupon, Integer> {

    @Query("select distinct c from Coupon c " +
            "left join CouponProductLink cpl on c.id = cpl.couponId " +
            "where cpl.productId = :productId")
    List<Coupon> findByProductId(Integer productId);

    @Query("select distinct c from Coupon c " +
            "left join  CouponGiftLink cgl on c.id = cgl.couponId " +
            "where cgl.productId = :productGiftId")
    List<Coupon> findByProductGiftId(Integer productGiftId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select c from Coupon c where c.id in (:couponIdList)")
    List<Coupon> lockByIds(List<Integer> couponIdList);

    @Query("select distinct new Coupon(c, rl.priority) from Coupon c" +
            " left join RecommendationLink rl on c.id = rl.objectId" +
            " where rl.recommendationId = :recommendationId " +
            " and c.isTrash = false " +
            " and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_COUPON " +
            " order by rl.priority, rl.createdDate")
    List<Coupon> findByRecommendationIdForAdmin(Integer recommendationId);
}

