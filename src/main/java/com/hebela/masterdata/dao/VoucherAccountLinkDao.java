package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.VoucherAccountLink;
import org.springframework.stereotype.Repository;

@Repository
public interface VoucherAccountLinkDao extends Dao<VoucherAccountLink, Integer> {
}

