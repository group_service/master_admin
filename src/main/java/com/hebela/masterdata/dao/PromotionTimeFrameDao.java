package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.PromotionTimeFrame;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionTimeFrameDao extends Dao<PromotionTimeFrame, Integer> {
}

