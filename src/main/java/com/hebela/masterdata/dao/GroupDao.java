package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Group;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupDao extends StatusDao<Group> {

    @Query("select g from Group g where g.isTrash = :isTrash and g.orgId = :orgId ")
    List<Group> findAllGroups(Boolean isTrash, Integer orgId);
}

