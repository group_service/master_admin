package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.PromotionProduct;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface PromotionProductDao extends Dao<PromotionProduct, Integer> {

    @Query("SELECT new PromotionProduct(pp, p, ppr) " +
            "FROM PromotionProduct pp " +
            "JOIN Product p on pp.productId = p.id " +
            "JOIN ProductPrice ppr on p.id = ppr.productId " +
            "WHERE pp.categoryId = :categoryId " +
            "AND (:orgId = 0 OR pp.orgId = :orgId) " +
            "AND ppr.startDate <= CURRENT_TIME and ppr.endDate > CURRENT_TIME " +
            "ORDER BY pp.priority, pp.createdDate")
    List<PromotionProduct> findPromotionProduct(Integer categoryId, Integer orgId);

    @Query("SELECT new PromotionProduct(pp, p, pc, pcf, ppr) " +
            "FROM PromotionProduct pp " +
            "JOIN PromotionTimeFrame ptf on ptf.id = pp.timeFrameId  " +
            "JOIN Product p on pp.productId = p.id " +
            "JOIN ProductPrice ppr on p.id = ppr.productId " +
            "JOIN PromotionCategory pc on pp.categoryId = pc.id " +
            "JOIN PromotionConfig pcf on pc.id = pcf.categoryId " +
            "WHERE ptf.id = :timeFrameId " +
            "AND (:categoryId is null or pp.categoryId = :categoryId) " +
            "AND pp.status in (:status) " +
            "AND ppr.startDate <= CURRENT_TIME and ppr.endDate > CURRENT_TIME " +
            "AND (:keyword is null or p.name like :keyword or p.sku like :keyword) " +
            "AND (:type is null or pcf.type = :type)" +
            "ORDER BY pc.id, pp.priority, pp.createdDate")
    List<PromotionProduct> findBySearch(Integer timeFrameId, Integer categoryId, List<Integer> status, Integer type, String keyword);

    @Query("select distinct new PromotionProduct(pp, p, ptf) " +
            "from PromotionProduct pp " +
            "join Promotion ptf on pp.promotionId = ptf.id " +
            "join Product p on pp.productId = p.id " +
            "where (ptf.startTime between :starTime and :endTime " +
            "or ptf.endTime between :starTime and :endTime " +
            "or (ptf.startTime <= :starTime and ptf.endTime >= :endTime)) " +
            "and ptf.id <> :promotionId " +
            "and ptf.status = com.hebela.masterdata.bo.Promotion.STATUS_ACTIVE " +
            "and pp.productId in (:productIds)")
    List<PromotionProduct> findFlashSaleByProductId(Integer promotionId, List<Integer> productIds, Instant starTime, Instant endTime);

    @Query("select distinct new PromotionProduct(pp, p, ptf) " +
            "from PromotionProduct pp " +
            "join Promotion ptf on pp.promotionId = ptf.id " +
            "join Product p on pp.productId = p.id " +
            "where (ptf.startTime between :starTime and :endTime " +
            "or ptf.endTime between :starTime and :endTime " +
            "or (ptf.startTime <= :starTime and ptf.endTime >= :endTime)) " +
            "and ptf.id <> :promotionId " +
            "and ptf.status = com.hebela.masterdata.bo.Promotion.STATUS_ACTIVE " +
            "and pp.productId in (:productIds) " +
            "and pp.id not in (:promotionProductIds)")
    List<PromotionProduct> findFlashSaleByProductId(Integer promotionId, List<Integer> productIds, List<Integer> promotionProductIds, Instant starTime, Instant endTime);

    @Query("select distinct new PromotionProduct(pp, p) " +
            "from PromotionProduct pp " +
            "join Product p on pp.productId = p.id " +
            "where pp.promotionId = :promotionId " +
            "and pp.timeFrameId = :timeFrameId " +
            "and pp.categoryId in (:categoryIds) " +
            "and pp.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE " +
            "and pp.productId in (:productIds) ")
    List<PromotionProduct> findByPromotionIdAndTimeFrameId(Integer promotionId, Integer timeFrameId, List<Integer> productIds, List<Integer> categoryIds);

    @Query("select distinct new PromotionProduct(pp, p) " +
            "from PromotionProduct pp " +
            "join Product p on pp.productId = p.id " +
            "where pp.promotionId = :promotionId " +
            "and pp.timeFrameId = :timeFrameId " +
            "and pp.productId in (:productIds) " +
            "and pp.categoryId in (:categoryIds) " +
            "and pp.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE " +
            "and pp.id not in (:promotionProductIds) ")
    List<PromotionProduct> findByPromotionIdAndTimeFrameId(Integer promotionId, Integer timeFrameId, List<Integer> productIds, List<Integer> promotionProductIds, List<Integer> categoryIds);

    @Query("select COALESCE(MAX(pp.priority), 0) from PromotionProduct pp where pp.categoryId = :categoryId")
    Integer getPriorityMaxByCategoryId(Integer categoryId);
}

