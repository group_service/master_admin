package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.OrderItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemDao extends Dao<OrderItem, Integer> {

    List<OrderItem> findByAccountId(Integer accountId);

    List<OrderItem> findByOrderIdIn(List<Integer> orderIdList);
}

