package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.Feedback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackDao extends Dao<Feedback, Integer> {

    @Query("SELECT DISTINCT NEW Feedback(fb, acc) FROM Feedback fb LEFT JOIN Account acc ON fb.accountId = acc.id " +
            "ORDER BY fb.createdDate DESC ")
    Page<Feedback> findPage(Pageable pageable);
}

