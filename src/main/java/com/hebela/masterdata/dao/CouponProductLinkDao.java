package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.CouponProductLink;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CouponProductLinkDao extends Dao<CouponProductLink, Integer> {

    List<CouponProductLink> findAllByCouponId(Integer couponId);

    @Query("select cpl from CouponProductLink cpl where cpl.couponId in (:couponId) and cpl.productId = :productId")
    List<CouponProductLink> findAllByCouponIdsAndProductId(List<Integer> couponId, Integer productId);

}

