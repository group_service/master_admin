package com.hebela.masterdata.dao;

import com.hebela.masterdata.bo.ObjectImage;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ObjectImageSaveDaoImpl implements ObjectImageSaveDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void customUpdate(ObjectImage objectImageDb, Integer userId, ObjectImage objectImageForClient) {
        if (objectImageDb != null) {
            if (this.isClearObjectLink(objectImageForClient)) {
                // delete objectLink
                entityManager.remove(objectImageDb);
            } else {
                // update objectLink
                objectImageDb.setPlatform(objectImageForClient.getPlatform());
                objectImageDb.setModifiedBy(userId);
                entityManager.merge(objectImageDb);
            }
        } else {
            // insert objectLink
            ObjectImage objectImage = new ObjectImage();
            objectImage.setObjectClass(objectImageForClient.getObjectClass());
            objectImage.setObjectClassId(objectImageForClient.getObjectClassId());
            objectImage.setDisplayType(objectImageForClient.getDisplayType());
            objectImage.setImageUrl(objectImageForClient.getImageUrl());
            objectImage.setPlatform(objectImageForClient.getPlatform());
            objectImage.setPriority(1);
            objectImage.setCreatedBy(userId);
            entityManager.persist(objectImage);
        }
    }

    private boolean isClearObjectLink(ObjectImage bo) {
        if (bo == null) {
            return true;
        }
        return bo.getDisplayType() == null && bo.getPlatform() == null;
    }
}
