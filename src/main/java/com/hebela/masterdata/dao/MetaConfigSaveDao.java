package com.hebela.masterdata.dao;

import com.hebela.masterdata.bo.MetaConfig;

public interface MetaConfigSaveDao {
    void customUpdate(Integer id, Integer type, Integer userId, MetaConfig metaConfigForClient);
}
