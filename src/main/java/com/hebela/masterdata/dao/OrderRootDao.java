package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.OrderRoot;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;

@Repository
public interface OrderRootDao extends Dao<OrderRoot, Integer> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    OrderRoot findDistinctByOrderRootCode(String orderRootCode);

    OrderRoot getOrderRootByOrderRootCode(String orderRootCode);
}

