package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.GroupProductLink;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.obj.ProductGroupModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupProductLinkDao extends Dao<GroupProductLink, Integer> {

    /*Get Group Detail: Lấy danh sách thuộc tính của group*/
    @Query("select distinct gpl.attributeId from GroupProductLink gpl " +
            "where gpl.groupId = :groupId")
    List<Integer> findAttributeIdByGroup(Integer groupId);

    /*Get Group Detail: Lấy danh sách sản phẩm của group*/
    @Query("select distinct p from Product p " +
            "join GroupProductLink gpl on p.id = gpl.productId " +
            "where gpl.groupId = :groupId")
    List<Product> findProductByGroup(Integer groupId);

    @Query("select distinct new com.hebela.masterdata.obj.ProductGroupModel(p.id, p.slug, p.name,p.imageThumbnail,pp.priceSale,ad) from Product p " +
            "join ProductPrice pp on p.id = pp.productId " +
            "join ProductAttributeLink pal on p.id = pal.productId " +
            "join GroupProductLink gpl on gpl.productAttributeLinkId = pal.id " +
            "join AttributeData ad on pal.attributeDataId = ad.id " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and gpl.groupId = :groupId " +
            "and gpl.attributeId = :attributeId " +
            "order by p.createdDate asc")
    List<ProductGroupModel> findProductGroupModel(Integer groupId, Integer attributeId);

    /*Lấy danh sách Group Product để xóa các GroupProduct có product đc thêm vào group khác */
    @Query("select gpl from GroupProductLink gpl  " +
            "where gpl.groupId <> :groupId " +
            "and gpl.productId in (:prductList)")
    List<GroupProductLink> findByProductList(Integer groupId, List<Integer> prductList);
}

