package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.PromotionCategory;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionCategoryDao extends Dao<PromotionCategory, Integer> {
}

