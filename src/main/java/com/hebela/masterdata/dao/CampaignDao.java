package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Campaign;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignDao extends StatusDao<Campaign> {
    @Query("SELECT DISTINCT cam FROM Campaign cam " +
            "WHERE cam.isTrash = :isTrash AND cam.status = :status " +
            "AND (com.hebela.masterdata.bo.Campaign.ORGANIZATION_ROOT = :orgId OR cam.orgId = :orgId) ORDER BY cam.createdDate DESC")
    List<Campaign> findAll(Boolean isTrash, Integer status, Integer orgId);

    @Query("SELECT DISTINCT cam FROM Campaign cam " +
            "WHERE cam.isTrash = :isTrash AND cam.status = :status " +
            "AND cam.orgId <> com.hebela.masterdata.bo.Campaign.ORGANIZATION_ROOT ORDER BY cam.createdDate DESC")
    List<Campaign> findAllShop(Boolean isTrash, Integer status);
}

