package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.extend.Seller;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerDao extends Dao<Seller, Integer> {
}

