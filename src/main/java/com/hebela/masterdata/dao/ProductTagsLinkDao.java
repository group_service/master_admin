package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductTagsLink;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductTagsLinkDao extends Dao<ProductTagsLink, Integer> {
}

