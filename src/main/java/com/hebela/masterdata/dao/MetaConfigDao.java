package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.MetaConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface MetaConfigDao extends MetaConfigSaveDao, Dao<MetaConfig, Integer> {
}
