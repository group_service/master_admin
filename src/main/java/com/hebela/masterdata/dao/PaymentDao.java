package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Payment;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentDao extends StatusDao<Payment> {
    Payment findFirstByMethod(Integer method);
}

