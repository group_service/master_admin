package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.ReviewSummary;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewSummaryDao extends StatusDao<ReviewSummary> {
}

