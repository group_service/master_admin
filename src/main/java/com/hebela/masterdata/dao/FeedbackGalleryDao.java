package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.FeedbackGallery;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackGalleryDao extends Dao<FeedbackGallery, Integer> {
}

