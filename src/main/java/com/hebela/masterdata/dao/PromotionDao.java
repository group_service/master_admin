package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Promotion;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionDao extends StatusDao<Promotion> {
}

