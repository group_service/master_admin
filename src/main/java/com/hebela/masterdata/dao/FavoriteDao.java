package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.Favorite;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoriteDao extends Dao<Favorite, Integer> {
    @Query("select productId from Favorite where accountId = :accountId and productId in (:productIdList)")
    List<Integer> isFavorite(Integer accountId, List<Integer> productIdList);

    @Query("select comboId from Favorite where accountId = :accountId and comboId in (:comboIdList)")
    List<Integer> isFavoriteCombo(Integer accountId, List<Integer> comboIdList);

    List<Integer> findByAccountIdAndProductId(Integer accountId, Integer productId);

    List<Integer> findByAccountIdAndComboId(Integer accountId, Integer comboId);

}

