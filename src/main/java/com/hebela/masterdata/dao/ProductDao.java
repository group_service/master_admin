package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Set;

@Repository
public interface ProductDao extends StatusDao<Product> {

    @Query("select new Product(p, pp, b, rl.priority) from Product p " +
            "join ProductPrice pp on p.id = pp.productId " +
            "join RecommendationLink rl on p.id = rl.objectId " +
            "and rl.recommendationId = :recommendationId " +
            "and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_PRODUCT " +
            "left join Brand b on p.brandId = b.id " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and p.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE " +
            "and p.isTrash = false " +
            "and p.type = com.hebela.masterdata.bo.Product.TYPE_SELL " +
            "order by rl.priority, rl.createdDate")
    List<Product> findByRecommendationId(Integer recommendationId);

    @Query("select new Product(p, pp, pc, b) from Product p " +
            "join ProductPrice pp on p.id = pp.productId " +
            "join ProductCategory pc on p.productCategoryId = pc.id " +
            "left join Brand b on p.brandId = b.id " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and p.isTrash = :isTrash " +
            "and (:orgId = 0 or p.orgId = :orgId) " +
            "and p.status in (:status) " +
            "and (p.name like :term or p.sku like :term) " +
            "and (:categoryId is null or p.productCategoryId = :categoryId) " +
            "and (:brandId is null or p.brandId = :brandId) " +
            "and (:type is null or p.type = :type) " +
            "order by p.createdDate desc"
    )
    Page<Product> findBySearch(Integer orgId, String term, Integer categoryId, Integer brandId, Integer type, Boolean isTrash, List<Integer> status, Pageable pageable);

    @Query("select new Product(p, pp) from Product p " +
            "join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and p.amount > 0 " +
            "and p.orgId = :orgId " +
            "and (:query is null or p.name like %:query%) " +
            "and p.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE " +
            "and p.type = com.hebela.masterdata.bo.Product.TYPE_SELL and p.isTrash = false " +
            "and p.id not in (select r.productId from ProductComboLink r where r.comboId = :comboId)")
    List<Product> findProductForCombo(Integer orgId, @Param("query") String query, Integer comboId);

    @Query("select new Product(p, pp, b) from Product p " +
            "join ProductPrice pp on p.id = pp.productId " +
            "left join Brand b on p.brandId = b.id " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and (:query is null or p.name like :query) " +
            "and p.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE " +
            "and p.type = com.hebela.masterdata.bo.Product.TYPE_SELL " +
            "and p.productCategoryId in (:categoryIdList) " +
            "and p.isTrash = false")
    Page<Product> findByFilter(@Param("query") String query, List<Integer> categoryIdList, Pageable pageable);

    /**
     * Lấy danh sách sản phẩm cho:
     * - thêm vào giỏ hàng
     * - thanh toán
     * - danh sách yêu thích
     */
    @Query("select new Product(p, pp) from Product p " +
            "left join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and p.id = :productId " +
            "and p.type = com.hebela.masterdata.bo.Product.TYPE_SELL " +
            "and p.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE and p.isTrash = false")
    Product getDetail(Integer productId);


    /**
     * Lấy thông tin sản phẩm cho:
     * - giỏ hàng của khách
     */
    @Query("select new Product(p, pp) from Product p " +
            "left join ProductPrice pp on p.id = pp.productId " +
            "where p.id = :productId ")
    Product getDetailForCart(Integer productId);

    /**
     * Lấy sản phẩm /quà tặng cho:
     * - chi tiết sản phẩm
     */
    @Query("select new Product(p, pp) from Product p " +
            "left join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and p.id = :productId " +
            "and p.isTrash = false")
    Product getProductAndGift(Integer productId);

    List<Product> findAllByProductCategoryIdAndType(Integer productCategoryId, Integer type);

    @Query("select p from Product p where p.id in (:productIdList) and p.isTrash = false")
    List<Product> getGiftByIds(List<Integer> productIdList);

    @Query("select new Product(p, pp) from Product p " +
            "left join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and (p.name like %:term% or p.sku like %:term% )" +
            "and p.isTrash = false " +
            "and p.amount > 0 " +
            "and p.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE " +
            "and p.type = com.hebela.masterdata.bo.Product.TYPE_SELL")
    List<Product> findProductForOrder(String term);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select p from Product p where p.id in (:productIdList)")
    List<Product> lockByIds(Set<Integer> productIdList);

    Product findDistinctBySku(String sku);

    /**
     * Lấy sản phẩm có giá
     */
    @Query("select new Product(p, pp) from Product p " +
            "join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and p.id in :productIdList ")
    List<Product> findProductWithPrice(List<Integer> productIdList);

    @Query("SELECT NEW Product(p, pp) FROM Product p " +
            "JOIN ProductPrice pp ON p.id = pp.productId " +
            "WHERE p.orgId = :orgId " +
            "AND p.id IN :productIdList " +
            "AND pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME")
    List<Product> findProductOfOrg(Integer orgId, List<Integer> productIdList);


    @Query("SELECT DISTINCT p FROM ProductCombo pc INNER JOIN ProductComboLink pcl ON pc.id = pcl.comboId " +
            "INNER JOIN Product p ON (p.id = pcl.productId AND p.orgId = pc.orgId) " +
            "WHERE pc.orgId = :orgId")
    List<Product> findProductInCombo(Integer orgId);

    @Query("SELECT DISTINCT p FROM Coupon c INNER JOIN CouponGiftLink cgl ON c.id = cgl.couponId " +
            "INNER JOIN Product p ON (p.id = cgl.productId AND p.orgId = c.orgId) " +
            "WHERE p.orgId = :orgId")
    List<Product> findProductInCoupon(Integer orgId);
}

