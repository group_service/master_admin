package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.Unit;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitDao extends Dao<Unit, Integer> {
}

