package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCategory;
import com.hebela.masterdata.bo.ProductCombo;
import com.hebela.masterdata.bo.Recommendation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecommendationDao extends StatusDao<Recommendation> {

    @Query(value = "select rc from Recommendation rc where rc.name like %:term% and rc.isTrash = false")
    List<Recommendation> findByTerm(String term);

//    @Query("select new Recommendation(g.id, g.name) from Recommendation g where g.name like %:query% and  g.id not in " +
//            "(select l.recommendationId from RecommendationLink l where l.productId = :productId )")
//    List<Recommendation> findRecommendationAble(@Param("query") String query, Integer productId);

    @Query("select r" +
            " from Recommendation r" +
            " where r.status = :status AND r.isTrash = :isTrash" +
            " order by r.priority ASC, r.createdDate DESC")
    List<Recommendation> findWithExtends(Integer status, Boolean isTrash);

    /**
     * tìm sản phẩm gợi ý cho app/web
     */
    @Query("select new Product(p, pp, b) from Recommendation pg " +
            "join RecommendationLink rl on pg.id = rl.recommendationId " +
            "join Product p on rl.objectId = p.id " +
            "join ProductPrice pp on p.id = pp.productId " +
            "left join Brand b on p.brandId = b.id " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and pg.id = :recommendationId and p.status = com.hebela.masterdata.bo.Product.STATUS_ACTIVE " +
            "and p.type = com.hebela.masterdata.bo.Product.TYPE_SELL and p.isTrash = false " +
            "and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_PRODUCT " +
            "order by rl.priority, rl.createdDate")
    Page<Product> findProduct(Integer recommendationId, Pageable pageable);

//    @Query("select pc from Recommendation pg " +
//            "join RecommendationLink pgl on pg.id = pgl.recommendationId " +
//            "join ProductCategory pc on pgl.productCategoryId = pc.id where pg.id = :recommendationId")
//    List<ProductCategory> findProductCategory(Integer recommendationId);

    @Query("select pc from Recommendation pg " +
            "join RecommendationLink rl on pg.id = rl.recommendationId " +
            "join ProductCategory pc on rl.objectId = pc.id " +
            "where pg.id = :recommendationId " +
            "and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_PRODUCT_CATEGORY " +
            "order by rl.priority, rl.createdDate")
    List<ProductCategory> findProductCategory(Integer recommendationId);

    @Query("select pc from Recommendation pg " +
            "join RecommendationLink rl on pg.id = rl.recommendationId " +
            "join ProductCombo pc on rl.objectId = pc.id " +
            "where pg.id = :recommendationId " +
            "and pc.isTrash = false " +
            "and pc.status = com.hebela.masterdata.bo.ProductCombo.STATUS_ACTIVE " +
            "and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_PRODUCT_COMBO " +
            "order by rl.priority, rl.createdDate")
    Page<ProductCombo> findCombo(Integer recommendationId, Pageable pageable);

}

