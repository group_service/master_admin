package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDao extends StatusDao<Order> {
    Order findDistinctByShippingCodeAndOdooCode(String shippingCode, String odooCode);

    Order findDistinctByCode(String code);

    Order getOrderByCode(String code);

    List<Order> findOrderByOrderRootId(Integer orderRootId);
}

