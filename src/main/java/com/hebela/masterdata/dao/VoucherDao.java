package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Voucher;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
public interface VoucherDao extends StatusDao<Voucher> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select v from Voucher v where v.code = :voucherCode and v.isTrash = false")
    Voucher lockByCode(String voucherCode);

    @Query("SELECT DISTINCT v FROM Voucher v LEFT JOIN VoucherGiftLink vgl ON v.id = vgl.voucherId WHERE vgl.productId = :productId")
    List<Voucher> findVoucherByProductId(Integer productId);
}

