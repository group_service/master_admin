package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ShippingFeeLocation;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingFeeLocationDao extends Dao<ShippingFeeLocation, Integer> {
}

