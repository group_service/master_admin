package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.MenuItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuItemDao extends StatusDao<MenuItem> {

    @Query("SELECT distinct new MenuItem(mi, ol) FROM MenuItem mi" +
            " LEFT JOIN ObjectLink ol ON ol.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_MENUITEM " +
            "                        AND ol.objectClassId = mi.id" +
            " WHERE mi.menuId = :menuId AND mi.status = :status AND mi.isTrash = :isTrash" +
            " ORDER BY mi.level ASC, mi.priority ASC, mi.createdDate DESC")
    List<MenuItem> findWithExtends(Integer menuId, Integer status, boolean isTrash);
}
