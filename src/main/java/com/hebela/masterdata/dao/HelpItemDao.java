package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.HelpItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HelpItemDao extends StatusDao<HelpItem> {
    @Query("select new HelpItem(hi, hc.name) from HelpItem hi " +
            "left join HelpCategory hc on hi.helpCategoryId = hc.id " +
            "where hi.isTrash = :isTrash " +
            "order by hc.priority asc, hi.priority asc")
    List<HelpItem> findByPriority(Boolean isTrash);

    @Query("select hi from HelpItem hi where hi.helpCategoryId in (:helpCategoryId) and hi.isTrash = false")
    List<HelpItem> findHelpItemByHelpCategoryIdIn(List<Integer> helpCategoryId);
}

