package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.core.bo.Account;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountDao extends Dao<Account, Integer> {

    //Tìm kiếm khách hàng mua đơn hàng view cho tài khoản shop
    Account getAccountById(Integer id);
}

