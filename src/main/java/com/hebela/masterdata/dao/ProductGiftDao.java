package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductGift;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductGiftDao extends Dao<ProductGift, Integer> {
    @Query("select pg from ProductGift  pg  where  pg.orgId = :orgId order by pg.productId, pg.productComboId, pg.priority, pg.createdDate")
    List<ProductGift> findGift(Integer orgId);

    List<ProductGift> findAllByProductGiftId(Integer productGiftId);

    @Query("select distinct p.productId from ProductGift p where p.productId is not null")
    List<Integer> findProductIdInProductGift();

    @Query("select distinct p.productComboId from ProductGift p where p.productComboId is not null")
    List<Integer> findProductComboIdInProductGift();

}

