package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.AttributeData;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttributeDataDao extends Dao<AttributeData, Integer> {

    List<AttributeData> findByAttributeIdIn(List<Integer> attributeIdList);

}

