package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.AccountAddress;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountAddressDao extends Dao<AccountAddress, Integer> {
    AccountAddress findByAccountIdAndIsDefaultTrue(Integer accountId);
}

