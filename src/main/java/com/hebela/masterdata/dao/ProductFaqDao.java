package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.ProductFaq;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductFaqDao extends StatusDao<ProductFaq> {
}

