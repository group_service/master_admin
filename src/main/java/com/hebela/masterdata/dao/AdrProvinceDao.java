package com.hebela.masterdata.dao;


import com.hebela.core.Dao;
import com.hebela.masterdata.bo.AdrProvince;
import org.springframework.stereotype.Repository;

@Repository
public interface AdrProvinceDao extends Dao<AdrProvince, Integer> {
}
