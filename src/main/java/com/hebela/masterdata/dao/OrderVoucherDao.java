package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.OrderVoucher;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderVoucherDao extends Dao<OrderVoucher, Integer> {
}

