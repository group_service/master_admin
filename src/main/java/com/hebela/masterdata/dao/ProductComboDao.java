package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Product;
import com.hebela.masterdata.bo.ProductCombo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Set;

@Repository
public interface ProductComboDao extends StatusDao<ProductCombo> {
    @Query("select new Product(p, pp, pcl.comboId) from Product p " +
            "join ProductComboLink pcl on p.id = pcl.productId " +
            "join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and pcl.comboId in (:productComboIdList)")
    List<Product> findAllProduct(List<Integer> productComboIdList);

    @Query("select distinct new ProductCombo(pc, rl.priority) from ProductCombo pc" +
            " left join RecommendationLink rl on pc.id = rl.objectId" +
            " where rl.recommendationId = :recommendationId " +
            " and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_PRODUCT_COMBO " +
            " and pc.isTrash = false " +
            " and pc.status = com.hebela.masterdata.bo.ProductCombo.STATUS_ACTIVE " +
            " order by rl.priority, rl.createdDate")
    List<ProductCombo> findByRecommendationId(Integer recommendationId);

    @Query(value = "select pc from ProductCombo pc " +
            "where (pc.name like %:term% or pc.sku like %:term%) " +
            "and pc.isTrash = false " +
            "and pc.status = com.hebela.masterdata.bo.ProductCombo.STATUS_ACTIVE " +
            "and (pc.orgId = :orgId or 0 = :orgId)")
    List<ProductCombo> findByTerm(Integer orgId, String term);

    /**
     * Tìm sản phẩm nằm trong combo xác định
     */
    @Query("select new Product(p, pp, pcl) from Product p " +
            "join ProductComboLink pcl on p.id = pcl.productId " +
            "join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and (p.type = com.hebela.masterdata.bo.Product.TYPE_SELL " +
            "or p.type = com.hebela.masterdata.bo.Product.TYPE_PRODUCT_IN_COMBO) " +
            "and pcl.comboId = :comboId")
    List<Product> findProduct(Integer comboId);

    /**
     * Tìm sản phẩm nằm trong combo xác định trong combo
     */
    @Query("select new Product(p, pp, pcl) from Product p " +
            "join ProductComboLink pcl on p.id = pcl.productId " +
            "join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and (p.type = com.hebela.masterdata.bo.Product.TYPE_SELL " +
            "or p.type = com.hebela.masterdata.bo.Product.TYPE_PRODUCT_IN_COMBO) " +
            "and pcl.comboId = :comboId")
    List<Product> findProductInCombo(Integer comboId);

    @Query("select pc from ProductCombo pc " +
            "join ProductComboLink pcl on pc.id = pcl.comboId " +
            "where pcl.productId = :productId " +
            "and (:isTrash is null or pc.isTrash = :isTrash)" +
            "and (:status is null or pc.status = :status)")
    List<ProductCombo> findByProductId(Integer productId, Integer status, Boolean isTrash, Pageable pageable);

    @Query("select pc from ProductCombo pc" +
            "   where pc.status = com.hebela.masterdata.bo.ProductCombo.STATUS_ACTIVE" +
            "       and pc.isTrash = false" +
            "       and pc.id in :comboIds")
    List<ProductCombo> getByIds(List<Integer> comboIds);

    /*Tìm kiếm combo trang admin*/
    @Query("select distinct pc from ProductCombo pc " +
            "join ProductCategory pcy on pc.productCategoryId = pcy.id " +
            "left join ProductComboLink pcl on pc.id = pcl.comboId " +
            "left join Product p on pcl.productId = p.id " +
            "where pc.status in :status " +
            "and (:orgId = 0 or p.orgId = :orgId) " +
            "and (:keyword is null or (pc.name like %:keyword% or p.name like %:keyword%)) " +
            "and (:sku is null or (pc.sku like %:sku% or p.sku like %:sku%)) " +
            "and (:barcode is null or (pc.barcode like %:barcode%)) " +
            "and (:isTrash is null or pc.isTrash = :isTrash) " +
            "and (:type is null or pc.type = :type) " +
            "and (:categoryId is null or pc.productCategoryId = :categoryId) " +
            "order by pc.priority, pc.id desc")
    Page<ProductCombo> findBySearch(Integer orgId, String keyword, String sku, String barcode, Boolean isTrash,
                                    Integer type, Integer categoryId, List<Integer> status, Pageable pageable);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select p from ProductCombo p where p.id in (:comboIdList)")
    List<ProductCombo> lockByIds(Set<Integer> comboIdList);

    ProductCombo findDistinctBySku(String sku);
}

