package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ObjectLink;
import org.springframework.stereotype.Repository;

@Repository
public interface ObjectLinkDao extends ObjectLinkSaveDao, Dao<ObjectLink, Integer> {
}

