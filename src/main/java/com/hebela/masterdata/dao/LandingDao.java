package com.hebela.masterdata.dao;

import com.hebela.core.OrgStatusDao;
import com.hebela.masterdata.bo.Landing;
import org.springframework.stereotype.Repository;

@Repository
public interface LandingDao extends OrgStatusDao<Landing> {
}

