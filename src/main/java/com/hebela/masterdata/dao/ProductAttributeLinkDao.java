package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.ProductAttributeLink;
import com.hebela.masterdata.obj.AttributeModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductAttributeLinkDao extends StatusDao<ProductAttributeLink> {
    @Query("select new ProductAttributeLink(p.id,pp.priceSale,p.imageThumbnail, a.name) " +
            "from ProductAttributeLink pal join Product p on p.id = pal.productId " +
            "join Attribute a on a.id = pal.attributeId " +
            "join ProductPrice pp on p.id = pp.productId " +
            "where pp.startDate <= CURRENT_TIME and pp.endDate > CURRENT_TIME " +
            "and pal.attributeId = :attributeId " +
            "and pal.id in (:productAttributeLinkIdList)")
    List<ProductAttributeLink> findProductLink(Integer attributeId, List<Integer> productAttributeLinkIdList);

    List<ProductAttributeLink> findAllByProductId(Integer productId);

    @Query("select new ProductAttributeLink(pal.id, at.id, at.name) from ProductAttributeLink pal " +
            "join  Attribute at on pal.attributeId = at.id where pal.productId = :productId")
    List<ProductAttributeLink> findAttribute(Integer productId);

    /*Lấy danh sách thuộc tính và các giá trị để hiển thị tại trang chi tiết sản phẩm*/
    @Query("select new com.hebela.masterdata.obj.AttributeModel(pal.productId, at.name, ad) " +
            "from ProductAttributeLink pal join  Attribute at on pal.attributeId = at.id " +
            "join  AttributeData ad on pal.attributeDataId = ad.id " +
            "where pal.productId = :productId " +
            "and at.status = 1 " +
            "and at.isTrash = false " +
            "order by at.priority asc, at.createdDate desc"
    )
    List<AttributeModel> findAttributeModel(Integer productId);
}

