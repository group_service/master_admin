package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Publisher;
import com.hebela.masterdata.bo.extend.CartAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublisherDao extends StatusDao<Publisher> {

    @Query("SELECT NEW Publisher(pl, ad.fullname) FROM Publisher pl " +
            "JOIN Admin ad ON pl.adminId = ad.id " +
            "WHERE pl.isTrash = :isTrash " +
            "AND pl.status IN (:statusList) " +
            "AND (:keyword IS NULL OR (pl.affCode LIKE :keyword OR ad.fullname LIKE :keyword)) " +
            "AND (:publisherType IS NULL OR pl.type = :publisherType) " +
            "ORDER BY pl.createdDate DESC")
    Page<Publisher> findPublisher(String keyword, Integer publisherType, Boolean isTrash, List<Integer> statusList, Pageable pageable);

    @Query("SELECT NEW Publisher(pl, ad.fullname) FROM Publisher pl " +
            "JOIN Admin ad ON pl.adminId = ad.id " +
            "WHERE 1 = 1 " +
            "AND (:publisherId is null or pl.id = :publisherId) " +
            "AND (:adminId is null or pl.adminId = :adminId)"
    )
    Publisher getPublisher(Integer publisherId, Integer adminId);

}

