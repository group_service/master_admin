package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.PromotionConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionConfigDao extends Dao<PromotionConfig, Integer> {
}

