package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Menu;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuDao extends StatusDao<Menu> {
}

