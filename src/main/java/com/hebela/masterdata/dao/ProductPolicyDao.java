package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.ProductPolicy;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductPolicyDao extends StatusDao<ProductPolicy> {
}

