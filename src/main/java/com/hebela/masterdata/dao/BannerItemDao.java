package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.BannerItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BannerItemDao extends StatusDao<BannerItem> {

    @Query("SELECT distinct new BannerItem(bi, ol) FROM BannerItem bi" +
            " LEFT JOIN ObjectLink ol ON ol.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_BANNER_ITEM " +
            "                        AND ol.objectClassId = bi.id" +
            " WHERE bi.bannerId = :bannerId " +
            " AND bi.isTrash = :isTrash" +
            " ORDER BY bi.priority ASC, bi.createdDate DESC")
    List<BannerItem> findWithBannerId(Integer bannerId, boolean isTrash);
}

