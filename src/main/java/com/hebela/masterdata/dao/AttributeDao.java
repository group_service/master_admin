package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Attribute;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttributeDao extends StatusDao<Attribute> {
    /*Lấy danh sách thuộc tính có data theo danh mục*/
    @Query("select distinct a from Attribute a join AttributeCategoryLink acl on a.id = acl.attributeId " +
            "join AttributeData ad on a.id = ad.attributeId " +
            "where acl.productCategoryId = :categoryId " +
            "and a.isTrash = false " +
            "order by a.priority asc,a.createdDate desc ")
    List<Attribute> findByCategory(Integer categoryId);

    /*Lấy danh sách thuộc tính của một sản phẩm*/
    @Query("select a from Attribute a " +
            "join  ProductAttributeLink pal on pal.attributeId = a.id " +
            "where pal.productId = :productId " +
            "and a.isTrash = false " +
            "order by a.priority asc,a.createdDate desc ")
    List<Attribute> findByProduct(Integer productId);

    /*Lấy danh sách thuộc tính tổng hợp của list sản phẩm*/
    @Query("select distinct a from Attribute a " +
            "join  ProductAttributeLink pal on pal.attributeId = a.id " +
            "left join GroupProductLink gpl on pal.productId = gpl.productId " +
            "where gpl.groupId = :groupId " +
            "and a.isTrash = false " +
            "order by a.priority asc,a.createdDate desc ")
    List<Attribute> findByGroup(Integer groupId);

    /*Lấy danh sách thuộc tính tổng hợp của list sản phẩm*/
    @Query("select distinct a from Attribute a " +
            "join GroupProductLink gpl on a.id = gpl.attributeId " +
            "join Group g on g.id = gpl.groupId " +
            "where gpl.groupId = :groupId " +
            "and a.isTrash = false " +
            "and g.isTrash = false " +
            "order by a.priority asc,a.createdDate desc ")
    List<Attribute> findDistinctByGroup(Integer groupId);
}

