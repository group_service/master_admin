package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.SuperVoucherProductLink;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperVoucherProductLinkDao extends Dao<SuperVoucherProductLink, Integer> {
}
