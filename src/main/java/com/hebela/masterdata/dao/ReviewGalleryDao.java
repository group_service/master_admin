package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.ReviewGallery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewGalleryDao extends StatusDao<ReviewGallery> {
}

