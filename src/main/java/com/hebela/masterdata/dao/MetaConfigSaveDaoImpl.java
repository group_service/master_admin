package com.hebela.masterdata.dao;

import com.hebela.masterdata.bo.MetaConfig;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

public class MetaConfigSaveDaoImpl implements MetaConfigSaveDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void customUpdate(Integer id, Integer type, Integer userId, MetaConfig metaConfigForClient) {
        MetaConfig metaConfig = getMetaConfig(id, type).orElse(null);
        if (metaConfig != null) {
            if (MetaConfig.isClearMetaConfig(metaConfigForClient)) {
                entityManager.remove(metaConfig);
            } else {
                metaConfigForClient.setModifiedBy(userId);
                entityManager.merge(MetaConfig.setMetaConfig(metaConfigForClient, metaConfig));
            }
        } else {
            metaConfig = MetaConfig.getMetaConfigExample(id, type);
            MetaConfig metaConfigNew = MetaConfig.setMetaConfig(metaConfigForClient, metaConfig);
            metaConfigNew.setCreatedBy(userId);
            entityManager.persist(metaConfigNew);
        }
    }

    private Optional<MetaConfig> getMetaConfig(Integer id, Integer type) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MetaConfig> query = builder.createQuery(MetaConfig.class);
        Root<MetaConfig> root = query.from(MetaConfig.class);
        query.select(root);
        if (MetaConfig.TYPE_PRODUCT.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._productId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_PRODUCT)
                    )
            );
        } else if (MetaConfig.TYPE_CATEGORY.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._productCategoryId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_CATEGORY)
                    )
            );
        } else if (MetaConfig.TYPE_BRAND.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._brandId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_BRAND)
                    )
            );
        } else if (MetaConfig.TYPE_COMBO.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._comboId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_COMBO)
                    )
            );
        } else if (MetaConfig.TYPE_RECOMMENDATION.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._recommendationId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_RECOMMENDATION)
                    )
            );
        } else if (MetaConfig.TYPE_COUPON.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._couponId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_COUPON)
                    )
            );
        } else if (MetaConfig.TYPE_HELP_CATEGORY.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._helpCategoryId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_HELP_CATEGORY)
                    )
            );
        } else if (MetaConfig.TYPE_HELP_ITEM.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._helpItemId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_HELP_ITEM)
                    )
            );
        } else if (MetaConfig.TYPE_LANDING_PAGE.equals(type)) {
            query.where(
                    builder.and(
                            builder.equal(root.get(MetaConfig._landingId), id),
                            builder.equal(root.get(MetaConfig._type), MetaConfig.TYPE_LANDING_PAGE)
                    )
            );
        }
        try {
            return Optional.of(entityManager.createQuery(query).getSingleResult());
        } catch (NoResultException var3) {
            return Optional.empty();
        }
    }
}
