package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.GiftAccountLink;
import org.springframework.stereotype.Repository;

@Repository
public interface GiftAccountLinkDao extends Dao<GiftAccountLink, Integer> {
    void deleteAllByAccountId(Integer accountId);

    void deleteAllByCouponIdAndProductId(Integer couponId, Integer productId);
}

