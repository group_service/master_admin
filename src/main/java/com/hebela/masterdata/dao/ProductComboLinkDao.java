package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductComboLink;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductComboLinkDao extends Dao<ProductComboLink, Integer> {

    @Query("SELECT SUM(quantity) FROM ProductComboLink WHERE comboId = :comboId")
    Integer sumQuantityComBo(Integer comboId);
}

