package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.ProductTags;
import com.hebela.masterdata.bo.ProductTags;
import com.hebela.masterdata.bo.ProductTags;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductTagsDao extends StatusDao<ProductTags> {
    @Query("select new ProductTags(p, (select count(pl) from ProductTagsLink pl where pl.productTagsId = p.id))" +
            " from ProductTags p" +
            " where p.status = :status AND p.isTrash = :isTrash" +
            " order by p.priority, p.createdDate")
    List<ProductTags> findWithExtends(Integer status, Boolean isTrash);
}

