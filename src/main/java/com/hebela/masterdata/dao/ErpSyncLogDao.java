package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ErpSyncLog;
import org.springframework.stereotype.Repository;

@Repository
public interface ErpSyncLogDao extends Dao<ErpSyncLog, Integer> {
}
