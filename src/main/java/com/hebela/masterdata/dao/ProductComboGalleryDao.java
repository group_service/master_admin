package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductComboGallery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductComboGalleryDao extends Dao<ProductComboGallery, Integer> {
    List<ProductComboGallery> findAllByProductComboIdIn(List<Integer> productComboIdList);
}

