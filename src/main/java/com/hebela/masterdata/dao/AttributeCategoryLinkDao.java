package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.Attribute;
import com.hebela.masterdata.bo.AttributeCategoryLink;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttributeCategoryLinkDao extends Dao<AttributeCategoryLink, Integer> {
    List<AttributeCategoryLink> findByAttributeIdIn(List<Integer> attributeIdList);
}

