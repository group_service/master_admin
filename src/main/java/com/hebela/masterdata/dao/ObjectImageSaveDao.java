package com.hebela.masterdata.dao;

import com.hebela.masterdata.bo.ObjectImage;

public interface ObjectImageSaveDao {
    void customUpdate(ObjectImage objectImageDb, Integer userId, ObjectImage objectImageForClient);

}
