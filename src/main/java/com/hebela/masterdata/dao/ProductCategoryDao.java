package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCategoryDao extends Dao<ProductCategory, Integer> {

    @Query("select distinct new ProductCategory(pc, pc.name, rl.priority) from ProductCategory pc " +
            "join RecommendationLink rl on pc.id =  rl.objectId " +
            "where pc.isTrash = false " +
            "and rl.recommendationId = :recommendationId " +
            "and rl.objectClass = com.hebela.masterdata.bo.ObjectLink.OBJ_CLASS_PRODUCT_CATEGORY " +
            "order by rl.priority, rl.createdDate")
    List<ProductCategory> findByRecommendationId(Integer recommendationId);

    @Query(value = "select pc from ProductCategory pc where pc.name like %:term% and pc.isTrash = false")
    List<ProductCategory> findByTerm(String term);

    @Query(value = "select pc.PRIORITY from TBL_PRODUCT_CATEGORY pc where pc.PARENT_ID = :parentId order by pc.PRIORITY desc limit 1", nativeQuery = true)
    Integer getMaxPriority(Integer parentId);

    @Query(value = "select pc.PRIORITY from TBL_PRODUCT_CATEGORY pc where pc.PARENT_ID is null order by pc.PRIORITY desc limit 1", nativeQuery = true)
    Integer getMaxPriorityRoot();

    ProductCategory findByIsDefaultIsTrue();

    @Query("select new ProductCategory(pc, (select count(p) from Product p where p.productCategoryId = pc.id), " +
            "(select count(acl) from AttributeCategoryLink acl where acl.productCategoryId = pc.id)) " +
            " from ProductCategory pc" +
            " where pc.status = :status AND pc.isTrash = :isTrash" +
            " order by pc.level asc, pc.priority asc, pc.createdDate desc")
    List<ProductCategory> findWithExtends(Integer status, Boolean isTrash);
}
