package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.tracking.bo.TrackingAction;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackingActionDao extends Dao<TrackingAction, Integer> {
}

