package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductGallery;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductGalleryDao extends Dao<ProductGallery, Integer> {

    @Query("select pg from ProductGallery pg where pg.productId = :productId")
    List<ProductGallery> findProductGalleriesByProductId(Integer productId);
}

