package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.Cart;
import com.hebela.masterdata.bo.extend.CartAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartDao extends Dao<Cart, Integer> {
    @Query("SELECT DISTINCT NEW com.hebela.masterdata.bo.extend.CartAccount(acc.id, acc.fullname) " +
            "FROM Account acc JOIN Cart tc ON acc.id = tc.accountId " +
            "LEFT JOIN Product p ON tc.productId = p.id " +
            "LEFT JOIN ProductCombo pc ON tc.comboId = pc.id " +
            "WHERE (:cartItemKeyword IS NULL OR (p.name LIKE %:cartItemKeyword% OR pc.name LIKE %:cartItemKeyword%)) " +
            "AND (:cartItemSku IS NULL OR (p.sku = :cartItemSku OR pc.sku = :cartItemSku))  " +
            "AND (:accountKeyword IS NULL OR (acc.fullname LIKE %:accountKeyword% OR acc.mobile LIKE %:accountKeyword%)) " +
            "ORDER BY tc.createdDate DESC")
    Page<CartAccount> findCartAccount(String cartItemKeyword, String cartItemSku, String accountKeyword, Pageable pageable);

    @Query("SELECT NEW Cart(tc, p, pc) " +
            "FROM Cart tc LEFT JOIN ProductCombo pc ON tc.comboId = pc.id " +
            "LEFT JOIN Product p ON tc.productId = p.id " +
            "AND tc.accountId in :accountIdList " +
            "ORDER BY tc.createdDate DESC")
    List<Cart> findCartByAccountList(List<Integer> accountIdList);

    @Query("SELECT count(od) FROM Order od " +
            "WHERE od.accountId = :accountId " +
            "AND od.status <> com.hebela.masterdata.bo.Order.STATUS_CANCEL")
    Integer countOrderByAccount(Integer accountId);
}
