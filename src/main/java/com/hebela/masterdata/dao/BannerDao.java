package com.hebela.masterdata.dao;

import com.hebela.core.StatusDao;
import com.hebela.masterdata.bo.Banner;
import org.springframework.stereotype.Repository;

@Repository
public interface BannerDao extends StatusDao<Banner> {
}

