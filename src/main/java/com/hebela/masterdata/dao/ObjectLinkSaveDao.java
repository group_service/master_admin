package com.hebela.masterdata.dao;

import com.hebela.masterdata.bo.ObjectLink;

import java.util.Optional;

public interface ObjectLinkSaveDao {
    void customUpdate(String objectClass, Integer objectClassId, Integer userId, ObjectLink objectLinkForClient);

    Optional<ObjectLink> getObjectLink(String objectClass, Integer objectClassId);
}
