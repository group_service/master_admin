package com.hebela.masterdata.dao;

import com.hebela.core.Dao;
import com.hebela.masterdata.bo.ProductPolicyLink;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductPolicyLinkDao extends Dao<ProductPolicyLink, Integer> {
}

