package com.hebela.masterdata.enumeration;

import java.io.Serializable;

public enum VoucherErrorEnum implements Serializable {
    voucher_not_exits("voucher_not_exits", "Xin lỗi, mã giảm giá không hợp lệ"),
    start_date_invalid("start_date_invalid", "Voucher chưa đến thời gian kích hoạt"),
    end_date_invalid("end_date_invalid", "Voucher đã hết thời gian áp dụng"),
    min_order_invalid("min_order_invalid", "Giá trị đơn hàng chưa đạt"),
    order_remaining_invalid("order_remaining_invalid", "Voucher đã sử dụng hết lượt"),
    platform_app("platform_app", "Voucher chỉ áp dụng cho ứng dụng trên di động"),
    platform_web("platform_web", "Voucher chỉ áp dụng cho phiên bản web"),
    platform_web_mobile("platform_web_mobile", "Voucher chỉ áp dụng cho phiên bản web mobile");

    private final String key;
    private final String value;

    VoucherErrorEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static VoucherErrorEnum build(String key) {
        return VoucherErrorEnum.valueOf(VoucherErrorEnum.class, key);
    }


    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
