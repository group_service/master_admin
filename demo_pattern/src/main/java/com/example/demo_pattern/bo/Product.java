package com.example.demo_pattern.bo;


import lombok.Data;


@Data
public class Product {

    private Integer id;

    private String code;

    private String name;

    private String sku;

    private String merchantInfo;


}
