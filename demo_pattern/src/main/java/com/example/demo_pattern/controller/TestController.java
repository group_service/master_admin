package com.example.demo_pattern.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@RestController
@RequestMapping("/api/test")
public class TestController {


//    public static void main(String[] args) {
//        List<Integer> numbers = Arrays.asList(12, 3, 4, 5, 6, 2);
//
//        int sum = numbers.stream().mapToInt(i -> i).sum();
//        int sumReduce = numbers.stream().filter(i -> i > 5).reduce(0, (a, b) -> a + b);
//        int sumReduceFilTer = numbers.stream().filter(i -> i > 5).reduce(Integer::sum).get().intValue();
//        List<String> strings = Arrays.asList("a,1", "b,12", "c,13", "d,14");
//        String[] abc = strings.stream().reduce(String::concat).get().split(",");
//        System.out.println("sum" + sum);
//        System.out.println("sumReduce" + sumReduce);
//        System.out.println("sumReduceFilTer " + sumReduceFilTer);
//        Arrays.stream(abc).filter(item -> item.length() > 1).peek(item -> System.out.println("value hihi " + item)).collect(Collectors.toList());
//        Stream.of("one", "two", "three", "four")
//                .filter(e -> e.length() > 3)
//                .peek(e -> System.out.println("Filtered value: " + e))
//                .map(String::toUpperCase)
//                .peek(e -> System.out.println("Mapped value: " + e))
//                .collect(Collectors.toList());
//        System.out.println("abc value  " + abc);
//
//
//    }
    @GetMapping("/getMessage")
    @KafkaListener(topics = "Int-Product-${spring.profiles.active}", concurrency = "", groupId = "test")
    public String getMessage(){
        System.out.println("abc");
        return "Demo kubernates";
    }
}
