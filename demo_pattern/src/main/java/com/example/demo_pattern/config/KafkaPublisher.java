package com.example.demo_pattern.config;

import com.example.demo_pattern.bo.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.support.MessageBuilder;

public abstract class KafkaPublisher extends KafkaBase {
    @Autowired
    public KafkaTemplate<Integer, ?> kafkaTemplate;

    public KafkaPublisher() {
    }

    public void send(String env, String action, Product bo) {
        try {
            this.kafkaTemplate.send(MessageBuilder.withPayload(this.objectMapper.writeValueAsString(bo)).setHeader("kafka_topic", this.getType() + "-" + bo.getClass().getSimpleName() + "-" + env).setHeader("kafka_action", action).setHeader("kafka_messageKey", bo.getId()).setHeader("kafka_context", this.objectMapper.writeValueAsString(1)).build());
        } catch (JsonProcessingException var5) {
        }

    }

    public void send(String action, Product bo) {
        this.send(this.env, action, bo);
    }

    protected abstract String getType();
}
