package com.example.demo_pattern.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public abstract class KafkaBase {
    public static final String ACTION_KEY = "kafka_action";
    public static final String ACTION_CREATE = "create";
    public static final String ACTION_UPDATE = "update";
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_CANCEL = "cancel";
    public static final String CONTEXT_KEY = "kafka_context";
    @Value("${spring.profiles.active}")
    protected String env;
    @Autowired
    protected ObjectMapper objectMapper;

    public KafkaBase() {
    }
}
